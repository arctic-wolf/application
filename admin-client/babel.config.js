module.exports = {
  presets: [["@vue/cli-plugin-babel/preset", { useBuiltIns: "entry" }]],
  env: {
    development: {
      presets: [["@vue/cli-plugin-babel/preset", { useBuiltIns: "entry" }]],
      plugins: ["dynamic-import-webpack", "@babel/syntax-dynamic-import"]
    }
  },
  compact: true
};
