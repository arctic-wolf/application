import org.apache.tools.ant.taskdefs.condition.Os

apply(plugin = "idea")
apply(plugin = "java")

repositories {
    mavenCentral()
}

tasks.register("nodeClean") {
    doLast {
        delete("dist")
        delete("../core/HTML/admin-client")
    }
}

tasks.register("nodeInstall") {
    doLast {
        exec {
            if (file("${projectDir}/node_modules").exists()) {
                println("Node Modules are already installed.")
                commandLine(nodeExecutable(), "version")
            } else {
                println("Couldn't find Node Modules, installing ...")
                commandLine(nodeExecutable(), "install")
            }
        }
    }
}

tasks.register("nodeBuild") {
    dependsOn("nodeInstall")

    doLast {
        exec {
            commandLine(nodeExecutable(), "run", "build")
        }
    }
}

//tasks.clean.get().dependsOn("nodeClean")
//tasks.jar.get().dependsOn("nodeBuild")

fun nodeExecutable(): String {
    var executable = "npm"
    if (Os.isFamily(Os.FAMILY_WINDOWS)) {
        executable = "npm.cmd"
    }

    return executable
}
