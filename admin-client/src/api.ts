import axios, { AxiosInstance, AxiosPromise } from "axios";
declare const Buffer;
const jsonBigInteger = require("json-bigint");

const ENDPOINT_FILE = require("@/assets/endpoint.json");
export const API_BASE_URL = ENDPOINT_FILE.apiBaseURL;
const AXIOS_INSTANCE: AxiosInstance = axios.create({
  baseURL:
    API_BASE_URL +
    `/` +
    ENDPOINT_FILE.apiPathName +
    `/` +
    ENDPOINT_FILE.apiVersion +
    `/`,
});

export function setJWT(jwt: string) {
  AXIOS_INSTANCE.defaults.headers["Authorization"] = `Bearer ${jwt}`;
}

export function clearJWT() {
  delete AXIOS_INSTANCE.defaults.headers["Authorization"];
}

export function get<T = any>(url: string, headers?: any): AxiosPromise<T> {
  return AXIOS_INSTANCE.get(url, {
    headers: headers ? headers : {},
    transformResponse: (data) => jsonBigInteger.parse(data),
  });
}

export function post<T = any>(
  url: string,
  data?: any,
  headers?: any
): AxiosPromise<T> {
  return AXIOS_INSTANCE.post(url, data, {
    headers: headers ? headers : {},
    transformResponse: (data) => jsonBigInteger.parse(data),
  });
}

export function put<T = any>(
  url: string,
  data?: any,
  headers?: any
): AxiosPromise<T> {
  return AXIOS_INSTANCE.put(url, data, {
    headers: headers ? headers : {},
    transformResponse: (data) => jsonBigInteger.parse(data),
  });
}

export function removeDelete<T = any>(
  url: string,
  data?: any,
  headers?: any
): AxiosPromise<T> {
  return AXIOS_INSTANCE.delete(url, {
    headers: headers ? headers : {},
    data: data ? data : {},
    transformResponse: (data) => jsonBigInteger.parse(data),
  });
}

export function getBase64<T = any>(
  url: string,
  headers?: any
): AxiosPromise<T> {
  return AXIOS_INSTANCE.get(url, {
    headers: headers ? headers : {},
    responseType: "arraybuffer",
  }).then((response) =>
    Buffer.from(response.data, "binary").toString("base64")
  );
}
