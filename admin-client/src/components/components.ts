import Vue from "vue";

import FooterTemplate from "@/components/templates/FooterTemplate.vue";
import GroupListTemplate from "@/components/templates/GroupListTemplate.vue";
import HeaderTemplate from "@/components/templates/HeaderTemplate.vue";
import MessagesTemplate from "@/components/templates/MessagesTemplate.vue";
import PageListTemplate from "@/components/templates/PageListTemplate.vue";
import PostListTemplate from "@/components/templates/PostListTemplate.vue";
import SidebarTemplate from "@/components/templates/SidebarTemplate.vue";
import SystemTemplate from "@/components/templates/SystemTemplate.vue";
import ThemesTemplate from "@/components/templates/ThemesTemplate.vue";
import UserListTemplate from "@/components/templates/UserListTemplate.vue";
import WidgetListTemplate from "@/components/templates/WidgetListTemplate.vue";
import TiptapEditor from "@/components/tiptap/TiptapEditor.vue";
import TiptapIcon from "@/components/tiptap/TiptapIcon.vue";
import FooterComponent from "@/components/ui/FooterComponent.vue";
import HeaderComponent from "@/components/ui/HeaderComponent.vue";
import NavbarComponent from "@/components/ui/NavbarComponent.vue";
import SidebarComponent from "@/components/ui/SidebarComponent.vue";
import AlertTemplate from "@/components/templates/AlertTemplate.vue";
import MenuButtonTemplate from "@/components/templates/MenuButtonTemplate.vue";
import LanguageSelectorTemplate from "@/components/templates/navbar/LanguageSelectorTemplate.vue";
import NotificationsTemplate from "@/components/templates/navbar/NotificationsTemplate.vue";
import PluginNavbarLinksTemplate from "@/components/templates/navbar/PluginNavbarLinksTemplate.vue";
import ThemeColorSelectorTemplate from "@/components/templates/navbar/ThemeColorSelectorTemplate.vue";
import TopMenuItemsTemplate from "@/components/templates/navbar/TopMenuItemsTemplate.vue";
import CrudFormFieldTemplate from "@/components/templates/crud/CrudFormFieldTemplate.vue";
import TooltipButtonTemplate from "@/components/templates/TooltipButtonTemplate.vue";
import NavbarTemplate from "@/components/templates/navbar/NavbarTemplate.vue";
import CrudTableTemplate from "@/components/templates/crud/CrudTableTemplate.vue";
import CrudFormTemplate from "@/components/templates/crud/CrudFormTemplate.vue";

Vue.component("AlertTemplate", AlertTemplate);
Vue.component("CrudTableTemplate", CrudTableTemplate);
Vue.component("CrudFormTemplate", CrudFormTemplate);
Vue.component("FooterTemplate", FooterTemplate);
Vue.component("GroupListTemplate", GroupListTemplate);
Vue.component("HeaderTemplate", HeaderTemplate);
Vue.component("MenuButtonTemplate", MenuButtonTemplate);
Vue.component("MessagesTemplate", MessagesTemplate);
Vue.component("LanguageSelectorTemplate", LanguageSelectorTemplate);
Vue.component("NotificationsTemplate", NotificationsTemplate);
Vue.component("PluginNavbarLinksTemplate", PluginNavbarLinksTemplate);
Vue.component("ThemeColorSelectorTemplate", ThemeColorSelectorTemplate);
Vue.component("TopMenuItemsTemplate", TopMenuItemsTemplate);
Vue.component("NavbarTemplate", NavbarTemplate);
Vue.component("PageListTemplate", PageListTemplate);
Vue.component("PostListTemplate", PostListTemplate);
Vue.component("SidebarTemplate", SidebarTemplate);
Vue.component("SystemTemplate", SystemTemplate);
Vue.component("CrudFormFieldTemplate", CrudFormFieldTemplate);
Vue.component("ThemesTemplate", ThemesTemplate);
Vue.component("TooltipButtonTemplate", TooltipButtonTemplate);
Vue.component("UserListTemplate", UserListTemplate);
Vue.component("WidgetListTemplate", WidgetListTemplate);
Vue.component("TiptapEditor", TiptapEditor);
Vue.component("TiptapIcon", TiptapIcon);
Vue.component("FooterComponent", FooterComponent);
Vue.component("HeaderComponent", HeaderComponent);
Vue.component("NavbarComponent", NavbarComponent);
Vue.component("SidebarComponent", SidebarComponent);

// cd src/components && fileList=$(find ./ -iname "*.vue" | sed "s/\.\//@\/components\//g" | awk -F. '{print $1}' | sed 's/@\/components\/templates\///g' | sed 's/navbar\///g' | sed 's/@\/components\/ui\///g' | sed 's/@\/components\///g' | grep -v 'FriendlyURLRouter\|Logout\|OAuth2RedirectHandler\|Styles'  ) ; for file in ${fileList}; do  echo "Vue.component(\"$file\", $file);"; done
