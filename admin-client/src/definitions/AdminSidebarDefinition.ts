export interface AdminSidebarDefinition {
  href: string;
  title: string;
  icon: string;
  child: Array<AdminSidebarDefinition>;
}
