export default interface AllowDisallowButtonDefinition {
  slot?: string;
  id?: string;
}
