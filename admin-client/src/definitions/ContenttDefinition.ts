import { ContentVisibility } from "@/definitions/enums/ContentVisibility";

export interface ContentDefinition {
  id: number;
  type: string;
  title: string;
  content: string;
  authorId: number;
  author: string;
  createTime: string;
  displayOrder: number;
  friendlyURL: string;
  icon: string;
  lang: string;
  visibility: Array<ContentVisibility>;
}

export interface ContentVisibilityDefinition {
  contentVisibility: ContentVisibility;
  message: string;
}
