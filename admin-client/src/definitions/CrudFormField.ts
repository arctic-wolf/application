export default interface CrudFormField {
  field?: string | any;
  label?: string | any;
  placeholder?: string | any;
  type: string | any;
  prependInnerIcon?: string | any;
  appendIcon?: string | any;
  checkBoxGroupOptions?: Array<Object>;
}
