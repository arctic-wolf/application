export default interface CustomButtonDefinition {
  slot?: string;
  id?: string;
  css?: string;
  routerLink?: string;
}
