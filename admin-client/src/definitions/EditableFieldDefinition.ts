export default interface EditableFieldDefinition {
  id?: string;
  slot?: string;
}
