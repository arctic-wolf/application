export interface ExternalComponentDefinition {
  name: string;
  path: string;
  version: string;
  description: string;
  author: string;
  createdAt: string;
  previewImage: string;
  jarFileName: string;
}
