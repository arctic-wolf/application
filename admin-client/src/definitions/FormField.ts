export default interface FormField {
  field?: string | any;
  labelText?: string | any;
  labelCSS?: string;
  inputType?: string;
  placeholder?: string | any;
  isTextArea?: boolean;
  isDatePicker?: boolean;
  isCheckBox?: boolean;
  isEditor?: boolean;
  wrapperCSS?: string;
  isCheckBoxGroup?: boolean;
  checkBoxGroupOptions?: Array<Object>;
}
