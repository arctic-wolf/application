export interface GroupDefinition {
  id: number;
  name: string;
  description: string;
  authorities: Array<string>;
  createTime: string;
}

export interface AuthorityDefinition {
  authority: string;
  message: string;
}
