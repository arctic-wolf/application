export default interface MessageDefinition {
  id: string;
  code: string;
  language: string;
  message: string;
}
