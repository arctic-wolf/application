export interface NotificationDefinition {
  id: number;
  icon: string;
  title: string;
  message: string;
  dateTime: string;
}
