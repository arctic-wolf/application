import { ContentDefinition } from "@/definitions/ContenttDefinition";

export interface PageDefinition extends ContentDefinition {}
