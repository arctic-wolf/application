import { ContentDefinition } from "@/definitions/ContenttDefinition";

export interface PostDefinition extends ContentDefinition {}

export class PostListParams {
  readonly lang: string;
  readonly page: number;

  constructor(lang: string, page: number) {
    this.lang = lang;
    this.page = page;
  }
}

export class PostListDefinition {
  readonly posts: Array<PostDefinition>;
  readonly pageCount: number;
  readonly totalCount: number;

  constructor(
    posts: Array<PostDefinition>,
    pageCount: number,
    totalCount: number
  ) {
    this.posts = posts;
    this.pageCount = pageCount;
    this.totalCount = totalCount;
  }
}
