import ServerPropertyDefinition from "@/definitions/ServerPropertyDefinition";

export default interface ServerPropertiesDefinition {
  propertiesGroup: Array<ServerPropertyDefinition>;
}
