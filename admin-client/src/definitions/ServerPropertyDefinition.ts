import { ServerPropertyType } from "@/definitions/enums/ServerPropertyType";

export default interface ServerPropertyDefinition {
  propertyType: ServerPropertyType;
  propertyName: string;
  actualValue: string;
  dropDownElements: Array<string>;
}
