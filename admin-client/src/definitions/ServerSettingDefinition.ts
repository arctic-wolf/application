export interface ServerSettingDefinition {
  name: string;
  value: string;
}
