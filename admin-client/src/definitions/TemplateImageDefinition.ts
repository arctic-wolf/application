export default interface TemplateImageDefinition {
  slot?: string;
  css?: string;
  defaultImage?: string;
  isImage?: boolean;
}
