import { ExternalComponentDefinition } from "@/definitions/ExternalComponentDefinition";

export interface ThemeDefinition extends ExternalComponentDefinition {}
