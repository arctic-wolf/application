import { ObjectWrapperDefinition } from "@/modules/AdminCrudModule";

export interface UserDefinition extends ObjectWrapperDefinition {
  id: number;
  username: string;
  email: string;
  displayName: string;
  birthDate: string;
  createTime: string;
  imageUrl: string;
  bio: string;
  active: boolean;
  provider: string;
  providerId: string;
}
