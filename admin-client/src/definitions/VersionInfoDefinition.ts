export default interface VersionInfoDefinition {
  name: string;
  value: string;
}
