import { LinkDefinition } from "@/definitions/LinkDefinition";
import { ContentVisibility } from "@/definitions/enums/ContentVisibility";

export interface WidgetDefinition {
  id: string;
  displayName: string;
  groupId: string;
  orderInTheGroup: number;
  content: string;
  links: Array<LinkDefinition>;
  lang: string;
  visibility: Array<ContentVisibility>;
}
