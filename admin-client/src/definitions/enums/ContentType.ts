export enum ContentType {
  NONE = "NONE",
  PAGE = "PAGE",
  POST = "POST",
}
