export enum ContentVisibility {
  GUEST = "GUEST",
  REGULAR_USER = "REGULAR_USER",
  ADMIN_SITE = "ADMIN_SITE",
}
