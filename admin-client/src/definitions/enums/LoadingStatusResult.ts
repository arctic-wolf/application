export enum LoadingStatusResult {
  NONE = "",
  SUCCESS = "success",
  WARNING = "warning",
  DANGER = "danger",
}
