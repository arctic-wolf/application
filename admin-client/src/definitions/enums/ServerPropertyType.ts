export enum ServerPropertyType {
  TEXT = "TEXT",
  NUMBER = "NUMBER",
  RADIO_BUTTON = "RADIO_BUTTON",
  SELECT = "SELECT",
  PASSWORD = "PASSWORD",
}
