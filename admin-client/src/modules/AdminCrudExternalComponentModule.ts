import { MutationAction, VuexModule } from "vuex-module-decorators";

import { get, getBase64, post, removeDelete } from "@/api";
import { ExternalComponentDefinition } from "@/definitions/ExternalComponentDefinition";

export abstract class AdminCrudExternalComponentsModule extends VuexModule {
  externalComponent: ExternalComponentDefinition | any = null;
  externalComponentList: ExternalComponentListWrapperDefinition | any = null;
  previewImagesMap: Map<string, any> = new Map<string, any>();

  @MutationAction({ mutate: ["externalComponent"], rawError: true })
  async componentAction(params: ExternalComponentParams) {
    const externalComponent =
      params.action === "delete"
        ? await componentDeleteAction(
            params.moduleUrl,
            params.name,
            params.action
          )
        : await componentAction(params.moduleUrl, params.name, params.action);
    return { externalComponent };
  }

  @MutationAction({ mutate: ["externalComponentList"], rawError: true })
  async fetchExternalComponents(params: ExternalComponentsParams) {
    const externalComponentList = await fetchExternalComponents(
      params.moduleUrl,
      params.page
    );
    return { externalComponentList };
  }

  @MutationAction({ mutate: ["externalComponentList"], rawError: true })
  async fetchFindExternalComponents(params: ExternalComponentsParams) {
    const externalComponentList = await fetchFindExternalComponents(
      params.moduleUrl,
      params.search,
      params.page
    );
    return { externalComponentList };
  }

  @MutationAction({ mutate: ["previewImagesMap"], rawError: true })
  async fetchPreviewImages(params: ExternalComponentsParams) {
    const previewImagesMap: Map<string, any> = new Map<string, any>();

    for (const externalComponent of params.externalComponents) {
      const previewImage = await fetchPreviewImage(
        params.moduleUrl,
        externalComponent.name
      );

      previewImagesMap.set(
        externalComponent.name,
        "data:image/png;base64," + previewImage
      );
    }

    return { previewImagesMap };
  }
}

async function componentAction(
  moduleUrl: string,
  name: string,
  action: string
): Promise<ExternalComponentDefinition> {
  const response = await post("admin/" + moduleUrl + "/" + name + "/" + action);
  return response.data as ExternalComponentDefinition;
}

async function componentDeleteAction(
  moduleUrl: string,
  name: string,
  action: string
): Promise<ExternalComponentDefinition> {
  const response = await removeDelete(
    "admin/" + moduleUrl + "/" + name + "/" + action
  );
  return response.data as ExternalComponentDefinition;
}

async function fetchExternalComponents(
  moduleUrl: string,
  page: number
): Promise<ExternalComponentListWrapperDefinition> {
  const response = await get("admin/" + moduleUrl + "?page=" + page);
  return new ExternalComponentListWrapperDefinition(
    response.data as Array<ExternalComponentDefinition>,
    response.headers["x-page-count"] as number,
    response.headers["x-total-count"] as number
  );
}

async function fetchFindExternalComponents(
  moduleUrl: string,
  search: string,
  page: number
): Promise<ExternalComponentListWrapperDefinition> {
  const response = await get(
    "admin/" + moduleUrl + "/find/" + search + "/?page=" + page
  );
  return new ExternalComponentListWrapperDefinition(
    response.data as Array<ExternalComponentDefinition>,
    response.headers["x-page-count"] as number,
    response.headers["x-total-count"] as number
  );
}

async function fetchPreviewImage(moduleUrl: string, name: string) {
  return getBase64("admin/" + moduleUrl + "/" + name + "/preview-image");
}

export class ExternalComponentParams {
  readonly moduleUrl: string | any;
  readonly name: string | any;
  readonly action: string | any;

  constructor(moduleUrl?: string, name?: string, action?: string) {
    this.moduleUrl = moduleUrl;
    this.name = name;
    this.action = action;
  }
}

export class ExternalComponentsParams {
  readonly moduleUrl: string | any;
  readonly page: number | any;
  readonly externalComponents: Array<ExternalComponentDefinition> | any;
  readonly search: string | any;

  constructor(
    moduleUrl?: string,
    page?: number,
    externalComponents?: Array<ExternalComponentDefinition>,
    search?: string | any
  ) {
    this.moduleUrl = moduleUrl;
    this.page = page ? page : 0;
    this.externalComponents = externalComponents;
    this.search = search;
  }
}

export class ExternalComponentListWrapperDefinition {
  objects: Array<ExternalComponentDefinition>;
  pageCount: number;
  totalCount: number;

  constructor(
    objects: Array<ExternalComponentDefinition>,
    pageCount: number,
    totalCount: number
  ) {
    this.objects = objects;
    this.pageCount = pageCount;
    this.totalCount = totalCount;
  }
}

export interface ExternalComponentButtonDefinition {
  text: string | any;
  routeName: string | any;
  routeAction: string | any;
  variant: string;
  css: string;
}
