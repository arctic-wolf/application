import { Action, MutationAction, VuexModule } from "vuex-module-decorators";

import { get, post, put, removeDelete } from "@/api";

export abstract class AdminCrudModule extends VuexModule {
  listWrapper: ListWrapperDefinition | any = null;
  objectWrapper: ObjectWrapperDefinition | any = null;

  @MutationAction({ mutate: ["listWrapper"], rawError: true })
  async fetchList(params: CrudParams) {
    const listWrapper = await fetchListWrapper(params.moduleUrl, params.page);
    return { listWrapper };
  }

  @MutationAction({ mutate: ["objectWrapper"], rawError: true })
  async fetchObject(params: CrudParams) {
    const objectWrapper = await fetchObjectWrapper(params.moduleUrl, params.id);
    return { objectWrapper };
  }

  @MutationAction({ mutate: ["listWrapper"], rawError: true })
  async fetchFindList(params: CrudParams) {
    const listWrapper = await fetchFindListWrapper(
      params.moduleUrl,
      params.search,
      params.page
    );
    return { listWrapper };
  }

  @MutationAction({ mutate: ["objectWrapper"], rawError: true })
  async add(params: CrudParams) {
    const objectWrapper = await addObjectWrapper(params.moduleUrl, params.data);
    return { objectWrapper };
  }

  @MutationAction({ mutate: ["objectWrapper"], rawError: true })
  async edit(params: CrudParams) {
    const objectWrapper = await editObjectWrapper(
      params.moduleUrl,
      params.id,
      params.data
    );
    return { objectWrapper };
  }

  @Action({ rawError: true })
  async delete(params: CrudParams) {
    await deleteObjectWrapper(params.moduleUrl, params.id);
  }
}

async function fetchListWrapper(
  moduleUrl: string,
  page: number
): Promise<ListWrapperDefinition> {
  const response = await get("admin/" + moduleUrl + "/all?page=" + page);
  return new ListWrapperDefinition(
    response.data as Array<ObjectWrapperDefinition>,
    response.headers["x-page-count"] as number,
    response.headers["x-total-count"] as number
  );
}

async function fetchObjectWrapper(
  moduleUrl: string,
  id: string
): Promise<ObjectWrapperDefinition> {
  const response = await get("admin/" + moduleUrl + "/" + id);
  return response.data as ObjectWrapperDefinition;
}

async function fetchFindListWrapper(
  moduleUrl: string,
  search: string,
  page: number
): Promise<ListWrapperDefinition> {
  const response = await get(
    "admin/" + moduleUrl + "/find/" + search + "?page=" + page
  );
  return new ListWrapperDefinition(
    response.data as Array<ObjectWrapperDefinition>,
    response.headers["x-page-count"] as number,
    response.headers["x-total-count"] as number
  );
}

async function addObjectWrapper(
  moduleUrl: string,
  data: ObjectWrapperDefinition
): Promise<ObjectWrapperDefinition> {
  const response = await post("admin/" + moduleUrl + "/add", data);
  return response.data as ObjectWrapperDefinition;
}

async function editObjectWrapper(
  moduleUrl: string,
  id: string,
  data: ObjectWrapperDefinition
): Promise<ObjectWrapperDefinition> {
  const response = await put("admin/" + moduleUrl + "/" + id + "/edit", data);
  return response.data as ObjectWrapperDefinition;
}

async function deleteObjectWrapper(
  moduleUrl: string,
  id: string
): Promise<boolean> {
  const response = await removeDelete(
    "admin/" + moduleUrl + "/" + id + "/delete"
  );
  return response.data as boolean;
}

export class ListWrapperDefinition {
  objects: Array<ObjectWrapperDefinition>;
  pageCount: number;
  totalCount: number;

  constructor(
    objects: Array<ObjectWrapperDefinition>,
    pageCount: number,
    totalCount: number
  ) {
    this.objects = objects;
    this.pageCount = pageCount;
    this.totalCount = totalCount;
  }
}

export interface ObjectWrapperDefinition {}

export class CrudParams {
  readonly moduleUrl: string | any;
  readonly id: string | any;
  readonly search: string | any;
  readonly page: number | any;
  readonly data: ObjectWrapperDefinition | any;

  constructor(
    moduleUrl: string | any,
    id?: string | any,
    search?: string | any,
    page?: number | any,
    data?: ObjectWrapperDefinition | any
  ) {
    this.moduleUrl = moduleUrl;
    this.id = id;
    this.search = search;
    this.page = page;
    this.data = data;
  }
}
