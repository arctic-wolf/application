import {
  Action,
  getModule,
  Module,
  MutationAction,
} from "vuex-module-decorators";

import store from "@/store";
import {
  AdminCrudModule,
  CrudParams,
  ListWrapperDefinition,
  ObjectWrapperDefinition,
} from "@/modules/AdminCrudModule";
import { get, post, removeDelete } from "@/api";
import { AuthorityDefinition } from "@/definitions/GroupDefinition";

@Module({
  dynamic: true,
  namespaced: true,
  name: "adminGroupsModule",
  store,
})
class AdminGroupsModule extends AdminCrudModule {
  allAuthoritiesArray: Array<AuthorityDefinition> | any = null;
  groupAuthoritiesArray: Array<AuthorityDefinition> | any = null;

  get allAuthorities() {
    return this.allAuthoritiesArray;
  }

  get groupAuthorities() {
    return this.groupAuthoritiesArray;
  }

  @MutationAction({ mutate: ["allAuthoritiesArray"], rawError: true })
  async fetchAllAuthorities() {
    const allAuthoritiesArray = await fetchAllAuthorities();
    return { allAuthoritiesArray };
  }

  @MutationAction({ mutate: ["groupAuthoritiesArray"], rawError: true })
  async fetchGroupAuthorities(id: any) {
    const groupAuthoritiesArray = await fetchGroupAuthorities(id);
    return { groupAuthoritiesArray };
  }

  @Action({ rawError: true })
  async addGroupMembers(params: GroupMemberUpdateParams) {
    await addGroupMembers(params.id, params.userIdList);
  }

  @Action({ rawError: true })
  async removeGroupMembers(params: GroupMemberUpdateParams) {
    await removeGroupMembers(params.id, params.userIdList);
  }

  get groups() {
    return this.listWrapper;
  }

  get group() {
    return this.objectWrapper;
  }
}

async function fetchAllAuthorities(): Promise<Array<AuthorityDefinition>> {
  const response = await get("admin/groups/authorities/all");
  return response.data as Array<AuthorityDefinition>;
}

async function fetchGroupAuthorities(
  id: string
): Promise<Array<AuthorityDefinition>> {
  const response = await get("admin/groups/authorities/" + id);
  return response.data as Array<AuthorityDefinition>;
}

export class GroupMemberUpdateParams {
  id: string;
  userIdList: Array<number>;

  constructor(id: any, userIdList: Array<number>) {
    this.id = id;
    this.userIdList = userIdList;
  }
}

async function addGroupMembers(
  id: string,
  userIdList: Array<number>
): Promise<boolean> {
  const response = await post(
    "admin/groups/" + id + "/members/add",
    userIdList
  );
  return response.data as boolean;
}

async function removeGroupMembers(
  id: string,
  userIdList: Array<number>
): Promise<boolean> {
  const response = await removeDelete(
    "admin/groups/" + id + "/members/remove",
    userIdList
  );
  return response.data as boolean;
}

export default getModule(AdminGroupsModule);
