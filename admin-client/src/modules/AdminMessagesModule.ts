import {
  Action,
  getModule,
  Module,
  MutationAction,
  VuexModule,
} from "vuex-module-decorators";

import store from "@/store";
import { get, post, put, removeDelete } from "@/api";
import { LanguageDefinition } from "@/definitions/LanguageDefinition";
import {
  AdminCrudModule,
  ListWrapperDefinition,
  ObjectWrapperDefinition,
} from "@/modules/AdminCrudModule";

@Module({
  dynamic: true,
  namespaced: true,
  name: "adminMessagesModule",
  store,
})
class AdminMessagesModule extends AdminCrudModule {
  languageObject: LanguageDefinition | any = null;
  languagesListWrapper: ListWrapperDefinition | any = null;

  get language() {
    return this.languageObject;
  }

  get languages() {
    return this.languagesListWrapper;
  }

  get message() {
    return this.objectWrapper;
  }

  get messages() {
    return this.listWrapper;
  }

  @MutationAction({ mutate: ["languageObject"], rawError: true })
  async addLanguage(data: LanguageDefinition) {
    const languageObject = await addLanguage(data);
    return { languageObject };
  }

  @Action({ rawError: true })
  async allowDisallowLanguage(lang: string) {
    await allowDisallowLanguage(lang);
  }

  @Action({ rawError: true })
  async deleteLanguage(lang: string) {
    await deleteLanguage(lang);
  }

  @MutationAction
  async fetchLanguages() {
    const languagesListWrapper = await fetchLanguages();
    return { languagesListWrapper };
  }
}

async function fetchLanguages(): Promise<ListWrapperDefinition> {
  const response = await get("admin/messages/languages");
  return new ListWrapperDefinition(
    response.data as Array<LanguageDefinition>,
    response.headers["x-page-count"] as number,
    response.headers["x-total-count"] as number
  );
}

async function addLanguage(
  data: LanguageDefinition
): Promise<LanguageDefinition> {
  const response = await post("admin/messages/languages/add", data);
  return response.data as LanguageDefinition;
}

async function allowDisallowLanguage(lang: string): Promise<boolean> {
  const response = await put(
    "admin/messages/languages/" + lang + "/allow-disallow"
  );
  return response.data as boolean;
}

async function deleteLanguage(lang: string): Promise<boolean> {
  const response = await removeDelete(
    "admin/messages/languages/" + lang + "/delete"
  );
  return response.data as boolean;
}

export default getModule(AdminMessagesModule);
