import { getModule, Module, MutationAction } from "vuex-module-decorators";

import store from "@/store";
import { AdminCrudModule } from "@/modules/AdminCrudModule";
import { get } from "@/api";
import { ContentVisibilityDefinition } from "@/definitions/ContenttDefinition";

@Module({
  dynamic: true,
  namespaced: true,
  name: "adminPostsModule",
  store,
})
class AdminPostsModule extends AdminCrudModule {
  allContentVisibilitiesArray: Array<ContentVisibilityDefinition> | any = null;

  get posts() {
    return this.listWrapper;
  }

  get post() {
    return this.objectWrapper;
  }

  get allContentVisibilities() {
    return this.allContentVisibilitiesArray;
  }

  @MutationAction({ mutate: ["allContentVisibilitiesArray"], rawError: true })
  async fetchAllContentVisibilities() {
    const allContentVisibilitiesArray = await fetchAllContentVisibilities();
    return { allContentVisibilitiesArray };
  }
}

async function fetchAllContentVisibilities(): Promise<
  Array<ContentVisibilityDefinition>
> {
  const response = await get("admin/posts/content-visibilities");
  return response.data as Array<ContentVisibilityDefinition>;
}

export default getModule(AdminPostsModule);
