import {
  getModule,
  Module,
  MutationAction,
  VuexModule,
} from "vuex-module-decorators";

import { get } from "@/api";
import store from "@/store";
import { AdminSidebarDefinition } from "@/definitions/AdminSidebarDefinition";

@Module({
  dynamic: true,
  namespaced: true,
  name: "adminSidebarModule",
  store,
})
class AdminSidebarModule extends VuexModule {
  sidebarArray: Array<AdminSidebarDefinition> = [];

  get sidebar() {
    return this.sidebarArray;
  }

  @MutationAction({ mutate: ["sidebarArray"], rawError: true })
  async fetchAdminSidebar(lang: string) {
    const sidebarArray = await fetchAdminSidebar(lang);
    return { sidebarArray };
  }
}

async function fetchAdminSidebar(
  lang: string
): Promise<Array<AdminSidebarDefinition>> {
  const response = await get("admin/sidebar/" + lang);
  return response.data as Array<AdminSidebarDefinition>;
}

export default getModule(AdminSidebarModule);
