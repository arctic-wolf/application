import {
  Action,
  getModule,
  Module,
  MutationAction,
  VuexModule,
} from "vuex-module-decorators";

import store from "@/store";
import { get, post } from "@/api";
import VersionInfoDefinition from "@/definitions/VersionInfoDefinition";
import ServerPropertyDefinition from "@/definitions/ServerPropertyDefinition";
import ServerPropertiesDefinition from "@/definitions/ServerPropertiesDefinition";

@Module({
  dynamic: true,
  namespaced: true,
  name: "adminSystemModule",
  store,
})
class AdminSystemModule extends VuexModule {
  propertiesGroupsArray: Array<string> = [];
  propertiesGroupArray: Array<ServerPropertyDefinition> = [];
  versionInfoArray: Array<VersionInfoDefinition> = [];

  get propertiesGroups() {
    return this.propertiesGroupsArray;
  }

  get propertiesGroup() {
    return this.propertiesGroupArray;
  }

  get versionInfo() {
    return this.versionInfoArray;
  }

  @MutationAction({ mutate: ["propertiesGroupsArray"], rawError: true })
  async fetchPropertiesGroups() {
    const propertiesGroupsArray = await fetchPropertiesGroups();
    return { propertiesGroupsArray };
  }

  @MutationAction({ mutate: ["propertiesGroupArray"], rawError: true })
  async fetchPropertiesGroup(group: string | any) {
    const propertiesGroupArray = await fetchPropertiesGroup(group);
    return { propertiesGroupArray };
  }

  @Action
  async saveProperties(properties: ServerPropertiesDefinition) {
    await saveProperties(properties);
  }

  @MutationAction({ mutate: ["versionInfoArray"], rawError: true })
  async fetchVersionInfo() {
    const versionInfoArray = await fetchVersionInfo();
    return { versionInfoArray };
  }
}

async function fetchPropertiesGroups(): Promise<Array<string>> {
  const response = await get("admin/system/properties/groups");
  return response.data as Array<string>;
}

async function fetchPropertiesGroup(
  group: string
): Promise<Array<ServerPropertyDefinition>> {
  const response = await get("admin/system/properties/" + group);
  return response.data as Array<ServerPropertyDefinition>;
}

async function saveProperties(
  properties: ServerPropertiesDefinition
): Promise<boolean> {
  const response = await post("admin/system/save-properties", properties);
  return response.data as boolean;
}

async function fetchVersionInfo(): Promise<Array<VersionInfoDefinition>> {
  const response = await get("admin/system/version-info");
  return response.data as Array<VersionInfoDefinition>;
}

export default getModule(AdminSystemModule);
