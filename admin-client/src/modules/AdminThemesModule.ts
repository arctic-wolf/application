import { Action, getModule, Module } from "vuex-module-decorators";

import store from "@/store";
import { AdminCrudExternalComponentsModule } from "@/modules/AdminCrudExternalComponentModule";

@Module({
  dynamic: true,
  namespaced: true,
  name: "adminThemesModule",
  store,
})
class AdminThemesModule extends AdminCrudExternalComponentsModule {
  get themes() {
    return this.externalComponentList;
  }

  get previewImages() {
    return this.previewImagesMap;
  }
}

export default getModule(AdminThemesModule);
