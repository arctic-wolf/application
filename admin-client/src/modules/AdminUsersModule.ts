import { getModule, Module } from "vuex-module-decorators";

import store from "@/store";
import { AdminCrudModule } from "@/modules/AdminCrudModule";

@Module({
  dynamic: true,
  namespaced: true,
  name: "adminUsersModule",
  store,
})
class AdminUsersModule extends AdminCrudModule {
  get users() {
    return this.listWrapper;
  }

  get user() {
    return this.objectWrapper;
  }
}

export default getModule(AdminUsersModule);
