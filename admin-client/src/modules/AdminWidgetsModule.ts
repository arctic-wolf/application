import { getModule, Module, MutationAction } from "vuex-module-decorators";

import store from "@/store";
import { AdminCrudModule } from "@/modules/AdminCrudModule";
import { get } from "@/api";
import { ContentVisibilityDefinition } from "@/definitions/ContenttDefinition";

@Module({
  dynamic: true,
  namespaced: true,
  name: "adminWidgetsModule",
  store,
})
class AdminWidgetsModule extends AdminCrudModule {
  allContentVisibilitiesArray: Array<ContentVisibilityDefinition> | any = null;

  get widgets() {
    return this.listWrapper;
  }

  get widget() {
    return this.objectWrapper;
  }

  get allContentVisibilities() {
    return this.allContentVisibilitiesArray;
  }

  @MutationAction({ mutate: ["allContentVisibilitiesArray"], rawError: true })
  async fetchAllContentVisibilities() {
    const allContentVisibilitiesArray = await fetchAllContentVisibilities();
    return { allContentVisibilitiesArray };
  }
}

async function fetchAllContentVisibilities(): Promise<
  Array<ContentVisibilityDefinition>
> {
  const response = await get("admin/widgets/content-visibilities");
  return response.data as Array<ContentVisibilityDefinition>;
}

export default getModule(AdminWidgetsModule);
