import {
  getModule,
  Module,
  Mutation,
  VuexModule,
} from "vuex-module-decorators";

import { clearJWT, post, setJWT } from "@/api";
import store from "@/store";

@Module({
  dynamic: true,
  namespaced: true,
  name: "authModule",
  store,
})
class AuthModule extends VuexModule {
  authenticated: boolean = false;

  get isAuthenticated() {
    return this.authenticated;
  }

  @Mutation
  authenticateWithToken(token: string) {
    if (token) {
      setJWT(token);
      this.authenticated = true;
    }
  }

  @Mutation
  clearAuthentication() {
    clearJWT();
    localStorage.removeItem(ACCESS_TOKEN);
    this.authenticated = false;
  }
}

export const ACCESS_TOKEN = "accessToken";

export default getModule(AuthModule);
