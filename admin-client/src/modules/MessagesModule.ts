import {
  getModule,
  Module,
  Mutation,
  MutationAction,
  VuexModule,
} from "vuex-module-decorators";
import { get } from "@/api";
import { LocaleMessageObject } from "vue-i18n";
import { LanguageDefinition } from "@/definitions/LanguageDefinition";
import store from "@/store";
import { getStoredLanguage } from "@/i18n";

@Module({
  dynamic: true,
  namespaced: true,
  name: "messagesModule",
  store,
})
class MessagesModule extends VuexModule {
  siteLanguageString: string = getStoredLanguage();
  availableLanguageObject: LanguageDefinition | any = null;
  availableLanguagesArray: Array<LanguageDefinition> = [];
  messagesObject: LocaleMessageObject = {};

  get siteLanguage() {
    return this.siteLanguageString;
  }

  get availableLanguage() {
    return this.availableLanguageObject;
  }

  get availableLanguages() {
    return this.availableLanguagesArray;
  }

  get messages() {
    return this.messagesObject;
  }

  @Mutation
  setSiteLanguage(siteLanguageString: string) {
    localStorage.setItem("siteLanguage", siteLanguageString);
    this.siteLanguageString = siteLanguageString;
  }

  @MutationAction
  async fetchLanguage(lang: string) {
    const availableLanguageObject = await fetchAvailableLanguage(lang);
    return { availableLanguageObject };
  }

  @MutationAction
  async fetchLanguages() {
    const availableLanguagesArray = await fetchAvailableLanguages();
    return { availableLanguagesArray };
  }

  @MutationAction
  async fetchMessages(language: string) {
    const messagesObject = await fetchMessages(language);
    return { messagesObject };
  }
}

async function fetchAvailableLanguages(): Promise<Array<LanguageDefinition>> {
  const response = await get("messages/available-languages");
  return response.data as Array<LanguageDefinition>;
}

async function fetchAvailableLanguage(
  lang: string
): Promise<LanguageDefinition> {
  const response = await get("messages/available-languages/" + lang);
  return response.data as LanguageDefinition;
}

async function fetchMessages(language: string): Promise<LocaleMessageObject> {
  const response = await get("messages/" + language);
  return response.data as LocaleMessageObject;
}

export default getModule(MessagesModule);
