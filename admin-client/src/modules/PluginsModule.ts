import {
  getModule,
  Module,
  MutationAction,
  VuexModule,
} from "vuex-module-decorators";

import { get } from "@/api";
import { PluginJavaScriptDefinition } from "@/definitions/PluginJavaScriptDefinition";
import { LinkDefinition } from "@/definitions/LinkDefinition";
import store from "@/store";
import { PluginCSSDefinition } from "@/definitions/PluginCSSDefinition";

@Module({
  dynamic: true,
  namespaced: true,
  name: "pluginsModule",
  store,
})
class PluginsModule extends VuexModule {
  pluginCSSArray: Array<PluginCSSDefinition> = [];
  pluginJavaScriptsArray: Array<PluginJavaScriptDefinition> = [];
  pluginNavbarLinksArray: Array<LinkDefinition> = [];

  get pluginCSS() {
    return this.pluginCSSArray;
  }

  get pluginJavaScripts() {
    return this.pluginJavaScriptsArray;
  }

  get pluginNavbarLinks() {
    return this.pluginNavbarLinksArray;
  }

  @MutationAction({ mutate: ["pluginCSSArray"], rawError: true })
  async fetchPluginCSS(lang: string) {
    const pluginCSSArray = await fetchPluginCSS(lang);
    return { pluginCSSArray };
  }

  @MutationAction({ mutate: ["pluginJavaScriptsArray"], rawError: true })
  async fetchPluginJavaScripts(lang: string) {
    const pluginJavaScriptsArray = await fetchPluginJavaScripts(lang);
    return { pluginJavaScriptsArray };
  }

  @MutationAction({ mutate: ["pluginNavbarLinksArray"], rawError: true })
  async fetchPluginNavbarLinks(lang: string) {
    const pluginNavbarLinksArray = await fetchPluginNavbarLinks(lang);
    return { pluginNavbarLinksArray };
  }
}

async function fetchPluginCSS(
  lang: string
): Promise<Array<PluginCSSDefinition>> {
  const response = await get("plugins/" + lang + "/css");
  return response.data as Array<PluginCSSDefinition>;
}

async function fetchPluginJavaScripts(
  lang: string
): Promise<Array<PluginJavaScriptDefinition>> {
  const response = await get("plugins/" + lang + "/java-scripts");
  return response.data as Array<PluginJavaScriptDefinition>;
}

async function fetchPluginNavbarLinks(
  lang: string
): Promise<Array<LinkDefinition>> {
  const response = await get("plugins/" + lang + "/navbar-links");
  return response.data as Array<LinkDefinition>;
}

export default getModule(PluginsModule);
