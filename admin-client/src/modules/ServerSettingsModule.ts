import {
  getModule,
  Module,
  MutationAction,
  VuexModule,
} from "vuex-module-decorators";

import { get } from "@/api";
import { ServerSettingDefinition } from "@/definitions/ServerSettingDefinition";
import store from "@/store";

@Module({
  dynamic: true,
  namespaced: true,
  name: "serverSettingsModule",
  store,
})
class ServerSettingsModule extends VuexModule {
  serverSettingsArray: Array<ServerSettingDefinition> = [];

  get serverSettings() {
    return this.serverSettingsArray;
  }

  @MutationAction
  async fetchServerSettings() {
    const serverSettingsArray = await fetchServerSettings();
    return { serverSettingsArray };
  }
}

export function getPropertyValue(
  serverSettingsArray: Array<ServerSettingDefinition>,
  name: string
) {
  return serverSettingsArray.filter((s) => s.name === name)[0].value;
}

async function fetchServerSettings(): Promise<Array<ServerSettingDefinition>> {
  const response = await get("server-settings/public");
  return response.data as Array<ServerSettingDefinition>;
}

export default getModule(ServerSettingsModule);
