import Vue from "vue";
import Router from "vue-router";
import Dashboard from "@/views/Dashboard.vue";
import NotFound from "@/views/NotFound.vue";
import Users from "@/views/Users.vue";
import Groups from "@/views/Groups.vue";
import Posts from "@/views/Posts.vue";
import Pages from "@/views/Pages.vue";
import Widgets from "@/views/Widgets.vue";
import System from "@/views/System.vue";
import Messages from "@/views/Messages.vue";
import RedirectedLogin from "@/views/RedirectedLogin.vue";
import Themes from "@/views/Themes.vue";

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: "/",
      name: "dashboard",
      component: Dashboard,
    },
    {
      path: "/users*",
      name: "users",
      component: Users,
    },
    {
      path: "/users/edit",
      name: "editUser",
      component: Users,
      props: (route) => ({ query: route.query.id }),
    },
    {
      path: "/groups*",
      name: "groups",
      component: Groups,
    },
    {
      path: "/groups/edit",
      name: "editGroup",
      component: Groups,
      props: (route) => ({ query: route.query.id }),
    },
    {
      path: "/groups/users",
      name: "usersInGroup",
      component: Groups,
      props: (route) => ({ query: route.query.id }),
    },
    {
      path: "/posts*",
      name: "posts",
      component: Posts,
    },
    {
      path: "/posts/edit",
      name: "editPost",
      component: Posts,
      props: (route) => ({ query: route.query.id }),
    },
    {
      path: "/pages*",
      name: "pages",
      component: Pages,
    },
    {
      path: "/pages/edit",
      name: "editPage",
      component: Pages,
      props: (route) => ({ query: route.query.id }),
    },
    {
      path: "/widgets*",
      name: "widgets",
      component: Widgets,
    },
    {
      path: "/widgets/edit",
      name: "editWidget",
      component: Widgets,
      props: (route) => ({ query: route.query.id }),
    },
    {
      path: "/themes/:action",
      name: "themeAction",
      component: Themes,
      props: (route) => ({ query: route.query.name }),
    },
    {
      path: "/themes*",
      name: "themes",
      component: Themes,
    },
    {
      path: "/messages*",
      name: "messages",
      component: Messages,
    },
    {
      path: "/system*",
      name: "system",
      component: System,
    },
    {
      path: "/system/properties",
      name: "propertiesByGroup",
      component: System,
      props: (route) => ({ query: route.query.group }),
    },
    {
      path: "/redirected-login",
      name: "redirected-login",
      component: RedirectedLogin,
      props: (route) => ({ query: route.query.token }),
    },
    {
      path: "/not-found",
      name: "not-found",
      component: NotFound,
    },
  ],
  scrollBehavior(to, from, savedPosition) {
    return { x: 0, y: 0 };
  },
});
