export function parseString(property: any, defaultValue: string): string {
  return property ? property : defaultValue;
}

export function parseNumber(property: any, defaultValue: number): number {
  return property ? property : defaultValue;
}

export function sleep(milliseconds: number) {
  return new Promise((resolve) => setTimeout(resolve, milliseconds));
}

export function addCSS(href: string) {
  const linkElement = document.createElement("link");
  linkElement.setAttribute("rel", "stylesheet");
  linkElement.setAttribute("type", "text/css");
  linkElement.setAttribute("href", href);
  document.head.appendChild(linkElement);
}

export function addScript(src: string) {
  const scriptElement = document.createElement("script");
  scriptElement.setAttribute("type", "text/javascript");
  scriptElement.setAttribute("src", src);
  document.head.appendChild(scriptElement);
}

export function isMobile(): boolean {
  return (
    /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(
      navigator.userAgent
    ) ||
    (window.innerWidth <= 800 && window.innerHeight <= 600)
  );
}
