plugins {
    id("org.ajoberstar.grgit") version "4.0.0"
    id("org.springframework.boot") version "3.2.2"
    id("io.spring.dependency-management") version "1.1.0"
    id("java")
    id("java-library")
}

repositories {
    mavenCentral()
}

allprojects {
    group = "org.arctic.wolf"
    version = "2.0"

    apply(plugin = "org.springframework.boot")
    apply(plugin = "io.spring.dependency-management")
    apply(plugin = "java")
    apply(plugin = "java-library")

    repositories {
        mavenCentral()
    }

    configure<JavaPluginExtension> {
        sourceCompatibility = JavaVersion.VERSION_17
        targetCompatibility = JavaVersion.VERSION_17
    }

    tasks.withType<JavaCompile> {
        options.encoding = "UTF-8"
//        options.compilerArgs.add("-Xlint:deprecation")
//        options.compilerArgs.add("-Xlint:unchecked")
    }

    tasks.withType<Test> {
        useJUnitPlatform()
    }
}
