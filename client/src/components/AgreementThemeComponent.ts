import ThemeComponent from "@/components/ThemeComponent";
import { Prop } from "vue-property-decorator";
import { AgreementDefinition } from "@/definitions/AgreementDefinition";

export default class AgreementThemeComponent extends ThemeComponent {
  @Prop({ type: Object as () => AgreementDefinition })
  currentAgreement!: AgreementDefinition;
}
