import ViewComponent from "@/components/ViewComponent";
import { AgreementDefinition } from "@/definitions/AgreementDefinition";
import pagesModule from "@/modules/PagesModule";
import { ContentParams } from "@/definitions/ContenttDefinition";
import messagesModule from "@/modules/MessagesModule";

export default class AgreementViewComponent extends ViewComponent {
  currentAgreement: AgreementDefinition | any = null;

  async beforeMount() {
    await this.initAgreement();
  }

  async initAgreement() {
    const locale = messagesModule.siteLanguage;

    await pagesModule.fetchAgreement(
      new ContentParams(locale, "", "", this.$route.fullPath)
    );
    this.currentAgreement = pagesModule.agreement;
  }
}
