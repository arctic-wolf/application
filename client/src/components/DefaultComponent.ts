import { Vue } from "vue-property-decorator";
import systemModule from "@/modules/SystemModule";

export default class DefaultComponent extends Vue {
  siteUrl: string = systemModule.siteUrl;
  siteTitle: string = systemModule.siteTitle;
}
