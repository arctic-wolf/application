export interface AgreementDefinition {
  privacyPolicy: string;
  termsOfService: string;
}
