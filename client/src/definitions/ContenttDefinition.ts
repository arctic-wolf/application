export interface ContentDefinition {
  id: number;
  type: string;
  title: string;
  content: string;
  authorId: number;
  author: string;
  createTime: string;
  displayOrder: number;
  friendlyURL: string;
  icon: string;
  lang: string;
}

export class ContentParams {
  readonly lang: string;
  readonly id: string;
  readonly friendlyURL: string;
  readonly routeFullPath: string;

  constructor(
    lang: string,
    id: string,
    friendlyURL: string,
    routeFullPath: string
  ) {
    this.lang = lang;
    this.id = id;
    this.friendlyURL = friendlyURL;
    this.routeFullPath = routeFullPath;
  }
}
