export interface LanguageDefinition {
  icon: string;
  language: string;
  displayName: string;
}
