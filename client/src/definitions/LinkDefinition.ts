export interface LinkDefinition {
  id: number;
  url: string;
  displayName: string;
  icon: string;
  target: string;
  useRouter: boolean;
  useDivider: boolean;
  subLinks: Array<LinkDefinition>;
}
