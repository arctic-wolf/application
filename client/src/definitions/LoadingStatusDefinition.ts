import { LoadingStatusResult } from "@/definitions/enums/LoadingStatusResult";

export interface LoadingStatusDefinition {
  current: number;
  max: number;
  result: LoadingStatusResult;
}
