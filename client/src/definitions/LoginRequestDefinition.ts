export interface LoginRequestDefinition {
  username: string;
  password: string;
}
