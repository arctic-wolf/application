export interface LoginResponseDefinition {
  accessToken: string;
  tokenType: string;
}
