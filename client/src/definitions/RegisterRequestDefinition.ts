export interface RegisterRequestDefinition {
  username: string;
  displayName: string;
  email: string;
  password: string;
  passwordConfirm: string;
  birthDate: string;
  recaptchaResponse: string;
}
