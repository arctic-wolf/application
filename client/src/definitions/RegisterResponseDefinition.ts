export interface RegisterResponseDefinition {
  success: boolean;
  message: string;
}
