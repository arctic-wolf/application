export interface UserDefinition {
  id: number;
  username: string;
  email: string;
  displayName: string;
  birthDate: string;
  createTime: string;
  imageUrl: string;
  bio: string;
  active: boolean;
  authorities: Array<string>;
}
