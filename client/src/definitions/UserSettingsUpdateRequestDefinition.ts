export interface UserSettingsUpdateRequestDefinition {
  password: string;
  passwordConfirm: string;
}
