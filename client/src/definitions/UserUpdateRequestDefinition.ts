export interface UserUpdateRequestDefinition {
  displayName: string;
  birthDate: string;
  imageUrl: string;
  bio: string;
}
