export interface UserVerificationResponseDefinition {
  success: boolean;
  message: string;
}
