export interface UserViewResponseDefinition {
  displayName: string;
  imageUrl: string;
  bio: string;
}
