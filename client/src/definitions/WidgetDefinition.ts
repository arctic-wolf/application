import { LinkDefinition } from "@/definitions/LinkDefinition";

export interface WidgetDefinition {
  id: string;
  displayName: string;
  groupId: string;
  orderInTheGroup: number;
  content: string;
  links: Array<LinkDefinition>;
  lang: string;
}
