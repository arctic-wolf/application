import VueI18n from "vue-i18n";
import { Vue } from "vue-property-decorator";

Vue.use(VueI18n);

export default new VueI18n({
  silentTranslationWarn: true,
});

export function getStoredLanguage(): string {
  return Object.prototype.hasOwnProperty.call(localStorage, "siteLanguage")
    ? "" + localStorage.getItem("siteLanguage")
    : "en";
}
