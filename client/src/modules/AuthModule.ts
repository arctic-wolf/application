import {
  Action,
  getModule,
  Module,
  Mutation,
  MutationAction,
  VuexModule,
} from "vuex-module-decorators";

import { clearJWT, post, setJWT } from "@/api";
import { Route, VueRouter } from "vue-router/types/router";
import { RegisterResponseDefinition } from "@/definitions/RegisterResponseDefinition";
import { RegisterRequestDefinition } from "@/definitions/RegisterRequestDefinition";
import store from "@/store";
import { LoginResponseDefinition } from "@/definitions/LoginResponseDefinition";
import { LoginRequestDefinition } from "@/definitions/LoginRequestDefinition";

@Module({
  dynamic: true,
  namespaced: true,
  name: "authModule",
  store,
})
class AuthModule extends VuexModule {
  authenticated: boolean = false;
  loginResponseObject: LoginResponseDefinition | any = null;
  registerResponseObject: RegisterResponseDefinition | any = null;

  get isAuthenticated() {
    return this.authenticated;
  }

  get loginResponse() {
    return this.loginResponseObject;
  }

  get registerResponse() {
    return this.registerResponseObject;
  }

  @Action
  async oauth2Redirect(params: AuthParams) {
    const token = "" + params.route.query.token;
    localStorage.setItem(ACCESS_TOKEN, token);

    await params.router.push("/", async () => {
      await params.vm.$emit("initApplication");
    });
  }

  @Mutation
  authenticateWithToken(token: string) {
    if (token) {
      setJWT(token);
      this.authenticated = true;
    }
  }

  @Mutation
  clearAuthentication() {
    clearJWT();
    localStorage.removeItem(ACCESS_TOKEN);
    this.authenticated = false;
  }

  @MutationAction({ mutate: ["loginResponseObject"], rawError: true })
  async login(params: AuthParams) {
    const loginResponseObject = await login(params.loginRequest);

    localStorage.setItem(ACCESS_TOKEN, loginResponseObject.accessToken);
    setJWT(loginResponseObject.accessToken);
    this.authenticated = true;

    await params.router.push("/", async () => {
      await params.vm.$emit("initApplication");
    });

    return { loginResponseObject };
  }

  @Mutation
  async logout(params: AuthParams) {
    clearJWT();
    localStorage.removeItem(ACCESS_TOKEN);
    this.authenticated = false;

    await params.router.push("/", async () => {
      await params.vm.$emit("initApplication");
    });
  }

  @MutationAction({ mutate: ["registerResponseObject"], rawError: true })
  async register(data: RegisterRequestDefinition) {
    const registerResponseObject = await register(data);
    return { registerResponseObject };
  }
}

async function login(
  data: LoginRequestDefinition
): Promise<LoginResponseDefinition> {
  const response = await post("auth/login", data);
  return response.data as LoginResponseDefinition;
}

async function register(
  data: RegisterRequestDefinition
): Promise<RegisterResponseDefinition> {
  const response = await post("auth/register", data);
  return response.data as RegisterResponseDefinition;
}

export const ACCESS_TOKEN = "accessToken";

export class AuthParams {
  vm: Vue;
  router: VueRouter;
  route: Route;
  loginRequest: LoginRequestDefinition;

  constructor(
    vm: Vue,
    router: VueRouter,
    route: Route,
    loginRequest: LoginRequestDefinition | any
  ) {
    this.vm = vm;
    this.router = router;
    this.route = route;
    this.loginRequest = loginRequest;
  }
}

export default getModule(AuthModule);
