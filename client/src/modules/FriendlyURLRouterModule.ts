import {
  getModule,
  Module,
  MutationAction,
  VuexModule,
} from "vuex-module-decorators";
import { ContentType } from "@/definitions/enums/ContentType";
import { get } from "@/api";
import { ContentParams } from "@/definitions/ContenttDefinition";
import store from "@/store";

@Module({
  dynamic: true,
  namespaced: true,
  name: "FriendlyURLRouterModule",
  store,
})
class FriendlyURLRouterModule extends VuexModule {
  contentTypeEnum: ContentType = ContentType.NONE;

  get contentType() {
    return this.contentTypeEnum;
  }

  @MutationAction
  async fetchFriendlyURLRouter(params: ContentParams) {
    const contentTypeEnum = await fetchFriendlyURLType(
      params.lang,
      params.friendlyURL,
      params.routeFullPath
    );
    return { contentTypeEnum };
  }
}

async function fetchFriendlyURLType(
  lang: string,
  friendlyURL: string,
  routeFullPath: string
): Promise<ContentType> {
  const response = await get(
    "friendly-url-router/" + lang + "/" + friendlyURL,
    {
      "Route-Full-Path": routeFullPath,
    }
  );
  return response.data as ContentType;
}

export default getModule(FriendlyURLRouterModule);
