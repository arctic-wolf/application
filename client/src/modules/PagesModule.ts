import {
  getModule,
  Module,
  MutationAction,
  VuexModule,
} from "vuex-module-decorators";

import { get } from "@/api";
import { PageDefinition } from "@/definitions/PageDefinition";
import { ContentParams } from "@/definitions/ContenttDefinition";
import { AgreementDefinition } from "@/definitions/AgreementDefinition";
import store from "@/store";

@Module({
  dynamic: true,
  namespaced: true,
  name: "pagesModule",
  store,
})
class PagesModule extends VuexModule {
  pagesArray: Array<PageDefinition> = [];
  pageObject: PageDefinition | any = null;
  agreementObject: AgreementDefinition | any = null;

  get pages() {
    return this.pagesArray;
  }

  get currentPage() {
    return this.pageObject;
  }

  get agreement() {
    return this.agreementObject;
  }

  @MutationAction
  async fetchPages(lang: string) {
    const pagesArray = await fetchPages(lang);
    return { pagesArray };
  }

  @MutationAction({ mutate: ["pageObject"], rawError: true })
  async fetchPageByURL(params: ContentParams) {
    const pageObject = await fetchPageByURL(
      params.lang,
      params.friendlyURL,
      params.routeFullPath
    );
    return { pageObject };
  }

  @MutationAction({ mutate: ["pageObject"], rawError: true })
  async fetchPageById(params: ContentParams) {
    const pageObject = await fetchPageById(
      params.lang,
      params.id,
      params.routeFullPath
    );
    return { pageObject };
  }

  @MutationAction({ mutate: ["agreementObject"], rawError: true })
  async fetchAgreement(params: ContentParams) {
    const agreementObject = await fetchAgreement(
      params.lang,
      params.routeFullPath
    );
    return { agreementObject };
  }
}

async function fetchPages(lang: string): Promise<Array<PageDefinition>> {
  const response = await get("pages/" + lang + "/all");
  return response.data as Array<PageDefinition>;
}

async function fetchPageByURL(
  lang: string,
  friendlyURL: string,
  routeFullPath: string
): Promise<PageDefinition> {
  const response = await get("pages/" + lang + "/by-url/" + friendlyURL, {
    "Route-Full-Path": routeFullPath,
  });
  return response.data as PageDefinition;
}

async function fetchPageById(
  lang: string,
  id: string,
  routeFullPath: string
): Promise<PageDefinition> {
  const response = await get("pages/" + lang + "/by-id/" + id, {
    "Route-Full-Path": routeFullPath,
  });
  return response.data as PageDefinition;
}

async function fetchAgreement(
  lang: string,
  routeFullPath: string
): Promise<AgreementDefinition> {
  const response = await get("pages/" + lang + "/agreement", {
    "Route-Full-Path": routeFullPath,
  });
  return response.data as AgreementDefinition;
}

export default getModule(PagesModule);
