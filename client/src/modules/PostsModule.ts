import {
  getModule,
  Module,
  MutationAction,
  VuexModule,
} from "vuex-module-decorators";

import { get } from "@/api";
import {
  PostDefinition,
  PostListDefinition,
  PostListParams,
} from "@/definitions/PostDefinition";
import { ContentParams } from "@/definitions/ContenttDefinition";
import store from "@/store";

@Module({
  dynamic: true,
  namespaced: true,
  name: "postsModule",
  store,
})
class PostsModule extends VuexModule {
  postList: PostListDefinition | any = null;
  postObject: PostDefinition | any = null;

  get posts() {
    return this.postList;
  }

  get currentPost() {
    return this.postObject;
  }

  @MutationAction
  async fetchPosts(params: PostListParams) {
    const postList = await fetchPosts(params.lang, params.page);
    return { postList };
  }

  @MutationAction({ mutate: ["postObject"], rawError: true })
  async fetchPostByURL(params: ContentParams) {
    const postObject = await fetchPostByURL(
      params.lang,
      params.friendlyURL,
      params.routeFullPath
    );
    return { postObject };
  }

  @MutationAction({ mutate: ["postObject"], rawError: true })
  async fetchPostById(params: ContentParams) {
    const postObject = await fetchPostById(
      params.lang,
      params.id,
      params.routeFullPath
    );
    return { postObject };
  }
}

async function fetchPosts(
  lang: string,
  page: number
): Promise<PostListDefinition> {
  const response = await get("posts/" + lang + "/all?page=" + page);

  return new PostListDefinition(
    response.data as Array<PostDefinition>,
    response.headers["x-page-count"] as number,
    response.headers["x-total-count"] as number
  );
}

async function fetchPostByURL(
  lang: string,
  friendlyURL: string,
  routeFullPath: string
): Promise<PostDefinition> {
  const response = await get("posts/" + lang + "/by-url/" + friendlyURL, {
    "Route-Full-Path": routeFullPath,
  });
  return response.data as PostDefinition;
}

async function fetchPostById(
  lang: string,
  id: string,
  routeFullPath: string
): Promise<PostDefinition> {
  const response = await get("posts/" + lang + "/by-id/" + id, {
    "Route-Full-Path": routeFullPath,
  });
  return response.data as PostDefinition;
}

export default getModule(PostsModule);
