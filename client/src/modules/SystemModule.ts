import {
  getModule,
  Module,
  Mutation,
  MutationAction,
  VuexModule,
} from "vuex-module-decorators";

import { LoadingStatusDefinition } from "@/definitions/LoadingStatusDefinition";
import { get } from "@/api";
import store from "@/store";
import { getStoredSiteThemeColor, SITE_THEME_COLOR } from "@/theme";

@Module({
  dynamic: true,
  namespaced: true,
  name: "systemModule",
  store,
})
class SystemModule extends VuexModule {
  isLoadingApi: boolean = true;
  loadingApiStatusObject: LoadingStatusDefinition | any = null;

  isLoadingClient: boolean = true;
  siteUrlString: string = "";
  siteTitleString: string = "";
  siteThemeColorString: string = getStoredSiteThemeColor();

  get loadingApi() {
    return this.isLoadingApi;
  }

  get loadingApiStatus() {
    return this.loadingApiStatusObject;
  }

  get loadingClient() {
    return this.isLoadingClient;
  }

  get siteUrl() {
    return this.siteUrlString;
  }

  get siteTitle() {
    return this.siteTitleString;
  }

  get siteThemeColor() {
    return this.siteThemeColorString;
  }

  @Mutation
  setLoadingApi(isLoadingApi: boolean) {
    this.isLoadingApi = isLoadingApi;
  }

  @MutationAction({ mutate: ["isLoadingApi"], rawError: true })
  async fetchLoading() {
    const isLoadingApi = await fetchLoading();
    return { isLoadingApi };
  }

  @MutationAction({ mutate: ["loadingApiStatusObject"], rawError: true })
  async fetchLoadingStatus() {
    const loadingApiStatusObject = await fetchLoadingStatus();
    return { loadingApiStatusObject };
  }

  @Mutation
  setLoadingClient(isLoadingClient: boolean) {
    this.isLoadingClient = isLoadingClient;
  }

  @Mutation
  setSiteUrl(siteUrlString: string) {
    this.siteUrlString = siteUrlString;
  }

  @Mutation
  setSiteTitle(siteTitleString: string) {
    this.siteTitleString = siteTitleString;
  }

  @Mutation
  setSiteThemeColor(siteThemeColorString: string) {
    localStorage.setItem(SITE_THEME_COLOR, siteThemeColorString);
    this.siteThemeColorString = siteThemeColorString;
  }
}

async function fetchLoading(): Promise<boolean> {
  const response = await get("system/loading");
  return response.data as boolean;
}

async function fetchLoadingStatus(): Promise<LoadingStatusDefinition> {
  const response = await get("system/loading-status");
  return response.data as LoadingStatusDefinition;
}

export default getModule(SystemModule);
