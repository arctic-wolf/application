import {
  Action,
  getModule,
  Module,
  MutationAction,
  VuexModule,
} from "vuex-module-decorators";

import { API_BASE_URL, get, post, removeDelete, setJWT } from "@/api";
import { UserDefinition } from "@/definitions/UserDefinition";
import store from "@/store";
import { ACCESS_TOKEN } from "@/modules/AuthModule";
import { NotificationDefinition } from "@/definitions/NotificationDefinition";
import { UserViewResponseDefinition } from "@/definitions/UserViewResponseDefinition";
import { UserUpdateRequestDefinition } from "@/definitions/UserUpdateRequestDefinition";
import { UserSettingsUpdateRequestDefinition } from "@/definitions/UserSettingsUpdateRequestDefinition";

@Module({
  dynamic: true,
  namespaced: true,
  name: "usersModule",
  store,
})
class UsersModule extends VuexModule {
  userObject: UserDefinition | any = null;
  currentUserObject: UserDefinition | any = null;
  myNotificationsArray: Array<NotificationDefinition> = [];

  get user() {
    return this.userObject;
  }

  get currentUser() {
    return this.currentUserObject;
  }

  get myNotifications() {
    return this.myNotificationsArray;
  }

  @MutationAction({ mutate: ["userObject"], rawError: true })
  async fetchUser(id: number) {
    const userObject = await fetchUser(id);
    return { userObject };
  }

  @MutationAction({ mutate: ["currentUserObject"], rawError: true })
  async fetchCurrentUser() {
    const currentUserObject = await fetchCurrentUser();
    return { currentUserObject };
  }

  @MutationAction({ mutate: ["currentUserObject"], rawError: true })
  async saveMyProfile(data: UserUpdateRequestDefinition) {
    const currentUserObject = await saveMyProfile(data);
    return { currentUserObject };
  }

  @MutationAction({ mutate: ["currentUserObject"], rawError: true })
  async saveMyUserSettings(data: UserSettingsUpdateRequestDefinition) {
    const currentUserObject = await saveMyUserSettings(data);
    return { currentUserObject };
  }

  @MutationAction({ mutate: ["myNotificationsArray"], rawError: true })
  async fetchMyNotifications() {
    const myNotificationsArray = await fetchMyNotifications();
    return { myNotificationsArray };
  }

  @MutationAction({ mutate: ["myNotificationsArray"], rawError: true })
  async removeNotification(notificationId: number) {
    const myNotificationsArray = await removeNotification(notificationId);
    return { myNotificationsArray };
  }
}

async function fetchUser(id: number): Promise<UserViewResponseDefinition> {
  if (!localStorage.getItem(ACCESS_TOKEN)) {
    return Promise.reject("No access token set!");
  }

  const response = await get("users/" + id);
  return response.data as UserViewResponseDefinition;
}

async function fetchCurrentUser(): Promise<UserDefinition> {
  if (!localStorage.getItem(ACCESS_TOKEN)) {
    return Promise.reject("No access token set!");
  }

  const response = await get("users/me");
  return response.data as UserDefinition;
}

async function saveMyProfile(
  data: UserUpdateRequestDefinition
): Promise<UserDefinition> {
  if (!localStorage.getItem(ACCESS_TOKEN)) {
    return Promise.reject("No access token set!");
  }
  const response = await post("users/me/save-my-profile", data);
  return response.data as UserDefinition;
}

async function saveMyUserSettings(
  data: UserSettingsUpdateRequestDefinition
): Promise<UserDefinition> {
  if (!localStorage.getItem(ACCESS_TOKEN)) {
    return Promise.reject("No access token set!");
  }

  const response = await post("users/me/save-my-user-settings", data);
  return response.data as UserDefinition;
}

async function fetchMyNotifications(): Promise<Array<NotificationDefinition>> {
  if (!localStorage.getItem(ACCESS_TOKEN)) {
    return Promise.reject("No access token set!");
  }

  const response = await get("users/me/my-notifications");
  return response.data as Array<NotificationDefinition>;
}

async function removeNotification(
  notificationId: number
): Promise<Array<NotificationDefinition>> {
  if (!localStorage.getItem(ACCESS_TOKEN)) {
    return Promise.reject("No access token set!");
  }

  const response = await removeDelete(
    "users/me/my-notifications/" + notificationId
  );
  return response.data as Array<NotificationDefinition>;
}

export default getModule(UsersModule);
