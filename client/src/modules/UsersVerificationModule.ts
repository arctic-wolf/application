import {
  getModule,
  Module,
  MutationAction,
  VuexModule,
} from "vuex-module-decorators";

import { post } from "@/api";
import store from "@/store";
import { UserVerificationResponseDefinition } from "@/definitions/UserVerificationResponseDefinition";

@Module({
  dynamic: true,
  namespaced: true,
  name: "usersVerificationModule",
  store,
})
class UsersVerificationModule extends VuexModule {
  verificationResponseObject: UserVerificationResponseDefinition | any = null;

  get verificationResponse() {
    return this.verificationResponseObject;
  }

  @MutationAction({ mutate: ["verificationResponseObject"], rawError: true })
  async verify(token: string) {
    const verificationResponseObject = await verify(token);
    return { verificationResponseObject };
  }
}

async function verify(
  token: string
): Promise<UserVerificationResponseDefinition> {
  const response = await post("users-verification/" + token);
  return response.data as UserVerificationResponseDefinition;
}

export default getModule(UsersVerificationModule);
