import {
  getModule,
  Module,
  MutationAction,
  VuexModule,
} from "vuex-module-decorators";

import { get } from "@/api";
import { WidgetDefinition } from "@/definitions/WidgetDefinition";
import store from "@/store";

@Module({
  dynamic: true,
  namespaced: true,
  name: "widgetsModule",
  store,
})
class WidgetsModule extends VuexModule {
  widgetsArray: Array<WidgetDefinition> = [];

  get widgets() {
    return this.widgetsArray;
  }

  @MutationAction
  async fetchWidgets(lang: string) {
    const widgetsArray = await fetchWidgets(lang);
    return { widgetsArray };
  }
}

async function fetchWidgets(lang: string): Promise<Array<WidgetDefinition>> {
  const response = await get("widgets/" + lang + "/all");
  return response.data as Array<WidgetDefinition>;
}

export default getModule(WidgetsModule);
