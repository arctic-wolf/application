import Vue from "vue";
import Router from "vue-router";
import Home from "@/views/Home.vue";
import Post from "@/views/Post.vue";
import Page from "@/views/Page.vue";
import FriendlyURLRouter from "@/components/FriendlyURLRouter.vue";
import Profile from "@/views/Profile.vue";
import Login from "@/views/Login.vue";
import Register from "@/views/Register.vue";
import UserSettings from "@/views/UserSettings.vue";
import NotFound from "@/views/NotFound.vue";
import OAuth2RedirectHandler from "@/components/OAuth2RedirectHandler.vue";
import Logout from "@/components/Logout.vue";
import RegistrationConfirm from "@/views/RegistrationConfirm.vue";

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: "/",
      name: "home",
      component: Home,
    },
    {
      path: "/post/:friendlyURL",
      name: "postByFriendlyURL",
      component: Post,
    },
    {
      path: "/post",
      name: "postById",
      component: Post,
      props: (route) => ({ query: route.query.id }),
    },
    {
      path: "/page/:friendlyURL",
      name: "pageByFriendlyURL",
      component: Page,
    },
    {
      path: "/page",
      name: "pageById",
      component: Page,
      props: (route) => ({ query: route.query.id }),
    },
    {
      path: "/login",
      name: "login",
      component: Login,
    },
    {
      path: "/register",
      name: "register",
      component: Register,
    },
    {
      path: "/registration-confirm",
      name: "registration-confirm",
      component: RegistrationConfirm,
      props: (route) => ({ query: route.query.token }),
    },
    {
      path: "/profile",
      name: "profileById",
      component: Profile,
      props: (route) => ({ query: route.query.id }),
    },
    {
      path: "/user-settings",
      name: "user-settings",
      component: UserSettings,
    },
    {
      path: "/not-found",
      name: "not-found",
      component: NotFound,
    },
    {
      path: "/oauth2-redirect*",
      name: "oauth2-redirect",
      component: OAuth2RedirectHandler,
      props: (route) => ({ query: route.query.token }),
    },
    {
      path: "/logout",
      name: "logout",
      component: Logout,
    },
    /** Friendly URL Router must always be the last! */
    {
      path: "/:friendlyURL",
      name: "friendlyURLRouter",
      component: FriendlyURLRouter,
    },
  ],
  scrollBehavior(to, from, savedPosition) {
    return { x: 0, y: 0 };
  },
});
