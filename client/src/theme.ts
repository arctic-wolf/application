export const SITE_THEME_COLOR = "siteThemeColor";
export const AVAILABLE_THEME_COLORS = [
  { color: "dark", icon: "fa fa fas fa-moon-o" },
  { color: "light", icon: "material-icons wb_sunny" },
];

export function getStoredSiteThemeColor(): string {
  return Object.prototype.hasOwnProperty.call(localStorage, SITE_THEME_COLOR)
    ? "" + localStorage.getItem(SITE_THEME_COLOR)
    : "light";
}
