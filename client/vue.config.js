// vue.config.js
const {defineConfig} = require('@vue/cli-service')
const path = require("path");

module.exports = defineConfig({
    outputDir: path.resolve(__dirname, "../core/HTML/client"),
    publicPath: process.env.NODE_ENV === "production" ? "./client" : "/",
    devServer: {
        open: process.platform === "darwin",
        host: "0.0.0.0",
        port: 3000,
        https: false
    },
    transpileDependencies: ["vuex-module-decorators", "vuetify"],
    filenameHashing: false,
    css: {
        extract: {
            filename: "[name].css",
            chunkFilename: "[name].css"
        }
    },
    configureWebpack: {
        module: {
            rules: [
                {
                    test: /\.(svg)(\?.*)?$/,
                    use: [
                        {
                            loader: "url-loader",
                            options: {
                                limit: -1,
                                name: "assets/svg/[name].[hash:5].[ext]"
                            }
                        }
                    ]
                }
            ]
        },
        output: {
            filename: "[name].js",
            chunkFilename: "[name].js"
        },
        optimization: {
            splitChunks: {
                cacheGroups: {
                    endpoint: {
                        test: /endpoint.json/,
                        name: "endpoint",
                        enforce: true,
                        chunks: "all"
                    }
                }
            }
        }
    },
    chainWebpack: config => {
        config.module
            .rule("images")
            .use("url-loader")
            .loader("url-loader")
            .tap(options => {
                if (options) {
                    options.limit = -1;
                    options.fallback.options = {
                        ...options.fallback.options,
                        name: "[path][name].[ext]",
                        context: "src",
                        publicPath: path => {
                            return path.startsWith("theme/") ? "../" + path : path;
                        }
                    };
                }
                return options;
            });

        config.module
            .rule("svg")
            .test(() => false)
            .use("file-loader");
    }
})

