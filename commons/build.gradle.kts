apply(plugin = "java")
apply(plugin = "java-library")
apply(plugin = "eclipse")
apply(plugin = "idea")

repositories {
	mavenCentral()
}

dependencies {
	// Spring Boot
	api("org.springframework.boot:spring-boot-starter-web")
	api("org.springframework.boot:spring-boot-starter-data-rest")
	api("org.springframework.boot:spring-boot-starter-security")
	api("org.springframework.boot:spring-boot-starter-data-jpa")

	// Other
	api("com.google.guava:guava:33.0.0-jre")
	api("com.github.lordrex34.config:commons-annotation-config:1.0.7")
	api("com.github.lordrex34.reflection:commons-reflection-utils:1.0.3")
	api("org.apache.commons:commons-lang3:3.14.0")
	api("org.apache.commons:commons-compress:1.21")
	api("org.mapstruct:mapstruct:1.5.5.Final")

	// Database
	api("org.hibernate.validator:hibernate-validator")

	// Lombok
	compileOnly("org.projectlombok:lombok")
	annotationProcessor("org.projectlombok:lombok")

	// MapStruct
	annotationProcessor("org.mapstruct:mapstruct-processor:1.5.5.Final")

	// Test
	testImplementation("org.junit.jupiter:junit-jupiter-engine:5.9.0")
}

tasks.test {
	useJUnitPlatform()
}

