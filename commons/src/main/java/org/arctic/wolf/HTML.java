/*
 * Copyright (c) 2018 Reginald Ravenhorst <lordrex34@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.arctic.wolf;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import lombok.experimental.UtilityClass;

/**
 * @author lord_rex
 */
@UtilityClass
public final class HTML {
	private static final Path HTML_ROOT;
	
	static {
		final Path path = Paths.get("HTML");
		if (Files.notExists(path) || !Files.isDirectory(path)) {
			throw new ExceptionInInitializerError("HTML root does not exist or is not a directory!");
		}
		
		HTML_ROOT = path;
	}
	
	private static final String SLASH = "/";
	public static final String HTML_SUFFIX = ".html";
	
	public static Path getRoot() {
		return HTML_ROOT;
	}
	
	public static String getRootToString() {
		final String rootToString = getRoot().toString();
		return rootToString.endsWith(SLASH) ? rootToString : rootToString + SLASH;
	}
	
	public static String getRootToUriString() {
		return getRoot().toUri().toString();
	}
}
