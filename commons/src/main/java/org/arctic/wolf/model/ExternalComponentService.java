package org.arctic.wolf.model;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.apache.commons.compress.utils.IOUtils;
import org.arctic.wolf.util.JarFileUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class ExternalComponentService<T extends IExternalComponent> {
	protected static final String NO_THUMBNAIL_PNG = "no_thumbnail.png";
	
	protected final Logger logger = LoggerFactory.getLogger(getClass());
	
	private final Map<String, T> availableComponents = new HashMap<>();
	private final Map<String, byte[]> images = new HashMap<>();
	
	protected void loadAll() {
		try {
			getJARs().forEach(this::loadComponent);
		}
		catch (IOException e) {
			logger.error("Error while loading available components.", e);
		}
		
		logger.info("Loaded {} available component(s).", availableComponents.size());
		logger.info("Loaded {} component preview image(s).", images.size());
	}
	
	protected void loadComponent(Path jarFile) {
		final T externalComponent = createNewComponent();
		externalComponent.initFrom(jarFile);
		availableComponents.put(externalComponent.getName(), externalComponent);
		loadPreviewImage(externalComponent);
	}
	
	protected abstract T createNewComponent();
	
	protected void unloadComponent(T component) {
		availableComponents.remove(component.getName());
	}
	
	protected abstract List<Path> getJARs() throws IOException;
	
	public Collection<T> getAvailableComponents() {
		return availableComponents.values();
	}
	
	public Optional<T> getAvailableComponent(String name) {
		return Optional.ofNullable(availableComponents.get(name));
	}
	
	private void loadPreviewImage(T externalComponent) {
		try {
			final Path jarFile = getJarFile(externalComponent);
			final Path path = getPath(externalComponent);
			final String previewImage = externalComponent.getPreviewImage();
			final Path imagePreviewPath = path.resolve(previewImage);
			
			if (Files.notExists(imagePreviewPath)) {
				Files.createDirectories(path);
				JarFileUtil.extract(jarFile, path, List.of(previewImage), Collections.emptyList());
			}
			
			final byte[] previewImageBytes = createPreviewImage(imagePreviewPath);
			images.put(externalComponent.getName(), previewImageBytes);
		}
		catch (IOException e) {
			logger.error("Error while loading preview images.", e);
		}
	}
	
	protected abstract Path getPath(T externalComponent);
	
	protected abstract Path getJarFile(T externalComponent);
	
	public byte[] getPreviewImage(String name) {
		return images.getOrDefault(name, createDefaultThumbnail());
	}
	
	protected byte[] createPreviewImage(Path path) {
		try (InputStream in = Files.newInputStream(path)) {
			return IOUtils.toByteArray(in);
		}
		catch (Exception e) {
			return createDefaultThumbnail();
		}
	}
	
	protected byte[] createDefaultThumbnail() {
		try (InputStream in = ExternalComponentService.class.getResourceAsStream(NO_THUMBNAIL_PNG)) {
			return IOUtils.toByteArray(in);
		}
		catch (Exception e) {
			return null;
		}
	}
}
