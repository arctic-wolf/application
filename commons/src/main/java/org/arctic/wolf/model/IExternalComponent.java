/*
 * Copyright (c) 2019 Reginald Ravenhorst <lordrex34@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.arctic.wolf.model;

import java.nio.file.Path;

import org.arctic.wolf.util.FuzzySearchUtil;

/**
 * @author lord_rex
 */
public interface IExternalComponent extends FuzzySearchUtil.Searchable {
	void setName(String name);
	
	@Override
	String getName();
	
	void setPath(String path);
	
	String getPath();
	
	void setVersion(String version);
	
	String getVersion();
	
	void setDescription(String description);
	
	String getDescription();
	
	void setAuthor(String author);
	
	String getAuthor();
	
	void setCreatedAt(String createdAt);
	
	String getCreatedAt();
	
	void setPreviewImage(String previewImage);
	
	String getPreviewImage();
	
	void setJarFileName(String jarFileName);
	
	String getJarFileName();
	
	default void initFrom(IExternalComponent externalComponent) {
		setName(externalComponent.getName());
		
		updateFrom(externalComponent);
	}
	
	default void updateFrom(IExternalComponent externalComponent) {
		setPath(externalComponent.getPath());
		setVersion(externalComponent.getVersion());
		setDescription(externalComponent.getDescription());
		setAuthor(externalComponent.getAuthor());
		setCreatedAt(externalComponent.getCreatedAt());
		setPreviewImage(externalComponent.getPreviewImage());
		setJarFileName(externalComponent.getJarFileName());
	}
	
	void initFrom(Path jarFile);
}
