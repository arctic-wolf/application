/*
 * Copyright (c) 2019 Reginald Ravenhorst <lordrex34@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.arctic.wolf.model.plugin;

import static org.arctic.wolf.model.plugin.PluginManifestAttributes.PLUGIN_AUTHOR;
import static org.arctic.wolf.model.plugin.PluginManifestAttributes.PLUGIN_CREATED_AT;
import static org.arctic.wolf.model.plugin.PluginManifestAttributes.PLUGIN_DESCRIPTION;
import static org.arctic.wolf.model.plugin.PluginManifestAttributes.PLUGIN_NAME;
import static org.arctic.wolf.model.plugin.PluginManifestAttributes.PLUGIN_PATH;
import static org.arctic.wolf.model.plugin.PluginManifestAttributes.PLUGIN_PREVIEW_IMAGE;
import static org.arctic.wolf.model.plugin.PluginManifestAttributes.PLUGIN_VERSION;

import java.nio.file.Path;
import java.util.List;

import org.arctic.wolf.model.IExternalComponent;
import org.arctic.wolf.model.link.ILink;
import org.arctic.wolf.model.plugin.dto.PluginCSS;
import org.arctic.wolf.model.plugin.dto.PluginJavaScript;
import com.github.lordrex34.reflection.util.jar.VersionInfo;

/**
 * @author lord_rex
 */
public interface IPlugin extends IExternalComponent {
	void setDisplayName(String displayName);
	
	String getDisplayName();
	
	void setState(PluginState state);
	
	PluginState getState();
	
	void setContent(String content);
	
	String getContent();
	
	void setCSS(List<PluginCSS> css);
	
	List<PluginCSS> getCSS();
	
	void setJavaScripts(List<PluginJavaScript> javaScripts);
	
	List<PluginJavaScript> getJavaScripts();
	
	void setNavbarLink(ILink navbarLink);
	
	ILink getNavbarLink();
	
	void setLang(String lang);
	
	String getLang();
	
	@Override
	default void updateFrom(IExternalComponent externalComponent) {
		final IPlugin plugin = (IPlugin) externalComponent;
		
		setDisplayName(plugin.getDisplayName());
		setState(plugin.getState());
		setContent(plugin.getContent());
		setCSS(plugin.getCSS());
		setJavaScripts(plugin.getJavaScripts());
		setNavbarLink(plugin.getNavbarLink());
		setLang(plugin.getLang());
	}
	
	@Override
	default void initFrom(Path jarFile) {
		final VersionInfo versionInfo = new VersionInfo(jarFile.toFile());
		setName(versionInfo.getManifest(PLUGIN_NAME));
		setPath(versionInfo.getManifest(PLUGIN_PATH));
		setVersion(versionInfo.getManifest(PLUGIN_VERSION));
		setDescription(versionInfo.getManifest(PLUGIN_DESCRIPTION));
		setAuthor(versionInfo.getManifest(PLUGIN_AUTHOR));
		setCreatedAt(versionInfo.getManifest(PLUGIN_CREATED_AT));
		setPreviewImage(versionInfo.getManifest(PLUGIN_PREVIEW_IMAGE));
		setJarFileName(jarFile.getFileName().toString());
	}
}
