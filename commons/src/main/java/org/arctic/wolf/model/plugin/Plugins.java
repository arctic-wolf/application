/*
 * Copyright (c) 2019 Reginald Ravenhorst <lordrex34@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.arctic.wolf.model.plugin;

import static org.arctic.wolf.util.JarFileUtil.JAR_EXTENSION;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

import lombok.experimental.UtilityClass;

/**
 * @author lord_rex
 */
@UtilityClass
public class Plugins {
	private static final Path PLUGINS_ROOT;
	
	static {
		final Path path = Paths.get("plugins");
		if (Files.notExists(path) || !Files.isDirectory(path)) {
			throw new ExceptionInInitializerError("Plugins root does not exist or is not a directory!");
		}
		
		PLUGINS_ROOT = path;
	}
	
	public static Path getRoot() {
		return PLUGINS_ROOT;
	}
	
	public static Path getPath(IPlugin plugin) {
		return getRoot().resolve(plugin.getPath());
	}
	
	public static Path getJarFile(IPlugin plugin) {
		return getRoot().resolve(plugin.getJarFileName());
	}
	
	public static List<Path> getPluginJARs() throws IOException {
		return Files.list(getRoot())
			.filter(path -> path.getFileName().toString().endsWith(JAR_EXTENSION))
			.collect(Collectors.toList());
	}
}
