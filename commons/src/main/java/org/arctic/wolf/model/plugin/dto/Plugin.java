/*
 * Copyright (c) 2019 Reginald Ravenhorst <lordrex34@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.arctic.wolf.model.plugin.dto;

import java.util.Collections;
import java.util.List;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.arctic.wolf.model.link.ILink;
import org.arctic.wolf.model.plugin.IPlugin;
import org.arctic.wolf.model.plugin.PluginState;

/**
 * @author lord_rex
 */
@Data
@NoArgsConstructor
@EqualsAndHashCode(of = { "name" })
public class Plugin implements IPlugin {
	private String name;
	private String path;
	private String version;
	private String description;
	private String author;
	private String createdAt;
	private String previewImage;
	private String jarFileName;
	private String displayName;
	private PluginState state = PluginState.INITIALIZED;
	private String content;
	private List<PluginCSS> CSS = Collections.emptyList();
	private List<PluginJavaScript> javaScripts = Collections.emptyList();
	private ILink navbarLink;
	private String lang = "en";
	
	public Plugin(IPlugin plugin) {
		initFrom(plugin);
	}
	
	public String getContent(String routeFullPath) {
		// TODO business logic here
		return content;
	}
}
