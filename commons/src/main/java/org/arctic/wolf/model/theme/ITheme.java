/*
 * Copyright (c) 2019 Reginald Ravenhorst <lordrex34@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.arctic.wolf.model.theme;

import static org.arctic.wolf.model.theme.ThemeManifestAttributes.THEME_AUTHOR;
import static org.arctic.wolf.model.theme.ThemeManifestAttributes.THEME_CREATED_AT;
import static org.arctic.wolf.model.theme.ThemeManifestAttributes.THEME_DESCRIPTION;
import static org.arctic.wolf.model.theme.ThemeManifestAttributes.THEME_NAME;
import static org.arctic.wolf.model.theme.ThemeManifestAttributes.THEME_PATH;
import static org.arctic.wolf.model.theme.ThemeManifestAttributes.THEME_PREVIEW_IMAGE;
import static org.arctic.wolf.model.theme.ThemeManifestAttributes.THEME_VERSION;

import java.nio.file.Path;

import org.arctic.wolf.model.IExternalComponent;
import com.github.lordrex34.reflection.util.jar.VersionInfo;

/**
 * @author lord_rex
 */
public interface ITheme extends IExternalComponent {
	@Override
	default void initFrom(Path jarFile) {
		final VersionInfo versionInfo = new VersionInfo(jarFile.toFile());
		setName(versionInfo.getManifest(THEME_NAME));
		setPath(versionInfo.getManifest(THEME_PATH));
		setVersion(versionInfo.getManifest(THEME_VERSION));
		setDescription(versionInfo.getManifest(THEME_DESCRIPTION));
		setAuthor(versionInfo.getManifest(THEME_AUTHOR));
		setCreatedAt(versionInfo.getManifest(THEME_CREATED_AT));
		setPreviewImage(versionInfo.getManifest(THEME_PREVIEW_IMAGE));
		setJarFileName(jarFile.getFileName().toString());
	}
}
