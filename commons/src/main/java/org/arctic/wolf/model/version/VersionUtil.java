/*
 * Copyright (c) 2018 Reginald Ravenhorst <lordrex34@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.arctic.wolf.model.version;

import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Properties;

import lombok.extern.slf4j.Slf4j;
import org.arctic.wolf.model.version.dto.VersionInfo;
import com.google.common.io.Resources;

/**
 * @author lord_rex
 */
@Slf4j
public final class VersionUtil {
	public static final String VERSION_INFO = "versionInfo";
	private static final String VERSION_INFO_PROPERTIES = "version-info.properties";
	
	private VersionUtil() {
		// utility class
	}
	
	public static List<VersionInfo> getVersionInfo(Class<?> clazz) {
		final List<VersionInfo> versionInfo = new ArrayList<>();
		
		try {
			final URL url = Resources.getResource(clazz, VERSION_INFO_PROPERTIES);
			
			final Properties properties = new Properties();
			properties.load(url.openStream());
			properties.forEach((k, v) -> {
				final VersionInfo info = new VersionInfo();
				info.setName(String.valueOf(k));
				info.setValue(String.valueOf(v));
				versionInfo.add(info);
			});
		}
		catch (Exception e) {
			return Collections.emptyList();
		}
		
		return versionInfo;
	}
}
