/*
 * Copyright (c) 2018 Reginald Ravenhorst <lordrex34@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.arctic.wolf.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import jakarta.persistence.EntityManager;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * @author lord_rex
 */
@Deprecated
public final class FuzzySearchUtil {
	private FuzzySearchUtil() {
		// utility class
	}
	
	private static <T> List<T> fuzzySearch(EntityManager entityManager, Class<T> clazz, String search, String... fields) {
		return Collections.emptyList();
	}
	
	public static <T> Page<T> pagedFuzzySearch(Pageable pageable, EntityManager entityManager, Class<T> clazz, String search, String... fields) {
		final List<T> elements = fuzzySearch(entityManager, clazz, search, fields);
		return PageUtil.getPagedView(pageable, elements);
	}
	
	public static <T> Page<T> pagedFilteredFuzzySearch(Pageable pageable, Predicate<T> predicate, EntityManager entityManager, Class<T> clazz, String search, String... fields) {
		final List<T> elements = fuzzySearch(entityManager, clazz, search, fields).stream().filter(predicate).collect(Collectors.toList());
		return PageUtil.getPagedView(pageable, elements);
	}
	
	public interface Searchable {
		String getName();
	}
	
	private static <T extends Searchable> List<T> fuzzySearch(List<T> source, String... keywords) {
		final List<T> result = new ArrayList<>();
		
		for (String keyword : keywords) {
			source
				.stream()
				.filter(searchable -> StringSimilarityUtil.similarity(searchable.getName(), keyword) >= 0.3)
				.forEach(result::add);
		}
		
		return result;
	}
	
	public static <T extends Searchable> Page<T> pagedFuzzySearch(Pageable pageable, List<T> source, String... keywords) {
		final List<T> elements = fuzzySearch(source, keywords);
		return PageUtil.getPagedView(pageable, elements);
	}
	
	public static <T extends Searchable> Page<T> pagedFilteredFuzzySearch(Pageable pageable, Predicate<T> predicate, List<T> source, String... keywords) {
		final List<T> elements = fuzzySearch(source, keywords).stream().filter(predicate).collect(Collectors.toList());
		return PageUtil.getPagedView(pageable, elements);
	}
}
