/*
 * Copyright (c) 2018 Reginald Ravenhorst <lordrex34@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.arctic.wolf.util;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import lombok.experimental.UtilityClass;
import com.google.common.base.CaseFormat;

/**
 * @author lord_rex
 */
@UtilityClass
public final class JarFileUtil {
	public static final String JAR_EXTENSION = ".jar";
	public static final String META_INF = "META-INF";
	public static final String META_INF_MANIFEST_MF = META_INF + "/MANIFEST.MF";
	
	public static void extract(Path jarFilePath, Path destinationPath, List<String> includes, List<String> excludes) throws IOException {
		Files.createDirectories(destinationPath);
		
		try (ZipFile archive = new ZipFile(jarFilePath.toFile())) {
			// Sort entries by name to always create folders first.
			final List<? extends ZipEntry> entries = archive.stream()
				.sorted(Comparator.comparing(ZipEntry::getName))
				.collect(Collectors.toList());
			
			for (ZipEntry entry : entries) {
				final String entryName = entry.getName();
				final Path entryDestination = destinationPath.resolve(entryName);
				
				if (excludes.stream().anyMatch(entryName::contains)) {
					continue;
				}
				
				if (!includes.isEmpty() && includes.stream().noneMatch(entryName::contains)) {
					continue;
				}
				
				if (entry.isDirectory()) {
					Files.createDirectories(entryDestination);
					continue;
				}
				
				Files.copy(archive.getInputStream(entry), entryDestination, StandardCopyOption.REPLACE_EXISTING);
			}
		}
	}
	
	public static String camelToHypenJarName(String name) {
		return CaseFormat.UPPER_CAMEL.to(CaseFormat.LOWER_HYPHEN, name) + ".jar";
	}
}
