/*
 * Copyright (c) 2019 Reginald Ravenhorst <lordrex34@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.arctic.wolf.util;

import java.util.List;
import jakarta.servlet.http.HttpServletResponse;

import lombok.experimental.UtilityClass;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

/**
 * @author lord_rex
 */
@UtilityClass
public class PageUtil {
	public static final String X_PAGE_COUNT = "X-Page-Count";
	public static final String X_TOTAL_COUNT = "X-Total-Count";
	
	public static <T> Page<T> getPagedView(Pageable pageable, List<T> elements) {
		final int start = (int) pageable.getOffset();
		final int end = (start + pageable.getPageSize()) > elements.size() ? elements.size() : (start + pageable.getPageSize());
		return new PageImpl<>(elements.subList(start, end), pageable, elements.size());
	}
	
	public static <T> List<T> getPagedContent(HttpServletResponse response, Page<T> pagedView) {
		response.setHeader(X_PAGE_COUNT, String.valueOf(pagedView.getTotalPages()));
		response.setHeader(X_TOTAL_COUNT, String.valueOf(pagedView.getTotalElements()));
		
		return pagedView.getContent();
	}
}
