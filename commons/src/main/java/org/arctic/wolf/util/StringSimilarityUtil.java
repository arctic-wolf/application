/*
 * Copyright (c) 2018 Reginald Ravenhorst <lordrex34@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.arctic.wolf.util;

/**
 * https://stackoverflow.com/questions/955110/similarity-string-comparison-in-java
 */
public final class StringSimilarityUtil {
	private StringSimilarityUtil() {
		// utility class
	}
	
	/**
	 * Calculates the similarity (a number within 0 and 1) between two strings.
	 */
	public static double similarity(String s1, String s2) {
		String longer = s1, shorter = s2;
		if (s1.length() < s2.length()) { // longer should always have greater length
			longer = s2;
			shorter = s1;
		}
		
		int longerLength = longer.length();
		if (longerLength == 0) {
			return 1.0; /* both strings are zero length */
		}
		
		return (longerLength - editDistance(longer, shorter)) / (double) longerLength;
	}
	
	// Example implementation of the Levenshtein Edit Distance
	// See http://rosettacode.org/wiki/Levenshtein_distance#Java
	public static int editDistance(String s1, String s2) {
		s1 = s1.toLowerCase();
		s2 = s2.toLowerCase();
		
		int[] costs = new int[s2.length() + 1];
		for (int i = 0; i <= s1.length(); i++) {
			int lastValue = i;
			for (int j = 0; j <= s2.length(); j++) {
				if (i == 0) {
					costs[j] = j;
				}
				else {
					if (j > 0) {
						int newValue = costs[j - 1];
						if (s1.charAt(i - 1) != s2.charAt(j - 1)) {
							newValue = Math.min(Math.min(newValue, lastValue),
								costs[j]) + 1;
						}
						costs[j - 1] = lastValue;
						lastValue = newValue;
					}
				}
			}
			if (i > 0) {
				costs[s2.length()] = lastValue;
			}
		}
		return costs[s2.length()];
	}
	
	public static void printSimilarity(String s, String t) {
		System.out.println(String.format(
			"%.3f is the similarity between \"%s\" and \"%s\"", similarity(s, t), s, t));
	}
	
	public static void main(String[] args) {
		printSimilarity("", "");
		printSimilarity("1234567890", "1");
		printSimilarity("1234567890", "123");
		printSimilarity("1234567890", "1234567");
		printSimilarity("1234567890", "1234567890");
		printSimilarity("1234567890", "1234567980");
		printSimilarity("47/2010", "472010");
		printSimilarity("47/2010", "472011");
		printSimilarity("47/2010", "AB.CDEF");
		printSimilarity("47/2010", "4B.CDEFG");
		printSimilarity("47/2010", "AB.CDEFG");
		printSimilarity("The quick fox jumped", "The fox jumped");
		printSimilarity("The quick fox jumped", "The fox");
		printSimilarity("kitten", "sitting");
	}
}
