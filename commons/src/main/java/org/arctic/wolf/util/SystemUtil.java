/*
 * Copyright (c) 2018 Reginald Ravenhorst <lordrex34@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.arctic.wolf.util;

import java.lang.management.ManagementFactory;
import java.text.DecimalFormat;
import java.time.LocalDateTime;
import java.util.Arrays;

import com.sun.management.OperatingSystemMXBean;
import lombok.extern.slf4j.Slf4j;

/**
 * @author NB4L1, Savormix & UnAfraid (memory statistics)
 * @author lord_rex (CPU statistics)
 */
@Slf4j
public final class SystemUtil {
	private SystemUtil() {
		// utility class
	}
	
	public static void printMemoryUsageStatistics() {
		Arrays.stream(getMemoryUsageStatistics()).forEach(LOGGER::info);
	}
	
	/**
	 * Gets the CPU usage of this application.
	 * @return process CPU load
	 */
	public static double getProcessCpuLoad() {
		return ((OperatingSystemMXBean) ManagementFactory.getOperatingSystemMXBean()).getProcessCpuLoad();
	}
	
	/**
	 * Gets the CPU usage of this application in percentage format.
	 * @return process CPU load
	 */
	public static String getFormattedProcessCpuLoad() {
		return new DecimalFormat("0.00%").format(getProcessCpuLoad());
	}
	
	/**
	 * Gets the used memory of this application.
	 * @return used memory
	 */
	public static long getMemoryUsage() {
		return Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
	}
	
	/**
	 * Gets the used memory of this application in a formatted view.
	 * @return used memory
	 */
	public static String getFormattedMemoryUsage() {
		return SystemUtil.formatBytes(getMemoryUsage());
	}
	
	/**
	 * Gets the memory usage statistics of this application.
	 * @return memory usage statistics
	 */
	public static String[] getMemoryUsageStatistics() {
		final double max = Runtime.getRuntime().maxMemory(); // maxMemory is the upper limit the jvm can use
		final double allocated = Runtime.getRuntime().totalMemory(); // totalMemory the size of the current allocation pool
		final double nonAllocated = max - allocated; // non allocated memory till jvm limit
		final double cached = Runtime.getRuntime().freeMemory(); // freeMemory the unused memory in the allocation pool
		final double used = allocated - cached; // really used memory
		final double useable = max - used; // allocated, but non-used and non-allocated memory
		
		final DecimalFormat df = new DecimalFormat(" (0.00'%')");
		
		return new String[] {
			"+----",
			"| Global Memory Information at: " + LocalDateTime.now(),
			"|    |", // ...
			"| Allowed Memory: " + formatBytes(max),
			"|    |= Allocated Memory: " + formatBytes(allocated) + df.format((allocated / max) * 100),
			"|    |= Non-Allocated Memory: " + formatBytes(nonAllocated) + df.format((nonAllocated / max) * 100),
			"| Allocated Memory: " + formatBytes(allocated),
			"|    |= Used Memory: " + formatBytes(used) + df.format((used / max) * 100),
			"|    |= Unused (cached) Memory: " + formatBytes(cached) + df.format((cached / max) * 100),
			"| Useable Memory: " + formatBytes(useable) + df.format((useable / max) * 100),
			"+----"
		};
	}
	
	public static String formatBytes(double bytes) {
		if (bytes < 1024) {
			return bytes + " B";
		}
		int z = (63 - Long.numberOfLeadingZeros(Math.round(bytes))) / 10;
		return String.format("%.1f %sB", bytes / (1L << (z * 10)), " KMGTPE".charAt(z));
	}
}
