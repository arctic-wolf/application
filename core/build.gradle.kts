import org.ajoberstar.grgit.Grgit
import java.text.SimpleDateFormat
import java.util.Date

plugins {
	distribution
}

apply(plugin = "java")
apply(plugin = "eclipse")
apply(plugin = "idea")
apply(plugin = "org.springframework.boot")
apply(plugin = "org.ajoberstar.grgit")
apply(plugin = "distribution")

repositories {
	mavenCentral()
}

sourceSets["main"].resources.srcDir("${projectDir}/HTML")

dependencies {
	// Arctic Wolf
	implementation(project(":commons"))
	implementation(project(":plugin-sdk"))

	// Spring Boot
	implementation("org.springframework.boot:spring-boot-starter-oauth2-client")
	implementation("org.springframework.boot:spring-boot-starter-thymeleaf")

	// Against flood
	implementation("com.giffing.bucket4j.spring.boot.starter:bucket4j-spring-boot-starter-springboot2:0.1.15")

	// Embedded WebServer
	implementation("org.springframework.boot:spring-boot-starter-tomcat")

	// Social & Mail
	implementation("org.springframework.boot:spring-boot-starter-mail")

	// Security
	implementation("io.jsonwebtoken:jjwt-impl:0.11.5")
	implementation("io.jsonwebtoken:jjwt-jackson:0.11.5")

	// Other
	implementation("org.glassfish.jaxb:jaxb-runtime")
	implementation("org.apache.httpcomponents:httpclient")
	implementation("org.apache.commons:commons-lang3")
	implementation("commons-beanutils:commons-beanutils:1.9.4")
	implementation("com.vaadin.external.google:android-json:0.0.20131108.vaadin1")
	implementation("com.github.unafraid.plugins:Plugins-API:1.1.5")
	implementation("com.squareup.retrofit2:retrofit:2.9.0")
	implementation("com.squareup.retrofit2:converter-jackson:2.9.0")
	implementation("com.github.lordrex34.config:commons-annotation-config:1.0.7")

	// Database Drivers
	runtimeOnly("mysql:mysql-connector-java")
	runtimeOnly("org.postgresql:postgresql")
	testRuntimeOnly("com.h2database:h2")

	// Lombok
	compileOnly("org.projectlombok:lombok")
	annotationProcessor("org.projectlombok:lombok")

	// MapStruct
	annotationProcessor("org.mapstruct:mapstruct-processor:1.5.3.Final")

	// Test
	testImplementation("org.springframework.boot:spring-boot-starter-test")
	testImplementation("org.springframework.security:spring-security-test")
	testImplementation("org.junit.jupiter:junit-jupiter-engine:5.9.0")
}

tasks.bootJar {
	enabled = true
	archiveClassifier.set("bin")
}

tasks.jar {
	enabled = true
	exclude("org/arctic/wolf/SiteApplication**")
}

distributions {
	main {
		contents {
			mkdir("dist/plugins")
			mkdir("dist/themes")

			from("dist") {
				exclude("**/*.java")
			}

			from("keystore.jks")

			from(tasks.bootJar)

			into("config") {
				from("src/main/resources/application.yml")
			}

			into("HTML") {
				from("HTML")
			}
		}
	}
}

tasks.register("createThemesPluginsDirectory") {
	doFirst {
		mkdir("HTML")
		mkdir("themes")
		mkdir("plugins")
	}
}

tasks.register("versionInfo") {
	doFirst {
		val grgit = Grgit.open {
			dir = project.rootDir
		}
		val createdBy = "Java " + System.getProperty("java.version") + " (" + System.getProperty("java.vendor") + ")"
		val builtBy = System.getProperty("user.name")
		val buildTime = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ").format(Date())
		val gitBranch = grgit.branch.current().name
		val gitHashFull = grgit.head().id
		val gitHashShort = grgit.head().abbreviatedId
		val gitCommitCount = grgit.log().size

		val versionInfoFile = file("$projectDir/src/main/resources/org/arctic/wolf/model/version/version-info.properties")
		versionInfoFile.delete()
		versionInfoFile.createNewFile()

		versionInfoFile.appendText("# Version Info\n")
		versionInfoFile.appendText("Version=$version\n")
		versionInfoFile.appendText("CreatedBy=$createdBy\n")
		versionInfoFile.appendText("BuiltBy=$builtBy\n")
		versionInfoFile.appendText("BuildTime=$buildTime\n")
		versionInfoFile.appendText("GitBranch=$gitBranch\n")
		versionInfoFile.appendText("GitHashFull=$gitHashFull\n")
		versionInfoFile.appendText("GitHashShort=$gitHashShort\n")
		versionInfoFile.appendText("GitCommitCount=$gitCommitCount\n")
	}
}

tasks.register("htmlClean") {
	doLast {
		delete("HTML")
	}
}

tasks.clean.get().dependsOn("htmlClean")
tasks.processResources.get().dependsOn("createThemesPluginsDirectory", "versionInfo")
tasks.distTar.get().dependsOn("jar", "bootJar")
tasks.distZip.get().dependsOn("jar", "bootJar")
