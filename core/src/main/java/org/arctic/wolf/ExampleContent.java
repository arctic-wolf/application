/*
 * Copyright (c) 2017 Reginald Ravenhorst <lordrex34@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.arctic.wolf;

import static org.arctic.wolf.model.server.ServerSettingsVariables.DEFAULT_EXAMPLE_CONTENT_INSTALLED_ON;
import static org.arctic.wolf.model.server.ServerSettingsVariables.EXAMPLE_CONTENT_INSTALLED_ON;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.EnumSet;
import java.util.Set;
import jakarta.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.arctic.wolf.exception.UserNotFoundException;
import org.arctic.wolf.model.content.ContentService;
import org.arctic.wolf.model.content.ContentVisibility;
import org.arctic.wolf.model.content.dto.form.PageForm;
import org.arctic.wolf.model.content.dto.form.PostForm;
import org.arctic.wolf.model.group.Authority;
import org.arctic.wolf.model.group.GroupService;
import org.arctic.wolf.model.group.dto.form.GroupForm;
import org.arctic.wolf.model.message.LanguageService;
import org.arctic.wolf.model.server.ServerSettingsService;
import org.arctic.wolf.model.theme.Themes;
import org.arctic.wolf.model.user.UserDB;
import org.arctic.wolf.model.user.UserService;
import org.arctic.wolf.model.user.dto.form.UserForm;
import org.arctic.wolf.model.widget.WidgetService;
import org.arctic.wolf.model.widget.dto.form.WidgetForm;
import org.springframework.jdbc.datasource.init.ScriptException;
import org.springframework.stereotype.Component;
import org.springframework.util.FileSystemUtils;
import com.google.common.io.Resources;

/**
 * @author lord_rex
 */
@Slf4j
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@Component
public class ExampleContent {
	private final ServerSettingsService serverSettingsService;
	private final UserService userService;
	private final GroupService groupService;
	private final WidgetService widgetService;
	private final ContentService contentService;
	private final LanguageService languageService;
	
	@PostConstruct
	void onStartup() throws ScriptException {
		if (serverSettingsService.getString(EXAMPLE_CONTENT_INSTALLED_ON, DEFAULT_EXAMPLE_CONTENT_INSTALLED_ON).equals(DEFAULT_EXAMPLE_CONTENT_INSTALLED_ON)) {
			LOGGER.info("Installing example content ...");
			
			createDefaultThemeBackup();
			createLanguages();
			createUsers();
			createGroups();
			createWidgets();
			createPosts();
			createPages();
			
			final String date = LocalDate.now().format(DateTimeFormatter.ISO_DATE);
			serverSettingsService.setProperty(EXAMPLE_CONTENT_INSTALLED_ON, date);
			LOGGER.info("Example content installed on: {}", date);
		}
		else {
			LOGGER.info("Skipping Example Content installation.");
		}
	}
	
	private void createDefaultThemeBackup() {
		final Path currentThemePath = Themes.getCurrentThemePath();
		final Path defaultThemeBackupPath = Themes.getDefaultThemeBackupPath();
		
		try {
			Files.createDirectories(defaultThemeBackupPath);
			FileSystemUtils.copyRecursively(currentThemePath, defaultThemeBackupPath);
			
			LOGGER.info("Default Theme '{}' copied successfully to backup root '{}'.", currentThemePath, defaultThemeBackupPath);
		}
		catch (IOException e) {
			LOGGER.error("Failed to backup default theme!", e);
		}
	}
	
	private void createLanguages() {
		languageService.createByForm("flag-icon flag-icon-gb", "en", "English");
		languageService.createByForm("flag-icon flag-icon-hu", "hu", "Hungarian");
	}
	
	private void createUsers() {
		createUser("admin", "admin", "admin@localhost", "Administrator");
		createUser("user", "user", "user@localhost", "User");
	}
	
	private void createGroups() {
		createGroup("users", "Group of regular users.", EnumSet.of(Authority.REGULAR_USER));
		
		final UserDB user = userService.findByUsernameDB("user").orElseThrow(UserNotFoundException::new);
		groupService.addUser("users", user);
		
		createGroup("admins", "Group of administrators.", EnumSet.allOf(Authority.class));
		
		final UserDB admin = userService.findByUsernameDB("admin").orElseThrow(UserNotFoundException::new);
		groupService.addUser("admins", admin);
	}
	
	private void createGroup(String name, String description, Set<Authority> authorities) {
		final GroupForm groupForm = new GroupForm();
		groupForm.setName(name);
		groupForm.setDescription(description);
		groupForm.setAuthorities(authorities);
		groupService.createByForm(groupForm);
	}
	
	private void createUser(String username, String password, String email, String displayName) {
		final UserForm userForm = new UserForm();
		userForm.setUsername(username);
		userForm.setPassword(password);
		userForm.setEmail(email);
		userForm.setDisplayName(displayName);
		userForm.setBirthDate(Date.from(LocalDate.of(1900, 4, 1).atStartOfDay(ZoneId.systemDefault()).toInstant()));
		userForm.setActive(true);
		userService.createByForm(userForm);
	}
	
	private void createWidgets() {
		createWidget("pagesMenu", "Pages Menu", "rightWidgetGroup", "page-list", 0, true);
		createWidget("firstExampleWidget", "First Example Widget", "rightWidgetGroup", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. <b>Aliquam et ex neque.</b> Suspendisse laoreet lectus elementum dolor feugiat facilisis.", 1, false);
		createWidget("secondExampleWidget", "Second Example Widget", "rightWidgetGroup", "Quisque nec dolor dignissim, viverra urna non, aliquet nulla. <i>Fusce arcu orci, congue in mollis vitae, pharetra a leo.</i> Morbi cursus auctor iaculis.", 2, false);
		createWidget("thirdExampleWidget", "Third Example Widget", "rightWidgetGroup", "Etiam dignissim, ex vitae aliquam hendrerit, mi lacus dictum dolor, nec <u>imperdiet arcu lorem vitae nisl.</u>", 3, false);
		createWidget("fourthExampleWidget", "Fourth Example Widget", "anotherExampleGroup", "Sed in nisi pulvinar diam tempus fermentum eu vel nulla.", 4, false);
	}
	
	private void createWidget(String id, String displayName, String groupId, String content, int orderInTheGroup, boolean fragment) {
		final WidgetForm widgetForm = new WidgetForm();
		widgetForm.setId(id);
		widgetForm.setDisplayName(displayName);
		widgetForm.setGroupId(groupId);
		widgetForm.setContent(content);
		widgetForm.setOrderInTheGroup(orderInTheGroup);
		widgetForm.setFragment(fragment);
		widgetForm.setLang("en");
		widgetForm.setVisibility(EnumSet.allOf(ContentVisibility.class));
		
		widgetService.createByForm(widgetForm);
	}
	
	private void createPosts() {
		createPost("First Post", 1L, "lord_rex", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam et ex neque. Suspendisse laoreet lectus elementum dolor feugiat facilisis. In leo ipsum, finibus in tellus eget, finibus imperdiet mi. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Etiam sit amet aliquet tortor. Quisque nec dolor dignissim, viverra urna non, aliquet nulla. Fusce arcu orci, congue in mollis vitae, pharetra a leo. Morbi cursus auctor iaculis. Integer feugiat neque a lacinia semper. Curabitur eget neque risus. Sed porttitor ullamcorper ligula in pharetra. <b>Duis varius magna nisl, sed pellentesque lorem interdum at.</b> Praesent ipsum augue, dignissim in tellus quis, lobortis dignissim dui. Pellentesque faucibus velit non ultrices iaculis.");
		createPost("Second Post", 1L, "lord_rex", "Etiam dignissim, ex vitae aliquam hendrerit, mi lacus dictum dolor, nec imperdiet arcu lorem vitae nisl. Nam porttitor vehicula felis imperdiet faucibus. Nam ac ultricies arcu. Maecenas mattis sed turpis molestie rhoncus. In cursus eleifend urna vitae pretium. Ut vel vulputate felis. Duis varius tincidunt sapien, eu aliquet tortor ornare vel. Donec scelerisque leo sit amet odio mattis vestibulum. Aliquam ligula ligula, condimentum et odio vel, fringilla fringilla augue. Mauris at lacus felis. Mauris fermentum porta justo in feugiat. In risus dui, ultricies et varius sit amet, fringilla nec tortor. Sed hendrerit nunc nec porta fermentum.");
		createPost("Third Post", 2L, "not_rex", "Sed porttitor auctor sapien eu volutpat. In hac habitasse platea dictumst. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Donec a tellus quis magna cursus ultricies ut eget augue. Sed sollicitudin suscipit magna, et sagittis sapien sodales eu. Suspendisse eu facilisis nisl. Sed in nisi pulvinar diam tempus fermentum eu vel nulla.");
		createPost("Fourth Post", 1L, "lord_rex", "Nullam at dolor enim. Praesent vitae fermentum nulla, vitae sodales lacus. <b>Proin semper bibendum venenatis.</b> Praesent volutpat non mauris id pretium. Suspendisse potenti. Vestibulum pretium, massa lacinia dictum hendrerit, odio odio rhoncus risus, ac dignissim dui arcu a dolor. Vivamus condimentum erat magna, vel posuere magna ullamcorper ut. Aliquam facilisis dictum iaculis. Quisque at diam tortor. Morbi a sapien sit amet massa eleifend sagittis et quis purus.");
		createPost("Fifth Post", 1L, "lord_rex", "Praesent lacinia odio sit amet leo imperdiet, id bibendum metus venenatis. Etiam neque metus, pharetra non congue sed, pulvinar molestie felis. Aenean rhoncus bibendum lectus, at fermentum libero aliquet a. Curabitur mi dolor, lacinia non commodo et, suscipit ac nisl. Morbi scelerisque fringilla tortor eget feugiat. Praesent in imperdiet purus, in lobortis augue. Integer porttitor libero lorem, vel posuere ex posuere sed.");
		createPost("Seventh Post", null, "lord_rex", "Suspendisse eleifend, nibh eu auctor aliquet, libero lorem cursus enim, ac elementum tellus justo a neque. <i>Morbi luctus massa eros, in tempor dui venenatis sed.</i> Morbi enim purus, feugiat non enim sit amet, tempus porttitor nisi. Maecenas molestie laoreet justo in iaculis. Etiam vitae consequat nisl, nec vulputate quam.");
		createPost("Eight Post", null, "lord_rex", "Phasellus fringilla condimentum erat egestas feugiat. Nulla facilisi. Ut maximus lorem nunc, sit amet imperdiet neque sagittis in. Praesent ac consequat neque. In augue elit, eleifend quis lacus ac, hendrerit vulputate mi. Duis quis nisi eget velit imperdiet imperdiet vel placerat lorem. Maecenas finibus accumsan cursus. Nulla nec lobortis lorem. Suspendisse faucibus lorem nec vestibulum blandit. Integer non sem ultricies lacus finibus faucibus vitae eget lorem. Praesent nec ullamcorper dui.");
	}
	
	private void createPost(String title, Long authorId, String author, String content) {
		final PostForm postForm = new PostForm();
		postForm.setTitle(title);
		postForm.setAuthorId(authorId);
		postForm.setAuthor(author);
		postForm.setContent(content);
		postForm.setLang("en");
		postForm.setVisibility(EnumSet.allOf(ContentVisibility.class));
		contentService.createContent(postForm);
	}
	
	private void createPages() {
		createPage("first-page", "First Page", 1L, "lord_rex", "Vivamus vitae arcu ut velit volutpat porttitor sed hendrerit dui. Donec augue augue, viverra eget vestibulum nec, vehicula in eros. Donec pretium aliquet laoreet. Pellentesque condimentum pellentesque turpis. Aenean aliquam lectus vel gravida rutrum. Phasellus vel diam lorem. Vivamus ac nunc sit amet risus condimentum ullamcorper. <b>Phasellus imperdiet felis odio, quis volutpat lorem euismod ac.</b> Vestibulum dignissim ligula nec dui porttitor laoreet.");
		createPage("second-page", "Second Page", 1L, "lord_rex", "Nullam vel lorem consectetur, sagittis tellus id, imperdiet dolor. Phasellus dapibus velit ante, eget porttitor libero iaculis ac. Praesent commodo risus et ligula egestas, non fringilla felis dapibus. Morbi et metus tempus, porttitor sem nec, facilisis sem. Maecenas interdum aliquam mi, in tincidunt tortor convallis at. Vivamus rutrum sagittis vestibulum. Vestibulum in posuere ipsum. Proin vehicula iaculis velit, sit amet vulputate lorem scelerisque vel. Duis erat neque, accumsan et iaculis ut, ornare a arcu. Etiam velit elit, vestibulum vitae ex efficitur, tincidunt consequat tellus. Sed id nulla vel nibh efficitur posuere. Ut finibus laoreet laoreet. Vivamus non odio eget nisi porttitor facilisis a eget orci. Ut porta pulvinar est pellentesque lobortis. Sed id urna eget nulla auctor sodales.");
		createPage("third-page", "Third Page", 2L, "not_rex", "Curabitur bibendum sapien vitae eleifend rutrum. In sit amet ligula sem. Fusce vitae urna justo. Vivamus vitae mattis ligula. Ut eget eros ligula. Suspendisse id ante et sem lobortis mattis. Fusce et purus ut nibh pharetra sagittis. Lorem ipsum dolor sit amet, consectetur adipiscing elit. In fringilla lorem nec vestibulum pellentesque. <i>Nunc vel felis lacus.</i> Vivamus dignissim aliquet dolor. Integer mattis pellentesque nunc eget feugiat. Praesent pellentesque id ante non tristique.");
		createPage("fourth-page", "Fourth Page", null, "lord_rex", "Maecenas posuere purus nec feugiat gravida. Sed mi erat, consequat nec tristique nec, gravida vitae nulla. Duis lobortis venenatis lectus, vitae consequat quam porta viverra. Donec ultrices neque at egestas porta. Vestibulum quis venenatis purus. Suspendisse et vehicula lectus. Donec tincidunt tellus ut sapien molestie, ut ultricies enim blandit. Maecenas pulvinar, magna sed sagittis commodo, sapien ex suscipit orci, ut dictum orci justo ut orci. Nulla eget tempor lacus. Integer venenatis rutrum ex.");
		createPage("fifth-page", "Fifth Page", null, "lord_rex", "Etiam ut eros ac leo imperdiet blandit ac vitae velit. Fusce feugiat mollis luctus.<br><br> Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Maecenas a dictum velit, et bibendum metus. Proin vitae tristique purus. Curabitur tempor porttitor venenatis. <u>Donec sit amet malesuada dolor, eu ornare sem.</u> Aenean vel enim in quam scelerisque commodo vel ut mauris. Donec ut aliquet ligula. Curabitur ut purus nisi. Aliquam erat volutpat. Mauris vulputate at risus nec pellentesque. Nam sollicitudin nisi quam, pulvinar pretium magna maximus nec.");
		createPage("privacy-policy", "Privacy Policy", null, "https://getterms.io", getResourceContent("privacy-policy.html"));
		createPage("terms-of-service", "Terms of Service", null, "https://getterms.io", getResourceContent("terms-of-service.html"));
		createPage("contact", "Contact", 1L, "lord_rex", getResourceContent("contact.html"));
	}
	
	public static String getResourceContent(String resourceName) {
		try {
			return Resources.toString(Resources.getResource(ExampleContent.class, resourceName), StandardCharsets.UTF_8);
		}
		catch (IOException e) {
			LOGGER.warn("", e);
			return null;
		}
	}
	
	private void createPage(String friendlyURL, String title, Long authorId, String author, String content) {
		final PageForm pageForm = new PageForm();
		pageForm.setFriendlyURL(friendlyURL);
		pageForm.setTitle(title);
		pageForm.setAuthorId(authorId);
		pageForm.setAuthor(author);
		pageForm.setContent(content);
		pageForm.setLang("en");
		pageForm.setVisibility(EnumSet.allOf(ContentVisibility.class));
		contentService.createContent(pageForm);
	}
}
