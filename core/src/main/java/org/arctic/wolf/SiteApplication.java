/*
 * Copyright (c) 2017 Reginald Ravenhorst <lordrex34@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.arctic.wolf;

import java.lang.management.ManagementFactory;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.concurrent.atomic.AtomicBoolean;
import org.springframework.beans.factory.annotation.Autowired;

import lombok.extern.slf4j.Slf4j;
import org.arctic.wolf.model.message.MessageMaintenance;
import org.arctic.wolf.model.version.VersionService;
import org.arctic.wolf.util.SystemUtil;
import org.arctic.wolf.util.TimeAmountInterpreter;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.event.EventListener;
import org.springframework.core.annotation.Order;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Slf4j
@EnableTransactionManagement
@ComponentScan(basePackages = "org.arctic.wolf", lazyInit = true)
@SpringBootApplication
public class SiteApplication extends SpringBootServletInitializer {
	public static final AtomicBoolean LOADING = new AtomicBoolean(true);
	
	private static final String SPRING_CONFIG_LOCATION = "spring.config.location";
	private static final String APPLICATION_CONFIG = "config/application.yml";
	
	private static final String HTTPS_PROTOCOLS = "https.protocols";
	private static final String DEFAULT_HTTPS_PROTOCOLS = "TLSv1.2,TLSv1.1,TLSv1";
	
	static {
		System.setProperty(HTTPS_PROTOCOLS, DEFAULT_HTTPS_PROTOCOLS);
		
		if (Files.exists(Paths.get(APPLICATION_CONFIG))) {
			System.setProperty(SPRING_CONFIG_LOCATION, APPLICATION_CONFIG);
		}
	}
	
	@SuppressWarnings("unused")
	@Autowired
	private VersionService versionService;
	
	@SuppressWarnings("unused")
	@Autowired
	private MessageMaintenance messageMaintenance;
	
	@SuppressWarnings("unused")
	@Autowired
	private ExampleContent exampleContent;
	
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(SiteApplication.class);
	}
	
	@Order(3)
	@EventListener(ApplicationReadyEvent.class)
	public void onStartup() {
		System.gc();
		System.runFinalization();
		SystemUtil.printMemoryUsageStatistics();
		
		LOADING.set(false);
		LOGGER.info("Application finished loading in {}.", TimeAmountInterpreter.consolidateMillis(ManagementFactory.getRuntimeMXBean().getUptime()));
	}
	
	public static void main(String[] args) {
		SpringApplication.run(SiteApplication.class, args);
	}
}
