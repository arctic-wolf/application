/*
 * Copyright (c) 2019 Reginald Ravenhorst <lordrex34@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.arctic.wolf.api.v1.controller;

import java.net.URI;
import org.springframework.beans.factory.annotation.Autowired;
import jakarta.validation.Valid;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.arctic.wolf.model.user.AuthenticationService;
import org.arctic.wolf.model.user.UserDB;
import org.arctic.wolf.model.user.UserRegisterService;
import org.arctic.wolf.model.user.dto.LoginRequest;
import org.arctic.wolf.model.user.dto.LoginResponse;
import org.arctic.wolf.model.user.dto.RegisterRequest;
import org.arctic.wolf.model.user.dto.RegisterResponse;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

/**
 * @author lord_rex
 */
@Slf4j
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@RestController
@RequestMapping("api/v1/auth")
public class AuthController extends AbstractController {
	private final UserRegisterService userRegisterService;
	private final AuthenticationService authenticationService;
	
	@PostMapping("register")
	public ResponseEntity<?> registerUser(@Valid @RequestBody RegisterRequest registerRequest, BindingResult bindingResult) {
		verifyBindingResult(bindingResult);
		
		final UserDB newUser = userRegisterService.registerUser(registerRequest);
		
		final URI location = ServletUriComponentsBuilder
			.fromCurrentContextPath().path("/profile/me")
			.buildAndExpand(newUser.getId()).toUri();
		
		return ResponseEntity.created(location)
			.body(new RegisterResponse(true, "register_success"));
	}
	
	@PostMapping("login")
	public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest, BindingResult bindingResult) {
		verifyBindingResult(bindingResult);
		
		final String token = authenticationService.login(loginRequest);
		
		return ResponseEntity.ok(new LoginResponse(token));
	}
}
