/*
 * Copyright (c) 2019 Reginald Ravenhorst <lordrex34@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.arctic.wolf.api.v1.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import jakarta.servlet.http.HttpServletResponse;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.arctic.wolf.model.content.ContentService;
import org.arctic.wolf.model.content.ContentType;
import org.arctic.wolf.model.content.dto.Agreement;
import org.arctic.wolf.model.content.dto.Content;
import org.arctic.wolf.model.plugin.PluginService;
import org.arctic.wolf.model.server.ServerSettingsService;
import org.arctic.wolf.security.CurrentUser;
import org.arctic.wolf.security.UserPrincipal;
import org.arctic.wolf.util.PageUtil;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@Slf4j
@RequestMapping("api/v1/pages")
@RestController
public class PageController extends AbstractController {
	private final ContentService contentService;
	private final PluginService pluginService;
	private final ServerSettingsService serverSettingsService;
	
	@GetMapping("{lang}/all")
	public List<Content> getPages(@CurrentUser UserPrincipal userPrincipal, @PageableDefault(size = 5) Pageable pageable, @PathVariable String lang, HttpServletResponse response) {
		final List<Content> pages = contentService.getContents(ContentType.PAGE, lang, getUserDB(userPrincipal));
		final Page<Content> pagedView = PageUtil.getPagedView(pageable, pages);
		
		return PageUtil.getPagedContent(response, pagedView);
	}
	
	@RequestMapping("{lang}/by-id/{id}")
	public Content getPage(@CurrentUser UserPrincipal userPrincipal, @PathVariable String lang, @PathVariable long id, @RequestHeader("Route-Full-Path") String routeFullPath) {
		return contentService.getContent(ContentType.PAGE, lang, id, routeFullPath, getUserDB(userPrincipal), pluginService, serverSettingsService);
	}
	
	@RequestMapping("{lang}/by-url/{friendlyURL}")
	public Content getPage(@CurrentUser UserPrincipal userPrincipal, @PathVariable String lang, @PathVariable String friendlyURL, @RequestHeader("Route-Full-Path") String routeFullPath) {
		return contentService.getContent(ContentType.PAGE, lang, friendlyURL, routeFullPath, getUserDB(userPrincipal), pluginService, serverSettingsService);
	}
	
	@RequestMapping("{lang}/agreement")
	public Agreement getAgreement(@CurrentUser UserPrincipal userPrincipal, @PathVariable String lang, @RequestHeader("Route-Full-Path") String routeFullPath) {
		return contentService.getAgreement(lang, routeFullPath, getUserDB(userPrincipal), pluginService, serverSettingsService);
	}
}
