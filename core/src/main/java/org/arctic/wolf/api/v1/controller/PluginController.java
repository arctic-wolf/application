/*
 * Copyright (c) 2019 Reginald Ravenhorst <lordrex34@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.arctic.wolf.api.v1.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;

import lombok.RequiredArgsConstructor;
import org.arctic.wolf.model.link.dto.Link;
import org.arctic.wolf.model.plugin.PluginService;
import org.arctic.wolf.model.plugin.dto.PluginCSS;
import org.arctic.wolf.model.plugin.dto.PluginJavaScript;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@RequestMapping("api/v1/plugins")
@RestController
public class PluginController {
	private final PluginService pluginService;
	
	@GetMapping("{lang}/css")
	public List<PluginCSS> getCSS(@PathVariable String lang) {
		return pluginService.getCSS(lang);
	}
	
	@GetMapping("{lang}/java-scripts")
	public List<PluginJavaScript> getJavaScripts(@PathVariable String lang) {
		return pluginService.getJavaScripts(lang);
	}
	
	@GetMapping("{lang}/navbar-links")
	public List<Link> getNavbarLinks(@PathVariable String lang) {
		return pluginService.getNavbarLinks(lang);
	}
}
