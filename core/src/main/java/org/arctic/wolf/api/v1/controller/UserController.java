/*
 * Copyright (c) 2019 Reginald Ravenhorst <lordrex34@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.arctic.wolf.api.v1.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import jakarta.validation.Valid;

import lombok.RequiredArgsConstructor;
import org.arctic.wolf.exception.UserNotFoundException;
import org.arctic.wolf.model.notification.NotificationService;
import org.arctic.wolf.model.notification.dto.Notification;
import org.arctic.wolf.model.user.IUser;
import org.arctic.wolf.model.user.UserDB;
import org.arctic.wolf.model.user.UserService;
import org.arctic.wolf.model.user.dto.User;
import org.arctic.wolf.model.user.dto.UserSettingsUpdateRequest;
import org.arctic.wolf.model.user.dto.UserUpdateRequest;
import org.arctic.wolf.model.user.dto.UserViewResponse;
import org.arctic.wolf.security.CurrentUser;
import org.arctic.wolf.security.UserPrincipal;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author lord_rex
 */
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@RestController
@RequestMapping("api/v1/users")
public class UserController extends AbstractController {
	private final UserService userService;
	private final NotificationService notificationService;
	
	@GetMapping("/{userId}")
	@PreAuthorize("hasRole(T(org.arctic.wolf.model.group.Authority).REGULAR_USER.getAuthority())")
	public UserViewResponse getUser(@PathVariable Long userId) {
		return userService.viewProfile(userId)
			.orElseThrow(UserNotFoundException::new);
	}
	
	@GetMapping("me")
	@PreAuthorize("hasRole(T(org.arctic.wolf.model.group.Authority).REGULAR_USER.getAuthority())")
	public User getCurrentUser(@CurrentUser UserPrincipal userPrincipal) {
		return userService.findByIdDTO(userPrincipal.getId())
			.orElseThrow(UserNotFoundException::new);
	}
	
	@PostMapping("me/save-my-profile")
	@PreAuthorize("hasRole(T(org.arctic.wolf.model.group.Authority).REGULAR_USER.getAuthority())")
	public IUser saveMyProfile(@CurrentUser UserPrincipal userPrincipal, @Valid @RequestBody UserUpdateRequest userUpdateRequest) {
		final UserDB userDB = userService.findByIdDB(userPrincipal.getId())
			.orElseThrow(UserNotFoundException::new);
		
		userService.updateProfile(userDB, userUpdateRequest);
		
		return userService.findByIdDTO(userDB.getId())
			.orElseThrow(UserNotFoundException::new);
	}
	
	@PostMapping("me/save-my-user-settings")
	@PreAuthorize("hasRole(T(org.arctic.wolf.model.group.Authority).REGULAR_USER.getAuthority())")
	public IUser saveMyUserSettings(@CurrentUser UserPrincipal userPrincipal, @Valid @RequestBody UserSettingsUpdateRequest userSettingsUpdateRequest) {
		final UserDB userDB = userService.findByIdDB(userPrincipal.getId())
			.orElseThrow(UserNotFoundException::new);
		
		userService.updateUserSettings(userDB, userSettingsUpdateRequest);
		
		return userService.findByIdDTO(userDB.getId())
			.orElseThrow(UserNotFoundException::new);
	}
	
	@GetMapping("me/my-notifications")
	@PreAuthorize("hasRole(T(org.arctic.wolf.model.group.Authority).REGULAR_USER.getAuthority())")
	public List<Notification> getMyNotifications(@CurrentUser UserPrincipal userPrincipal) {
		return notificationService.getNotificationsDTO(userPrincipal.getUserDB());
	}
	
	@DeleteMapping("me/my-notifications/{notificationId}")
	@PreAuthorize("hasRole(T(org.arctic.wolf.model.group.Authority).REGULAR_USER.getAuthority())")
	public List<Notification> removeNotification(@CurrentUser UserPrincipal userPrincipal, @PathVariable Long notificationId) {
		final UserDB userDB = userPrincipal.getUserDB();
		
		notificationService.removeNotification(userDB, notificationId);
		
		return notificationService.getNotificationsDTO(userDB);
	}
}
