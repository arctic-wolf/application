/*
 * Copyright (c) 2019 Reginald Ravenhorst <lordrex34@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.arctic.wolf.api.v1.controller;

import org.springframework.beans.factory.annotation.Autowired;

import lombok.RequiredArgsConstructor;
import org.arctic.wolf.exception.BadRequestException;
import org.arctic.wolf.exception.UserVerificationTokenNotFoundException;
import org.arctic.wolf.model.user.UserService;
import org.arctic.wolf.model.user.verification.UserVerificationService;
import org.arctic.wolf.model.user.verification.UserVerificationTokenDB;
import org.arctic.wolf.model.user.verification.dto.UserVerificationResponse;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author lord_rex
 */
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@RequestMapping("api/v1/users-verification")
@RestController
public class UserVerificationController {
	private final UserVerificationService userVerificationService;
	private final UserService userService;
	
	@PostMapping("/{token}")
	public UserVerificationResponse verify(@PathVariable String token) {
		final UserVerificationTokenDB userVerificationTokenDB = userVerificationService.findByToken(token)
			.orElseThrow(() -> new UserVerificationTokenNotFoundException("user_verification_token_invalid"));
		
		if (userVerificationTokenDB.isUsed()) {
			throw new BadRequestException("user_verification_token_used");
		}
		
		if (userVerificationTokenDB.isExpired()) {
			throw new BadRequestException("user_verification_token_expired");
		}
		
		userVerificationService.useToken(userVerificationTokenDB);
		userService.activateUser(userVerificationTokenDB.getUserDB());
		
		return new UserVerificationResponse(true, "user_verification_success");
	}
}
