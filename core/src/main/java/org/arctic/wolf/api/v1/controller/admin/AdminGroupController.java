/*
 * Copyright (c) 2019 Reginald Ravenhorst <lordrex34@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.arctic.wolf.api.v1.controller.admin;

import static org.arctic.wolf.model.group.AdminGroupManagementMessages.ADMIN_EDIT_GROUP_NAME_ALREADY_EXIST;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.validation.Valid;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.arctic.wolf.api.v1.controller.AbstractController;
import org.arctic.wolf.exception.BadRequestException;
import org.arctic.wolf.exception.GroupNotFoundException;
import org.arctic.wolf.model.group.Authority;
import org.arctic.wolf.model.group.GroupDB;
import org.arctic.wolf.model.group.GroupService;
import org.arctic.wolf.model.group.dto.AuthorityModel;
import org.arctic.wolf.model.group.dto.Group;
import org.arctic.wolf.model.group.dto.form.EditGroupForm;
import org.arctic.wolf.model.group.dto.form.GroupForm;
import org.arctic.wolf.model.user.UserService;
import org.arctic.wolf.model.user.dto.User;
import org.arctic.wolf.util.PageUtil;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author lord_rex
 */
@Slf4j
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@RestController
@RequestMapping("api/v1/admin/groups")
public class AdminGroupController extends AbstractController {
	private final GroupService groupService;
	private final UserService userService;
	
	@GetMapping("authorities/all")
	@PreAuthorize("hasRole(T(org.arctic.wolf.model.group.Authority).MANAGE_GROUPS_AUTHORITIES.getAuthority())")
	public List<AuthorityModel> getAuthorities() {
		return Arrays.stream(Authority.values())
			.map(AuthorityModel::new)
			.collect(Collectors.toList());
	}
	
	@GetMapping("authorities/{id}")
	@PreAuthorize("hasRole(T(org.arctic.wolf.model.group.Authority).MANAGE_GROUPS_AUTHORITIES.getAuthority())")
	public List<AuthorityModel> getAuthorities(@PathVariable long id) {
		return groupService.getGroupDTO(id)
			.orElseThrow(GroupNotFoundException::new).getAuthorities()
			.stream()
			.map(AuthorityModel::new)
			.collect(Collectors.toList());
	}
	
	@GetMapping("all")
	@PreAuthorize("hasRole(T(org.arctic.wolf.model.group.Authority).MANAGE_GROUPS_LIST.getAuthority())")
	public List<Group> getGroups(@PageableDefault(size = 15) Pageable pageable, HttpServletResponse response) {
		final Page<Group> pagedView = groupService.getGroupsDTO(pageable);
		return PageUtil.getPagedContent(response, pagedView);
	}
	
	@GetMapping("/{id}")
	@PreAuthorize("hasRole(T(org.arctic.wolf.model.group.Authority).MANAGE_GROUPS_LIST.getAuthority())")
	public Group getGroup(@PathVariable long id) {
		return groupService.getGroupDTO(id)
			.orElseThrow(GroupNotFoundException::new);
	}
	
	@GetMapping("find/{search}")
	@PreAuthorize("hasRole(T(org.arctic.wolf.model.group.Authority).MANAGE_GROUPS_FIND.getAuthority())")
	public List<Group> findGroups(@PathVariable String search, @PageableDefault(size = 15) Pageable pageable, HttpServletResponse response) {
		final Page<Group> pagedView = groupService.pagedFuzzySearchDTO(search, pageable);
		return PageUtil.getPagedContent(response, pagedView);
	}
	
	@PostMapping("add")
	@PreAuthorize("hasRole(T(org.arctic.wolf.model.group.Authority).MANAGE_GROUPS_ADD.getAuthority())")
	public Group createGroup(@Valid @RequestBody GroupForm groupForm, BindingResult bindingResult) {
		verifyBindingResult(bindingResult);
		
		final GroupDB byForm = groupService.createByForm(groupForm);
		
		return groupService.getGroupDTO(byForm.getId())
			.orElseThrow(GroupNotFoundException::new);
	}
	
	@DeleteMapping("{id}/delete")
	@PreAuthorize("hasRole(T(org.arctic.wolf.model.group.Authority).MANAGE_GROUPS_DELETE.getAuthority())")
	public boolean deleteGroup(@PathVariable long id) {
		try {
			groupService.delete(id);
			return true;
		}
		catch (Exception e) {
			throw new GroupNotFoundException("Couldn't delete group by ID " + id + "!");
		}
	}
	
	@PutMapping("{id}/edit")
	@PreAuthorize("hasRole(T(org.arctic.wolf.model.group.Authority).MANAGE_GROUPS_EDIT.getAuthority())")
	public Group editGroup(@PathVariable long id, @Valid @RequestBody EditGroupForm editGroupForm, BindingResult bindingResult) {
		verifyBindingResult(bindingResult);
		
		final GroupDB groupDB = groupService.getGroupDB(id)
			.orElseThrow(GroupNotFoundException::new);
		
		final String name = editGroupForm.getName();
		if (!groupDB.getName().equals(name) && StringUtils.isNotEmpty(name) && groupService.exists(name)) {
			throw new BadRequestException(ADMIN_EDIT_GROUP_NAME_ALREADY_EXIST);
		}
		
		groupService.updateByForm(groupDB, editGroupForm);
		
		return groupService.getGroupDTO(id)
			.orElseThrow(GroupNotFoundException::new);
	}
	
	@GetMapping("{id}/members/all")
	@PreAuthorize("hasRole(T(org.arctic.wolf.model.group.Authority).MANAGE_GROUPS_LIST_MEMBERS.getAuthority())")
	public List<User> getGroupMembers(@PathVariable long id, @PageableDefault(size = 15) Pageable pageable, HttpServletResponse response) {
		final Page<User> pagedView = groupService.getPagedUsersDTO(pageable, id);
		return PageUtil.getPagedContent(response, pagedView);
	}
	
	@PostMapping("{id}/members/add")
	@PreAuthorize("hasRole(T(org.arctic.wolf.model.group.Authority).MANAGE_GROUPS_ADD_MEMBER.getAuthority())")
	public boolean addGroupMembers(@PathVariable long id, @RequestBody List<Long> userIdList) {
		userIdList.forEach(userId -> userService.findByIdDB(userId)
			.ifPresent(userDB -> groupService.addUser(id, userDB)));
		
		return true;
	}
	
	@DeleteMapping("{id}/members/remove")
	@PreAuthorize("hasRole(T(org.arctic.wolf.model.group.Authority).MANAGE_GROUPS_REMOVE_MEMBER.getAuthority())")
	public boolean removeGroupMembers(@PathVariable long id, @RequestBody List<Long> userIdList) {
		userIdList.forEach(userId -> userService.findByIdDB(userId)
			.ifPresent(userDB -> groupService.removeUser(id, userDB)));
		
		return true;
	}
}
