/*
 * Copyright (c) 2019 Reginald Ravenhorst <lordrex34@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.arctic.wolf.api.v1.controller.admin;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.validation.Valid;

import lombok.RequiredArgsConstructor;
import org.arctic.wolf.api.v1.controller.AbstractController;
import org.arctic.wolf.exception.LanguageNotFoundException;
import org.arctic.wolf.exception.MessageNotFoundException;
import org.arctic.wolf.model.message.LanguageService;
import org.arctic.wolf.model.message.MessageDB;
import org.arctic.wolf.model.message.MessageService;
import org.arctic.wolf.model.message.dto.Language;
import org.arctic.wolf.model.message.dto.MessageModel;
import org.arctic.wolf.model.message.dto.form.LanguageForm;
import org.arctic.wolf.model.message.dto.form.MessageForm;
import org.arctic.wolf.model.message.dto.form.MessagesForm;
import org.arctic.wolf.util.PageUtil;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author lord_rex
 */
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@RequestMapping("api/v1/admin/messages")
@RestController
public class AdminMessagesController extends AbstractController {
	private final LanguageService languageService;
	private final MessageService messageService;
	
	@GetMapping("languages")
	@PreAuthorize("hasRole(T(org.arctic.wolf.model.group.Authority).MANAGE_LANGUAGES_LIST.getAuthority())")
	public List<Language> getLanguages(@PageableDefault(size = 15) Pageable pageable, HttpServletResponse response) {
		final Page<Language> pagedView = PageUtil.getPagedView(pageable, languageService.getLanguagesDTO());
		return PageUtil.getPagedContent(response, pagedView);
	}
	
	@GetMapping("languages/{lang}")
	@PreAuthorize("hasRole(T(org.arctic.wolf.model.group.Authority).MANAGE_LANGUAGES_LIST.getAuthority())")
	public Language getLanguage(@PathVariable String lang) {
		return languageService.findByLanguageDTO(lang)
			.orElseThrow(LanguageNotFoundException::new);
	}
	
	@PostMapping("languages/add")
	@PreAuthorize("hasRole(T(org.arctic.wolf.model.group.Authority).MANAGE_LANGUAGES_ADD.getAuthority())")
	public Language addLanguage(@Valid @RequestBody LanguageForm languageForm) {
		languageService.createByForm(languageForm);
		
		return languageService.findByLanguageDTO(languageForm.getLanguage())
			.orElseThrow(LanguageNotFoundException::new);
	}
	
	@PutMapping("languages/{lang}/allow-disallow")
	@PreAuthorize("hasRole(T(org.arctic.wolf.model.group.Authority).MANAGE_LANGUAGES_ALLOW_DISALLOW.getAuthority())")
	public boolean allowDisallowLanguage(@PathVariable String lang) {
		return languageService.allowDisallow(lang);
	}
	
	@DeleteMapping("languages/{lang}/delete")
	@PreAuthorize("hasRole(T(org.arctic.wolf.model.group.Authority).MANAGE_LANGUAGES_DELETE.getAuthority())")
	public boolean deleteLanguage(@PathVariable String lang) {
		languageService.delete(lang);
		return true;
	}
	
	@GetMapping("{lang}/all")
	@PreAuthorize("hasRole(T(org.arctic.wolf.model.group.Authority).MANAGE_MESSAGES.getAuthority())")
	public List<MessageModel> getMessages(@PageableDefault(size = 25) Pageable pageable, @PathVariable String lang, HttpServletResponse response) {
		final List<MessageModel> messages = messageService.getMessagesDTO(lang);
		final Page<MessageModel> pagedView = PageUtil.getPagedView(pageable, messages);
		
		return PageUtil.getPagedContent(response, pagedView);
	}
	
	@PostMapping("add")
	@PreAuthorize("hasRole(T(org.arctic.wolf.model.group.Authority).MANAGE_MESSAGES_ADD.getAuthority())")
	public boolean addMessage(@Valid @RequestBody MessageForm messageForm, BindingResult bindingResult) {
		verifyBindingResult(bindingResult);
		
		messageService.createByForm(messageForm);
		
		return true;
	}
	
	@PostMapping("add-list")
	@PreAuthorize("hasRole(T(org.arctic.wolf.model.group.Authority).MANAGE_MESSAGES_ADD.getAuthority())")
	public boolean addMessages(@Valid @RequestBody MessagesForm messagesForm, BindingResult bindingResult) {
		verifyBindingResult(bindingResult);
		
		for (MessageForm messageForm : messagesForm.getMessages()) {
			messageService.createByForm(messageForm);
		}
		
		return true;
	}
	
	@DeleteMapping("{id}/delete")
	@PreAuthorize("hasRole(T(org.arctic.wolf.model.group.Authority).MANAGE_MESSAGES_DELETE.getAuthority())")
	public boolean deleteMessage(@PathVariable long id) {
		if (messageService.findByIdDB(id).isPresent()) {
			messageService.delete(id);
			return true;
		}
		
		throw new MessageNotFoundException();
	}
	
	@PutMapping("{id}/edit")
	@PreAuthorize("hasRole(T(org.arctic.wolf.model.group.Authority).MANAGE_MESSAGES_EDIT.getAuthority())")
	public boolean editMessage(@PathVariable long id, @Valid @RequestBody MessageForm messageForm, BindingResult bindingResult) {
		verifyBindingResult(bindingResult);
		
		final MessageDB messageDB = messageService.findByIdDB(id)
			.orElseThrow(MessageNotFoundException::new);
		
		messageService.updateByForm(messageDB, messageForm);
		
		return true;
	}
}
