/*
 * Copyright (c) 2019 Reginald Ravenhorst <lordrex34@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.arctic.wolf.api.v1.controller.admin;

import java.util.Collections;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;

import lombok.RequiredArgsConstructor;
import org.arctic.wolf.api.v1.controller.AbstractController;
import org.arctic.wolf.model.plugin.dto.Plugin;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author lord_rex
 */
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@RequestMapping("api/v1/admin/plugins")
@RestController
public class AdminPluginController extends AbstractController {
	@GetMapping("available-plugins")
	@PreAuthorize("hasRole(T(org.arctic.wolf.model.group.Authority).MANAGE_PLUGINS_AVAILABLE_PLUGINS.getAuthority())")
	public List<Plugin> getAvailablePlugins() {
		// TODO JIRA CORE-69
		return Collections.emptyList();
	}
	
	@GetMapping("installed-plugins")
	@PreAuthorize("hasRole(T(org.arctic.wolf.model.group.Authority).MANAGE_PLUGINS_INSTALLED_PLUGINS.getAuthority())")
	public List<Plugin> getInstalledPlugins() {
		// TODO JIRA CORE-69
		return Collections.emptyList();
	}
	
	@GetMapping("find-new-plugins/{search}")
	@PreAuthorize("hasRole(T(org.arctic.wolf.model.group.Authority).MANAGE_PLUGINS_FIND_NEW.getAuthority())")
	public List<Plugin> findNewPlugins(@PathVariable String search) {
		// TODO JIRA CORE-69
		return Collections.emptyList();
	}
	
	@GetMapping("plugin-repository")
	@PreAuthorize("hasRole(T(org.arctic.wolf.model.group.Authority).MANAGE_PLUGINS_REPOSITORY.getAuthority())")
	public List<Plugin> getPluginRepository() {
		// TODO JIRA CORE-69
		return Collections.emptyList();
	}
	
	@PostMapping("{name}/update")
	@PreAuthorize("hasRole(T(org.arctic.wolf.model.group.Authority).MANAGE_PLUGINS_UPDATE.getAuthority())")
	public Plugin updatePlugin(@PathVariable String name) {
		// TODO JIRA CORE-69
		return null;
	}
	
	@DeleteMapping("{name}/uninstall")
	@PreAuthorize("hasRole(T(org.arctic.wolf.model.group.Authority).MANAGE_PLUGINS_UNINSTALL.getAuthority())")
	public Plugin uninstallPlugin(@PathVariable String name) {
		// TODO JIRA CORE-69
		return null;
	}
	
	@PostMapping("{name}/install")
	@PreAuthorize("hasRole(T(org.arctic.wolf.model.group.Authority).MANAGE_PLUGINS_INSTALL.getAuthority())")
	public Plugin installPlugin(@PathVariable String name) {
		// TODO JIRA CORE-69
		return null;
	}
	
	@PostMapping("{name}/download")
	@PreAuthorize("hasRole(T(org.arctic.wolf.model.group.Authority).MANAGE_PLUGINS_DOWNLOAD.getAuthority())")
	public Plugin downloadPlugin(@PathVariable String name) {
		// TODO JIRA CORE-69
		return null;
	}
	
	@DeleteMapping("{name}/delete")
	@PreAuthorize("hasRole(T(org.arctic.wolf.model.group.Authority).MANAGE_PLUGINS_DELETE.getAuthority())")
	public Plugin deletePlugin(@PathVariable String name) {
		// TODO JIRA CORE-69
		return null;
	}
	
	@PostMapping("{name}/start")
	@PreAuthorize("hasRole(T(org.arctic.wolf.model.group.Authority).MANAGE_PLUGINS_START.getAuthority())")
	public Plugin startPlugin(@PathVariable String name) {
		// TODO JIRA CORE-69
		return null;
	}
	
	@PostMapping("{name}/stop")
	@PreAuthorize("hasRole(T(org.arctic.wolf.model.group.Authority).MANAGE_PLUGINS_STOP.getAuthority())")
	public Plugin stopPlugin(@PathVariable String name) {
		// TODO JIRA CORE-69
		return null;
	}
	
	@PostMapping("{name}/auto-start")
	@PreAuthorize("hasRole(T(org.arctic.wolf.model.group.Authority).MANAGE_PLUGINS_AUTO_START.getAuthority())")
	public Plugin autoStartPlugin(@PathVariable String name) {
		// TODO JIRA CORE-69
		return null;
	}
}
