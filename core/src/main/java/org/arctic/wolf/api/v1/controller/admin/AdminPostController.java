/*
 * Copyright (c) 2019 Reginald Ravenhorst <lordrex34@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.arctic.wolf.api.v1.controller.admin;

import static org.arctic.wolf.model.content.dto.form.PostFormMessages.POST_FORM_FRIENDLY_URL_USED;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.validation.Valid;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.arctic.wolf.api.v1.controller.AbstractController;
import org.arctic.wolf.exception.BadRequestException;
import org.arctic.wolf.exception.ContentNotFoundException;
import org.arctic.wolf.model.content.ContentDB;
import org.arctic.wolf.model.content.ContentService;
import org.arctic.wolf.model.content.ContentType;
import org.arctic.wolf.model.content.ContentVisibility;
import org.arctic.wolf.model.content.dto.Content;
import org.arctic.wolf.model.content.dto.ContentVisibilityModel;
import org.arctic.wolf.model.content.dto.form.PostForm;
import org.arctic.wolf.util.PageUtil;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author lord_rex
 */
@Slf4j
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@RestController
@RequestMapping("api/v1/admin/posts")
public class AdminPostController extends AbstractController {
	private final ContentService contentService;
	
	@GetMapping("content-visibilities")
	@PreAuthorize("hasRole(T(org.arctic.wolf.model.group.Authority).MANAGE_POSTS_LIST.getAuthority())")
	public List<ContentVisibilityModel> getContentVisibilities() {
		return Arrays.stream(ContentVisibility.values())
			.map(ContentVisibilityModel::new)
			.collect(Collectors.toList());
	}
	
	@GetMapping("{lang}/all")
	@PreAuthorize("hasRole(T(org.arctic.wolf.model.group.Authority).MANAGE_POSTS_LIST.getAuthority())")
	public List<Content> getPosts(@PageableDefault(size = 15) Pageable pageable, @PathVariable String lang, HttpServletResponse response) {
		final List<Content> contents = contentService.getContentsDTO(ContentType.POST, lang);
		final Page<Content> pagedView = PageUtil.getPagedView(pageable, contents);
		
		return PageUtil.getPagedContent(response, pagedView);
	}
	
	@GetMapping("{lang}/by-id/{id}")
	@PreAuthorize("hasRole(T(org.arctic.wolf.model.group.Authority).MANAGE_POSTS_LIST.getAuthority())")
	public Content getPost(@PathVariable String lang, @PathVariable long id) {
		return contentService.getContentDTO(ContentType.POST, lang, id)
			.orElseThrow(ContentNotFoundException::new);
	}
	
	@GetMapping("{lang}/by-url/{friendlyURL}")
	@PreAuthorize("hasRole(T(org.arctic.wolf.model.group.Authority).MANAGE_POSTS_LIST.getAuthority())")
	public Content getPost(@PathVariable String lang, @PathVariable String friendlyURL) {
		return contentService.getContentDTO(ContentType.POST, lang, friendlyURL)
			.orElseThrow(ContentNotFoundException::new);
	}
	
	@GetMapping("{lang}/find/{search}")
	@PreAuthorize("hasRole(T(org.arctic.wolf.model.group.Authority).MANAGE_POSTS_FIND.getAuthority())")
	public List<Content> findPosts(@PathVariable String lang, @PathVariable String search, @PageableDefault(size = 15) Pageable pageable, HttpServletResponse response) {
		final Page<Content> pagedView = contentService.pagedFuzzySearchDTO(lang, search, ContentType.POST, pageable);
		
		return PageUtil.getPagedContent(response, pagedView);
	}
	
	@PostMapping({ "write", "add" })
	@PreAuthorize("hasRole(T(org.arctic.wolf.model.group.Authority).MANAGE_POSTS_WRITE_NEW.getAuthority())")
	public Content writePost(@Valid @RequestBody PostForm postForm, BindingResult bindingResult) {
		verifyBindingResult(bindingResult);
		
		final String friendlyURL = postForm.getFriendlyURL();
		if (StringUtils.isNotEmpty(friendlyURL)
			&& contentService.exists(ContentType.POST, postForm.getLang(), friendlyURL)) {
			throw new BadRequestException(POST_FORM_FRIENDLY_URL_USED);
		}
		
		final ContentDB contentDB = contentService.createContent(postForm);
		
		return contentService.getContentDTO(ContentType.POST, contentDB.getLang(), contentDB.getId())
			.orElseThrow(ContentNotFoundException::new);
	}
	
	@DeleteMapping("{id}/delete")
	@PreAuthorize("hasRole(T(org.arctic.wolf.model.group.Authority).MANAGE_POSTS_DELETE.getAuthority())")
	public boolean deletePost(@PathVariable long id) {
		try {
			contentService.delete(id);
		}
		catch (Exception e) {
			throw new BadRequestException(e.getMessage());
		}
		
		return true;
	}
	
	@PutMapping("{lang}/{id}/edit")
	@PreAuthorize("hasRole(T(org.arctic.wolf.model.group.Authority).MANAGE_POSTS_EDIT.getAuthority())")
	public Content editPost(@PathVariable String lang, @PathVariable long id, @Valid @RequestBody PostForm postForm, BindingResult bindingResult) {
		verifyBindingResult(bindingResult);
		
		final ContentDB contentDB = contentService.getContentDB(ContentType.POST, lang, id)
			.orElseThrow(ContentNotFoundException::new);
		
		final String currentFriendlyURL = contentDB.getFriendlyURL();
		final String friendlyURL = postForm.getFriendlyURL();
		if (!StringUtils.equals(currentFriendlyURL, friendlyURL)
			&& StringUtils.isNotEmpty(friendlyURL)
			&& contentService.exists(ContentType.POST, lang, friendlyURL)) {
			throw new BadRequestException(POST_FORM_FRIENDLY_URL_USED);
		}
		
		contentService.updateByForm(contentDB, postForm);
		
		return contentService.getContentDTO(ContentType.POST, contentDB.getLang(), contentDB.getId())
			.orElseThrow(ContentNotFoundException::new);
	}
}
