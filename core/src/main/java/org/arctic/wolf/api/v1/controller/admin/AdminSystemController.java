/*
 * Copyright (c) 2019 Reginald Ravenhorst <lordrex34@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.arctic.wolf.api.v1.controller.admin;

import java.util.List;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.validation.Valid;

import lombok.RequiredArgsConstructor;
import org.arctic.wolf.api.v1.controller.AbstractController;
import org.arctic.wolf.model.server.ServerPropertyRegistry;
import org.arctic.wolf.model.server.ServerSettingsService;
import org.arctic.wolf.model.server.dto.form.ServerPropertiesForm;
import org.arctic.wolf.model.server.dto.form.ServerPropertyForm;
import org.arctic.wolf.model.version.VersionResourceMarker;
import org.arctic.wolf.model.version.VersionUtil;
import org.arctic.wolf.model.version.dto.VersionInfo;
import org.arctic.wolf.util.PageUtil;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author lord_rex
 */
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@RequestMapping("api/v1/admin/system")
@RestController
public class AdminSystemController extends AbstractController {
	private final ServerSettingsService serverSettingsService;
	
	@GetMapping("properties/groups")
	@PreAuthorize("hasRole(T(org.arctic.wolf.model.group.Authority).MANAGE_SYSTEM.getAuthority())")
	public Set<String> getPropertyGroups() {
		return ServerPropertyRegistry.getGroupNames();
	}
	
	@GetMapping("properties/{group}")
	@PreAuthorize("hasRole(T(org.arctic.wolf.model.group.Authority).MANAGE_SYSTEM.getAuthority())")
	public List<ServerPropertyForm> getPropertyGroup(@PageableDefault(size = 25) Pageable pageable, @PathVariable String group, HttpServletResponse response) {
		final Page<ServerPropertyForm> pagedView = PageUtil.getPagedView(pageable, serverSettingsService.getFormByGroupName(group));
		return PageUtil.getPagedContent(response, pagedView);
	}
	
	@PostMapping("save-properties")
	@PreAuthorize("hasRole(T(org.arctic.wolf.model.group.Authority).MANAGE_SYSTEM.getAuthority())")
	public boolean saveProperties(@Valid @RequestBody ServerPropertiesForm serverPropertiesForm, BindingResult bindingResult) {
		verifyBindingResult(bindingResult);
		
		serverSettingsService.updateByForm(serverPropertiesForm);
		
		return true;
	}
	
	@GetMapping("version-info")
	@PreAuthorize("hasRole(T(org.arctic.wolf.model.group.Authority).MANAGE_SYSTEM.getAuthority())")
	public List<VersionInfo> getVersionInfo() {
		return VersionUtil.getVersionInfo(VersionResourceMarker.class);
	}
}
