/*
 * Copyright (c) 2019 Reginald Ravenhorst <lordrex34@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.arctic.wolf.api.v1.controller.admin;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import jakarta.servlet.http.HttpServletResponse;

import lombok.RequiredArgsConstructor;
import org.arctic.wolf.api.v1.controller.AbstractController;
import org.arctic.wolf.exception.ThemeNotFoundException;
import org.arctic.wolf.model.theme.ITheme;
import org.arctic.wolf.model.theme.ThemeService;
import org.arctic.wolf.model.theme.dto.Theme;
import org.arctic.wolf.util.PageUtil;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author lord_rex
 */
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@RequestMapping("api/v1/admin/themes")
@RestController
public class AdminThemeController extends AbstractController {
	private final ThemeService themeService;
	
	@PostMapping({
		"remote/{name}/download",
		"{name}/download"
	})
	@PreAuthorize("hasRole(T(org.arctic.wolf.model.group.Authority).MANAGE_THEMES_INSTALLER.getAuthority())")
	public Theme remoteDownloadTheme(@PathVariable String name) {
		themeService.remoteDownloadTheme(name);
		return themeService.getAvailableComponent(name).orElseThrow(ThemeNotFoundException::new);
	}
	
	@GetMapping("remote/all/info")
	@PreAuthorize("hasRole(T(org.arctic.wolf.model.group.Authority).MANAGE_THEMES_LIST.getAuthority())")
	public Theme[] getRemoteThemeInfos(HttpServletResponse response, @RequestParam(defaultValue = "0") int page) {
		return themeService.getRemoteThemeInfos(response, page);
	}
	
	@GetMapping("remote/{name}/info")
	@PreAuthorize("hasRole(T(org.arctic.wolf.model.group.Authority).MANAGE_THEMES_LIST.getAuthority())")
	public Theme getRemoteThemeInfo(@PathVariable String name) {
		return themeService.getRemoteThemeInfo(name);
	}
	
	@GetMapping("remote/find/{search}")
	@PreAuthorize("hasRole(T(org.arctic.wolf.model.group.Authority).MANAGE_THEMES_LIST.getAuthority())")
	public Theme[] remoteFindTheme(@PathVariable String search, HttpServletResponse response, @RequestParam(defaultValue = "0") int page) {
		return themeService.remoteFindTheme(search, response, page);
	}
	
	@GetMapping(value = "remote/{name}/preview-image", produces = MediaType.IMAGE_JPEG_VALUE)
	@PreAuthorize("hasRole(T(org.arctic.wolf.model.group.Authority).MANAGE_THEMES_LIST.getAuthority())")
	public @ResponseBody byte[] remotePreviewImage(@PathVariable String name) {
		return themeService.remotePreviewImage(name);
	}
	
	@GetMapping(value = "available-themes/{name}/preview-image", produces = MediaType.IMAGE_JPEG_VALUE)
	@PreAuthorize("hasRole(T(org.arctic.wolf.model.group.Authority).MANAGE_THEMES_LIST.getAuthority())")
	public @ResponseBody byte[] previewAvailableImage(@PathVariable String name) {
		return themeService.getPreviewImage(name);
	}
	
	@GetMapping("available-themes/{name}")
	@PreAuthorize("hasRole(T(org.arctic.wolf.model.group.Authority).MANAGE_THEMES_LIST.getAuthority())")
	public Theme getAvailableTheme(@PathVariable String name) {
		return themeService.getAvailableComponent(name)
			.orElseThrow(ThemeNotFoundException::new);
	}
	
	@GetMapping("available-themes")
	@PreAuthorize("hasRole(T(org.arctic.wolf.model.group.Authority).MANAGE_THEMES_LIST.getAuthority())")
	public List<Theme> getAvailableThemes(@PageableDefault(size = 6) Pageable pageable, HttpServletResponse response) {
		final List<Theme> availableComponents = new ArrayList<>(themeService.getAvailableComponents());
		final Page<Theme> pagedView = PageUtil.getPagedView(pageable, availableComponents);
		
		return PageUtil.getPagedContent(response, pagedView);
	}
	
	@GetMapping("installed-themes/{name}")
	@PreAuthorize("hasRole(T(org.arctic.wolf.model.group.Authority).MANAGE_THEMES_LIST.getAuthority())")
	public Theme getInstalledTheme(@PathVariable String name) {
		return themeService.getInstalledTheme(name)
			.orElseThrow(ThemeNotFoundException::new);
	}
	
	@GetMapping("installed-themes")
	@PreAuthorize("hasRole(T(org.arctic.wolf.model.group.Authority).MANAGE_THEMES_LIST.getAuthority())")
	public List<Theme> getInstalledThemes(@PageableDefault(size = 6) Pageable pageable, HttpServletResponse response) {
		final List<Theme> installedThemes = themeService.getInstalledThemes();
		final Page<Theme> pagedView = PageUtil.getPagedView(pageable, installedThemes);
		
		return PageUtil.getPagedContent(response, pagedView);
	}
	
	@PostMapping("{name}/update")
	@PreAuthorize("hasRole(T(org.arctic.wolf.model.group.Authority).MANAGE_THEMES_INSTALLER.getAuthority())")
	public Theme updateTheme(@PathVariable String name) {
		themeService.updateTheme(name);
		
		return themeService.getInstalledTheme(name)
			.orElseThrow(ThemeNotFoundException::new);
	}
	
	@DeleteMapping("{name}/uninstall")
	@PreAuthorize("hasRole(T(org.arctic.wolf.model.group.Authority).MANAGE_THEMES_INSTALLER.getAuthority())")
	public boolean uninstallTheme(@PathVariable String name) {
		themeService.uninstallTheme(name);
		return true;
	}
	
	@PostMapping("{name}/install")
	@PreAuthorize("hasRole(T(org.arctic.wolf.model.group.Authority).MANAGE_THEMES_INSTALLER.getAuthority())")
	public ITheme installTheme(@PathVariable String name) {
		themeService.installTheme(name);
		
		return themeService.getInstalledTheme(name)
			.orElseThrow(ThemeNotFoundException::new);
	}
	
	@DeleteMapping("{name}/delete")
	@PreAuthorize("hasRole(T(org.arctic.wolf.model.group.Authority).MANAGE_THEMES_INSTALLER.getAuthority())")
	public boolean deleteTheme(@PathVariable String name) {
		themeService.deleteTheme(name);
		return true;
	}
	
	@PostMapping("{name}/set")
	@PreAuthorize("hasRole(T(org.arctic.wolf.model.group.Authority).MANAGE_THEMES_SET.getAuthority())")
	public Theme setTheme(@PathVariable String name) {
		themeService.setTheme(name);
		
		return themeService.getInstalledTheme(name)
			.orElseThrow(ThemeNotFoundException::new);
	}
	
	@PostMapping("restore-default-theme")
	@PreAuthorize("hasRole(T(org.arctic.wolf.model.group.Authority).MANAGE_THEMES_SET.getAuthority())")
	public boolean restoreDefaultTheme() {
		themeService.restoreDefaultTheme();
		
		return true;
	}
}
