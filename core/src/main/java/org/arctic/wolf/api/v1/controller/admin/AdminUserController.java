/*
 * Copyright (c) 2019 Reginald Ravenhorst <lordrex34@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.arctic.wolf.api.v1.controller.admin;

import static org.arctic.wolf.model.user.AdminUserManagementMessages.ADMIN_EDIT_USER_EMAIL_ALREADY_USED;
import static org.arctic.wolf.model.user.AdminUserManagementMessages.ADMIN_EDIT_USER_USERNAME_ALREADY_EXIST;

import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.validation.Valid;

import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.arctic.wolf.api.v1.controller.AbstractController;
import org.arctic.wolf.exception.BadRequestException;
import org.arctic.wolf.exception.UserNotFoundException;
import org.arctic.wolf.model.user.UserDB;
import org.arctic.wolf.model.user.UserService;
import org.arctic.wolf.model.user.dto.User;
import org.arctic.wolf.model.user.dto.form.EditUserForm;
import org.arctic.wolf.model.user.dto.form.UserForm;
import org.arctic.wolf.util.PageUtil;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author lord_rex
 */
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@RequestMapping("api/v1/admin/users")
@RestController
public class AdminUserController extends AbstractController {
	private final UserService userService;
	
	@GetMapping("all")
	@PreAuthorize("hasRole(T(org.arctic.wolf.model.group.Authority).MANAGE_USERS_LIST.getAuthority())")
	public List<User> getUsers(@PageableDefault(size = 15) Pageable pageable, HttpServletResponse response) {
		return PageUtil.getPagedContent(response, userService.getUsersDTO(pageable));
	}
	
	@GetMapping("{id}")
	@PreAuthorize("hasRole(T(org.arctic.wolf.model.group.Authority).MANAGE_USERS_LIST.getAuthority())")
	public User getUser(@PathVariable long id) {
		return userService.findByIdDTO(id)
			.orElseThrow(UserNotFoundException::new);
	}
	
	@GetMapping("find/{search}")
	@PreAuthorize("hasRole(T(org.arctic.wolf.model.group.Authority).MANAGE_USERS_FIND.getAuthority())")
	public List<User> findUsers(@PathVariable String search, @PageableDefault(size = 15) Pageable pageable, HttpServletResponse response) {
		final Page<User> pagedView = userService.pagedFuzzySearchDTO(search, pageable);
		
		return PageUtil.getPagedContent(response, pagedView);
	}
	
	@PostMapping("add")
	@PreAuthorize("hasRole(T(org.arctic.wolf.model.group.Authority).MANAGE_USERS_ADD.getAuthority())")
	public User addUser(@Valid @RequestBody UserForm userForm) {
		userService.createByForm(userForm);
		
		return userService.findByUsernameDTO(userForm.getUsername())
			.orElseThrow(UserNotFoundException::new);
	}
	
	@DeleteMapping("{id}/delete")
	@PreAuthorize("hasRole(T(org.arctic.wolf.model.group.Authority).MANAGE_USERS_DELETE.getAuthority())")
	public boolean deleteUser(@PathVariable long id) {
		userService.delete(id);
		return true;
	}
	
	@PutMapping("{id}/edit")
	@PreAuthorize("hasRole(T(org.arctic.wolf.model.group.Authority).MANAGE_USERS_EDIT.getAuthority())")
	public User editUser(@PathVariable long id, @Valid @RequestBody EditUserForm editUserForm) {
		final UserDB userDB = userService.findByIdDB(id)
			.orElseThrow(UserNotFoundException::new);
		
		final String username = editUserForm.getUsername();
		if (!userDB.getUsername().equals(username) && StringUtils.isNotEmpty(username)) {
			final Optional<UserDB> byUsername = userService.findByUsernameDB(username);
			if (byUsername.isPresent()) {
				throw new BadRequestException(ADMIN_EDIT_USER_USERNAME_ALREADY_EXIST);
			}
		}
		
		final String email = editUserForm.getEmail();
		if (!userDB.getEmail().equals(email) && StringUtils.isNotEmpty(email)) {
			final Optional<UserDB> byEmail = userService.findByEmailDB(email);
			if (byEmail.isPresent()) {
				throw new BadRequestException(ADMIN_EDIT_USER_EMAIL_ALREADY_USED);
			}
		}
		
		userService.updateByForm(userDB, editUserForm);
		
		return userService.findByIdDTO(id)
			.orElseThrow(UserNotFoundException::new);
	}
}
