/*
 * Copyright (c) 2019 Reginald Ravenhorst <lordrex34@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.arctic.wolf.api.v1.controller.admin;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.validation.Valid;

import lombok.RequiredArgsConstructor;
import org.arctic.wolf.api.v1.controller.AbstractController;
import org.arctic.wolf.exception.BadRequestException;
import org.arctic.wolf.exception.WidgetNotFoundException;
import org.arctic.wolf.model.content.ContentVisibility;
import org.arctic.wolf.model.content.dto.ContentVisibilityModel;
import org.arctic.wolf.model.widget.WidgetDB;
import org.arctic.wolf.model.widget.WidgetService;
import org.arctic.wolf.model.widget.dto.Widget;
import org.arctic.wolf.model.widget.dto.form.EditWidgetForm;
import org.arctic.wolf.model.widget.dto.form.WidgetForm;
import org.arctic.wolf.util.PageUtil;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author lord_rex
 */
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@RequestMapping("api/v1/admin/widgets")
@RestController
public class AdminWidgetController extends AbstractController {
	private final WidgetService widgetService;
	
	@GetMapping("content-visibilities")
	@PreAuthorize("hasRole(T(org.arctic.wolf.model.group.Authority).MANAGE_WIDGETS_LIST.getAuthority())")
	public List<ContentVisibilityModel> getContentVisibilities() {
		return Arrays.stream(ContentVisibility.values())
			.map(ContentVisibilityModel::new)
			.collect(Collectors.toList());
	}
	
	@GetMapping("{lang}/all")
	@PreAuthorize("hasRole(T(org.arctic.wolf.model.group.Authority).MANAGE_WIDGETS_LIST.getAuthority())")
	public List<Widget> getWidgets(@PageableDefault(size = 15) Pageable pageable, @PathVariable String lang, HttpServletResponse response) {
		final Page<Widget> pagedView = PageUtil.getPagedView(pageable, widgetService.getWidgetsDTO(lang));
		return PageUtil.getPagedContent(response, pagedView);
	}
	
	@GetMapping("{lang}/{id}")
	@PreAuthorize("hasRole(T(org.arctic.wolf.model.group.Authority).MANAGE_WIDGETS_LIST.getAuthority())")
	public Widget getWidget(@PathVariable String lang, @PathVariable String id) {
		return widgetService.getWidgetDTO(lang, id)
			.orElseThrow(WidgetNotFoundException::new);
	}
	
	@PostMapping({ "create", "add" })
	@PreAuthorize("hasRole(T(org.arctic.wolf.model.group.Authority).MANAGE_WIDGETS_ADD_NEW.getAuthority())")
	public Widget createWidget(@Valid @RequestBody WidgetForm widgetForm) {
		final WidgetDB widgetDB = widgetService.createByForm(widgetForm);
		
		return widgetService.getWidgetDTO(widgetDB.getLang(), widgetDB.getId())
			.orElseThrow(WidgetNotFoundException::new);
	}
	
	@DeleteMapping("{id}/delete")
	@PreAuthorize("hasRole(T(org.arctic.wolf.model.group.Authority).MANAGE_WIDGETS_DELETE.getAuthority())")
	public boolean deleteWidget(@PathVariable String id) {
		try {
			widgetService.delete(id);
		}
		catch (Exception e) {
			throw new BadRequestException(e.getMessage());
		}
		
		return true;
	}
	
	@PutMapping("{lang}/{id}/edit")
	@PreAuthorize("hasRole(T(org.arctic.wolf.model.group.Authority).MANAGE_WIDGETS_EDIT.getAuthority())")
	public Widget editWidget(@PathVariable String lang, @PathVariable String id, @Valid @RequestBody EditWidgetForm editWidgetForm) {
		final WidgetDB widgetDB = widgetService.getWidgetDB(lang, id)
			.orElseThrow(WidgetNotFoundException::new);
		
		widgetService.updateByForm(widgetDB, editWidgetForm);
		
		return widgetService.getWidgetDTO(widgetDB.getLang(), widgetDB.getId())
			.orElseThrow(WidgetNotFoundException::new);
	}
}
