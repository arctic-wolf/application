/*
 * Copyright (c) 2018 Reginald Ravenhorst <lordrex34@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.arctic.wolf.config;

import static org.arctic.wolf.model.server.ServerSettingsVariables.*;

import java.util.Properties;
import org.springframework.beans.factory.annotation.Autowired;

import org.arctic.wolf.model.server.ServerSettingsService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;

/**
 * @author lord_rex
 */
@Configuration
public class MailConfig {
	private static final String MAIL_TRANSPORT_PROTOCOL = "mail.transport.protocol";
	private static final String MAIL_SMTPS_AUTH = "mail.smtps.auth";
	private static final String MAIL_SMTPS_STARTTLS_ENABLE = "mail.smtps.starttls.enable";
	private static final String MAIL_SMTPS_TIMEOUT = "mail.smtps.timeout";
	
	private final ServerSettingsService serverSettingsService;
	
	@Autowired
	public MailConfig(ServerSettingsService serverSettingsService) {
		this.serverSettingsService = serverSettingsService;
	}
	
	@Bean
	public JavaMailSender javaMailSender() {
		final JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
		mailSender.setHost(serverSettingsService.getString(SMTP_HOST, DEFAULT_SMTP_HOST));
		mailSender.setPort(serverSettingsService.getInteger(SMTP_PORT, DEFAULT_SMTP_PORT));
		mailSender.setUsername(serverSettingsService.getString(SMTP_USERNAME, DEFAULT_SMTP_USERNAME));
		mailSender.setPassword(serverSettingsService.getString(SMTP_PASSWORD, DEFAULT_SMTP_PASSWORD));
		mailSender.setProtocol(serverSettingsService.getString(SMTP_PROTOCOL, DEFAULT_SMTP_PROTOCOL));
		
		final Properties properties = mailSender.getJavaMailProperties();
		properties.put(MAIL_TRANSPORT_PROTOCOL, serverSettingsService.getString(SMTP_MAIL_TRANSPORT_PROTOCOL, DEFAULT_SMTP_MAIL_TRANSPORT_PROTOCOL));
		properties.put(MAIL_SMTPS_AUTH, serverSettingsService.getBoolean(SMTP_MAIL_SMTPS_AUTH, DEFAULT_SMTP_MAIL_SMTPS_AUTH));
		properties.put(MAIL_SMTPS_STARTTLS_ENABLE, serverSettingsService.getBoolean(SMTP_MAIL_SMTPS_STARTTLS_ENABLE, DEFAULT_SMTP_MAIL_SMTPS_STARTTLS_ENABLE));
		properties.put(MAIL_SMTPS_TIMEOUT, serverSettingsService.getInteger(SMTP_MAIL_SMTPS_TIMEOUT, DEFAULT_SMTP_MAIL_SMTPS_TIMEOUT));
		
		return mailSender;
	}
}
