/*
 * Copyright (c) 2017 Reginald Ravenhorst <lordrex34@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.arctic.wolf.config;

import static org.arctic.wolf.model.server.ServerSettingsVariables.DEFAULT_RECAPTCHA_ALLOWED;
import static org.arctic.wolf.model.server.ServerSettingsVariables.DEFAULT_RECAPTCHA_SECRET;
import static org.arctic.wolf.model.server.ServerSettingsVariables.DEFAULT_RECAPTCHA_SITE_KEY;
import static org.arctic.wolf.model.server.ServerSettingsVariables.RECAPTCHA_ALLOWED;
import static org.arctic.wolf.model.server.ServerSettingsVariables.RECAPTCHA_SECRET;
import static org.arctic.wolf.model.server.ServerSettingsVariables.RECAPTCHA_SITE_KEY;

import org.springframework.beans.factory.annotation.Autowired;

import lombok.RequiredArgsConstructor;
import org.arctic.wolf.model.server.ServerSettingsService;
import org.arctic.wolf.recaptcha.ReCaptcha;
import org.arctic.wolf.recaptcha.ReCaptchaImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author lord_rex
 */
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@Configuration
public class ReCaptchaConfiguration {
	private final ServerSettingsService serverSettingsService;
	
	@Bean
	public ReCaptcha reCaptcha() {
		return serverSettingsService.getBoolean(RECAPTCHA_ALLOWED, DEFAULT_RECAPTCHA_ALLOWED) ?
			new ReCaptchaImpl(
				serverSettingsService.getString(RECAPTCHA_SITE_KEY, DEFAULT_RECAPTCHA_SITE_KEY),
				serverSettingsService.getString(RECAPTCHA_SECRET, DEFAULT_RECAPTCHA_SECRET))
			: null;
	}
}
