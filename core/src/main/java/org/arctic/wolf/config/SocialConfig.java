/*
 * Copyright (c) 2019 Reginald Ravenhorst <lordrex34@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.arctic.wolf.config;

import static org.arctic.wolf.model.server.ServerSettingsVariables.DEFAULT_FACEBOOK_APP_ID;
import static org.arctic.wolf.model.server.ServerSettingsVariables.DEFAULT_FACEBOOK_APP_SECRET;
import static org.arctic.wolf.model.server.ServerSettingsVariables.DEFAULT_GOOGLE_APP_ID;
import static org.arctic.wolf.model.server.ServerSettingsVariables.DEFAULT_GOOGLE_APP_SECRET;
import static org.arctic.wolf.model.server.ServerSettingsVariables.FACEBOOK_APP_ID;
import static org.arctic.wolf.model.server.ServerSettingsVariables.FACEBOOK_APP_SECRET;
import static org.arctic.wolf.model.server.ServerSettingsVariables.GOOGLE_APP_ID;
import static org.arctic.wolf.model.server.ServerSettingsVariables.GOOGLE_APP_SECRET;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;

import lombok.RequiredArgsConstructor;
import org.arctic.wolf.model.server.ServerSettingsService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.oauth2.client.CommonOAuth2Provider;
import org.springframework.security.oauth2.client.InMemoryOAuth2AuthorizedClientService;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClientService;
import org.springframework.security.oauth2.client.registration.ClientRegistration;
import org.springframework.security.oauth2.client.registration.ClientRegistrationRepository;
import org.springframework.security.oauth2.client.registration.InMemoryClientRegistrationRepository;

/**
 * @author lord_rex
 */
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@Configuration
public class SocialConfig {
	public static final String FACEBOOK = "facebook";
	public static final String GOOGLE = "google";
	
	private final ServerSettingsService serverSettingsService;
	
	@Bean
	public ClientRegistrationRepository clientRegistrationRepository() {
		final List<ClientRegistration> registrationList = new ArrayList<>();
		
		registrationList.add(
			CommonOAuth2Provider.FACEBOOK.getBuilder(FACEBOOK)
				.clientId(serverSettingsService.getString(FACEBOOK_APP_ID, DEFAULT_FACEBOOK_APP_ID))
				.clientSecret(serverSettingsService.getString(FACEBOOK_APP_SECRET, DEFAULT_FACEBOOK_APP_SECRET))
				.redirectUri("{baseUrl}/oauth2/callback/{registrationId}")
				.scope("public_profile", "email")
				.authorizationUri("https://www.facebook.com/v3.0/dialog/oauth")
				.tokenUri("https://graph.facebook.com/v3.0/oauth/access_token")
				.userInfoUri("https://graph.facebook.com/v3.0/me?fields=id,first_name,middle_name,last_name,name,email,verified,is_verified,picture.width(250).height(250)")
				.build());
		
		registrationList.add(
			CommonOAuth2Provider.GOOGLE.getBuilder(GOOGLE)
				.clientId(serverSettingsService.getString(GOOGLE_APP_ID, DEFAULT_GOOGLE_APP_ID))
				.clientSecret(serverSettingsService.getString(GOOGLE_APP_SECRET, DEFAULT_GOOGLE_APP_SECRET))
				.redirectUri("{baseUrl}/oauth2/callback/{registrationId}")
				.scope("profile", "email")
				.build());
		
		return new InMemoryClientRegistrationRepository(registrationList);
	}
	
	@Bean
	public OAuth2AuthorizedClientService authorizedClientService() {
		return new InMemoryOAuth2AuthorizedClientService(clientRegistrationRepository());
	}
}
