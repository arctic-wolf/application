/*
 * Copyright (c) 2018 Reginald Ravenhorst <lordrex34@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.arctic.wolf.event;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.PriorityBlockingQueue;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.arctic.wolf.event.annotations.Priority;
import org.arctic.wolf.event.annotations.RegisterEvent;
import org.arctic.wolf.event.listeners.AbstractEventListener;
import org.arctic.wolf.event.listeners.AnnotationEventListener;

/**
 * @author UnAfraid
 */
@Slf4j
public abstract class AbstractScript {
	@Getter
	private final Queue<AbstractEventListener> listeners = new PriorityBlockingQueue<>();
	
	public AbstractScript() {
		init();
	}
	
	protected void init() {
		initializeAnnotationListeners();
		onLoad();
	}
	
	protected void initializeAnnotationListeners() {
		Class<?> parent = getClass();
		do {
			registerAnnotationFromMethods(parent);
			parent = parent.getSuperclass();
		}
		while (parent != null);
	}
	
	protected void registerAnnotationFromMethods(Class<?> clazz) {
		final List<Integer> ids = new ArrayList<>();
		for (Method method : clazz.getDeclaredMethods()) {
			if (method.isAnnotationPresent(RegisterEvent.class)) {
				final EventType eventType = EventTypeCache.get(method.getParameterTypes()[0]);
				checkEventType(eventType, method);
				
				final int priority = method.isAnnotationPresent(Priority.class) ? method.getAnnotation(Priority.class).value() : 0;
				final RegisterEvent registerEvent = method.getAnnotation(RegisterEvent.class);
				
				if (registerEvent != null) {
					final AnnotationEventListener listener = new AnnotationEventListener(Containers.GLOBAL, eventType, method, this, priority);
					listener.getContainer().addListener(listener);
					listeners.add(listener);
				}
			}
		}
	}
	
	private void checkEventType(EventType eventType, Method method) {
		final String className = getClass().getName();
		final String methodName = method.getName();
		
		if (eventType == null) {
			throw new NullPointerException("EventType is null on class '" + className + "', on method: " + methodName + ", please fix it!");
		}
		
		// Mistakes like that should fail the loading of the entire script,
		// as if a restriction is being ignored of loading it may provide users unwanted benefits or render site features useless.
		if (method.getParameterCount() != 1) {
			throw new IllegalArgumentException("Non-properly defined annotation listener on class '" + className + "', on method: " + methodName + ", expected parameter count is 1 but found: " + method.getParameterCount());
		}
		else if (!eventType.isEventClass(method.getParameterTypes()[0])) {
			throw new IllegalArgumentException("Non-properly defined annotation listener on class '" + className + "', on method: " + methodName + ", expected parameter to be type of: " + eventType.getEventClass().getSimpleName() + " but found: " + method.getParameterTypes()[0].getSimpleName());
		}
		else if (!eventType.isReturnClass(method.getReturnType())) {
			throw new IllegalArgumentException("Non-properly defined annotation listener on class '" + className + "', on method: " + methodName + ", expected return type to be one of: " + Arrays.toString(eventType.getReturnClasses()) + " but found: " + method.getReturnType().getSimpleName());
		}
	}
	
	public void unLoad() {
		listeners.forEach(AbstractEventListener::unregisterMe);
		listeners.clear();
		
		onUnLoad();
	}
	
	protected abstract void onLoad();
	
	protected abstract void onUnLoad();
}
