/*
 * Copyright (c) 2018 Reginald Ravenhorst <lordrex34@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.arctic.wolf.event;

import java.util.Objects;
import java.util.Queue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import lombok.extern.slf4j.Slf4j;
import org.arctic.wolf.event.listeners.AbstractEventListener;
import org.arctic.wolf.event.returns.AbstractEventReturn;
import org.springframework.stereotype.Service;

/**
 * @author UnAfraid
 */
@Slf4j
@Service
public class EventService {
	private static final ExecutorService EVENT_EXECUTORS = Executors.newCachedThreadPool();
	private static final ScheduledExecutorService EVENT_SCHEDULERS = Executors.newScheduledThreadPool(1);
	
	public <T extends AbstractEventReturn> T notifyEvent(IBaseEvent event) {
		return notifyEvent(event, null, null);
	}
	
	public <T extends AbstractEventReturn> T notifyEvent(IBaseEvent event, Class<T> callbackClass) {
		return notifyEvent(event, null, callbackClass);
	}
	
	public <T extends AbstractEventReturn> T notifyEvent(IBaseEvent event, ListenersContainer container) {
		return notifyEvent(event, container, null);
	}
	
	public <T extends AbstractEventReturn> T notifyEvent(IBaseEvent event, ListenersContainer container, Class<T> callbackClass) {
		try {
			return Containers.GLOBAL.hasListener(event.getType()) || ((container != null) && container.hasListener(event.getType())) ? notifyEventImpl(event, callbackClass, container) : null;
		}
		catch (Exception e) {
			LOGGER.warn("Couldn't notify event " + event.getClass().getSimpleName(), e);
		}
		return null;
	}
	
	public void notifyEventAsync(IBaseEvent event, ListenersContainer... containers) {
		if (event == null) {
			throw new NullPointerException("Event cannot be null!");
		}
		
		boolean hasListeners = Containers.GLOBAL.hasListener(event.getType());
		if (!hasListeners) {
			for (ListenersContainer container : containers) {
				if (container.hasListener(event.getType())) {
					hasListeners = true;
					break;
				}
			}
		}
		
		if (hasListeners) {
			EVENT_EXECUTORS.execute(() -> notifyEventImpl(event, null, containers));
		}
	}
	
	public void notifyEventAsyncDelayed(IBaseEvent event, ListenersContainer container, long delay) {
		if (Containers.GLOBAL.hasListener(event.getType()) || container.hasListener(event.getType())) {
			EVENT_SCHEDULERS.schedule(() -> notifyEvent(event, container, null), delay, TimeUnit.MILLISECONDS);
		}
	}
	
	public void notifyEventAsyncDelayed(IBaseEvent event, ListenersContainer container, long delay, TimeUnit unit) {
		if (Containers.GLOBAL.hasListener(event.getType()) || container.hasListener(event.getType())) {
			EVENT_SCHEDULERS.schedule(() -> notifyEvent(event, container, null), delay, unit);
		}
	}
	
	private <T extends AbstractEventReturn> T notifyEventImpl(IBaseEvent event, Class<T> callbackClass, ListenersContainer... containers) {
		Objects.requireNonNull(event, "Event cannot be null!");
		
		try {
			T callback = null;
			if (containers != null) {
				// Local listeners container first.
				for (ListenersContainer container : containers) {
					if (container != null) {
						if ((callback == null) || !callback.abort()) {
							callback = notifyToListeners(container.getListeners(event.getType()), event, callbackClass, callback);
						}
					}
				}
			}
			
			// Global listener container.
			if ((callback == null) || !callback.abort()) {
				callback = notifyToListeners(Containers.GLOBAL.getListeners(event.getType()), event, callbackClass, callback);
			}
			
			return callback;
		}
		catch (Exception e) {
			LOGGER.warn("Couldn't notify event " + event.getClass().getSimpleName(), e);
			throw e;
		}
		finally {
			event.onFinally();
		}
	}
	
	private <T extends AbstractEventReturn> T notifyToListeners(Queue<AbstractEventListener> listeners, IBaseEvent event, Class<T> returnBackClass, T callback) {
		for (AbstractEventListener listener : listeners) {
			try {
				final T rb = listener.executeEvent(event, returnBackClass);
				if (rb == null) {
					continue;
				}
				else if ((callback == null) || rb.override()) // Let's check if this listener wants to override previous return object or we simply don't have one
				{
					callback = rb;
				}
				else if (rb.abort()) // This listener wants to abort the notification to others.
				{
					break;
				}
			}
			catch (Exception e) {
				LOGGER.warn("Exception during notification of event: " + event.getClass().getSimpleName() + " listener: " + listener.getClass().getSimpleName(), e);
			}
		}
		
		return callback;
	}
}
