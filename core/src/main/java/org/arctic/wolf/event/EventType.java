/*
 * Copyright (c) 2018 Reginald Ravenhorst <lordrex34@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.arctic.wolf.event;

import org.apache.commons.lang3.ArrayUtils;
import org.arctic.wolf.event.impl.OnAddBasicAttributes;
import org.arctic.wolf.event.impl.OnAdminPageGet;
import org.arctic.wolf.event.impl.OnAdminPagePost;
import org.arctic.wolf.event.impl.OnHomeGet;
import org.arctic.wolf.event.impl.OnHomePost;
import org.arctic.wolf.event.impl.OnLoginGet;
import org.arctic.wolf.event.impl.OnPageGet;
import org.arctic.wolf.event.impl.OnPagePost;
import org.arctic.wolf.event.impl.OnPostGet;
import org.arctic.wolf.event.impl.OnPostPost;
import org.arctic.wolf.event.impl.OnProfileGet;
import org.arctic.wolf.event.impl.OnProfilePost;
import org.arctic.wolf.event.impl.OnRegisterConfirmGet;
import org.arctic.wolf.event.impl.OnRegisterGet;
import org.arctic.wolf.event.impl.OnRegisterPost;
import org.arctic.wolf.event.impl.OnUserSettingsGet;
import org.arctic.wolf.event.impl.OnUserSettingsPost;

/**
 * @author UnAfraid
 */
public enum EventType {
	LOGIN_GET(OnLoginGet.class, void.class),
	REGISTER_GET(OnRegisterGet.class, void.class),
	REGISTER_POST(OnRegisterPost.class, void.class),
	REGISTER_CONFIRM_GET(OnRegisterConfirmGet.class, void.class),
	PROFILE_GET(OnProfileGet.class, void.class),
	PROFILE_POST(OnProfilePost.class, void.class),
	USER_SETTINGS_GET(OnUserSettingsGet.class, void.class),
	USER_SETTINGS_POST(OnUserSettingsPost.class, void.class),
	HOME_GET(OnHomeGet.class, void.class),
	HOME_POST(OnHomePost.class, void.class),
	PAGE_GET(OnPageGet.class, void.class),
	PAGE_POST(OnPagePost.class, void.class),
	ADMIN_PAGE_GET(OnAdminPageGet.class, void.class),
	ADMIN_PAGE_POST(OnAdminPagePost.class, void.class),
	POST_GET(OnPostGet.class, void.class),
	POST_POST(OnPostPost.class, void.class),
	ON_ADD_BASIC_ATTRIBUTES(OnAddBasicAttributes.class, void.class)
	
	// ...
	;
	
	private final Class<? extends IBaseEvent> eventClass;
	private final Class<?>[] returnClasses;
	
	EventType(Class<? extends IBaseEvent> eventClass, Class<?>... returnClasses) {
		this.eventClass = eventClass;
		this.returnClasses = returnClasses;
	}
	
	public Class<? extends IBaseEvent> getEventClass() {
		return eventClass;
	}
	
	public Class<?>[] getReturnClasses() {
		return returnClasses;
	}
	
	public boolean isEventClass(Class<?> clazz) {
		return eventClass == clazz;
	}
	
	public boolean isReturnClass(Class<?> clazz) {
		return ArrayUtils.contains(returnClasses, clazz);
	}
}
