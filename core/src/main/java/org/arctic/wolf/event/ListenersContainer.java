/*
 * Copyright (c) 2018 Reginald Ravenhorst <lordrex34@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.arctic.wolf.event;

import java.util.Map;
import java.util.Queue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.function.Predicate;

import org.arctic.wolf.event.listeners.AbstractEventListener;
import org.arctic.wolf.util.EmptyQueue;

/**
 * @author UnAfraid
 */
public class ListenersContainer {
	private volatile Map<EventType, Queue<AbstractEventListener>> listeners = null;
	
	public AbstractEventListener addListener(AbstractEventListener listener) {
		if ((listener == null)) {
			throw new NullPointerException("Listener cannot be null!");
		}
		getListeners().computeIfAbsent(listener.getType(), k -> new PriorityBlockingQueue<>()).add(listener);
		return listener;
	}
	
	public AbstractEventListener removeListener(AbstractEventListener listener) {
		if ((listener == null)) {
			throw new NullPointerException("Listener cannot be null!");
		}
		else if (listeners == null) {
			throw new NullPointerException("Listeners container is not initialized!");
		}
		else if (!listeners.containsKey(listener.getType())) {
			throw new IllegalAccessError("Listeners container doesn't had " + listener.getType() + " event type added!");
		}
		
		listeners.get(listener.getType()).remove(listener);
		return listener;
	}
	
	public Queue<AbstractEventListener> getListeners(EventType type) {
		return (listeners != null) && listeners.containsKey(type) ? listeners.get(type) : EmptyQueue.emptyQueue();
	}
	
	public void removeListenerIf(EventType type, Predicate<? super AbstractEventListener> filter) {
		getListeners(type).stream().filter(filter).forEach(AbstractEventListener::unregisterMe);
	}
	
	public void removeListenerIf(Predicate<? super AbstractEventListener> filter) {
		if (listeners != null) {
			getListeners().values().forEach(queue -> queue.stream().filter(filter).forEach(AbstractEventListener::unregisterMe));
		}
	}
	
	public boolean hasListener(EventType type) {
		return !getListeners(type).isEmpty();
	}
	
	private Map<EventType, Queue<AbstractEventListener>> getListeners() {
		if (listeners == null) {
			synchronized (this) {
				if (listeners == null) {
					listeners = new ConcurrentHashMap<>();
				}
			}
		}
		return listeners;
	}
}
