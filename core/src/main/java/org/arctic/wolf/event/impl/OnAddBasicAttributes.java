/*
 * Copyright (c) 2018 Reginald Ravenhorst <lordrex34@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.arctic.wolf.event.impl;

import java.security.Principal;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.arctic.wolf.event.EventType;
import org.arctic.wolf.event.IBaseEvent;
import org.springframework.ui.Model;

/**
 * @author lord_rex
 */
@RequiredArgsConstructor
public final class OnAddBasicAttributes implements IBaseEvent {
	@Getter
	private final Principal principal;
	
	@Getter
	private final Model model;
	
	@Getter
	private final boolean isAdminView;
	
	@Override
	public EventType getType() {
		return EventType.ON_ADD_BASIC_ATTRIBUTES;
	}
	
	public void addProperty(String attributeName, Object attributeValue) {
		if (model != null) {
			model.addAttribute(attributeName, attributeValue);
		}
	}
}
