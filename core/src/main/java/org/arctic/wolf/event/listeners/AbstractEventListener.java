/*
 * Copyright (c) 2018 Reginald Ravenhorst <lordrex34@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.arctic.wolf.event.listeners;

import org.arctic.wolf.event.EventType;
import org.arctic.wolf.event.IBaseEvent;
import org.arctic.wolf.event.ListenersContainer;
import org.arctic.wolf.event.returns.AbstractEventReturn;

/**
 * @author UnAfraid
 */
public abstract class AbstractEventListener implements Comparable<AbstractEventListener> {
	private int priority = 0;
	private final ListenersContainer container;
	private final EventType type;
	private final Object owner;
	
	public AbstractEventListener(ListenersContainer container, EventType type, Object owner) {
		this.container = container;
		this.type = type;
		this.owner = owner;
	}
	
	public ListenersContainer getContainer() {
		return container;
	}
	
	public EventType getType() {
		return type;
	}
	
	public Object getOwner() {
		return owner;
	}
	
	public int getPriority() {
		return priority;
	}
	
	public void setPriority(int priority) {
		this.priority = priority;
	}
	
	public abstract <R extends AbstractEventReturn> R executeEvent(IBaseEvent event, Class<R> returnBackClass);
	
	public void unregisterMe() {
		getContainer().removeListener(this);
	}
	
	@Override
	public int compareTo(AbstractEventListener o) {
		return Integer.compare(o.getPriority(), getPriority());
	}
}
