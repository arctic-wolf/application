/*
 * Copyright (c) 2018 Reginald Ravenhorst <lordrex34@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.arctic.wolf.event.listeners;

import java.lang.reflect.Method;

import lombok.extern.slf4j.Slf4j;
import org.arctic.wolf.event.EventType;
import org.arctic.wolf.event.IBaseEvent;
import org.arctic.wolf.event.ListenersContainer;
import org.arctic.wolf.event.returns.AbstractEventReturn;

/**
 * Annotation event listener provides dynamically attached callback to any method operation with or without any return object.
 * @author UnAfraid
 */
@Slf4j
public class AnnotationEventListener extends AbstractEventListener {
	private final Method callback;
	
	public AnnotationEventListener(ListenersContainer container, EventType type, Method callback, Object owner, int priority) {
		super(container, type, owner);
		this.callback = callback;
		setPriority(priority);
	}
	
	@Override
	public <R extends AbstractEventReturn> R executeEvent(IBaseEvent event, Class<R> returnBackClass) {
		final Method method = callback;
		try {
			if (!method.canAccess(getOwner())) {
				method.setAccessible(true);
			}
			
			final Object result = method.invoke(getOwner(), event);
			if (method.getReturnType() == returnBackClass) {
				return returnBackClass.cast(result);
			}
		}
		catch (Exception e) {
			LOGGER.warn("Error while invoking {} on {}", method.getName(), getOwner(), e);
		}
		return null;
	}
}
