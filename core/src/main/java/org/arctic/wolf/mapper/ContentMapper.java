package org.arctic.wolf.mapper;

import org.arctic.wolf.model.content.ContentDB;
import org.arctic.wolf.model.content.dto.Content;
import org.arctic.wolf.model.content.dto.form.IContentForm;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface ContentMapper {
	
	ContentMapper INSTANCE = Mappers.getMapper(ContentMapper.class);
	
	Content toDTO(ContentDB contentDB);
	
	Content toContent(Content content);
	
	ContentDB toDB(IContentForm contentForm);
	
}
