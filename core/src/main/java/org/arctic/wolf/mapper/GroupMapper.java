package org.arctic.wolf.mapper;

import org.arctic.wolf.model.group.GroupDB;
import org.arctic.wolf.model.group.dto.Group;
import org.arctic.wolf.model.group.dto.form.IGroupForm;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface GroupMapper {
	
	GroupMapper INSTANCE = Mappers.getMapper(GroupMapper.class);
	
	Group toDTO(GroupDB groupDB);
	
	Group toGroup(Group group);
	
	GroupDB toDB(IGroupForm groupForm);
	
}
