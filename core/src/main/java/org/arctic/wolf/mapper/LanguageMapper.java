package org.arctic.wolf.mapper;

import org.arctic.wolf.model.message.LanguageDB;
import org.arctic.wolf.model.message.dto.Language;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface LanguageMapper {
	
	LanguageMapper INSTANCE = Mappers.getMapper(LanguageMapper.class);
	
	Language toDTO(LanguageDB languageDB);
	
	Language toLanguage(Language language);
	
	//	LanguageDB toDB(ILanguageForm languageForm);
	
}
