package org.arctic.wolf.mapper;

import org.arctic.wolf.model.message.MessageDB;
import org.arctic.wolf.model.message.dto.MessageModel;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface MessageMapper {
	
	MessageMapper INSTANCE = Mappers.getMapper(MessageMapper.class);
	
	MessageModel toDTO(MessageDB languageDB);
	
	MessageModel toMessage(MessageModel language);
	
	//	MessageDB toDB(IMessageForm languageForm);
	
}
