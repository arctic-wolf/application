package org.arctic.wolf.mapper;

import org.arctic.wolf.model.notification.NotificationDB;
import org.arctic.wolf.model.notification.dto.Notification;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface NotificationMapper {
	
	NotificationMapper INSTANCE = Mappers.getMapper(NotificationMapper.class);
	
	Notification toDTO(NotificationDB notificationDB);
	
	Notification toNotification(Notification notification);
	
	//	NotificationDB toDB(INotificationForm notificationForm);
	
}
