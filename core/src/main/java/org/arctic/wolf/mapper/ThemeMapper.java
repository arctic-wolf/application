package org.arctic.wolf.mapper;

import org.arctic.wolf.model.theme.ThemeDB;
import org.arctic.wolf.model.theme.dto.Theme;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface ThemeMapper {
	
	ThemeMapper INSTANCE = Mappers.getMapper(ThemeMapper.class);
	
	Theme toDTO(ThemeDB themeDB);
	
	Theme toTheme(Theme theme);
	
	ThemeDB toDB(Theme theme);
	
}
