package org.arctic.wolf.mapper;

import org.arctic.wolf.model.user.UserDB;
import org.arctic.wolf.model.user.dto.User;
import org.arctic.wolf.model.user.dto.form.IUserForm;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface UserMapper {
	
	UserMapper INSTANCE = Mappers.getMapper(UserMapper.class);
	
	User toDTO(UserDB userDB);
	
	User toUser(User user);
	
	UserDB toDB(IUserForm userForm);
	
}
