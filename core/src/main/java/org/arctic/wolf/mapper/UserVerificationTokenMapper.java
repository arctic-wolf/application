package org.arctic.wolf.mapper;

import org.arctic.wolf.model.user.verification.UserVerificationTokenDB;
import org.arctic.wolf.model.user.verification.dto.UserVerificationToken;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface UserVerificationTokenMapper {
	
	UserVerificationTokenMapper INSTANCE = Mappers.getMapper(UserVerificationTokenMapper.class);
	
	UserVerificationToken toDTO(UserVerificationTokenDB userVerificationTokenDB);
	
	UserVerificationToken toUserVerificationToken(UserVerificationToken userVerificationToken);
	
}
