package org.arctic.wolf.mapper;

import org.arctic.wolf.model.widget.WidgetDB;
import org.arctic.wolf.model.widget.dto.Widget;
import org.arctic.wolf.model.widget.dto.form.IWidgetForm;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface WidgetMapper {
	
	WidgetMapper INSTANCE = Mappers.getMapper(WidgetMapper.class);
	
	Widget toDTO(WidgetDB widgetDB);
	
	Widget toWidget(Widget widget);
	
	WidgetDB toDB(IWidgetForm widgetForm);
	
}
