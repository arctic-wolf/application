/*
 * Copyright (c) 2019 Reginald Ravenhorst <lordrex34@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.arctic.wolf.model.admin;

import static org.arctic.wolf.model.group.Authority.MANAGE_GROUPS_ADD;
import static org.arctic.wolf.model.group.Authority.MANAGE_GROUPS_DELETE;
import static org.arctic.wolf.model.group.Authority.MANAGE_GROUPS_EDIT;
import static org.arctic.wolf.model.group.Authority.MANAGE_GROUPS_FIND;
import static org.arctic.wolf.model.group.Authority.MANAGE_GROUPS_LIST;
import static org.arctic.wolf.model.group.Authority.MANAGE_LANGUAGES_ADD;
import static org.arctic.wolf.model.group.Authority.MANAGE_LANGUAGES_DELETE;
import static org.arctic.wolf.model.group.Authority.MANAGE_LANGUAGES_LIST;
import static org.arctic.wolf.model.group.Authority.MANAGE_MESSAGES;
import static org.arctic.wolf.model.group.Authority.MANAGE_MESSAGES_ADD;
import static org.arctic.wolf.model.group.Authority.MANAGE_MESSAGES_DELETE;
import static org.arctic.wolf.model.group.Authority.MANAGE_MESSAGES_EDIT;
import static org.arctic.wolf.model.group.Authority.MANAGE_PAGES_CREATE_NEW;
import static org.arctic.wolf.model.group.Authority.MANAGE_PAGES_DELETE;
import static org.arctic.wolf.model.group.Authority.MANAGE_PAGES_EDIT;
import static org.arctic.wolf.model.group.Authority.MANAGE_PAGES_FIND;
import static org.arctic.wolf.model.group.Authority.MANAGE_PAGES_LIST;
import static org.arctic.wolf.model.group.Authority.MANAGE_PLUGINS_AUTO_START;
import static org.arctic.wolf.model.group.Authority.MANAGE_PLUGINS_AVAILABLE_PLUGINS;
import static org.arctic.wolf.model.group.Authority.MANAGE_PLUGINS_DELETE;
import static org.arctic.wolf.model.group.Authority.MANAGE_PLUGINS_DOWNLOAD;
import static org.arctic.wolf.model.group.Authority.MANAGE_PLUGINS_FIND_NEW;
import static org.arctic.wolf.model.group.Authority.MANAGE_PLUGINS_INSTALL;
import static org.arctic.wolf.model.group.Authority.MANAGE_PLUGINS_INSTALLED_PLUGINS;
import static org.arctic.wolf.model.group.Authority.MANAGE_PLUGINS_REPOSITORY;
import static org.arctic.wolf.model.group.Authority.MANAGE_PLUGINS_START;
import static org.arctic.wolf.model.group.Authority.MANAGE_PLUGINS_STOP;
import static org.arctic.wolf.model.group.Authority.MANAGE_PLUGINS_UNINSTALL;
import static org.arctic.wolf.model.group.Authority.MANAGE_PLUGINS_UPDATE;
import static org.arctic.wolf.model.group.Authority.MANAGE_POSTS_DELETE;
import static org.arctic.wolf.model.group.Authority.MANAGE_POSTS_EDIT;
import static org.arctic.wolf.model.group.Authority.MANAGE_POSTS_FIND;
import static org.arctic.wolf.model.group.Authority.MANAGE_POSTS_LIST;
import static org.arctic.wolf.model.group.Authority.MANAGE_POSTS_WRITE_NEW;
import static org.arctic.wolf.model.group.Authority.MANAGE_SYSTEM;
import static org.arctic.wolf.model.group.Authority.MANAGE_THEMES_INSTALLER;
import static org.arctic.wolf.model.group.Authority.MANAGE_THEMES_LIST;
import static org.arctic.wolf.model.group.Authority.MANAGE_THEMES_SET;
import static org.arctic.wolf.model.group.Authority.MANAGE_USERS_ADD;
import static org.arctic.wolf.model.group.Authority.MANAGE_USERS_DELETE;
import static org.arctic.wolf.model.group.Authority.MANAGE_USERS_EDIT;
import static org.arctic.wolf.model.group.Authority.MANAGE_USERS_FIND;
import static org.arctic.wolf.model.group.Authority.MANAGE_USERS_LIST;
import static org.arctic.wolf.model.group.Authority.MANAGE_WIDGETS_ADD_NEW;
import static org.arctic.wolf.model.group.Authority.MANAGE_WIDGETS_DELETE;
import static org.arctic.wolf.model.group.Authority.MANAGE_WIDGETS_EDIT;
import static org.arctic.wolf.model.group.Authority.MANAGE_WIDGETS_LIST;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;

import lombok.RequiredArgsConstructor;
import org.arctic.wolf.model.group.Authority;
import org.arctic.wolf.model.message.MessageService;
import org.arctic.wolf.model.user.UserDB;
import org.springframework.stereotype.Service;

/**
 * @author lord_rex
 */
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@Service
public class AdminSidebarService {
	private final MessageService messageService;
	
	public List<Menu> getMenu(UserDB userDB, String lang) {
		final List<Menu> menu = new ArrayList<>();
		
		addDashboard(userDB, lang, menu);
		addUserManagement(userDB, lang, menu);
		addGroupManagement(userDB, lang, menu);
		addPostsManagement(userDB, lang, menu);
		addPagesManagement(userDB, lang, menu);
		addWidgetsManagement(userDB, lang, menu);
		addThemeManagement(userDB, lang, menu);
		addPluginManagement(userDB, lang, menu);
		addMessagesManagement(userDB, lang, menu);
		addSystemManagement(userDB, lang, menu);
		
		return menu;
	}
	
	private void addDashboard(UserDB userDB, String lang, List<Menu> menu) {
		menu.add(menu("/", "admin_dashboard", lang, "fa fa-dashboard", null));
	}
	
	private void addUserManagement(UserDB userDB, String lang, List<Menu> menu) {
		if (!userDB.hasAnyAuthority(MANAGE_USERS_LIST, MANAGE_USERS_FIND, MANAGE_USERS_EDIT, MANAGE_USERS_ADD, MANAGE_USERS_DELETE)) {
			return;
		}
		
		final List<Menu> child = new ArrayList<>();
		addChild(child, userDB, MANAGE_USERS_LIST, "/users/list", "admin_users_management_list", lang, "fa fa-user");
		addChild(child, userDB, MANAGE_USERS_ADD, "/users/add_new", "admin_users_management_add_new", lang, "fa fa-user-plus");
		addChild(child, userDB, MANAGE_USERS_FIND, "/users/find", "admin_users_management_find", lang, "fa fa-search");
		
		menu.add(menu("/users", "admin_users_management", lang, "fa fa-user", child));
	}
	
	private void addGroupManagement(UserDB userDB, String lang, List<Menu> menu) {
		if (!userDB.hasAnyAuthority(MANAGE_GROUPS_LIST, MANAGE_GROUPS_FIND, MANAGE_GROUPS_EDIT, MANAGE_GROUPS_ADD, MANAGE_GROUPS_DELETE)) {
			return;
		}
		
		final List<Menu> child = new ArrayList<>();
		addChild(child, userDB, MANAGE_GROUPS_LIST, "/groups/list", "admin_groups_management_list", lang, "fa fa-users");
		addChild(child, userDB, MANAGE_GROUPS_ADD, "/groups/add_new", "admin_groups_management_create_new", lang, "fa fa-user-plus");
		addChild(child, userDB, MANAGE_GROUPS_FIND, "/groups/find", "admin_groups_management_find", lang, "fa fa-search");
		
		menu.add(menu("/groups", "admin_groups_management", lang, "fa fa-users", child));
	}
	
	private void addPostsManagement(UserDB userDB, String lang, List<Menu> menu) {
		if (!userDB.hasAnyAuthority(MANAGE_POSTS_LIST, MANAGE_POSTS_FIND, MANAGE_POSTS_EDIT, MANAGE_POSTS_WRITE_NEW, MANAGE_POSTS_DELETE)) {
			return;
		}
		
		final List<Menu> child = new ArrayList<>();
		addChild(child, userDB, MANAGE_POSTS_LIST, "/posts/list", "admin_posts_list_all", lang, "fa fa-thumb-tack");
		addChild(child, userDB, MANAGE_POSTS_WRITE_NEW, "/posts/write_new", "admin_posts_write_new", lang, "fa fa-pencil");
		addChild(child, userDB, MANAGE_POSTS_FIND, "/posts/find", "admin_posts_find", lang, "fa fa-search");
		
		menu.add(menu("/posts", "admin_posts", lang, "fa fa-thumb-tack", child));
	}
	
	private void addPagesManagement(UserDB userDB, String lang, List<Menu> menu) {
		if (!userDB.hasAnyAuthority(MANAGE_PAGES_LIST, MANAGE_PAGES_FIND, MANAGE_PAGES_EDIT, MANAGE_PAGES_CREATE_NEW, MANAGE_PAGES_DELETE)) {
			return;
		}
		
		final List<Menu> child = new ArrayList<>();
		addChild(child, userDB, MANAGE_PAGES_LIST, "/pages/list", "admin_pages_list_all", lang, "fa fa-file");
		addChild(child, userDB, MANAGE_PAGES_CREATE_NEW, "/pages/create_new", "admin_pages_create_new", lang, "fa fa-pencil");
		addChild(child, userDB, MANAGE_PAGES_FIND, "/pages/find", "admin_pages_find", lang, "fa fa-search");
		
		menu.add(menu("/pages", "admin_pages", lang, "fa fa-file", child));
	}
	
	private void addWidgetsManagement(UserDB userDB, String lang, List<Menu> menu) {
		if (!userDB.hasAnyAuthority(MANAGE_WIDGETS_LIST, MANAGE_WIDGETS_EDIT, MANAGE_WIDGETS_ADD_NEW, MANAGE_WIDGETS_DELETE)) {
			return;
		}
		
		final List<Menu> child = new ArrayList<>();
		addChild(child, userDB, MANAGE_WIDGETS_LIST, "/widgets/list", "admin_widgets_list_all", lang, "fa fa-list-alt");
		addChild(child, userDB, MANAGE_WIDGETS_ADD_NEW, "/widgets/add_new", "admin_widgets_add_new", lang, "fa fa-plus");
		
		menu.add(menu("/widgets", "admin_widgets", lang, "fa fa-list-alt", child));
	}
	
	private void addThemeManagement(UserDB userDB, String lang, List<Menu> menu) {
		if (!userDB.hasAnyAuthority(MANAGE_THEMES_LIST, MANAGE_THEMES_INSTALLER, MANAGE_THEMES_SET)) {
			return;
		}
		
		final List<Menu> child = new ArrayList<>();
		addChild(child, userDB, MANAGE_THEMES_LIST, "/themes/available", "admin_themes_available_themes", lang, "fa fa-wrench");
		addChild(child, userDB, MANAGE_THEMES_LIST, "/themes/installed", "admin_themes_installed_themes", lang, "fa fa-wrench");
		addChild(child, userDB, MANAGE_THEMES_LIST, "/themes/repository", "admin_themes_theme_repository", lang, "fa fa-wrench");
		addChild(child, userDB, MANAGE_THEMES_LIST, "/themes/find-new", "admin_themes_find_new_themes", lang, "fa fa-search");
		
		menu.add(menu("/themes", "admin_themes", lang, "fa fa-wrench", child));
	}
	
	private void addPluginManagement(UserDB userDB, String lang, List<Menu> menu) {
		if (!userDB.hasAnyAuthority(MANAGE_PLUGINS_AVAILABLE_PLUGINS, MANAGE_PLUGINS_INSTALLED_PLUGINS, MANAGE_PLUGINS_FIND_NEW, MANAGE_PLUGINS_REPOSITORY,
			MANAGE_PLUGINS_UPDATE, MANAGE_PLUGINS_UNINSTALL, MANAGE_PLUGINS_INSTALL, MANAGE_PLUGINS_DOWNLOAD, MANAGE_PLUGINS_DELETE,
			MANAGE_PLUGINS_START, MANAGE_PLUGINS_STOP, MANAGE_PLUGINS_AUTO_START)) {
			return;
		}
		
		final List<Menu> child = new ArrayList<>();
		addChild(child, userDB, MANAGE_PLUGINS_AVAILABLE_PLUGINS, "/plugins/available", "admin_plugins_available_plugins", lang, "fa fa-plug");
		addChild(child, userDB, MANAGE_PLUGINS_INSTALLED_PLUGINS, "/plugins/installed", "admin_plugins_installed_plugins", lang, "fa fa-plug");
		addChild(child, userDB, MANAGE_PLUGINS_REPOSITORY, "/plugins/repository", "admin_plugins_plugin_repository", lang, "fa fa-plug");
		addChild(child, userDB, MANAGE_PLUGINS_FIND_NEW, "/plugins/find-new", "admin_plugins_find_new_plugins", lang, "fa fa-search");
		
		menu.add(menu("/plugins", "admin_plugins", lang, "fa fa-plug", child));
	}
	
	private void addMessagesManagement(UserDB userDB, String lang, List<Menu> menu) {
		if (!userDB.hasAnyAuthority(MANAGE_LANGUAGES_LIST, MANAGE_LANGUAGES_ADD, MANAGE_LANGUAGES_DELETE,
			MANAGE_MESSAGES, MANAGE_MESSAGES_DELETE, MANAGE_MESSAGES_ADD, MANAGE_MESSAGES_EDIT)) {
			return;
		}
		
		final List<Menu> child = new ArrayList<>();
		addChild(child, userDB, MANAGE_MESSAGES, "/messages/messages", "admin_messages", lang, "fa fa-comments");
		addChild(child, userDB, MANAGE_LANGUAGES_LIST, "/messages/languages", "admin_languages", lang, "fa fa-language");
		
		menu.add(menu("/messages", "admin_messages", lang, "fa fa-comments", child));
	}
	
	private void addSystemManagement(UserDB userDB, String lang, List<Menu> menu) {
		if (!userDB.hasAnyAuthority(MANAGE_SYSTEM)) {
			return;
		}
		
		final List<Menu> child = new ArrayList<>();
		addChild(child, userDB, MANAGE_SYSTEM, "/system/properties", "admin_system_properties", lang, "fa fa-cog");
		addChild(child, userDB, MANAGE_SYSTEM, "/system/version-info", "admin_system_version_info", lang, "fa fa-info-circle");
		
		menu.add(menu("/system", "admin_system", lang, "fa fa-cogs", child));
	}
	
	private void addChild(List<Menu> child, UserDB userDB, Authority authority, String href, String message, String lang, String icon) {
		if (!userDB.hasAuthority(authority)) {
			return;
		}
		
		child.add(menu(href, message, lang, icon, null));
	}
	
	private Menu menu(String href, String message, String lang, String icon, List<Menu> child) {
		return new Menu(href, messageService.getMessage(message, lang), icon, child);
	}
}
