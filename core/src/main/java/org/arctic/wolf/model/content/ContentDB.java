/*
 * Copyright (c) 2017 Reginald Ravenhorst <lordrex34@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.arctic.wolf.model.content;

import java.util.Date;
import java.util.Set;
import jakarta.persistence.Column;
import jakarta.persistence.ElementCollection;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Lob;
import jakarta.persistence.Table;
import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.arctic.wolf.model.plugin.IPluginContentOwner;
import org.hibernate.annotations.Type;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * @author lord_rex
 */
@Entity
@Table(name = "content")
@EqualsAndHashCode(of = "id")
@Setter
@Getter
@ToString(callSuper = true)
public class ContentDB implements IContent {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(unique = true, nullable = false)
	private Long id;
	
	@Enumerated(EnumType.STRING)
	@Column(nullable = false)
	private ContentType type = ContentType.POST;
	
	@Column
	private String title;
	
	@Lob
	@Column
	private String content;
	
	@Column
	private Long authorId;
	
	@Column
	private String author;
	
	@DateTimeFormat(pattern = "MM/dd/yyyy")
	@Temporal(TemporalType.TIMESTAMP)
	@Column(nullable = false)
	private Date createTime = new Date();
	
	@Column(nullable = false)
	private int displayOrder = 0;
	
	@ElementCollection
	@Column(nullable = false)
	private Set<ContentVisibility> visibility;
	
	@Column(unique = true)
	private String friendlyURL;
	
	@Column(nullable = false)
	private boolean adminPage = false;
	
	@Column
	private String icon;
	
	@Column(nullable = false)
	private String lang = "en";
	
	@Override
	public IPluginContentOwner withPluginContent(String pluginContent) {
		throw new UnsupportedOperationException();
	}
}
