/*
 * Copyright (c) 2017 Reginald Ravenhorst <lordrex34@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.arctic.wolf.model.content;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import jakarta.persistence.EntityManager;

import lombok.RequiredArgsConstructor;
import org.arctic.wolf.exception.ContentNotFoundException;
import org.arctic.wolf.mapper.ContentMapper;
import org.arctic.wolf.model.content.dto.Agreement;
import org.arctic.wolf.model.content.dto.Content;
import org.arctic.wolf.model.content.dto.form.IContentForm;
import org.arctic.wolf.model.plugin.PluginService;
import org.arctic.wolf.model.server.ServerSettingsService;
import org.arctic.wolf.model.user.UserDB;
import org.arctic.wolf.util.FuzzySearchUtil;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.google.common.annotations.VisibleForTesting;

/**
 * @author lord_rex
 */
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@Service
@Transactional
public class ContentService {
	public static final String PRIVACY_POLICY_URL = "privacy-policy";
	public static final String TERMS_OF_SERVICE_URL = "terms-of-service";
	
	private static final Comparator<ContentDB> CONTENT_DB_COMPARATOR = (o1, o2) -> o1.getDisplayOrder() > 0 ? Integer.compare(o1.getDisplayOrder(), o2.getDisplayOrder()) : o2.getCreateTime().compareTo(o1.getCreateTime());
	
	private final ContentDBRepository repository;
	private final EntityManager entityManager;
	
	public ContentDB createContent(IContentForm contentForm) {
		final ContentDB contentDB = ContentMapper.INSTANCE.toDB(contentForm);
		return repository.save(contentDB);
	}
	
	public void update(ContentDB contentDB) {
		repository.save(contentDB);
	}
	
	public void updateByForm(ContentDB contentDB, IContentForm contentForm) {
		contentDB.updateFrom(contentForm);
		
		update(contentDB);
	}
	
	@Transactional(readOnly = true)
	public Optional<ContentDB> getContentDB(ContentType type, String lang, long id) {
		return Optional.ofNullable(repository.findByTypeAndLangAndId(type, lang, id));
	}
	
	public Optional<Content> getContentDTO(ContentType type, String lang, long id) {
		return getContentDB(type, lang, id).map(ContentMapper.INSTANCE::toDTO);
	}
	
	public Content getContent(ContentType type, String lang, long id, String routeFullPath, UserDB userDB, PluginService pluginService, ServerSettingsService serverSettingsService) {
		return getContentDTO(type, lang, id)
			.filter(content -> !content.isAdminPage())
			.filter(content -> content.isVisibleFor(userDB))
			.map(content -> processContent(content, routeFullPath, pluginService, serverSettingsService))
			.orElseThrow(ContentNotFoundException::new);
	}
	
	@Transactional(readOnly = true)
	public Optional<ContentDB> getContentDB(ContentType type, String lang, String friendlyURL) {
		return Optional.ofNullable(repository.findByTypeAndLangAndFriendlyURL(type, lang, friendlyURL));
	}
	
	public Optional<Content> getContentDTO(ContentType type, String lang, String friendlyURL) {
		return getContentDB(type, lang, friendlyURL).map(ContentMapper.INSTANCE::toDTO);
	}
	
	public Content getContent(ContentType type, String lang, String friendlyURL, String routeFullPath, UserDB userDB, PluginService pluginService, ServerSettingsService serverSettingsService) {
		return getContentDTO(type, lang, friendlyURL)
			.filter(content -> !content.isAdminPage())
			.filter(content -> content.isVisibleFor(userDB))
			.map(content -> processContent(content, routeFullPath, pluginService, serverSettingsService))
			.orElseThrow(ContentNotFoundException::new);
	}
	
	private static Content processContent(Content content, String routeFullPath, PluginService pluginService, ServerSettingsService serverSettingsService) {
		final String contentText = content.getContent();
		content.setContent(serverSettingsService.processContentVariables(contentText));
		return (Content) pluginService.processContent(content, routeFullPath);
	}
	
	public boolean exists(ContentType type, String lang, long id) {
		return getContentDB(type, lang, id).isPresent();
	}
	
	public boolean exists(ContentType type, String lang, String friendlyURL) {
		return getContentDB(type, lang, friendlyURL).isPresent();
	}
	
	@Transactional(readOnly = true)
	public List<ContentDB> getContentsDB(ContentType type, String lang) {
		return repository.findByTypeAndLang(type, lang);
	}
	
	public List<Content> getContentsDTO(ContentType type, String lang) {
		return getContentsDB(type, lang)
			.stream()
			.map(ContentMapper.INSTANCE::toDTO)
			.collect(Collectors.toList());
	}
	
	public List<Content> getContents(ContentType type, String lang, UserDB userDB) {
		return getContentsDB(type, lang)
			.stream()
			.filter(content -> !content.isAdminPage())
			.filter(contentDB -> contentDB.isVisibleFor(userDB))
			.sorted(CONTENT_DB_COMPARATOR)
			.map(ContentMapper.INSTANCE::toDTO)
			.collect(Collectors.toList());
	}
	
	public Agreement getAgreement(String lang, String routeFullPath, UserDB userDB, PluginService pluginService, ServerSettingsService serverSettingsService) {
		final Agreement agreement = new Agreement();
		agreement.setPrivacyPolicy(getContent(ContentType.PAGE, lang, PRIVACY_POLICY_URL, routeFullPath, userDB, pluginService, serverSettingsService).getContent());
		agreement.setTermsOfService(getContent(ContentType.PAGE, lang, TERMS_OF_SERVICE_URL, routeFullPath, userDB, pluginService, serverSettingsService).getContent());
		return agreement;
	}
	
	public Page<ContentDB> pagedFuzzySearchDB(String lang, String search, ContentType type, Pageable pageable) {
		final Predicate<ContentDB> filter = contentDB -> contentDB.getLang().equals(lang) && contentDB.getType() == type;
		return FuzzySearchUtil.pagedFilteredFuzzySearch(pageable, filter, entityManager, ContentDB.class, search, "title", "author");
	}
	
	public Page<Content> pagedFuzzySearchDTO(String lang, String search, ContentType type, Pageable pageable) {
		return pagedFuzzySearchDB(lang, search, type, pageable).map(ContentMapper.INSTANCE::toDTO);
	}
	
	public void delete(long id) {
		repository.deleteById(id);
	}
	
	@VisibleForTesting
	void deleteAll() {
		repository.deleteAll();
	}
}
