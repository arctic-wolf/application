/*
 * Copyright (c) 2017 Reginald Ravenhorst <lordrex34@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.arctic.wolf.model.content;

/**
 * @author lord_rex
 */
public interface ContentVariables {
	String CONTENT_ID = "contentId";
	
	String CONTENT_TITLE = "contentTitle";
	
	String PAGE_FORM = "pageForm";
	String PAGES = "pages";
	String PAGE = "page";
	String NO_PAGE_CONTENT = "content_no_page_content";
	String NO_PAGE_TITLE = "content_no_page_title";
	String ADMIN_PAGES = "adminPages";
	
	String FRAGMENT = "fragment";
	
	String POST_FORM = "postForm";
	String POSTS = "posts";
	String POST = "post";
	String NO_POST_CONTENT = "content_no_post_content";
	String NO_POST_TITLE = "content_no_post_title";
	String NO_POST_AUTHOR = "content_no_post_author";
	String NO_POST_CREATE_TIME = "content_no_post_create_time";
	
	String POST_AUTHOR = "postAuthor";
	String POST_AUTHOR_ID = "postAuthorId";
	String POST_CREATE_TIME = "postCreateTime";
	
	String ALL_VISIBILITY = "allVisibility";
	String ALL_DESTINATIONS = "allDestinations";
	
	String FRIENDLY_URL = "friendlyURL";
	int UNKNOWN_PAGE_ID = -1;
	
	String ICON = "icon";
}
