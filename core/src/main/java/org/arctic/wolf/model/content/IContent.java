/*
 * Copyright (c) 2018 Reginald Ravenhorst <lordrex34@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.arctic.wolf.model.content;

import java.util.Date;

import org.arctic.wolf.model.plugin.IPluginContentOwner;
import com.google.common.base.Strings;

/**
 * @author lord_rex
 */
public interface IContent extends IPluginContentOwner, IContentVisibilityOwner {
	void setId(Long id);
	
	Long getId();
	
	void setType(ContentType type);
	
	ContentType getType();
	
	void setTitle(String title);
	
	String getTitle();
	
	void setContent(String content);
	
	@Override
	String getContent();
	
	void setAuthorId(Long authorId);
	
	Long getAuthorId();
	
	void setAuthor(String author);
	
	String getAuthor();
	
	void setCreateTime(Date createTime);
	
	Date getCreateTime();
	
	void setDisplayOrder(int displayOrder);
	
	int getDisplayOrder();
	
	void setFriendlyURL(String friendlyURL);
	
	String getFriendlyURL();
	
	boolean isAdminPage();
	
	void setAdminPage(boolean adminPage);
	
	void setIcon(String icon);
	
	String getIcon();
	
	void setLang(String lang);
	
	String getLang();
	
	default void updateFrom(IContent content) {
		setTitle(content.getTitle());
		setContent(content.getContent());
		setAuthorId(content.getAuthorId());
		setAuthor(content.getAuthor());
		setCreateTime(content.getCreateTime());
		setDisplayOrder(content.getDisplayOrder());
		setFriendlyURL(Strings.emptyToNull(content.getFriendlyURL()));
		setAdminPage(content.isAdminPage());
		setIcon(content.getIcon());
		setLang(content.getLang());
		setVisibility(content.getVisibility());
	}
}
