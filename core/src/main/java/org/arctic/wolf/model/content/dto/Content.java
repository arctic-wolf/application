/*
 * Copyright (c) 2019 Reginald Ravenhorst <lordrex34@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.arctic.wolf.model.content.dto;

import java.util.Date;
import java.util.Set;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.arctic.wolf.mapper.ContentMapper;
import org.arctic.wolf.model.content.ContentType;
import org.arctic.wolf.model.content.ContentVisibility;
import org.arctic.wolf.model.content.IContent;
import org.arctic.wolf.model.plugin.IPluginContentOwner;
import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * @author lord_rex
 */
@NoArgsConstructor
@Data
public class Content implements IContent, IPluginContentOwner {
	private Long id;
	private ContentType type;
	private String title;
	private String content;
	private Long authorId;
	private String author;
	private Date createTime;
	private int displayOrder;
	private String friendlyURL;
	
	@JsonIgnore
	private boolean adminPage;
	
	private String icon;
	private String lang;
	
	private Set<ContentVisibility> visibility;
	
	@Override
	public IPluginContentOwner withPluginContent(String pluginContent) {
		final Content content = ContentMapper.INSTANCE.toContent(this);
		content.setContent(pluginContent);
		return content;
	}
}
