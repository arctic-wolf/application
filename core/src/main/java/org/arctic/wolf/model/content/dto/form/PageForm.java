/*
 * Copyright (c) 2018 Reginald Ravenhorst <lordrex34@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.arctic.wolf.model.content.dto.form;

import static org.arctic.wolf.model.content.dto.form.PageFormMessages.PAGE_FORM_PROVIDE_AUTHOR;
import static org.arctic.wolf.model.content.dto.form.PageFormMessages.PAGE_FORM_PROVIDE_CONTENT;
import static org.arctic.wolf.model.content.dto.form.PageFormMessages.PAGE_FORM_PROVIDE_LANG;
import static org.arctic.wolf.model.content.dto.form.PageFormMessages.PAGE_FORM_PROVIDE_TITLE;

import java.util.Date;
import java.util.EnumSet;
import java.util.Set;
import jakarta.validation.constraints.NotBlank;

import lombok.Data;
import org.arctic.wolf.model.content.ContentType;
import org.arctic.wolf.model.content.ContentVisibility;
import org.arctic.wolf.model.plugin.IPluginContentOwner;

/**
 * @author lord_rex
 */
@Data
public class PageForm implements IContentForm {
	private Long id = 0L;
	
	@NotBlank(message = PAGE_FORM_PROVIDE_TITLE)
	private String title;
	
	@NotBlank(message = PAGE_FORM_PROVIDE_CONTENT)
	private String content;
	
	private Long authorId = null;
	
	@NotBlank(message = PAGE_FORM_PROVIDE_AUTHOR)
	private String author;
	
	private Date createTime = new Date();
	
	private int displayOrder = 0;
	
	private Set<ContentVisibility> visibility = EnumSet.allOf(ContentVisibility.class);
	
	@Override
	public void setType(ContentType type) {
		// do nothing
	}
	
	@Override
	public ContentType getType() {
		return ContentType.PAGE;
	}
	
	private String friendlyURL;
	
	private boolean adminPage = false;
	
	private String icon;
	
	@NotBlank(message = PAGE_FORM_PROVIDE_LANG)
	private String lang;
	
	@Override
	public IPluginContentOwner withPluginContent(String pluginContent) {
		throw new UnsupportedOperationException();
	}
}
