/*
 * Copyright (c) 2018 Reginald Ravenhorst <lordrex34@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.arctic.wolf.model.content.dto.form;

/**
 * @author lord_rex
 */
public interface PageFormMessages {
	String PAGE_FORM_PROVIDE_TITLE = "page_form_provide_title";
	String PAGE_FORM_PROVIDE_AUTHOR = "page_form_provide_author";
	String PAGE_FORM_PROVIDE_CONTENT = "page_form_provide_content";
	String PAGE_FORM_PROVIDE_LANG = "page_form_provide_lang";
	String PAGE_FORM_FRIENDLY_URL_USED = "page_form_friendly_url_used";
	
	String ADMIN_WRITE_NEW_PAGE_SUCCESS = "admin_pages_create_new_success";
	String ADMIN_EDIT_PAGE_SUCCESS = "admin_pages_edit_success";
	String ADMIN_DELETE_PAGE_SUCCESS = "admin_pages_delete_success";
}
