/*
 * Copyright (c) 2018 Reginald Ravenhorst <lordrex34@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.arctic.wolf.model.group;

/**
 * @author lord_rex
 */
public interface AdminGroupManagementMessages {
	String ADMIN_CREATE_NEW_GROUP_SUCCESS = "admin_groups_create_new_success";
	String ADMIN_DELETE_GROUP_SUCCESS = "admin_groups_delete_success";
	String ADMIN_EDIT_GROUP_NAME_ALREADY_EXIST = "admin_groups_edit_already_exist";
	String ADMIN_EDIT_GROUP_SUCCESS = "admin_groups_edit_success";
	
	String ADMIN_GROUP_USER_ADD_NOT_EXIST = "admin_groups_users_add_not_exist";
	String ADMIN_GROUP_USER_ADD_INVALID = "{admin_groups_users_add_provide_user}";
	String ADMIN_GROUP_USER_ADD_ALREADY_MEMBER = "admin_groups_users_add_already_member";
	String ADMIN_GROUP_USER_ADD_SUCCESS = "admin_groups_users_add_success";
	
	String ADMIN_GROUP_USER_REMOVE_SUCCESS = "admin_groups_users_remove_success";
}
