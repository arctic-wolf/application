/*
 * Copyright (c) 2018 Reginald Ravenhorst <lordrex34@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.arctic.wolf.model.group;

import lombok.Getter;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

/**
 * @author lord_rex
 */
@Getter
public enum Authority {
	REGULAR_USER,
	ADMIN_SITE,
	
	MANAGE_USERS_LIST,
	MANAGE_USERS_FIND,
	MANAGE_USERS_EDIT,
	MANAGE_USERS_ADD,
	MANAGE_USERS_DELETE,
	
	MANAGE_GROUPS_AUTHORITIES,
	MANAGE_GROUPS_LIST,
	MANAGE_GROUPS_FIND,
	MANAGE_GROUPS_EDIT,
	MANAGE_GROUPS_ADD,
	MANAGE_GROUPS_DELETE,
	MANAGE_GROUPS_LIST_MEMBERS,
	MANAGE_GROUPS_ADD_MEMBER,
	MANAGE_GROUPS_REMOVE_MEMBER,
	
	MANAGE_POSTS_LIST,
	MANAGE_POSTS_FIND,
	MANAGE_POSTS_EDIT,
	MANAGE_POSTS_WRITE_NEW,
	MANAGE_POSTS_DELETE,
	
	USE_ADMIN_PAGES,
	MANAGE_PAGES_LIST,
	MANAGE_PAGES_FIND,
	MANAGE_PAGES_EDIT,
	MANAGE_PAGES_CREATE_NEW,
	MANAGE_PAGES_DELETE,
	
	MANAGE_WIDGETS_LIST,
	MANAGE_WIDGETS_EDIT,
	MANAGE_WIDGETS_ADD_NEW,
	MANAGE_WIDGETS_DELETE,
	
	MANAGE_THEMES_LIST,
	MANAGE_THEMES_INSTALLER,
	MANAGE_THEMES_SET,
	
	MANAGE_PLUGINS_AVAILABLE_PLUGINS,
	MANAGE_PLUGINS_INSTALLED_PLUGINS,
	MANAGE_PLUGINS_FIND_NEW,
	MANAGE_PLUGINS_REPOSITORY,
	MANAGE_PLUGINS_UPDATE,
	MANAGE_PLUGINS_UNINSTALL,
	MANAGE_PLUGINS_INSTALL,
	MANAGE_PLUGINS_DOWNLOAD,
	MANAGE_PLUGINS_DELETE,
	MANAGE_PLUGINS_START,
	MANAGE_PLUGINS_STOP,
	MANAGE_PLUGINS_AUTO_START,
	
	MANAGE_SYSTEM,
	
	MANAGE_LANGUAGES_LIST,
	MANAGE_LANGUAGES_ALLOW_DISALLOW,
	MANAGE_LANGUAGES_ADD,
	MANAGE_LANGUAGES_DELETE,
	
	MANAGE_MESSAGES,
	MANAGE_MESSAGES_DELETE,
	MANAGE_MESSAGES_ADD,
	MANAGE_MESSAGES_EDIT,
	
	// ...
	;
	
	private static final String ROLE_PREFIX = "ROLE_";
	private static final String MESSAGE_PREFIX = "admin_authority_";
	
	private final String authority;
	private final SimpleGrantedAuthority grantedAuthority;
	private final String message;
	
	Authority() {
		this.authority = ROLE_PREFIX + name();
		this.grantedAuthority = new SimpleGrantedAuthority(authority);
		this.message = MESSAGE_PREFIX + name().toLowerCase();
	}
}
