/*
 * Copyright (c) 2018 Reginald Ravenhorst <lordrex34@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.arctic.wolf.model.group;

import java.util.ArrayList;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import jakarta.persistence.EntityManager;

import lombok.RequiredArgsConstructor;
import org.arctic.wolf.exception.GroupNotFoundException;
import org.arctic.wolf.mapper.GroupMapper;
import org.arctic.wolf.mapper.UserMapper;
import org.arctic.wolf.model.group.dto.Group;
import org.arctic.wolf.model.group.dto.form.IGroupForm;
import org.arctic.wolf.model.user.UserDB;
import org.arctic.wolf.model.user.dto.User;
import org.arctic.wolf.util.FuzzySearchUtil;
import org.arctic.wolf.util.PageUtil;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.google.common.annotations.VisibleForTesting;

/**
 * @author lord_rex
 */
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@Service
@Transactional
public class GroupService {
	private final GroupDBRepository repository;
	private final EntityManager entityManager;
	
	public void update(GroupDB groupDB) {
		repository.save(groupDB);
	}
	
	public GroupDB createByForm(IGroupForm groupForm) {
		final GroupDB groupDB = GroupMapper.INSTANCE.toDB(groupForm);
		return repository.save(groupDB);
	}
	
	public void updateByForm(GroupDB groupDB, IGroupForm groupForm) {
		groupDB.updateFrom(groupForm);
		
		update(groupDB);
	}
	
	public void delete(long id) {
		repository.deleteById(id);
	}
	
	@VisibleForTesting
	void deleteAll() {
		repository.deleteAll();
	}
	
	@VisibleForTesting
	@Transactional(readOnly = true)
	long count() {
		return repository.count();
	}
	
	@Transactional(readOnly = true)
	public Optional<GroupDB> getGroupDB(long id) {
		return repository.findById(id);
	}
	
	public Optional<Group> getGroupDTO(long id) {
		return getGroupDB(id).map(GroupMapper.INSTANCE::toDTO);
	}
	
	@Transactional(readOnly = true)
	public Optional<GroupDB> getGroupDB(String name) {
		return Optional.ofNullable(repository.findByName(name));
	}
	
	public Optional<Group> getGroupDTO(String name) {
		return getGroupDB(name).map(GroupMapper.INSTANCE::toDTO);
	}
	
	@Transactional(readOnly = true)
	public boolean exists(String name) {
		return repository.findByName(name) != null;
	}
	
	@Transactional(readOnly = true)
	public Page<GroupDB> getGroupsDB(Pageable pageable) {
		return repository.findAll(pageable);
	}
	
	public Page<Group> getGroupsDTO(Pageable pageable) {
		return getGroupsDB(pageable).map(GroupMapper.INSTANCE::toDTO);
	}
	
	@Transactional(readOnly = true)
	public Page<GroupDB> pagedFuzzySearchDB(String search, Pageable pageable) {
		return FuzzySearchUtil.pagedFuzzySearch(pageable, entityManager, GroupDB.class, search, "name");
	}
	
	public Page<Group> pagedFuzzySearchDTO(String search, Pageable pageable) {
		return pagedFuzzySearchDB(search, pageable).map(GroupMapper.INSTANCE::toDTO);
	}
	
	public void addUser(String groupName, UserDB userDB) {
		final GroupDB groupDB = getGroupDB(groupName)
			.orElseThrow(GroupNotFoundException::new);
		
		addUser(groupDB, userDB);
	}
	
	public void addUser(long groupId, UserDB userDB) {
		final GroupDB groupDB = getGroupDB(groupId)
			.orElseThrow(GroupNotFoundException::new);
		
		addUser(groupDB, userDB);
	}
	
	private void addUser(GroupDB groupDB, UserDB userDB) {
		groupDB.getUsers().add(userDB);
		
		update(groupDB);
	}
	
	public void removeUser(String groupName, UserDB userDB) {
		final GroupDB groupDB = getGroupDB(groupName)
			.orElseThrow(GroupNotFoundException::new);
		
		removeUser(groupDB, userDB);
	}
	
	public void removeUser(long groupId, UserDB userDB) {
		final GroupDB groupDB = getGroupDB(groupId)
			.orElseThrow(GroupNotFoundException::new);
		
		removeUser(groupDB, userDB);
	}
	
	private void removeUser(GroupDB groupDB, UserDB userDB) {
		groupDB.getUsers().remove(userDB);
		
		update(groupDB);
	}
	
	public boolean hasUser(String groupName, UserDB userDB) {
		final GroupDB groupDB = getGroupDB(groupName)
			.orElseThrow(GroupNotFoundException::new);
		
		return hasUser(groupDB, userDB);
	}
	
	public boolean hasUser(long groupId, UserDB userDB) {
		final GroupDB groupDB = getGroupDB(groupId)
			.orElseThrow(GroupNotFoundException::new);
		
		return hasUser(groupDB, userDB);
	}
	
	private boolean hasUser(GroupDB groupDB, UserDB userDB) {
		return groupDB != null && groupDB.getUsers().contains(userDB);
	}
	
	@Transactional(readOnly = true)
	public Set<UserDB> getUsersDB(String groupName) {
		final GroupDB groupDB = getGroupDB(groupName)
			.orElseThrow(GroupNotFoundException::new);
		
		return groupDB.getUsers();
	}
	
	public Set<User> getUsersDTO(String groupName) {
		return getUsersDB(groupName)
			.stream()
			.map(UserMapper.INSTANCE::toDTO)
			.collect(Collectors.toSet());
	}
	
	public Page<User> getPagedUsersDTO(Pageable pageable, String groupName) {
		return PageUtil.getPagedView(pageable, new ArrayList<>(getUsersDTO(groupName)));
	}
	
	@Transactional(readOnly = true)
	public Set<UserDB> getUsersDB(long groupId) {
		final GroupDB groupDB = getGroupDB(groupId)
			.orElseThrow(GroupNotFoundException::new);
		
		return groupDB.getUsers();
	}
	
	public Set<User> getUsersDTO(long groupId) {
		return getUsersDB(groupId)
			.stream()
			.map(UserMapper.INSTANCE::toDTO)
			.collect(Collectors.toSet());
	}
	
	public Page<User> getPagedUsersDTO(Pageable pageable, long groupId) {
		return PageUtil.getPagedView(pageable, new ArrayList<>(getUsersDTO(groupId)));
	}
	
}
