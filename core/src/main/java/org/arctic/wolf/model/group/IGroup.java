/*
 * Copyright (c) 2018 Reginald Ravenhorst <lordrex34@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.arctic.wolf.model.group;

import java.util.Date;
import java.util.Set;

/**
 * @author lord_rex
 */
public interface IGroup {
	void setId(Long id);
	
	Long getId();
	
	void setName(String name);
	
	String getName();
	
	void setDescription(String description);
	
	String getDescription();
	
	void setAuthorities(Set<Authority> authorities);
	
	Set<Authority> getAuthorities();
	
	void setCreateTime(Date createTime);
	
	Date getCreateTime();
	
	default void updateFrom(IGroup group) {
		setName(group.getName());
		setDescription(group.getDescription());
		setAuthorities(group.getAuthorities());
		setCreateTime(group.getCreateTime());
	}
}
