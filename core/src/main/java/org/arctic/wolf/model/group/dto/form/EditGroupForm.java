/*
 * Copyright (c) 2018 Reginald Ravenhorst <lordrex34@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.arctic.wolf.model.group.dto.form;

import static org.arctic.wolf.model.group.dto.form.GroupFormMessages.GROUP_FORM_PROVIDE_DESCRIPTION;
import static org.arctic.wolf.model.group.dto.form.GroupFormMessages.GROUP_FORM_PROVIDE_NAME;

import java.util.Date;
import java.util.EnumSet;
import java.util.Set;
import jakarta.validation.constraints.NotBlank;

import lombok.Data;
import org.arctic.wolf.model.group.Authority;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * @author lord_rex
 */
@Data
public class EditGroupForm implements IGroupForm {
	private Long id;
	
	@NotBlank(message = GROUP_FORM_PROVIDE_NAME)
	private String name;
	
	@NotBlank(message = GROUP_FORM_PROVIDE_DESCRIPTION)
	private String description;
	
	private Set<Authority> authorities = EnumSet.of(Authority.REGULAR_USER);
	
	@DateTimeFormat(pattern = "MM/dd/yyyy")
	private Date createTime = new Date();
}
