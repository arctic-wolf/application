/*
 * Copyright (c) 2019 Reginald Ravenhorst <lordrex34@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.arctic.wolf.model.link.dto;

import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

import lombok.Data;
import org.arctic.wolf.model.content.IContent;
import org.arctic.wolf.model.link.ILink;

/**
 * @author lord_rex
 */
@Data
public class Link implements ILink {
	private Long id;
	private String url;
	private String displayName;
	private String icon;
	private String target = "_self";
	private boolean useRouter;
	private boolean useDivider;
	private List<ILink> subLinks;
	
	public Link(IContent content) {
		setId(ThreadLocalRandom.current().nextLong());
		setUrl(content.getFriendlyURL());
		setDisplayName(content.getTitle());
		setIcon(content.getIcon());
	}
}
