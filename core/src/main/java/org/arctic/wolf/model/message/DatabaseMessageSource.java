/*
 * Copyright (c) 2018 Reginald Ravenhorst <lordrex34@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.arctic.wolf.model.message;

import static org.arctic.wolf.model.server.ServerSettingsVariables.DEFAULT_SITE_LANGUAGE;
import static org.arctic.wolf.model.server.ServerSettingsVariables.DEFAULT_USE_BROWSER_LANGUAGE;
import static org.arctic.wolf.model.server.ServerSettingsVariables.SITE_LANGUAGE;
import static org.arctic.wolf.model.server.ServerSettingsVariables.USE_BROWSER_LANGUAGE;

import java.text.MessageFormat;
import java.util.Locale;
import org.springframework.beans.factory.annotation.Autowired;

import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.LocaleUtils;
import org.arctic.wolf.model.server.ServerSettingsService;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceResolvable;
import org.springframework.context.NoSuchMessageException;
import org.springframework.stereotype.Component;

/**
 * @author lord_rex
 */
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@Component
public class DatabaseMessageSource implements MessageSource {
	private final ServerSettingsService serverSettingsService;
	private final MessageService messageService;
	
	@Override
	public String getMessage(String code, Object[] args, String defaultMessage, Locale locale) {
		return resolveMessage(code, args, locale);
	}
	
	@Override
	public String getMessage(String code, Object[] args, Locale locale) throws NoSuchMessageException {
		return resolveMessage(code, args, locale);
	}
	
	@Override
	public String getMessage(MessageSourceResolvable resolvable, Locale locale) throws NoSuchMessageException {
		
		// First, search messages by code.
		for (String code : resolvable.getCodes()) {
			final String message = resolveMessage(code, resolvable.getArguments(), locale);
			if (message != null) {
				return message;
			}
		}
		
		// If no messages by code found in database, then search by default message (custom code).
		// Also instead of having a null (empty) message, display the default message.
		final String message = resolveMessage(resolvable.getDefaultMessage(), resolvable.getArguments(), locale);
		return message == null ? resolvable.getDefaultMessage() : message;
	}
	
	private String resolveMessage(String code, Object[] args, Locale locale) {
		final String language = serverSettingsService.getBoolean(USE_BROWSER_LANGUAGE, DEFAULT_USE_BROWSER_LANGUAGE)
			? locale.getLanguage()
			: serverSettingsService.getString(SITE_LANGUAGE, DEFAULT_SITE_LANGUAGE);
		
		final String message = messageService.getMessage(code, language);
		if (message == null) {
			return null;
		}
		final MessageFormat messageFormat = new MessageFormat(message, LocaleUtils.toLocale(language));
		return messageFormat.format(args);
	}
}
