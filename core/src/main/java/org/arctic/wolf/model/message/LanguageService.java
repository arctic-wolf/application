/*
 * Copyright (c) 2019 Reginald Ravenhorst <lordrex34@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.arctic.wolf.model.message;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;

import lombok.RequiredArgsConstructor;
import org.arctic.wolf.exception.LanguageNotFoundException;
import org.arctic.wolf.mapper.LanguageMapper;
import org.arctic.wolf.model.message.dto.Language;
import org.arctic.wolf.model.message.dto.form.ILanguageForm;
import org.arctic.wolf.model.message.dto.form.LanguageForm;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.google.common.annotations.VisibleForTesting;

/**
 * @author lord_rex
 */
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@Service
@Transactional
public class LanguageService {
	private final LanguageDBRepository repository;
	
	public void createByForm(ILanguageForm languageForm) {
		final LanguageDB languageDB = new LanguageDB();
		languageDB.setIcon(languageForm.getIcon());
		languageDB.setLanguage(languageForm.getLanguage());
		languageDB.setDisplayName(languageForm.getDisplayName());
		languageDB.setAvailable(languageForm.isAvailable());
		
		repository.save(languageDB);
	}
	
	public void createByForm(String icon, String language, String displayName) {
		final ILanguageForm languageForm = new LanguageForm();
		languageForm.setIcon(icon);
		languageForm.setLanguage(language);
		languageForm.setDisplayName(displayName);
		
		createByForm(languageForm);
	}
	
	@Transactional(readOnly = true)
	public Optional<LanguageDB> findByLanguageDB(String language) {
		return Optional.ofNullable(repository.findByLanguage(language));
	}
	
	public Optional<Language> findByLanguageDTO(String language) {
		return findByLanguageDB(language)
			.map(LanguageMapper.INSTANCE::toDTO);
	}
	
	@Transactional(readOnly = true)
	public List<LanguageDB> getLanguagesDB() {
		return repository.findAll();
	}
	
	public List<Language> getLanguagesDTO() {
		return getLanguagesDB()
			.stream()
			.map(LanguageMapper.INSTANCE::toDTO)
			.collect(Collectors.toList());
	}
	
	public List<Language> getAvailableLanguages() {
		return getLanguagesDB()
			.stream()
			.filter(LanguageDB::isAvailable)
			.map(LanguageMapper.INSTANCE::toDTO)
			.collect(Collectors.toList());
	}
	
	public Optional<Language> getAvailableLanguage(String lang) {
		return getLanguagesDB()
			.stream()
			.filter(LanguageDB::isAvailable)
			.filter(languageDB -> languageDB.getLanguage().equals(lang))
			.map(LanguageMapper.INSTANCE::toDTO)
			.findFirst();
	}
	
	public boolean allowDisallow(String lang) {
		final LanguageDB languageDB = findByLanguageDB(lang)
			.orElseThrow(LanguageNotFoundException::new);
		
		final boolean available = languageDB.isAvailable();
		languageDB.setAvailable(!available);
		repository.save(languageDB);
		
		return findByLanguageDB(lang)
			.orElseThrow(LanguageNotFoundException::new)
			.isAvailable();
	}
	
	public void delete(String lang) {
		repository.deleteById(lang);
	}
	
	@VisibleForTesting
	void deleteAll() {
		repository.deleteAll();
	}
}
