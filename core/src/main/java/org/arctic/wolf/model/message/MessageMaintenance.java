/*
 * Copyright (c) 2018 Reginald Ravenhorst <lordrex34@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.arctic.wolf.model.message;

import static org.arctic.wolf.model.server.ServerSettingsVariables.DEFAULT_SITE_LANGUAGE;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import jakarta.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.mutable.MutableInt;
import org.springframework.stereotype.Component;
import com.google.common.annotations.VisibleForTesting;

/**
 * @author lord_rex
 */
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@Slf4j
@Component
public class MessageMaintenance {
	private static final String DEFAULT_EN_MESSAGES_PROPERTIES = "default_en_messages.properties";
	
	private final MessageService messageService;
	
	@PostConstruct
	void onStartup() throws IOException {
		manageDefaultContent();
		reportDuplicates();
	}
	
	private void manageDefaultContent() throws IOException {
		final Properties properties = getProperties();
		
		final MutableInt count = new MutableInt();
		properties.forEach((key, value) -> {
			final String code = String.valueOf(key);
			final String message = String.valueOf(value);
			
			final boolean added = messageService.addIfNotExist(code, DEFAULT_SITE_LANGUAGE, message);
			if (added) {
				count.increment();
			}
		});
		
		LOGGER.info("Registered {} language string(s) into database.", count.getValue());
	}
	
	@VisibleForTesting
	Properties getProperties() throws IOException {
		final Properties properties = new Properties();
		try (InputStream resource = getClass().getResourceAsStream(DEFAULT_EN_MESSAGES_PROPERTIES)) {
			properties.load(resource);
		}
		return properties;
	}
	
	private void reportDuplicates() {
		final Set<String> reported = new HashSet<>();
		final Set<String> duplicated = new HashSet<>();
		
		for (MessageDB messageSourceDB : messageService.getMessagesDB()) {
			final String code = messageSourceDB.getCode();
			final String language = messageSourceDB.getLanguage();
			
			if (reported.contains(code + language)) {
				continue;
			}
			
			final List<MessageDB> messagesDB = messageService.getMessagesDB(code, language);
			
			if (messagesDB.size() > 1) {
				LOGGER.error("There are multiple strings registered for code '{}' and language '{}'.", code, language);
				duplicated.add(code + language);
			}
			
			reported.add(code + language);
		}
		
		if (!duplicated.isEmpty()) {
			throw new ExceptionInInitializerError("There are " + reported.size() + " duplicated message(s) you must fix!");
		}
	}
}
