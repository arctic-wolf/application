/*
 * Copyright (c) 2018 Reginald Ravenhorst <lordrex34@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.arctic.wolf.model.message;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;

import lombok.RequiredArgsConstructor;
import org.arctic.wolf.mapper.MessageMapper;
import org.arctic.wolf.model.message.dto.MessageModel;
import org.arctic.wolf.model.message.dto.form.IMessageForm;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.google.common.annotations.VisibleForTesting;

/**
 * @author lord_rex
 */
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@Service
@Transactional
public class MessageService {
	private static final String MESSAGE = "message";
	
	private final MessageDBRepository repository;
	
	public void add(String code, String language, String message) {
		final MessageDB messageDB = new MessageDB();
		messageDB.setCode(code);
		messageDB.setLanguage(language);
		messageDB.setMessage(message);
		repository.save(messageDB);
	}
	
	public boolean addIfNotExist(String code, String language, String message) {
		if (repository.findByCodeAndLanguage(code, language) == null) {
			add(code, language, message);
			return true;
		}
		return false;
	}
	
	public void createByForm(IMessageForm messageForm) {
		final String code = messageForm.getCode();
		final String language = messageForm.getLanguage();
		final String message = messageForm.getMessage();
		
		final MessageDB messageDB = repository.findByCodeAndLanguage(code, language);
		if (messageDB != null) {
			messageDB.setMessage(message);
			update(messageDB);
		}
		else {
			add(code, language, message);
		}
	}
	
	public void delete(Long id) {
		repository.deleteById(id);
	}
	
	public void update(MessageDB messageDB) {
		repository.save(messageDB);
	}
	
	public void updateByForm(MessageDB messageDB, IMessageForm messageForm) {
		messageDB.updateFrom(messageForm);
		update(messageDB);
	}
	
	@Transactional(readOnly = true)
	public Optional<MessageDB> findByIdDB(long id) {
		return repository.findById(id);
	}
	
	@Transactional(readOnly = true)
	public Optional<MessageModel> findByIdDTO(long id) {
		return findByIdDB(id).map(MessageMapper.INSTANCE::toDTO);
	}
	
	@Transactional(readOnly = true)
	public Optional<MessageDB> getMessageDB(String code, String language) {
		return Optional.ofNullable(repository.findByCodeAndLanguage(code, language));
	}
	
	public Optional<MessageModel> getMessageDTO(String code, String language) {
		return getMessageDB(code, language).map(MessageMapper.INSTANCE::toDTO);
	}
	
	public String getMessage(String code, String language) {
		return getMessageDB(code, language).map(MessageDB::getMessage).orElse(null);
	}
	
	@Transactional(readOnly = true)
	public List<MessageDB> getMessagesDB(String code, String language) {
		return repository.findAllByCodeAndLanguage(code, language);
	}
	
	public List<MessageModel> getMessagesDTO(String code, String language) {
		return getMessagesDB(code, language)
			.stream()
			.map(MessageMapper.INSTANCE::toDTO)
			.collect(Collectors.toList());
	}
	
	@Transactional(readOnly = true)
	public List<MessageDB> getMessagesDB(String language) {
		return repository.findAllByLanguage(language);
	}
	
	public List<MessageModel> getMessagesDTO(String language) {
		return getMessagesDB(language)
			.stream()
			.map(MessageMapper.INSTANCE::toDTO)
			.collect(Collectors.toList());
	}
	
	public Map<String, Map<String, String>> getMessages(String language) {
		final Map<String, String> map = getMessagesDB(language)
			.stream()
			.map(MessageMapper.INSTANCE::toDTO)
			.collect(Collectors.toMap(MessageModel::getCode, MessageModel::getMessage));
		
		return Map.of(MESSAGE, map);
	}
	
	@Transactional(readOnly = true)
	List<MessageDB> getMessagesDB() {
		return repository.findAll();
	}
	
	@Transactional(readOnly = true)
	@VisibleForTesting
	long count() {
		return repository.count();
	}
}
