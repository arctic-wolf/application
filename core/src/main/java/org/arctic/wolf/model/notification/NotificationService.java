/*
 * Copyright (c) 2019 Reginald Ravenhorst <lordrex34@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.arctic.wolf.model.notification;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;

import lombok.RequiredArgsConstructor;
import org.arctic.wolf.mapper.NotificationMapper;
import org.arctic.wolf.model.notification.dto.Notification;
import org.arctic.wolf.model.user.UserDB;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.google.common.annotations.VisibleForTesting;

/**
 * @author lord_rex
 */
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@Transactional
@Service
public class NotificationService {
	private final NotificationDBRepository repository;
	
	public INotification addNotification(UserDB userDB, String icon, String title, String message) {
		final NotificationDB notificationDB = new NotificationDB();
		notificationDB.setUserDB(userDB);
		notificationDB.setIcon(icon);
		notificationDB.setTitle(title);
		notificationDB.setMessage(message);
		notificationDB.setDateTime(new Date());
		
		final NotificationDB result = repository.save(notificationDB);
		return NotificationMapper.INSTANCE.toDTO(result);
	}
	
	public void removeNotification(UserDB userDB, Long notificationId) {
		repository.deleteByUserDBAndId(userDB, notificationId);
	}
	
	@Transactional(readOnly = true)
	public List<NotificationDB> getNotificationsDB(UserDB userDB) {
		return repository.findByUserDB(userDB);
	}
	
	@Transactional(readOnly = true)
	public List<Notification> getNotificationsDTO(UserDB userDB) {
		return getNotificationsDB(userDB)
			.stream()
			.map(NotificationMapper.INSTANCE::toDTO)
			.collect(Collectors.toList());
	}
	
	@VisibleForTesting
	Optional<NotificationDB> findById(long id) {
		return repository.findById(id);
	}
	
	@VisibleForTesting
	void deleteAll() {
		repository.deleteAll();
	}
	
	@VisibleForTesting
	long count() {
		return repository.count();
	}
}
