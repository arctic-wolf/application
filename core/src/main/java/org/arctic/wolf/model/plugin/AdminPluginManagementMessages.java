/*
 * Copyright (c) 2018 Reginald Ravenhorst <lordrex34@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.arctic.wolf.model.plugin;

/**
 * @author lord_rex
 */
public interface AdminPluginManagementMessages {
	String ADMIN_UPDATE_PLUGIN_SUCCESS = "admin_plugins_update_success";
	String ADMIN_UNINSTALL_PLUGIN_SUCCESS = "admin_plugins_uninstall_success";
	String ADMIN_DELETE_PLUGIN_SUCCESS = "admin_plugins_delete_success";
	String ADMIN_DOWNLOAD_PLUGIN_SUCCESS = "admin_plugins_download_success";
	String ADMIN_DOWNLOAD_PLUGIN_FAIL = "admin_plugins_download_fail";
	String ADMIN_INSTALL_PLUGIN_SUCCESS = "admin_plugins_install_success";
	String ADMIN_START_PLUGIN_SUCCESS = "admin_plugins_start_success";
	String ADMIN_STOP_PLUGIN_SUCCESS = "admin_plugins_stop_success";
	String ADMIN_AUTO_START_PLUGIN_SUCCESS = "admin_plugins_auto_start_success";
}
