/*
 * Copyright (c) 2018 Reginald Ravenhorst <lordrex34@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.arctic.wolf.model.plugin;

import static org.arctic.wolf.model.plugin.PluginService.*;

import java.io.IOException;
import java.nio.file.Path;
import java.util.Map;
import java.util.Objects;

import com.github.unafraid.plugins.AbstractPlugin;
import lombok.Getter;
import lombok.Setter;
import org.arctic.wolf.HTML;
import org.arctic.wolf.model.content.ContentService;
import org.arctic.wolf.model.group.GroupService;
import org.arctic.wolf.model.message.MessageService;
import org.arctic.wolf.model.server.ServerSettingsService;
import org.arctic.wolf.model.user.UserService;
import org.arctic.wolf.model.widget.WidgetService;

/**
 * @author lord_rex
 */
@Setter
@Getter
public abstract class ArcticPlugin extends AbstractPlugin {
	private ContentService contentService;
	private GroupService groupService;
	private UserService userService;
	private WidgetService widgetService;
	private ServerSettingsService serverSettingsService;
	private MessageService messageService;
	
	public Path getHtmlRoot() {
		return PLUGINS_PATH.resolve(getName())
			.toAbsolutePath()
			.relativize(HTML.getRoot().toAbsolutePath());
	}
	
	public final RemotePlugin getRemotePlugin(Map<String, RemotePlugin> pluginInfoList) {
		return pluginInfoList.get(getJarPath().getFileName().toString());
	}
	
	public final boolean isUpToDate(Map<String, RemotePlugin> pluginInfoList) throws IOException {
		final RemotePlugin remotePlugin = getRemotePlugin(pluginInfoList);
		return remotePlugin == null || Objects.equals(remotePlugin.getJarFileHash(), getJarHash());
	}
}
