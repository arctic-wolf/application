/*
 * Copyright (c) 2018 Reginald Ravenhorst <lordrex34@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.arctic.wolf.model.plugin;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import jakarta.persistence.EntityManager;

import lombok.RequiredArgsConstructor;
import org.arctic.wolf.util.FuzzySearchUtil;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author lord_rex
 */
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@Service
@Transactional
public class PluginDBService {
	private final PluginDBRepository repository;
	private final EntityManager entityManager;
	
	public void update(PluginDB pluginDB) {
		repository.save(pluginDB);
	}
	
	public void delete(Long id) {
		repository.deleteById(id);
	}
	
	@Transactional(readOnly = true)
	public PluginDB findById(Long id) {
		return repository.findById(id).orElse(null);
	}
	
	@Transactional(readOnly = true)
	public PluginDB findByName(String name) {
		return repository.findByName(name);
	}
	
	@Transactional(readOnly = true)
	List<PluginDB> findAll() {
		return repository.findAll();
	}
	
	@Transactional(readOnly = true)
	public Page<PluginDB> findAll(Pageable pageable) {
		return repository.findAll(pageable);
	}
	
	public Page<PluginDB> pagedFuzzySearch(String search, Pageable pageable) {
		return FuzzySearchUtil.pagedFuzzySearch(pageable, entityManager, PluginDB.class, search, "name");
	}
	
	@Transactional(readOnly = true)
	public long count() {
		return repository.count();
	}
	
	void updateFromSite(IRemotePlugin remotePlugin) {
		final PluginDB pluginDB = findByName(remotePlugin.getName());
		if (pluginDB != null) {
			pluginDB.setVersion(remotePlugin.getVersion());
			update(pluginDB);
		}
	}
}
