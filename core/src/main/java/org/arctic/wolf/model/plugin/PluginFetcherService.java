/*
 * Copyright (c) 2018 Reginald Ravenhorst <lordrex34@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.arctic.wolf.model.plugin;

import static org.arctic.wolf.model.plugin.PluginService.PLUGINS_PATH;
import static org.arctic.wolf.model.server.ServerSettingsVariables.AUTO_UPDATE_PLUGINS;
import static org.arctic.wolf.model.server.ServerSettingsVariables.DEFAULT_AUTO_UPDATE_PLUGINS;
import static org.arctic.wolf.model.server.ServerSettingsVariables.DEFAULT_PLUGIN_SITE;
import static org.arctic.wolf.model.server.ServerSettingsVariables.DEFAULT_PLUGIN_SITE_PASSWORD;
import static org.arctic.wolf.model.server.ServerSettingsVariables.DEFAULT_PLUGIN_SITE_USERNAME;
import static org.arctic.wolf.model.server.ServerSettingsVariables.PLUGIN_SITE;
import static org.arctic.wolf.model.server.ServerSettingsVariables.PLUGIN_SITE_PASSWORD;
import static org.arctic.wolf.model.server.ServerSettingsVariables.PLUGIN_SITE_USERNAME;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.arctic.wolf.model.server.ServerSettingsService;
import org.arctic.wolf.util.JarFileUtil;
import org.arctic.wolf.util.RetrofitUtil;
import org.springframework.stereotype.Service;
import com.github.unafraid.plugins.exceptions.PluginException;
import com.github.unafraid.plugins.util.FileHashUtil;
import okhttp3.ResponseBody;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

/**
 * @author lord_rex
 */
@Slf4j
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@Service
public class PluginFetcherService {
	private static final File CACHE_DIR = new File(System.getProperty("java.io.tmpdir"), PluginFetcherService.class.getSimpleName());
	private static final String SLASH = "/";
	
	private final ServerSettingsService serverSettingsService;
	private final PluginDBService pluginDBService;
	
	private String getSiteURL() {
		final String pluginSite = serverSettingsService.getString(PLUGIN_SITE, DEFAULT_PLUGIN_SITE);
		return pluginSite.endsWith(SLASH) ? pluginSite : pluginSite + SLASH;
	}
	
	private String getSiteUsername() {
		return serverSettingsService.getString(PLUGIN_SITE_USERNAME, DEFAULT_PLUGIN_SITE_USERNAME);
	}
	
	private String getSitePassword() {
		return serverSettingsService.getString(PLUGIN_SITE_PASSWORD, DEFAULT_PLUGIN_SITE_PASSWORD);
	}
	
	private PluginRetrofit getRetrofit() {
		final Retrofit retrofit = RetrofitUtil.get(getSiteURL(), JacksonConverterFactory.create(), getSiteUsername(), getSitePassword(), CACHE_DIR, 45, TimeUnit.SECONDS);
		return retrofit.create(PluginRetrofit.class);
	}
	
	Map<String, RemotePlugin> getPluginInfoList() {
		try {
			return RetrofitUtil.getResponse(getRetrofit().getPluginInfoList());
		}
		catch (Exception e) {
			return Collections.emptyMap();
		}
	}
	
	void lookForInstalledPlugins() throws IOException {
		final List<String> names = pluginDBService.findAll().stream().map(PluginDB::getJarFileName).collect(Collectors.toList());
		if (names.isEmpty()) {
			return;
		}
		
		final List<RemotePlugin> response = RetrofitUtil.getResponse(getRetrofit().getPluginInfoBunch(RetrofitUtil.asUrlList(names)));
		response.forEach(remotePlugin -> {
			try {
				checkInstalledPlugin(remotePlugin);
			}
			catch (PluginException e) {
				LOGGER.warn("Failed to update plugin '{}'!", remotePlugin.getName(), e);
			}
			catch (IOException e) {
				LOGGER.warn("Plugin site '{}' is unreachable. Cannot fetch plugin '{}'!", getSiteURL(), remotePlugin.getName());
			}
		});
	}
	
	private void checkInstalledPlugin(RemotePlugin remotePlugin) throws PluginException, IOException {
		final String jarFileName = remotePlugin.getJarFileName();
		final Path jarFile = PLUGINS_PATH.resolve(jarFileName);
		
		if (Files.exists(jarFile)) {
			final String jarFileHash = FileHashUtil.getFileHash(jarFile).toString();
			if (!Objects.equals(jarFileHash, remotePlugin.getJarFileHash())) {
				LOGGER.info("Plugin '{}' is NOT up-to-date.", jarFileName);
				
				if (serverSettingsService.getBoolean(AUTO_UPDATE_PLUGINS, DEFAULT_AUTO_UPDATE_PLUGINS)) {
					updateInstalledPlugin(remotePlugin);
				}
			}
			else {
				LOGGER.info("Plugin '{}' is up-to-date.", jarFileName);
			}
		}
		else {
			LOGGER.info("Plugin '{}' is missing. Fetching ...", jarFileName);
			fetchPlugin(getSiteURL(), jarFileName, jarFile);
		}
	}
	
	private void updateInstalledPlugin(RemotePlugin remotePlugin) throws IOException, PluginException {
		final String jarFileName = remotePlugin.getJarFileName();
		final Path jarFile = PLUGINS_PATH.resolve(jarFileName);
		
		LOGGER.info("Updating plugin '{}' ...", remotePlugin.getName());
		
		Files.deleteIfExists(jarFile);
		fetchPlugin(getSiteURL(), jarFileName, jarFile);
		
		pluginDBService.updateFromSite(remotePlugin);
	}
	
	private void fetchPlugin(String downloadURL, String jarFileName, Path jarFile) {
		try {
			final Response<ResponseBody> response = getRetrofit()
				.download(jarFileName)
				.execute();
			
			RetrofitUtil.onResponseDownload(response, jarFile.toFile(), downloadURL);
		}
		catch (IOException e) {
			LOGGER.warn("", e);
		}
	}
	
	void fetchPlugin(String pluginName) {
		final String jarFileName = JarFileUtil.camelToHypenJarName(pluginName);
		final Path jarFile = PLUGINS_PATH.resolve(jarFileName);
		fetchPlugin(getSiteURL(), jarFileName, jarFile);
	}
}
