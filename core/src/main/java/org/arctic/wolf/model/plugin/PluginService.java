/*
 * Copyright (c) 2018 Reginald Ravenhorst <lordrex34@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.arctic.wolf.model.plugin;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.springframework.beans.factory.annotation.Autowired;

import lombok.extern.slf4j.Slf4j;
import org.arctic.wolf.model.content.ContentService;
import org.arctic.wolf.model.group.GroupService;
import org.arctic.wolf.model.link.dto.Link;
import org.arctic.wolf.model.message.MessageService;
import org.arctic.wolf.model.plugin.dto.Plugin;
import org.arctic.wolf.model.plugin.dto.PluginCSS;
import org.arctic.wolf.model.plugin.dto.PluginJavaScript;
import org.arctic.wolf.model.server.ServerSettingsService;
import org.arctic.wolf.model.user.UserService;
import org.arctic.wolf.model.widget.WidgetService;
import org.arctic.wolf.util.JarFileUtil;
import org.springframework.stereotype.Service;
import com.github.unafraid.plugins.AbstractPlugin;
import com.github.unafraid.plugins.PluginRepository;
import com.github.unafraid.plugins.PluginState;
import com.github.unafraid.plugins.exceptions.PluginException;
import com.github.unafraid.plugins.util.PathUtil;

/**
 * @author lord_rex
 */
@Slf4j
@Service
public class PluginService extends PluginRepository<ArcticPlugin> {
	private static final String CONTENT_REFERENCE_TAG = "[CR]";
	
	static final Path PLUGINS_PATH;
	
	static {
		final Path path = Paths.get("plugins");
		PLUGINS_PATH = Files.exists(path) ? path : PathUtil.getClassLocation(PluginService.class).resolve(path);
		
		try {
			Files.createDirectories(PLUGINS_PATH);
		}
		catch (IOException e) {
			LOGGER.warn("", e);
		}
	}
	
	private final PluginDBService pluginDBService;
	private final PluginFetcherService pluginFetcherService;
	private final ContentService contentService;
	private final GroupService groupService;
	private final UserService userService;
	private final WidgetService widgetService;
	private final ServerSettingsService serverSettingsService;
	private final MessageService messageService;
	
	@Autowired
	public PluginService(PluginDBService pluginDBService, PluginFetcherService pluginFetcherService, ContentService contentService, GroupService groupService, UserService userService, WidgetService widgetService, ServerSettingsService serverSettingsService, MessageService messageService) {
		super(PLUGINS_PATH, PluginService.class.getClassLoader());
		
		this.pluginDBService = pluginDBService;
		this.pluginFetcherService = pluginFetcherService;
		this.contentService = contentService;
		this.groupService = groupService;
		this.userService = userService;
		this.widgetService = widgetService;
		this.serverSettingsService = serverSettingsService;
		this.messageService = messageService;
	}
	
	synchronized void scan() {
		scan(ArcticPlugin.class);
		getAvailablePlugins().forEach(this::setServices);
	}
	
	@Override
	public synchronized void startAll() {
		final List<PluginDB> installedPlugins = pluginDBService.findAll();
		
		getAvailablePlugins()
			.filter(plugin -> installedPlugins
				.stream()
				.anyMatch(dbPlugin -> dbPlugin.getName().equalsIgnoreCase(plugin.getName())
					&& (dbPlugin.getVersion() == plugin.getVersion())))
			.forEach(plugin ->
			{
				if (plugin.setState(PluginState.INITIALIZED, PluginState.INSTALLED)) {
					final PluginDB dbPlugin = pluginDBService.findByName(plugin.getName());
					if ((dbPlugin != null) && dbPlugin.isAutoStart()) {
						try {
							plugin.start();
							LOGGER.info("System auto-started plugin '{}'.", plugin.getName());
						}
						catch (PluginException e) {
							LOGGER.warn("Failed to start plugin {}", plugin.getName(), e);
						}
					}
				}
			});
	}
	
	@Override
	public synchronized void stopAll() {
		getInstalledPlugins()
			.filter(plugin -> plugin.getState() == PluginState.STARTED)
			.forEach(plugin ->
			{
				try {
					plugin.stop();
					LOGGER.info("System stopped plugin '{}'.", plugin.getName());
				}
				catch (PluginException e) {
					LOGGER.warn("Failed to stop plugin {}", plugin.getName(), e);
				}
			});
	}
	
	List<RemotePlugin> getInstallableRemotePlugins() {
		return pluginFetcherService.getPluginInfoList()
			.values()
			.stream()
			.filter(p -> pluginDBService.findByName(p.getName()) == null)
			.collect(Collectors.toList());
	}
	
	private ArcticPlugin getInstalledPlugin(String name) {
		Objects.requireNonNull(name);
		
		return getInstalledPlugins()
			.filter(plugin -> name.equalsIgnoreCase(plugin.getName()))
			.findFirst()
			.orElse(null);
	}
	
	List<InstalledPluginView> getInstalledPluginList() {
		final List<InstalledPluginView> installedPlugin = new ArrayList<>();
		
		final Map<String, RemotePlugin> pluginInfoList = pluginFetcherService.getPluginInfoList();
		
		pluginDBService.findAll().forEach(p -> {
			final ArcticPlugin plugin = getInstalledPlugin(p.getName());
			if (plugin == null) {
				LOGGER.warn("Plugin {} exist as installed in the database, but not installed by the system!", p);
			}
			
			final InstalledPluginView view = new InstalledPluginView();
			view.setId(p.getId());
			view.setName(p.getName());
			view.setAuthor(plugin.getAuthor());
			view.setCreatedAt(plugin.getCreatedAt());
			view.setDescription(plugin.getDescription());
			view.setVersion(plugin.getVersion());
			try {
				view.setUpdateRequired(!plugin.isUpToDate(pluginInfoList));
			}
			catch (IOException e) {
				// ignore
			}
			view.setStarted(plugin.getState() == PluginState.STARTED);
			view.setAutoStart(p.isAutoStart());
			installedPlugin.add(view);
		});
		
		return installedPlugin;
	}
	
	private Stream<ArcticPlugin> getInstalledPlugins() {
		final List<PluginDB> installedPlugins = pluginDBService.findAll();
		
		return getAvailablePlugins()
			.filter(plugin -> installedPlugins
				.stream()
				.anyMatch(dbPlugin ->
					dbPlugin.getName().equalsIgnoreCase(plugin.getName())
						&& (dbPlugin.getVersion() == plugin.getVersion())));
	}
	
	public synchronized void installPlugin(String pluginName) throws PluginException {
		final PluginDB dbPlugin = pluginDBService.findByName(pluginName);
		if (dbPlugin != null) {
			throw new PluginException("Plugin is already installed!");
		}
		
		AbstractPlugin plugin = getAvailablePlugin(pluginName);
		if (plugin == null) {
			pluginFetcherService.fetchPlugin(pluginName);
			
			scan();
			
			plugin = getAvailablePlugin(pluginName);
			if (plugin == null) {
				throw new PluginException("No such plugin!");
			}
		}
		
		plugin.install();
		
		final PluginDB pluginDB = new PluginDB();
		pluginDB.setName(plugin.getName());
		pluginDB.setVersion(plugin.getVersion());
		pluginDB.setJarFileName(JarFileUtil.camelToHypenJarName(pluginName));
		pluginDBService.update(pluginDB);
		
		LOGGER.info("Plugin '{}' installed successfully.", pluginName);
	}
	
	public synchronized void uninstallPlugin(String pluginName) throws PluginException {
		final PluginDB dbPlugin = pluginDBService.findByName(pluginName);
		if (dbPlugin == null) {
			throw new PluginException("Plugin is not installed yet!");
		}
		
		final ArcticPlugin plugin = getInstalledPlugin(pluginName);
		if (plugin == null) {
			throw new PluginException("Plugin is not installed yet!");
		}
		
		if (plugin.getState() == PluginState.STARTED) {
			plugin.stop();
		}
		
		plugin.uninstall();
		pluginDBService.delete(dbPlugin.getId());
		
		LOGGER.info("Plugin '{}' uninstalled successfully.", pluginName);
	}
	
	public synchronized void updateAutoStart(String pluginName, boolean autoStart) throws PluginException {
		final PluginDB pluginDB = pluginDBService.findByName(pluginName);
		if (pluginDB == null) {
			throw new PluginException("Plugin is not installed yet!");
		}
		
		pluginDB.setAutoStart(autoStart);
		
		pluginDBService.update(pluginDB);
		
		LOGGER.info("Configured auto-start status '{}' for plugin '{}'.", autoStart, pluginName);
	}
	
	public synchronized void startPlugin(String pluginName) throws PluginException {
		final ArcticPlugin plugin = getInstalledPlugin(pluginName);
		plugin.start();
		LOGGER.info("Started plugin '{}'.", pluginName);
	}
	
	public synchronized void stopPlugin(String pluginName) throws PluginException {
		getInstalledPlugin(pluginName).stop();
		LOGGER.info("Stopped plugin '{}'.", pluginName);
	}
	
	@Override
	public synchronized void unload(ArcticPlugin plugin) throws PluginException {
		super.unload(plugin);
	}
	
	public synchronized void deletePlugin(String pluginName) throws PluginException {
		if (pluginDBService.findByName(pluginName) != null) {
			uninstallPlugin(pluginName);
		}
		
		unload(getAvailablePlugin(pluginName));
		
		try {
			final Path jarPath = PLUGINS_PATH.resolve(JarFileUtil.camelToHypenJarName(pluginName));
			Files.deleteIfExists(jarPath);
		}
		catch (IOException e) {
			// To be investigated in the future.
			LOGGER.warn("", e);
		}
		
		LOGGER.info("Deleted plugin '{}'.", pluginName);
	}
	
	public synchronized void downloadPlugin(String pluginName) throws IOException {
		pluginFetcherService.fetchPlugin(pluginName);
		LOGGER.info("Downloaded plugin '{}'.", pluginName);
		
		// Scan  in order to look for the newly downloaded plugins.
		scan();
	}
	
	public synchronized void updatePlugin(String pluginName) throws IOException, PluginException {
		final ArcticPlugin plugin = getInstalledPlugin(pluginName);
		if (plugin == null) {
			throw new PluginException("Plugin is not installed yet!");
		}
		
		if (plugin.isUpToDate(pluginFetcherService.getPluginInfoList())) {
			return;
		}
		
		deletePlugin(pluginName);
		downloadPlugin(pluginName);
		installPlugin(pluginName);
		
		LOGGER.info("Updated plugin '{}'.", pluginName);
	}
	
	private void setServices(ArcticPlugin plugin) {
		plugin.setContentService(contentService);
		plugin.setUserService(userService);
		plugin.setGroupService(groupService);
		plugin.setWidgetService(widgetService);
		plugin.setServerSettingsService(serverSettingsService);
		plugin.setMessageService(messageService);
	}
	
	public Collection<Plugin> getPlugins() {
		// TODO business logic here
		return Collections.emptyList();
	}
	
	public Plugin getPlugin(String pluginName) {
		// TODO business logic here
		return null;
	}
	
	public List<PluginCSS> getCSS(String lang) {
		// TODO business logic here
		return Collections.emptyList();
	}
	
	public List<PluginJavaScript> getJavaScripts(String lang) {
		// TODO business logic here
		return Collections.emptyList();
	}
	
	public List<Link> getNavbarLinks(String lang) {
		// TODO business logic here
		return Collections.emptyList();
	}
	
	public IPluginContentOwner processContent(IPluginContentOwner pluginContentOwner, String routeFullPath) {
		if (pluginContentOwner == null) {
			return null;
		}
		
		final String content = pluginContentOwner.getContent();
		if (isContentReference(content)) {
			final String pluginName = contentReferenceToPluginName(content);
			final Plugin plugin = getPlugin(pluginName);
			if (plugin != null) {
				return pluginContentOwner.withPluginContent(plugin.getContent(routeFullPath));
			}
		}
		
		return pluginContentOwner;
	}
	
	public static String pluginNameToContentReference(String pluginName) {
		return CONTENT_REFERENCE_TAG + pluginName + CONTENT_REFERENCE_TAG;
	}
	
	private static String contentReferenceToPluginName(String contentReference) {
		return contentReference.replace(CONTENT_REFERENCE_TAG, "");
	}
	
	private static boolean isContentReference(String content) {
		return content.startsWith(CONTENT_REFERENCE_TAG) && content.endsWith(CONTENT_REFERENCE_TAG);
	}
}
