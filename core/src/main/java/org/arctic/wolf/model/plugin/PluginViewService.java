/*
 * Copyright (c) 2018 Reginald Ravenhorst <lordrex34@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.arctic.wolf.model.plugin;

import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;

import lombok.extern.slf4j.Slf4j;
import org.arctic.wolf.util.FuzzySearchUtil;
import org.arctic.wolf.util.PageUtil;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

/**
 * @author lord_rex
 */
@Slf4j
@Service
public class PluginViewService {
	private final PluginService pluginService;
	private final PluginDBService pluginDBService;
	
	@Autowired
	public PluginViewService(PluginService pluginService, PluginDBService pluginDBService) {
		this.pluginService = pluginService;
		this.pluginDBService = pluginDBService;
	}
	
	public Page<RemotePlugin> pagedPluginRepository(Pageable pageable) {
		return PageUtil.getPagedView(pageable, pluginService.getInstallableRemotePlugins());
	}
	
	public Page<RemotePlugin> pagedFuzzySearchForNewPlugins(String search, Pageable pageable) {
		return FuzzySearchUtil.pagedFuzzySearch(pageable, pluginService.getInstallableRemotePlugins(), search);
	}
	
	public Page<InstalledPluginView> pagedInstalledPlugins(Pageable pageable) {
		return PageUtil.getPagedView(pageable, pluginService.getInstalledPluginList());
	}
	
	public Page<ArcticPlugin> pagedAvailablePlugins(Pageable pageable) {
		// Get the non-installed available plugins.
		final List<ArcticPlugin> availablePlugins = pluginService.getAvailablePlugins()
			.filter(plugin -> pluginDBService.findByName(plugin.getName()) == null)
			.collect(Collectors.toList());
		
		return PageUtil.getPagedView(pageable, availablePlugins);
	}
}
