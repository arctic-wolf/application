/*
 * Copyright (c) 2018 Reginald Ravenhorst <lordrex34@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.arctic.wolf.model.plugin.hooks;

import static org.arctic.wolf.HTML.*;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;

import com.github.unafraid.plugins.IPluginFunction;
import com.github.unafraid.plugins.exceptions.PluginException;
import com.github.unafraid.plugins.installers.file.FileInstaller;
import lombok.Getter;
import org.arctic.wolf.model.plugin.ArcticPlugin;

/**
 * @author lord_rex
 */
public class FragmentHookFunction<T extends ArcticPlugin> implements IPluginFunction<T> {
	@Getter
	private final T plugin;
	private final FileInstaller fileInstaller;
	
	public FragmentHookFunction(T plugin, FileInstaller fileInstaller) {
		this.plugin = plugin;
		this.fileInstaller = fileInstaller;
		
		loadAnnotations(LoadMode.FILE_INSTALLER);
	}
	
	private enum LoadMode {
		FILE_INSTALLER,
		HOOK_CONTAINER;
	}
	
	private void loadAnnotations(LoadMode mode) {
		for (Method method : plugin.getClass().getDeclaredMethods()) {
			for (Annotation annotation : method.getAnnotations()) {
				if (annotation instanceof FragmentHook) {
					registerHook((FragmentHook) annotation, mode);
				}
				else if (annotation instanceof FragmentHooks) {
					final FragmentHooks hooks = (FragmentHooks) annotation;
					for (FragmentHook hook : hooks.value()) {
						registerHook(hook, mode);
					}
				}
			}
		}
	}
	
	private void registerHook(FragmentHook annotation, LoadMode mode) {
		switch (mode) {
			case FILE_INSTALLER:
				final String sourceFile = annotation.path() + HTML_SUFFIX;
				final String destinationFile = plugin.getHtmlRoot() + sourceFile;
				
				fileInstaller.addFile(sourceFile, destinationFile);
				break;
			
			case HOOK_CONTAINER:
				FragmentHookContainer.register(new FragmentHookHolder(plugin.getName(), annotation.place(), annotation.path(), annotation.name()));
				break;
		}
	}
	
	@Override
	public void onStart() throws PluginException {
		loadAnnotations(LoadMode.HOOK_CONTAINER);
	}
	
	@Override
	public void onStop() throws PluginException {
		FragmentHookContainer.remove(plugin.getName());
	}
}
