/*
 * Copyright (c) 2018 Reginald Ravenhorst <lordrex34@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.arctic.wolf.model.plugin.messages;

import static org.arctic.wolf.model.server.ServerSettingsVariables.*;

import java.io.InputStream;
import java.util.Properties;

import com.github.unafraid.plugins.IPluginFunction;
import com.github.unafraid.plugins.exceptions.PluginException;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.arctic.wolf.model.plugin.ArcticPlugin;

/**
 * @author lord_rex
 */
@Slf4j
public class MessagesFunction<T extends ArcticPlugin> implements IPluginFunction<T> {
	private static final String FILE_SUFFIX = "_messages.properties";
	
	@Getter
	private final T plugin;
	private final Properties properties = new Properties();
	
	public MessagesFunction(T plugin) {
		this.plugin = plugin;
		
		loadDefaultMessages();
	}
	
	private void loadDefaultMessages() {
		final String filename = plugin.getName() + FILE_SUFFIX;
		try (InputStream resource = plugin.getClass().getResourceAsStream(filename)) {
			properties.load(resource);
		}
		catch (Exception e) {
			LOGGER.warn("Couldn't load '{}'!", filename);
		}
	}
	
	@Override
	public void onStart() throws PluginException {
		properties.entrySet().forEach(entry -> {
			final String code = String.valueOf(entry.getKey());
			final String message = String.valueOf(entry.getValue());
			plugin.getMessageService().addIfNotExist(code, DEFAULT_SITE_LANGUAGE, message);
		});
	}
	
	@Override
	public void onStop() throws PluginException {
		// do nothing
	}
}
