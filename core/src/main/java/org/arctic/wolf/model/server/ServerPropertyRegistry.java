/*
 * Copyright (c) 2018 Reginald Ravenhorst <lordrex34@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.arctic.wolf.model.server;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;

/**
 * @author lord_rex
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Slf4j
public final class ServerPropertyRegistry {
	private static final String DEFAULT_PREFIX = "DEFAULT_";
	
	private static final Set<String> NATIVE_PROPERTIES = new HashSet<>();
	private static final Map<String, String> CONTENT_VARIABLE_PROPERTIES = new LinkedHashMap<>();
	private static final Map<String, Set<ConstantServerProperty>> BY_GROUP_NAME = new LinkedHashMap<>();
	private static final Map<String, Object> PUBLIC_SERVER_PROPERTIES = new LinkedHashMap<>();
	
	static {
		register(ServerSettingsVariables.class, NATIVE_PROPERTIES);
	}
	
	public static synchronized void register(Class<?> clazz) {
		register(clazz, null);
	}
	
	private static synchronized void register(Class<?> clazz, Set<String> nativeProperties) {
		final Map<String, ConstantServerProperty> byPropertyName = new LinkedHashMap<>();
		
		final List<Field> fields = Arrays.asList(clazz.getDeclaredFields());
		for (Field field : fields) {
			final String fieldName = field.getName();
			if (fieldName.startsWith(DEFAULT_PREFIX)) {
				continue;
			}
			
			final ServerSettingsInfo info = field.getAnnotation(ServerSettingsInfo.class);
			if (info == null) {
				continue;
			}
			
			try {
				registerProperty(nativeProperties, byPropertyName, fields, field, fieldName, info);
			}
			catch (Exception e) {
				LOGGER.warn("Failed to register field {}!", fieldName, e);
			}
		}
		
		BY_GROUP_NAME.putAll(byPropertyName
			.values()
			.stream()
			.collect(Collectors.groupingBy(ConstantServerProperty::getGroupName, Collectors.toCollection(LinkedHashSet::new))));
	}
	
	private static synchronized void registerProperty(Set<String> nativeProperties, Map<String, ConstantServerProperty> byPropertyName, List<Field> fields, Field field, String fieldName, ServerSettingsInfo info) throws IllegalAccessException {
		final String propertyName = (String) field.get(null);
		final Field defaultValueField = fields
			.stream()
			.filter(f -> f.getName().startsWith(DEFAULT_PREFIX) && f.getName().contains(fieldName))
			.findFirst()
			.orElseThrow(() -> new NullPointerException("Field '" + fieldName + "' doesn't have a default value pair!"));
		final Class<?> defaultValueFieldType = defaultValueField.getType();
		final Object defaultValue = defaultValueField.get(null);
		final ServerPropertyType propertyType;
		String[] dropDownElements = {};
		if (defaultValueFieldType.isEnum()) {
			propertyType = ServerPropertyType.SELECT;
			for (Object enumConstant : defaultValueFieldType.getEnumConstants()) {
				dropDownElements = ArrayUtils.add(dropDownElements, String.valueOf(enumConstant));
			}
		}
		else if ((defaultValueFieldType == Boolean.class) || (defaultValueFieldType == Boolean.TYPE)) {
			propertyType = ServerPropertyType.RADIO_BUTTON;
		}
		else if ((defaultValueFieldType == Integer.class) || (defaultValueFieldType == Integer.TYPE)) {
			propertyType = ServerPropertyType.NUMBER;
		}
		else {
			propertyType = info.password() ? ServerPropertyType.PASSWORD : ServerPropertyType.TEXT;
		}
		
		if (NATIVE_PROPERTIES.contains(propertyName)) {
			LOGGER.warn("Property key '{}' tried to overwrite a native property. This is forbidden operation!", propertyName);
			return;
		}
		
		final ConstantServerProperty constantServerProperty = new ConstantServerProperty(info.groupName(), propertyType, propertyName, String.valueOf(defaultValue), dropDownElements);
		byPropertyName.put(constantServerProperty.getPropertyName(), constantServerProperty);
		if (nativeProperties != null) {
			nativeProperties.add(propertyName);
		}
		
		final String contentVariableName = info.contentVariableName();
		if (StringUtils.isNotEmpty(contentVariableName)) {
			CONTENT_VARIABLE_PROPERTIES.put(contentVariableName, propertyName);
		}
		
		if (info.publicProperty()) {
			PUBLIC_SERVER_PROPERTIES.put(propertyName, defaultValue);
		}
	}
	
	public static synchronized void unregister(Class<?> clazz) {
		// Get properties to be unregistered.
		final Set<String> propertiesToUnregister = new HashSet<>();
		for (Field field : clazz.getDeclaredFields()) {
			final ServerSettingsInfo info = field.getAnnotation(ServerSettingsInfo.class);
			if (info != null) {
				try {
					final String propertyName = (String) field.get(null);
					propertiesToUnregister.add(propertyName);
				}
				catch (IllegalAccessException e) {
					// ignore at this point
				}
			}
		}
		
		// Unregister properties.
		BY_GROUP_NAME
			.values()
			.forEach(constantServerProperties ->
				constantServerProperties.removeIf(p -> propertiesToUnregister.contains(p.getPropertyName())));
		
		// Remove empty groups.
		final Set<String> groupsToBeCleaned = new HashSet<>();
		BY_GROUP_NAME
			.entrySet()
			.stream()
			.filter(e -> e.getValue().isEmpty())
			.forEach(e -> groupsToBeCleaned.add(e.getKey()));
		groupsToBeCleaned.forEach(BY_GROUP_NAME::remove);
	}
	
	public static Set<String> getGroupNames() {
		return new TreeSet<>(BY_GROUP_NAME.keySet());
	}
	
	public static Set<ConstantServerProperty> getByGroupName(String groupName) {
		return BY_GROUP_NAME.get(groupName);
	}
	
	public static Set<Map.Entry<String, String>> getContentVariableProperties() {
		return Collections.unmodifiableSet(CONTENT_VARIABLE_PROPERTIES.entrySet());
	}
	
	public static Map<String, Object> getPublicServerProperties() {
		return Collections.unmodifiableMap(PUBLIC_SERVER_PROPERTIES);
	}
}
