/*
 * Copyright (c) 2017 Reginald Ravenhorst <lordrex34@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.arctic.wolf.model.server;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import jakarta.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;

import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.arctic.wolf.model.server.dto.PublicServerProperty;
import org.arctic.wolf.model.server.dto.form.ServerPropertiesForm;
import org.arctic.wolf.model.server.dto.form.ServerPropertyForm;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.github.lordrex34.config.lang.FieldParser;
import com.google.common.annotations.VisibleForTesting;

/**
 * @author lord_rex
 */
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@Service
@Transactional
public class ServerSettingsService {
	private static final String CONTENT_VARIABLE_PREFIX = "$";
	
	private final ServerSettingsDBRepository repository;
	
	@PostConstruct
	void onStartup() {
		// Init public properties on service startup.
		ServerPropertyRegistry.getPublicServerProperties().forEach(this::getProperty);
	}
	
	@Transactional(readOnly = true)
	public ServerSettingsDB findByName(String name) {
		return repository.findByName(name);
	}
	
	@Transactional(readOnly = true)
	public List<ServerSettingsDB> findAll() {
		return repository.findAll();
	}
	
	private ServerSettingsDB create(String name, String value) {
		final ServerSettingsDB serverSettings = new ServerSettingsDB();
		serverSettings.setName(name);
		serverSettings.setValue(value);
		return repository.save(serverSettings);
	}
	
	public void update(ServerSettingsDB serverSettings) {
		repository.save(serverSettings);
	}
	
	public void updateByForm(ServerPropertiesForm serverPropertiesForm) {
		final List<ServerPropertyForm> propertiesGroup = serverPropertiesForm.getPropertiesGroup();
		if (propertiesGroup != null && !propertiesGroup.isEmpty()) {
			propertiesGroup.forEach(serverPropertyForm -> setProperty(serverPropertyForm.getPropertyName(), serverPropertyForm.getActualValue()));
		}
	}
	
	public void delete(String name) {
		repository.deleteById(name);
	}
	
	private List<ServerProperty> getByGroupName(String groupName) {
		return ServerPropertyRegistry.getByGroupName(groupName).stream().map(constantServerProperty -> {
			final String propertyName = constantServerProperty.getPropertyName();
			final String defaultValue = constantServerProperty.getDefaultValue();
			return new ServerProperty(
				constantServerProperty.getGroupName(),
				constantServerProperty.getPropertyType(),
				propertyName,
				defaultValue,
				getProperty(propertyName, defaultValue),
				constantServerProperty.getDropDownElements()
			);
		}).collect(Collectors.toList());
	}
	
	public List<ServerPropertyForm> getFormByGroupName(String groupName) {
		return getByGroupName(groupName)
			.stream()
			.map(serverProperty -> {
				final ServerPropertyForm serverPropertyForm = new ServerPropertyForm();
				serverPropertyForm.setPropertyType(serverProperty.getPropertyType());
				serverPropertyForm.setPropertyName(serverProperty.getPropertyName());
				serverPropertyForm.setActualValue(serverProperty.getActualValue());
				serverPropertyForm.setDropDownElements(serverProperty.getDropDownElements());
				return serverPropertyForm;
			}).collect(Collectors.toList());
	}
	
	@Transactional(readOnly = true)
	public Map<String, String> getContentVariables() {
		final Map<String, String> contentVariables = new HashMap<>();
		
		ServerPropertyRegistry.getContentVariableProperties().forEach(e -> {
			final String contentVariableName = e.getKey();
			final String propertyKey = e.getValue();
			
			final ServerSettingsDB byName = findByName(propertyKey);
			if (byName != null) {
				contentVariables.put(CONTENT_VARIABLE_PREFIX + contentVariableName, byName.getValue());
			}
		});
		
		return contentVariables;
	}
	
	@Transactional(readOnly = true)
	public Set<PublicServerProperty> getPublicServerProperties() {
		final Set<PublicServerProperty> publicServerProperties = new LinkedHashSet<>();
		
		ServerPropertyRegistry.getPublicServerProperties().forEach((propertyName, propertyValue) -> {
			final ServerSettingsDB byName = findByName(propertyName);
			if (byName != null) {
				publicServerProperties.add(new PublicServerProperty(byName.getName(), byName.getValue()));
			}
		});
		
		return publicServerProperties;
	}
	
	public void setProperty(String key, Object value) {
		ServerSettingsDB serverSettings = findByName(key);
		if (serverSettings == null) {
			serverSettings = new ServerSettingsDB();
		}
		
		serverSettings.setName(key);
		
		if (value instanceof Collection) {
			final StringBuilder sb = new StringBuilder();
			final Collection<?> collection = (Collection) value;
			collection.forEach(element -> sb.append(element).append(","));
			serverSettings.setValue(sb.toString());
		}
		else {
			serverSettings.setValue(String.valueOf(value));
		}
		
		repository.save(serverSettings);
	}
	
	/**
	 * Gets the property from the database.<br>
	 * In case it doesn't exist it creates it with the given default value.
	 * @param key the given key used for lookup
	 * @param defaultValue the default value used for property creation
	 * @return property
	 */
	@VisibleForTesting
	String getProperty(String key, Object defaultValue) {
		ServerSettingsDB serverSettings = findByName(key);
		if (serverSettings == null) {
			serverSettings = create(key, defaultValue != null ? String.valueOf(defaultValue) : null);
		}
		
		return serverSettings.getValue();
	}
	
	public String getString(String key, String defaultValue) {
		try {
			return getProperty(key, defaultValue);
		}
		catch (Exception e) {
			return defaultValue;
		}
	}
	
	public boolean getBoolean(String key, boolean defaultValue) {
		try {
			return Boolean.parseBoolean(getProperty(key, defaultValue));
		}
		catch (Exception e) {
			return defaultValue;
		}
	}
	
	public <T extends Enum<T>> T getEnum(String key, Class<T> clazz, T defaultValue) {
		try {
			return Enum.valueOf(clazz, getProperty(key, defaultValue));
		}
		catch (Exception e) {
			return defaultValue;
		}
	}
	
	public int getInteger(String key, int defaultValue) {
		try {
			return Integer.parseInt(getProperty(key, defaultValue));
		}
		catch (Exception e) {
			return defaultValue;
		}
	}
	
	public byte getByte(String key, byte defaultValue) {
		try {
			return Byte.parseByte(getProperty(key, defaultValue));
		}
		catch (Exception e) {
			return defaultValue;
		}
	}
	
	public short getShort(String key, short defaultValue) {
		try {
			return Short.parseShort(getProperty(key, defaultValue));
		}
		catch (Exception e) {
			return defaultValue;
		}
	}
	
	public long getLong(String key, long defaultValue) {
		try {
			return Long.parseLong(getProperty(key, defaultValue));
		}
		catch (Exception e) {
			return defaultValue;
		}
	}
	
	public double getDouble(String key, double defaultValue) {
		try {
			return Double.parseDouble(getProperty(key, defaultValue));
		}
		catch (Exception e) {
			return defaultValue;
		}
	}
	
	public float getFloat(String key, float defaultValue) {
		try {
			return Float.parseFloat(getProperty(key, defaultValue));
		}
		catch (Exception e) {
			return defaultValue;
		}
	}
	
	public void deleteAll() {
		repository.deleteAll();
	}
	
	public <T> Set<T> getSet(String key, Class<T> clazz, Collection<T> defaultValue) {
		return (Set<T>) getCollection(key, clazz, HashSet::new, defaultValue);
	}
	
	public <T> List<T> getList(String key, Class<T> clazz, Collection<T> defaultValue) {
		return (List<T>) getCollection(key, clazz, ArrayList::new, defaultValue);
	}
	
	@SuppressWarnings("unchecked")
	public <T> Collection<T> getCollection(String key, Class<T> clazz, Supplier<Collection<T>> collectionSupplier, Collection<T> defaultValue) {
		try {
			final String property = getProperty(key, null);
			if (property != null) {
				final Collection<T> collection = collectionSupplier.get();
				
				for (String value : property.split(",")) {
					collection.add((T) FieldParser.get(clazz, value));
				}
				
				return collection;
			}
		}
		catch (Exception e) {
			// ignore
		}
		
		setProperty(key, defaultValue);
		return defaultValue;
	}
	
	public String processContentVariables(String text) {
		if (StringUtils.isEmpty(text)) {
			return text;
		}
		
		for (Map.Entry<String, String> entry : getContentVariables().entrySet()) {
			text = text.replace(entry.getKey(), entry.getValue());
		}
		
		return text;
	}
}
