/*
 * Copyright (c) 2017 Reginald Ravenhorst <lordrex34@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.arctic.wolf.model.server;

import static org.arctic.wolf.model.plugin.PluginControllerMappings.PLUGINS_URL_PREFIX;

import org.arctic.wolf.model.user.UserActivation;

/**
 * @author lord_rex
 */
public interface ServerSettingsVariables {
	@ServerSettingsInfo(groupName = ServerSettingsGroups.GENERAL, contentVariableName = "SITE_URL", publicProperty = true)
	String SITE_URL = "siteUrl";
	String DEFAULT_SITE_URL = "https://localhost:8443";
	
	@ServerSettingsInfo(groupName = ServerSettingsGroups.GENERAL, contentVariableName = "SITE_TITLE", publicProperty = true)
	String SITE_TITLE = "siteTitle";
	String DEFAULT_SITE_TITLE = "Arctic Wolf CMS";
	
	@ServerSettingsInfo(groupName = ServerSettingsGroups.GENERAL)
	String SITE_LANGUAGE = "siteLanguage";
	String DEFAULT_SITE_LANGUAGE = "en";
	
	@ServerSettingsInfo(groupName = ServerSettingsGroups.GENERAL)
	String USE_BROWSER_LANGUAGE = "useBrowserLanguage";
	boolean DEFAULT_USE_BROWSER_LANGUAGE = false;
	
	@ServerSettingsInfo(groupName = ServerSettingsGroups.GENERAL)
	String REPOSITORY_SERVER_URL = "repositoryServerUrl";
	String DEFAULT_REPOSITORY_SERVER_URL = "http://localhost:9090/api/v1/";
	
	@ServerSettingsInfo(groupName = ServerSettingsGroups.GENERAL)
	String REPOSITORY_SERVER_USERNAME = "repositoryServerUsername";
	String DEFAULT_REPOSITORY_SERVER_USERNAME = "admin";
	
	@ServerSettingsInfo(groupName = ServerSettingsGroups.GENERAL, password = true)
	String REPOSITORY_SERVER_PASSWORD = "repositoryServerPassword";
	String DEFAULT_REPOSITORY_SERVER_PASSWORD = "admin";
	
	@ServerSettingsInfo(groupName = ServerSettingsGroups.PLUGINS)
	String PLUGIN_SITE = "pluginSite";
	String DEFAULT_PLUGIN_SITE = "http://127.0.0.1:9090" + PLUGINS_URL_PREFIX;
	
	@ServerSettingsInfo(groupName = ServerSettingsGroups.PLUGINS)
	String PLUGIN_SITE_USERNAME = "pluginSiteUsername";
	String DEFAULT_PLUGIN_SITE_USERNAME = "username";
	
	@ServerSettingsInfo(groupName = ServerSettingsGroups.PLUGINS, password = true)
	String PLUGIN_SITE_PASSWORD = "pluginSitePassword";
	String DEFAULT_PLUGIN_SITE_PASSWORD = "password";
	
	@ServerSettingsInfo(groupName = ServerSettingsGroups.PLUGINS)
	String AUTO_UPDATE_PLUGINS = "autoUpdatePlugins";
	boolean DEFAULT_AUTO_UPDATE_PLUGINS = false;
	
	@ServerSettingsInfo(groupName = ServerSettingsGroups.THEMES)
	String THEME_SITE_USERNAME = "themeSiteUsername";
	String DEFAULT_THEME_SITE_USERNAME = "username";
	
	@ServerSettingsInfo(groupName = ServerSettingsGroups.THEMES, password = true)
	String THEME_SITE_PASSWORD = "themeSitePassword";
	String DEFAULT_THEME_SITE_PASSWORD = "password";
	
	@ServerSettingsInfo(groupName = ServerSettingsGroups.THEMES)
	String SITE_THEME = "siteTheme";
	String DEFAULT_SITE_THEME = "bloghome";
	
	@ServerSettingsInfo(groupName = ServerSettingsGroups.THEMES)
	String LOADING_THEME_COLOR = "loadingThemeColor";
	String DEFAULT_LOADING_THEME_COLOR = "light";
	
	@ServerSettingsInfo(groupName = ServerSettingsGroups.THEMES)
	String SITE_THEME_COLOR = "siteThemeColor";
	String DEFAULT_SITE_THEME_COLOR = "light";
	
	@ServerSettingsInfo(groupName = ServerSettingsGroups.THEMES)
	String ADMIN_THEME_COLOR = "adminThemeColor";
	String DEFAULT_ADMIN_THEME_COLOR = "light";
	
	@ServerSettingsInfo(groupName = ServerSettingsGroups.THEMES)
	String AUTO_UPDATE_THEMES = "autoUpdateThemes";
	boolean DEFAULT_AUTO_UPDATE_THEMES = false;
	
	String EXAMPLE_CONTENT_INSTALLED_ON = "exampleContentInstalledOn";
	String DEFAULT_EXAMPLE_CONTENT_INSTALLED_ON = "not installed";
	
	@ServerSettingsInfo(groupName = ServerSettingsGroups.REGISTRATION, publicProperty = true)
	String REGULAR_LOGIN_ALLOWED = "regularLoginAllowed";
	boolean DEFAULT_REGULAR_LOGIN_ALLOWED = true;
	
	@ServerSettingsInfo(groupName = ServerSettingsGroups.REGISTRATION, publicProperty = true)
	String REGULAR_REGISTRATION_ALLOWED = "regularRegistrationAllowed";
	boolean DEFAULT_REGULAR_REGISTRATION_ALLOWED = true;
	
	@ServerSettingsInfo(groupName = ServerSettingsGroups.REGISTRATION, publicProperty = true)
	String RECAPTCHA_ALLOWED = "reCaptchaVerificationAllowed";
	boolean DEFAULT_RECAPTCHA_ALLOWED = true;
	
	@ServerSettingsInfo(groupName = ServerSettingsGroups.REGISTRATION, publicProperty = true)
	String RECAPTCHA_SITE_KEY = "reCaptchaSiteKey";
	String DEFAULT_RECAPTCHA_SITE_KEY = "6Le8eTkUAAAAABnjI6WK7_RgBhr6QszXroH1Pewr";
	
	@ServerSettingsInfo(groupName = ServerSettingsGroups.REGISTRATION)
	String RECAPTCHA_SECRET = "reCaptchaSecret";
	String DEFAULT_RECAPTCHA_SECRET = "6Le8eTkUAAAAAGvGIEqN4lINXDpLokuWtl3NwIY_";
	
	@ServerSettingsInfo(groupName = ServerSettingsGroups.REGISTRATION, publicProperty = true)
	String SSO_LOGIN_ALLOWED = "ssoLoginAllowed";
	boolean DEFAULT_SSO_LOGIN_ALLOWED = true;
	
	@ServerSettingsInfo(groupName = ServerSettingsGroups.REGISTRATION, publicProperty = true)
	String SSO_FACEBOOK_LOGIN_ALLOWED = "ssoFacebookLoginAllowed";
	boolean DEFAULT_SSO_FACEBOOK_LOGIN_ALLOWED = true;
	
	@ServerSettingsInfo(groupName = ServerSettingsGroups.REGISTRATION)
	String FACEBOOK_APP_ID = "facebookAppID";
	String DEFAULT_FACEBOOK_APP_ID = "1518199391609316";
	
	@ServerSettingsInfo(groupName = ServerSettingsGroups.REGISTRATION)
	String FACEBOOK_APP_SECRET = "facebookAppSecret";
	String DEFAULT_FACEBOOK_APP_SECRET = "37860b769db6a2381fa692dbaa7572d7";
	
	@ServerSettingsInfo(groupName = ServerSettingsGroups.REGISTRATION, publicProperty = true)
	String FACEBOOK_AUTH_URL = "facebookAuthURL";
	String DEFAULT_FACEBOOK_AUTH_URL = "http://localhost:8080/oauth2/authorize/facebook?redirect_uri=http://localhost:3000/%23/oauth2-redirect";
	
	@ServerSettingsInfo(groupName = ServerSettingsGroups.REGISTRATION, publicProperty = true)
	String SSO_GOOGLE_LOGIN_ALLOWED = "ssoGoogleLoginAllowed";
	boolean DEFAULT_SSO_GOOGLE_LOGIN_ALLOWED = true;
	
	@ServerSettingsInfo(groupName = ServerSettingsGroups.REGISTRATION)
	String GOOGLE_APP_ID = "googleAppID";
	String DEFAULT_GOOGLE_APP_ID = "181419433313-7s2rnn78qs817a0h9vkvlbe4v7a8gnj7.apps.googleusercontent.com";
	
	@ServerSettingsInfo(groupName = ServerSettingsGroups.REGISTRATION)
	String GOOGLE_APP_SECRET = "googleAppSecret";
	String DEFAULT_GOOGLE_APP_SECRET = "PjNvcG79_zHOjgIhbPvykWG9";
	
	@ServerSettingsInfo(groupName = ServerSettingsGroups.REGISTRATION, publicProperty = true)
	String GOOGLE_AUTH_URL = "googleAuthURL";
	String DEFAULT_GOOGLE_AUTH_URL = "http://localhost:8080/oauth2/authorize/google?redirect_uri=http://localhost:3000/%23/oauth2-redirect";
	
	@ServerSettingsInfo(groupName = ServerSettingsGroups.REGISTRATION)
	String AUTHORIZED_REDIRECT_URIS = "authorizedRedirectURIs";
	String DEFAULT_AUTHORIZED_REDIRECT_URIS = "http://localhost:3000/%23/oauth2-redirect";
	
	@ServerSettingsInfo(groupName = ServerSettingsGroups.REGISTRATION)
	String COOKIE_EXPIRE_SECONDS = "cookieExpireSeconds";
	int DEFAULT_COOKIE_EXPIRE_SECONDS = 1200;
	
	@ServerSettingsInfo(groupName = ServerSettingsGroups.REGISTRATION)
	String TOKEN_LIFE_TIME_IN_SECONDS = "tokenLifeTimeInSeconds";
	int DEFAULT_TOKEN_LIFE_TIME_IN_SECONDS = 1200;
	
	@ServerSettingsInfo(groupName = ServerSettingsGroups.REGISTRATION)
	String USER_ACTIVATION = "userActivation";
	UserActivation DEFAULT_USER_ACTIVATION = UserActivation.EMAIL;
	
	@ServerSettingsInfo(groupName = ServerSettingsGroups.REGISTRATION)
	String SUPPORT_EMAIL = "supportEmail";
	String DEFAULT_SUPPORT_EMAIL = "arctic.wolf.example@gmail.com";
	
	@ServerSettingsInfo(groupName = ServerSettingsGroups.REGISTRATION)
	String USER_GROUP_NAME = "userGroupName";
	String DEFAULT_USER_GROUP_NAME = "users";
	
	@ServerSettingsInfo(groupName = ServerSettingsGroups.CONTENT, publicProperty = true)
	String POST_READ_MORE = "postReadMore";
	boolean DEFAULT_POST_READ_MORE = true;
	
	@ServerSettingsInfo(groupName = ServerSettingsGroups.CONTENT, publicProperty = true)
	String READ_MORE_CHARACTER_LIMIT = "readMoreCharacterLimit";
	int DEFAULT_READ_MORE_CHARACTER_LIMIT = 300;
	
	@ServerSettingsInfo(groupName = ServerSettingsGroups.CONTENT, publicProperty = true)
	String HOME_CONTENT_PAGE_ID = "homeContentPageId";
	int DEFAULT_HOME_CONTENT_PAGE_ID = -1;
	
	@ServerSettingsInfo(groupName = ServerSettingsGroups.MAIL, contentVariableName = "GENERAL_CONTACT")
	String GENERAL_CONTACT = "generalContact";
	String DEFAULT_GENERAL_CONTACT = "arctic.wolf.example@gmail.com";
	
	@ServerSettingsInfo(groupName = ServerSettingsGroups.MAIL)
	String SMTP_HOST = "smtpHost";
	String DEFAULT_SMTP_HOST = "smtp.gmail.com";
	
	@ServerSettingsInfo(groupName = ServerSettingsGroups.MAIL)
	String SMTP_PORT = "smtpPort";
	int DEFAULT_SMTP_PORT = 465;
	
	@ServerSettingsInfo(groupName = ServerSettingsGroups.MAIL)
	String SMTP_USERNAME = "smtpUsername";
	String DEFAULT_SMTP_USERNAME = "arctic.wolf.example@gmail.com";
	
	@ServerSettingsInfo(groupName = ServerSettingsGroups.MAIL, password = true)
	String SMTP_PASSWORD = "smtpPassword";
	String DEFAULT_SMTP_PASSWORD = "my_example_password01";
	
	@ServerSettingsInfo(groupName = ServerSettingsGroups.MAIL)
	String SMTP_PROTOCOL = "smtpProtocol";
	String DEFAULT_SMTP_PROTOCOL = "smtps";
	
	@ServerSettingsInfo(groupName = ServerSettingsGroups.MAIL)
	String SMTP_MAIL_SMTPS_AUTH = "smtpMailSmtpsAuth";
	boolean DEFAULT_SMTP_MAIL_SMTPS_AUTH = true;
	
	@ServerSettingsInfo(groupName = ServerSettingsGroups.MAIL)
	String SMTP_MAIL_SMTPS_STARTTLS_ENABLE = "smtpMailSmtpsStartTlsEnable";
	boolean DEFAULT_SMTP_MAIL_SMTPS_STARTTLS_ENABLE = true;
	
	@ServerSettingsInfo(groupName = ServerSettingsGroups.MAIL)
	String SMTP_MAIL_SMTPS_TIMEOUT = "smtpMailSmtpsTimeout";
	int DEFAULT_SMTP_MAIL_SMTPS_TIMEOUT = 8_000;
	
	@ServerSettingsInfo(groupName = ServerSettingsGroups.MAIL)
	String SMTP_MAIL_TRANSPORT_PROTOCOL = "smtpMailProtocol";
	String DEFAULT_SMTP_MAIL_TRANSPORT_PROTOCOL = "smtps";
}
