/*
 * Copyright (c) 2019 Reginald Ravenhorst <lordrex34@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.arctic.wolf.model.system;

import org.springframework.beans.factory.annotation.Autowired;

import lombok.RequiredArgsConstructor;
import org.arctic.wolf.SiteApplication;
import org.arctic.wolf.model.plugin.PluginService;
import org.arctic.wolf.model.plugin.PluginState;
import org.arctic.wolf.model.system.dto.LoadingStatus;
import org.springframework.stereotype.Service;

/**
 * @author lord_rex
 */
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@Service
public class SystemService {
	private final PluginService pluginService;
	
	public boolean isLoading() {
		return SiteApplication.LOADING.get();
	}
	
	public LoadingStatus getLoadingStatus() {
		// current plugin count loaded
		final int current = (int) pluginService.getPlugins()
			.stream()
			.filter(plugin -> plugin.getState() == PluginState.STARTED)
			.count();
		
		// max
		final int max = pluginService.getPlugins().size();
		
		return new LoadingStatus(current, max, LoadingStatus.Result.NONE.getVariant());
	}
}
