/*
 * Copyright (c) 2019 Reginald Ravenhorst <lordrex34@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.arctic.wolf.model.theme;

import static org.arctic.wolf.util.JarFileUtil.META_INF;
import static org.arctic.wolf.util.JarFileUtil.META_INF_MANIFEST_MF;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.arctic.wolf.exception.ThemeEventException;
import org.arctic.wolf.mapper.ThemeMapper;
import org.arctic.wolf.model.theme.dto.Theme;
import org.arctic.wolf.util.JarFileUtil;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.FileSystemUtils;

/**
 * @author lord_rex
 */
@Slf4j
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@Service
@Transactional
class ThemeInstaller {
	private final ThemeDBRepository repository;
	
	void install(Theme theme) {
		try {
			final String name = theme.getName();
			final Path themePath = Themes.getPath(theme);
			final Path jarFile = Themes.getJarFile(theme);
			
			// Delete theme if it's exist in the database.
			repository.findById(name).ifPresent(repository::delete);
			
			// Delete the theme's installation directory if it's exists.
			FileSystemUtils.deleteRecursively(themePath);
			
			// Create empty theme installation directory.
			Files.createDirectories(themePath);
			
			// Extract the theme from the JAR to the theme installation directory.
			JarFileUtil.extract(jarFile, themePath, Collections.emptyList(), List.of(META_INF, META_INF_MANIFEST_MF));
			
			// Register the theme in the database.
			final ThemeDB themeDB = ThemeMapper.INSTANCE.toDB(theme);
			repository.save(themeDB);
			
			LOGGER.debug("Theme '{}' installed.", name);
		}
		catch (Exception e) {
			throw new ThemeEventException(e);
		}
	}
	
	void uninstall(Theme theme) {
		try {
			final String name = theme.getName();
			final Path themePath = Themes.getPath(theme);
			
			// Delete theme if it's exist in the database.
			repository.findById(name).ifPresent(repository::delete);
			
			// Delete the theme's installation directory if it's exists.
			FileSystemUtils.deleteRecursively(themePath);
			
			LOGGER.debug("Theme '{}' uninstalled.", name);
		}
		catch (Exception e) {
			throw new ThemeEventException(e);
		}
	}
	
	List<Theme> getInstalledThemes() {
		return repository.findAll()
			.stream()
			.map(ThemeMapper.INSTANCE::toDTO)
			.collect(Collectors.toList());
	}
	
	Optional<Theme> getInstalledTheme(String name) {
		return repository.findById(name)
			.map(ThemeMapper.INSTANCE::toDTO);
	}
}
