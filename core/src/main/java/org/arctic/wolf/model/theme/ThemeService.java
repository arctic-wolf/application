/*
 * Copyright (c) 2018 Reginald Ravenhorst <lordrex34@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.arctic.wolf.model.theme;

import static org.arctic.wolf.util.PageUtil.X_PAGE_COUNT;
import static org.arctic.wolf.util.PageUtil.X_TOTAL_COUNT;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import jakarta.servlet.http.HttpServletResponse;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.arctic.wolf.exception.ThemeEventException;
import org.arctic.wolf.model.ExternalComponentService;
import org.arctic.wolf.model.theme.dto.Theme;
import org.arctic.wolf.repository.server.RepositoryServerService;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.FileSystemUtils;

/**
 * @author lord_rex
 */
@Slf4j
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@Service
@Transactional
public class ThemeService extends ExternalComponentService<Theme> {
	private final ThemeInstaller themeInstaller;
	private final RepositoryServerService repositoryServerService;
	
	@Order(1)
	@EventListener(ApplicationReadyEvent.class)
	public void onStartup() {
		loadAll();
	}
	
	@Override
	protected Theme createNewComponent() {
		return new Theme();
	}
	
	@Override
	protected List<Path> getJARs() throws IOException {
		return Themes.getThemeJARs();
	}
	
	@Override
	protected Path getPath(Theme externalComponent) {
		return Themes.getPath(externalComponent);
	}
	
	@Override
	protected Path getJarFile(Theme externalComponent) {
		return Themes.getJarFile(externalComponent);
	}
	
	public synchronized void installTheme(String name) {
		getAvailableComponent(name).ifPresentOrElse(themeInstaller::install, () -> {
			throw new ThemeEventException("Theme '" + name + "' does not exist, so it cannot be installed.");
		});
	}
	
	public synchronized void uninstallTheme(String name) {
		getInstalledTheme(name).ifPresentOrElse(themeInstaller::uninstall, () -> {
			throw new ThemeEventException("Theme '" + name + "' is not installed, so it cannot be uninstalled.");
		});
	}
	
	public List<Theme> getInstalledThemes() {
		return themeInstaller.getInstalledThemes();
	}
	
	public Optional<Theme> getInstalledTheme(String name) {
		return themeInstaller.getInstalledTheme(name);
	}
	
	public synchronized void setTheme(String name) {
		getInstalledTheme(name).ifPresentOrElse(theme -> {
			final Path themePath = Themes.getPath(theme);
			final Path currentThemePath = Themes.getCurrentThemePath();
			
			setTheme(themePath, currentThemePath);
			LOGGER.debug("Theme '{}' is active now.", name);
		}, () -> {
			throw new ThemeEventException("Theme '" + name + "' is not installed, so it cannot be activated.");
		});
	}
	
	public synchronized void restoreDefaultTheme() {
		final Path themePath = Themes.getDefaultThemeBackupPath();
		final Path currentThemePath = Themes.getCurrentThemePath();
		
		setTheme(themePath, currentThemePath);
		
		LOGGER.debug("Default theme is restored.");
	}
	
	private static void setTheme(Path themePath, Path currentThemePath) {
		try {
			// Clear current theme directory.
			FileSystemUtils.deleteRecursively(currentThemePath);
			
			// Copy the chosen theme to current theme directory.
			FileSystemUtils.copyRecursively(themePath, currentThemePath);
		}
		catch (Exception e) {
			throw new ThemeEventException(e);
		}
	}
	
	public synchronized void deleteTheme(String name) {
		getAvailableComponent(name).ifPresentOrElse(theme -> {
			unloadComponent(theme);
			
			final Path jarFilePath = Themes.getJarFile(theme);
			try {
				Files.deleteIfExists(jarFilePath);
			}
			catch (IOException e) {
				throw new ThemeEventException(e);
			}
			
			LOGGER.debug("Theme '{}' deleted.", name);
		}, () -> {
			throw new ThemeEventException("Theme '" + name + "' does not exist, so it cannot be deleted.");
		});
	}
	
	public synchronized void remoteDownloadTheme(String name) {
		final Path destinationFile = Themes.getRoot().resolve(name + ".jar");
		
		if (Files.exists(destinationFile)) {
			deleteTheme(name);
		}
		
		repositoryServerService.download("themes/" + name + "/download", null, destinationFile.toFile());
		LOGGER.debug("Theme '{}' downloaded.", name);
		
		loadComponent(destinationFile);
	}
	
	public Theme[] getRemoteThemeInfos(HttpServletResponse response, int page) {
		final ResponseEntity<Theme[]> responseEntity = repositoryServerService.exchange("themes/all/info?page=" + page, HttpMethod.GET, null, null, Theme[].class);
		final HttpHeaders headers = responseEntity.getHeaders();
		response.setHeader(X_PAGE_COUNT, headers.getFirst(X_PAGE_COUNT));
		response.setHeader(X_TOTAL_COUNT, headers.getFirst(X_TOTAL_COUNT));
		return responseEntity.getBody();
	}
	
	public Theme getRemoteThemeInfo(String name) {
		return repositoryServerService.exchange("themes/" + name + "/info", HttpMethod.GET, null, null, Theme.class).getBody();
	}
	
	public Theme[] remoteFindTheme(String search, HttpServletResponse response, int page) {
		final ResponseEntity<Theme[]> responseEntity = repositoryServerService.exchange("themes/find/" + search + "?page=" + page, HttpMethod.GET, null, null, Theme[].class);
		final HttpHeaders headers = responseEntity.getHeaders();
		response.setHeader(X_PAGE_COUNT, headers.getFirst(X_PAGE_COUNT));
		response.setHeader(X_TOTAL_COUNT, headers.getFirst(X_TOTAL_COUNT));
		return responseEntity.getBody();
	}
	
	public byte[] remotePreviewImage(String name) {
		return repositoryServerService.exchange("themes/" + name + "/preview-image", HttpMethod.GET, null, null, byte[].class).getBody();
	}
	
	public synchronized void updateTheme(String name) {
		uninstallTheme(name);
		deleteTheme(name);
		remoteDownloadTheme(name);
		installTheme(name);
	}
}
