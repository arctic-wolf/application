/*
 * Copyright (c) 2017 Reginald Ravenhorst <lordrex34@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.arctic.wolf.model.user;

/**
 * @author lord_rex
 */
public interface AdminUserManagementMessages {
	String ADMIN_ADD_NEW_USER_USERNAME_ALREADY_EXIST = "admin_users_add_new_username_already_exists";
	String ADMIN_ADD_NEW_USER_EMAIL_ALREADY_USED = "admin_users_add_new_email_already_used";
	String ADMIN_ADD_NEW_USER_SUCCESS = "admin_users_add_new_success";
	
	String ADMIN_EDIT_USER_USERNAME_ALREADY_EXIST = "admin_users_edit_username_already_exists";
	String ADMIN_EDIT_USER_EMAIL_ALREADY_USED = "admin_users_edit_email_already_used";
	String ADMIN_EDIT_USER_SUCCESS = "admin_users_edit_success";
	
	String ADMIN_DELETE_USER_SUCCESS = "admin_users_delete_success";
}
