/*
 * Copyright (c) 2017 Reginald Ravenhorst <lordrex34@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.arctic.wolf.model.user;

import java.util.Date;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.arctic.wolf.model.group.Authority;
import org.springframework.security.config.oauth2.client.CommonOAuth2Provider;

/**
 * @author lord_rex
 */
public interface IUser {
	void setId(Long id);
	
	Long getId();
	
	void setUsername(String username);
	
	String getUsername();
	
	void setPassword(String password);
	
	String getPassword();
	
	void setEmail(String email);
	
	String getEmail();
	
	void setDisplayName(String displayName);
	
	String getDisplayName();
	
	void setBirthDate(Date birthDate);
	
	Date getBirthDate();
	
	void setCreateTime(Date createTime);
	
	Date getCreateTime();
	
	void setImageUrl(String imageUrl);
	
	String getImageUrl();
	
	void setBio(String bio);
	
	String getBio();
	
	void setActive(boolean active);
	
	boolean isActive();
	
	void setProvider(CommonOAuth2Provider provider);
	
	CommonOAuth2Provider getProvider();
	
	void setProviderId(String providerId);
	
	String getProviderId();
	
	void setAuthorities(Set<Authority> authorities);
	
	Set<Authority> getAuthorities();
	
	default void updateFrom(IUser user) {
		setUsername(user.getUsername());
		
		final String password = user.getPassword();
		if (StringUtils.isNotEmpty(password)) {
			setPassword(password);
		}
		
		setEmail(user.getEmail());
		setDisplayName(user.getDisplayName());
		setBirthDate(user.getBirthDate());
		setCreateTime(user.getCreateTime());
		setImageUrl(user.getImageUrl());
		setBio(user.getBio());
		setActive(user.isActive());
		setProvider(user.getProvider());
		setProviderId(user.getProviderId());
		setAuthorities(user.getAuthorities());
	}
}
