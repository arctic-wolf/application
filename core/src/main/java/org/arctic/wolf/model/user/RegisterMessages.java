/*
 * Copyright (c) 2017 Reginald Ravenhorst <lordrex34@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.arctic.wolf.model.user;

/**
 * @author lord_rex
 */
public interface RegisterMessages {
	String REGISTER_USERNAME_ALREADY_EXIST = "register_username_already_exists";
	String REGISTER_EMAIL_ALREADY_USED = "register_email_already_used";
	
	String REGISTER_BAD_CAPTCHA = "register_bad_captcha";
	
	String REGISTER_EMAIL_ACTIVATION_TILE = "register_email_activation_title";
	String REGISTER_EMAIL_ACTIVATION_TEXT = "register_email_activation_text";
	String REGISTER_SUCCESS = "register_success";
	String REGISTER_WAIT_FOR_MAIL = "register_wait_for_mail";
	String REGISTER_WAIT_FOR_ADMIN = "register_wait_for_admin";
}
