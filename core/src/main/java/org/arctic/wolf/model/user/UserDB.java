/*
 * Copyright (c) 2017 Reginald Ravenhorst <lordrex34@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.arctic.wolf.model.user;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.JoinTable;
import jakarta.persistence.Lob;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.OneToMany;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;
import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.ArrayUtils;
import org.arctic.wolf.model.group.Authority;
import org.arctic.wolf.model.group.GroupDB;
import org.arctic.wolf.model.notification.NotificationDB;
import org.arctic.wolf.model.user.verification.UserVerificationTokenDB;
import org.springframework.security.config.oauth2.client.CommonOAuth2Provider;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

/**
 * @author lord_rex
 */
@Entity
@Table(name = "users")
@Setter
@Getter
@NoArgsConstructor
@EqualsAndHashCode(of = { "id", "username", "email" })
public class UserDB implements IUser {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(unique = true, nullable = false)
	private Long id;
	
	@Column(unique = true, nullable = false)
	private String username;
	
	@Column(nullable = false)
	private String password;
	
	@Column(unique = true, nullable = false)
	private String email;
	
	@Column
	private String displayName;
	
	@Temporal(TemporalType.DATE)
	@Column(nullable = false)
	private Date birthDate;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(nullable = false)
	private Date createTime = new Date();
	
	@Lob
	@Column
	private String imageUrl;
	
	@Lob
	@Column
	private String bio;
	
	@Column
	private boolean active = false;
	
	@Column
	private CommonOAuth2Provider provider;
	
	@Column
	private String providerId;
	
	@ManyToMany(targetEntity = GroupDB.class, fetch = FetchType.EAGER)
	@JoinTable(
		name = UserGroupVariables.TABLE_NAME,
		joinColumns = @JoinColumn(name = UserGroupVariables.USER_ID, referencedColumnName = UserGroupVariables.ID),
		inverseJoinColumns = @JoinColumn(name = UserGroupVariables.GROUP_ID, referencedColumnName = UserGroupVariables.ID))
	private Set<GroupDB> groups = new HashSet<>();
	
	@OneToOne(mappedBy = "userDB", cascade = CascadeType.ALL, orphanRemoval = true)
	private UserVerificationTokenDB verificationToken;
	
	@OneToMany(mappedBy = "userDB", cascade = CascadeType.ALL, orphanRemoval = true)
	private Set<NotificationDB> notifications = new HashSet<>();
	
	@Override
	public void setAuthorities(Set<Authority> authorities) {
		// do nothing, use group system for this modification instead
	}
	
	@Override
	public Set<Authority> getAuthorities() {
		final Set<Authority> authorities = new HashSet<>();
		getGroups().forEach(groupDB -> authorities.addAll(groupDB.getAuthorities()));
		return authorities;
	}
	
	public boolean hasAuthority(Authority authority) {
		return getAuthorities().contains(authority);
	}
	
	public boolean hasAnyAuthority(Authority... authorities) {
		return getAuthorities().stream()
			.anyMatch(authority -> ArrayUtils.contains(authorities, authority));
	}
	
	public Set<SimpleGrantedAuthority> getGrantedAuthorities() {
		return getAuthorities().stream()
			.map(Authority::getGrantedAuthority)
			.collect(Collectors.toSet());
	}
}
