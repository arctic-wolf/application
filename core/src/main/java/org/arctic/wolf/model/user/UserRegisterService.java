/*
 * Copyright (c) 2019 Reginald Ravenhorst <lordrex34@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.arctic.wolf.model.user;

import static org.arctic.wolf.model.server.ServerSettingsVariables.DEFAULT_RECAPTCHA_ALLOWED;
import static org.arctic.wolf.model.server.ServerSettingsVariables.DEFAULT_SITE_URL;
import static org.arctic.wolf.model.server.ServerSettingsVariables.DEFAULT_SUPPORT_EMAIL;
import static org.arctic.wolf.model.server.ServerSettingsVariables.DEFAULT_USER_ACTIVATION;
import static org.arctic.wolf.model.server.ServerSettingsVariables.DEFAULT_USER_GROUP_NAME;
import static org.arctic.wolf.model.server.ServerSettingsVariables.RECAPTCHA_ALLOWED;
import static org.arctic.wolf.model.server.ServerSettingsVariables.SITE_URL;
import static org.arctic.wolf.model.server.ServerSettingsVariables.SUPPORT_EMAIL;
import static org.arctic.wolf.model.server.ServerSettingsVariables.USER_ACTIVATION;
import static org.arctic.wolf.model.server.ServerSettingsVariables.USER_GROUP_NAME;
import static org.arctic.wolf.model.user.RegisterMessages.REGISTER_EMAIL_ACTIVATION_TEXT;
import static org.arctic.wolf.model.user.RegisterMessages.REGISTER_EMAIL_ACTIVATION_TILE;

import org.springframework.beans.factory.annotation.Autowired;

import lombok.RequiredArgsConstructor;
import org.apache.http.ParseException;
import org.arctic.wolf.exception.BadRequestException;
import org.arctic.wolf.exception.UserNotFoundException;
import org.arctic.wolf.model.group.GroupService;
import org.arctic.wolf.model.server.ServerSettingsService;
import org.arctic.wolf.model.user.dto.RegisterRequest;
import org.arctic.wolf.model.user.verification.IUserVerificationToken;
import org.arctic.wolf.model.user.verification.UserVerificationService;
import org.arctic.wolf.recaptcha.ReCaptcha;
import org.arctic.wolf.recaptcha.dto.ReCaptchaResponse;
import org.json.JSONException;
import org.springframework.context.MessageSource;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@Service
public class UserRegisterService {
	private static final String REGISTRATION_CONFIRM_TOKEN_URL = "/#/registration-confirm?token=";
	
	private final UserService userService;
	private final UserVerificationService userVerificationService;
	private final GroupService groupService;
	private final ServerSettingsService serverSettingsService;
	private final ReCaptcha reCaptcha;
	private final JavaMailSender mailSender;
	private final MessageSource messageSource;
	
	public UserDB registerUser(RegisterRequest registerRequest) {
		verifyReCaptcha(registerRequest);
		
		userService.registerNewRegularUser(registerRequest);
		
		// Query the newly created user as a verification.
		final UserDB newUser = userService.findByUsernameDB(registerRequest.getUsername())
			.orElseThrow(UserNotFoundException::new);
		
		// Handle user activation process.
		userActivation(newUser);
		
		// Add new user to users group.
		final String userGroupName = serverSettingsService.getString(USER_GROUP_NAME, DEFAULT_USER_GROUP_NAME);
		groupService.addUser(userGroupName, newUser);
		return newUser;
	}
	
	private void verifyReCaptcha(RegisterRequest registerRequest) {
		final boolean reCaptchaAllowed = serverSettingsService.getBoolean(RECAPTCHA_ALLOWED, DEFAULT_RECAPTCHA_ALLOWED);
		if (!reCaptchaAllowed) {
			return;
		}
		
		try {
			final ReCaptchaResponse reCaptchaResponse = reCaptcha.verifyResponse(registerRequest.getRecaptchaResponse());
			if (!reCaptchaResponse.isSuccess()) {
				throw new BadRequestException("register_bad_captcha");
			}
		}
		catch (ParseException | JSONException e) {
			throw new BadRequestException("register_bad_captcha");
		}
	}
	
	private void userActivation(UserDB userDB) {
		final UserActivation userActivation = serverSettingsService.getEnum(USER_ACTIVATION, UserActivation.class, DEFAULT_USER_ACTIVATION);
		switch (userActivation) {
			case EMAIL:
				final IUserVerificationToken userVerificationToken = userVerificationService.createToken(userDB);
				sendMail(userDB, userVerificationToken);
				break;
			case ADMIN_APPROVE:
				// Wait for administrator's manual activation.
				break;
			default:
				userService.activateUser(userDB);
				break;
		}
	}
	
	private void sendMail(UserDB userDB, IUserVerificationToken userVerificationToken) {
		final String supportEmail = serverSettingsService.getString(SUPPORT_EMAIL, DEFAULT_SUPPORT_EMAIL);
		final String subject = messageSource.getMessage(REGISTER_EMAIL_ACTIVATION_TILE, null, null);
		final String registerEmailActivationText = messageSource.getMessage(REGISTER_EMAIL_ACTIVATION_TEXT, null, null);
		final String siteURL = serverSettingsService.getString(SITE_URL, DEFAULT_SITE_URL);
		
		final SimpleMailMessage email = new SimpleMailMessage();
		email.setFrom(supportEmail);
		email.setTo(userDB.getEmail());
		email.setSubject(subject);
		
		final StringBuilder sb = new StringBuilder();
		sb.append(registerEmailActivationText).append(System.lineSeparator());
		sb.append(siteURL).append(REGISTRATION_CONFIRM_TOKEN_URL).append(userVerificationToken.getToken());
		
		email.setText(sb.toString());
		mailSender.send(email);
	}
}
