/*
 * Copyright (c) 2017 Reginald Ravenhorst <lordrex34@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.arctic.wolf.model.user;

import java.time.LocalDate;
import java.time.Month;
import java.util.Date;
import java.util.Optional;
import java.util.concurrent.ThreadLocalRandom;
import org.springframework.beans.factory.annotation.Autowired;
import jakarta.persistence.EntityManager;

import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.arctic.wolf.exception.UserNotFoundException;
import org.arctic.wolf.mapper.UserMapper;
import org.arctic.wolf.model.user.dto.RegisterRequest;
import org.arctic.wolf.model.user.dto.User;
import org.arctic.wolf.model.user.dto.UserViewResponse;
import org.arctic.wolf.model.user.dto.form.IProfileForm;
import org.arctic.wolf.model.user.dto.form.IUserForm;
import org.arctic.wolf.model.user.dto.form.IUserSettingsForm;
import org.arctic.wolf.security.oauth2.user.OAuth2UserInfo;
import org.arctic.wolf.util.FuzzySearchUtil;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.config.oauth2.client.CommonOAuth2Provider;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.client.userinfo.OAuth2UserRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.google.common.annotations.VisibleForTesting;

/**
 * @author lord_rex
 */
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@Service
@Transactional
public class UserService {
	private static final Date SSO_UNKNOWN_BIRTHDAY = java.sql.Date.valueOf(
		LocalDate.of(1990, Month.APRIL, 1)
	);
	
	private final UserDBRepository repository;
	private final PasswordEncoder passwordEncoder;
	private final EntityManager entityManager;
	
	public void update(UserDB userDB) {
		repository.save(userDB);
	}
	
	public IUser registerNewSSOUser(OAuth2UserRequest oAuth2UserRequest, OAuth2UserInfo oAuth2UserInfo) {
		final String clientRegistrationId = oAuth2UserRequest.getClientRegistration().getRegistrationId().toUpperCase();
		final CommonOAuth2Provider provider = CommonOAuth2Provider.valueOf(clientRegistrationId);
		
		final UserDB userDB = new UserDB();
		userDB.setUsername(getSSOUsername(oAuth2UserInfo, provider));
		userDB.setPassword(passwordEncoder.encode(ThreadLocalRandom.current().nextLong() + "_random_password"));
		userDB.setDisplayName(oAuth2UserInfo.getName());
		userDB.setEmail(oAuth2UserInfo.getEmail());
		userDB.setBirthDate(SSO_UNKNOWN_BIRTHDAY);
		userDB.setProvider(provider);
		userDB.setProviderId(oAuth2UserInfo.getId());
		userDB.setImageUrl(oAuth2UserInfo.getImageUrl());
		userDB.setActive(true);
		
		repository.save(userDB);
		
		return UserMapper.INSTANCE.toDTO(userDB);
	}
	
	@VisibleForTesting
	static String getSSOUsername(OAuth2UserInfo oAuth2UserInfo, CommonOAuth2Provider provider) {
		return provider.name().toLowerCase() + "_user_" + oAuth2UserInfo.getId();
	}
	
	public IUser registerNewRegularUser(RegisterRequest registerRequest) {
		final UserDB userDB = new UserDB();
		userDB.setUsername(registerRequest.getUsername());
		userDB.setPassword(passwordEncoder.encode(registerRequest.getPassword()));
		userDB.setDisplayName(registerRequest.getDisplayName());
		userDB.setEmail(registerRequest.getEmail());
		userDB.setBirthDate(registerRequest.getBirthDate());
		// userDB.setProvider(null); // local registration does not have provider
		// userDB.setProviderId(null); // set after registration in profile menu
		// userDB.setImageUrl(null); // set after registration in profile menu
		userDB.setActive(false);
		
		repository.save(userDB);
		
		return UserMapper.INSTANCE.toDTO(userDB);
	}
	
	public void activateUser(UserDB userDB) {
		userDB.setActive(true);
		
		update(userDB);
	}
	
	public UserDB createByForm(IUserForm userForm) {
		final UserDB userDB = UserMapper.INSTANCE.toDB(userForm);
		userDB.setCreateTime(new Date());
		userDB.setPassword(passwordEncoder.encode(userForm.getPassword())); // Encrypt password before it is being saved into DB.
		return repository.save(userDB);
	}
	
	public void updateByForm(UserDB userDB, IUserForm userForm) {
		userDB.updateFrom(userForm);
		
		final String password = userForm.getPassword();
		if (StringUtils.isNotEmpty(password)) {
			userDB.setPassword(passwordEncoder.encode(password)); // Encrypt password before it is being saved into DB.
		}
		
		repository.save(userDB);
	}
	
	public void updateOAuth2User(IUser user, OAuth2UserInfo oAuth2UserInfo) {
		final UserDB userDB = findByEmailDB(user.getEmail()).orElseThrow(UserNotFoundException::new);
		userDB.setDisplayName(oAuth2UserInfo.getName());
		userDB.setImageUrl(oAuth2UserInfo.getImageUrl());
		
		update(userDB);
	}
	
	public void updateProfile(UserDB userDB, IProfileForm profileForm) {
		userDB.setDisplayName(profileForm.getDisplayName());
		userDB.setBirthDate(profileForm.getBirthDate());
		userDB.setImageUrl(profileForm.getImageUrl());
		userDB.setBio(profileForm.getBio());
		
		update(userDB);
	}
	
	public Optional<UserViewResponse> viewProfile(Long id) {
		return findByIdDB(id).map(UserService::createProfileView);
	}
	
	private static UserViewResponse createProfileView(IUser user) {
		final UserViewResponse userViewResponse = new UserViewResponse();
		userViewResponse.setDisplayName(user.getDisplayName());
		userViewResponse.setImageUrl(user.getImageUrl());
		userViewResponse.setBio(user.getBio());
		return userViewResponse;
	}
	
	public void updateUserSettings(UserDB userDB, IUserSettingsForm userSettingsForm) {
		final String password = userSettingsForm.getPassword();
		if (StringUtils.isNotEmpty(password)) {
			userDB.setPassword(passwordEncoder.encode(password)); // Encrypt password before it is being saved into DB.
		}
		
		update(userDB);
	}
	
	public void delete(long id) {
		repository.deleteById(id);
	}
	
	@Transactional(readOnly = true)
	public Optional<UserDB> findByIdDB(long id) {
		return repository.findById(id);
	}
	
	public Optional<User> findByIdDTO(long id) {
		return findByIdDB(id).map(UserMapper.INSTANCE::toDTO);
	}
	
	@Transactional(readOnly = true)
	public Optional<UserDB> findByUsernameDB(String name) {
		return Optional.ofNullable(repository.findByUsername(name));
	}
	
	public Optional<User> findByUsernameDTO(String name) {
		return findByUsernameDB(name).map(UserMapper.INSTANCE::toDTO);
	}
	
	@Transactional(readOnly = true)
	public Optional<UserDB> findByEmailDB(String email) {
		return Optional.ofNullable(repository.findByEmail(email));
	}
	
	public Optional<User> findByEmailDTO(String email) {
		return findByEmailDB(email).map(UserMapper.INSTANCE::toDTO);
	}
	
	@Transactional(readOnly = true)
	public Page<UserDB> getUsersDB(Pageable pageable) {
		return repository.findAll(pageable);
	}
	
	public Page<User> getUsersDTO(Pageable pageable) {
		return getUsersDB(pageable).map(UserMapper.INSTANCE::toDTO);
	}
	
	public Page<UserDB> pagedFuzzySearchDB(String search, Pageable pageable) {
		return FuzzySearchUtil.pagedFuzzySearch(pageable, entityManager, UserDB.class, search, "username", "email", "displayName");
	}
	
	public Page<User> pagedFuzzySearchDTO(String search, Pageable pageable) {
		return pagedFuzzySearchDB(search, pageable).map(UserMapper.INSTANCE::toDTO);
	}
	
	@Transactional(readOnly = true)
	public long count() {
		return repository.count();
	}
	
	public void deleteAll() {
		repository.deleteAll();
	}
}
