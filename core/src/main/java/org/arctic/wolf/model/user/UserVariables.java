/*
 * Copyright (c) 2017 Reginald Ravenhorst <lordrex34@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.arctic.wolf.model.user;

/**
 * @author lord_rex
 */
public interface UserVariables {
	String USERNAME = "username";
	String DISPLAY_NAME = "displayName";
	String USER_IMAGE_URL = "userImageUrl";
	String NAME = "name";
	String EMAIL = "email";
	String PICTURE = "picture";
	String PASSWORD = "password";
	String BIRTH_DATE = "birthDate";
	
	String USER_FORM = "userForm";
	String PROFILE_FORM = "profileForm";
	String DISABLED_PROFILE_FORM_EDIT = "disabledProfileFormEdit";
	String USER_SETTINGS_FORM = "userSettingsForm";
	String DISABLED_USER_SETTINGS_FORM_EDIT = "disabledUserSettingsFormEdit";
	String ADMIN_EDIT_USER_FORM = "adminEditUserForm";
	String REGISTER = "register";
	String LOGIN = "login";
	
	String USER = "user";
	String USERS = "users";
	
	String G_RECAPTCHA_RESPONSE = "g-recaptcha-response";
}