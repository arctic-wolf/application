/*
 * Copyright (c) 2019 Reginald Ravenhorst <lordrex34@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.arctic.wolf.model.user.dto;

import static org.arctic.wolf.model.user.RegisterMessages.REGISTER_EMAIL_ALREADY_USED;
import static org.arctic.wolf.model.user.RegisterMessages.REGISTER_USERNAME_ALREADY_EXIST;
import static org.arctic.wolf.model.user.dto.form.UserFormMessages.USER_FORM_NOT_VALID_EMAIL;
import static org.arctic.wolf.model.user.dto.form.UserFormMessages.USER_FORM_PASSWORDS_DO_NOT_MATCH;
import static org.arctic.wolf.model.user.dto.form.UserFormMessages.USER_FORM_PASSWORD_TOO_SHORT;
import static org.arctic.wolf.model.user.dto.form.UserFormMessages.USER_FORM_PROVIDE_BIRTH_DATE;
import static org.arctic.wolf.model.user.dto.form.UserFormMessages.USER_FORM_PROVIDE_EMAIL;
import static org.arctic.wolf.model.user.dto.form.UserFormMessages.USER_FORM_PROVIDE_PASSWORD;
import static org.arctic.wolf.model.user.dto.form.UserFormMessages.USER_FORM_PROVIDE_USERNAME;
import static org.arctic.wolf.model.user.dto.form.UserFormMessages.USER_FORM_USERNAME_TOO_SHORT;

import java.util.Date;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;

import lombok.Data;
import org.arctic.wolf.validator.constraint.FieldMatch;
import org.arctic.wolf.validator.constraint.UniqueEmail;
import org.arctic.wolf.validator.constraint.UniqueUsername;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;

@Data
@FieldMatch(first = "password", second = "passwordConfirm", message = USER_FORM_PASSWORDS_DO_NOT_MATCH)
public class RegisterRequest {
	@UniqueUsername(message = REGISTER_USERNAME_ALREADY_EXIST)
	@Length(min = 3, message = USER_FORM_USERNAME_TOO_SHORT)
	@NotBlank(message = USER_FORM_PROVIDE_USERNAME)
	private String username;
	
	private String displayName = "";
	
	@UniqueEmail(message = REGISTER_EMAIL_ALREADY_USED)
	@NotBlank(message = USER_FORM_PROVIDE_EMAIL)
	@Email(message = USER_FORM_NOT_VALID_EMAIL)
	private String email;
	
	@Length(min = 6, message = USER_FORM_PASSWORD_TOO_SHORT)
	@NotBlank(message = USER_FORM_PROVIDE_PASSWORD)
	private String password;
	
	@Length(min = 6, message = USER_FORM_PASSWORD_TOO_SHORT)
	@NotBlank(message = USER_FORM_PROVIDE_PASSWORD)
	private String passwordConfirm;
	
	@NotNull(message = USER_FORM_PROVIDE_BIRTH_DATE)
	@DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
	private Date birthDate;
	
	private String recaptchaResponse;
}
