/*
 * Copyright (c) 2019 Reginald Ravenhorst <lordrex34@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.arctic.wolf.model.user.dto;

import java.util.Date;
import java.util.Set;

import lombok.Data;
import org.arctic.wolf.model.group.Authority;
import org.arctic.wolf.model.user.IUser;
import org.springframework.security.config.oauth2.client.CommonOAuth2Provider;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Data
public class User implements IUser {
	private Long id;
	private String username;
	
	@JsonIgnore
	private String password;
	
	private String email;
	
	private String displayName;
	private Date birthDate;
	private Date createTime = new Date();
	private String imageUrl;
	private String bio;
	
	private boolean active = false;
	
	private CommonOAuth2Provider provider;
	
	private String providerId;
	
	private Set<Authority> authorities;
}
