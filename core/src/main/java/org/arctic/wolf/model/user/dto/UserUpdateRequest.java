/*
 * Copyright (c) 2019 Reginald Ravenhorst <lordrex34@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.arctic.wolf.model.user.dto;

import static org.arctic.wolf.model.user.dto.form.ProfileFormMessages.PROFILE_BIO_TOO_LONG;
import static org.arctic.wolf.model.user.dto.form.ProfileFormMessages.PROFILE_DISPLAY_NAME_NOT_BLANK;
import static org.arctic.wolf.model.user.dto.form.ProfileFormMessages.PROFILE_PROVIDE_BIRTH_DATE;

import java.util.Date;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;

import lombok.Data;
import org.arctic.wolf.model.user.dto.form.IProfileForm;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * @author lord_rex
 */
@Data
public class UserUpdateRequest implements IProfileForm {
	@NotBlank(message = PROFILE_DISPLAY_NAME_NOT_BLANK)
	private String displayName;
	
	@NotNull(message = PROFILE_PROVIDE_BIRTH_DATE)
	@DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
	private Date birthDate;
	
	private String imageUrl;
	
	@Length(max = 1000, message = PROFILE_BIO_TOO_LONG)
	private String bio;
}
