/*
 * Copyright (c) 2018 Reginald Ravenhorst <lordrex34@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.arctic.wolf.model.user.dto.form;

/**
 * @author lord_rex
 */
public interface UserFormMessages {
	String USER_FORM_PROVIDE_USERNAME = "user_form_provide_username";
	String USER_FORM_PROVIDE_EMAIL = "user_form_provide_email";
	String USER_FORM_PROVIDE_PASSWORD = "user_form_provide_password";
	String USER_FORM_PROVIDE_BIRTH_DATE = "user_form_provide_birth_date";
	
	String USER_FORM_NOT_VALID_EMAIL = "user_form_not_valid_email";
	String USER_FORM_USERNAME_TOO_SHORT = "user_form_username_too_short";
	String USER_FORM_PASSWORD_TOO_SHORT = "user_form_password_too_short";
	String USER_FORM_PASSWORDS_DO_NOT_MATCH = "user_form_passwords_do_not_match";
}
