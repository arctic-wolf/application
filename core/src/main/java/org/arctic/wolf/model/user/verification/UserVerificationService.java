/*
 * Copyright (c) 2017 Reginald Ravenhorst <lordrex34@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.arctic.wolf.model.user.verification;

import java.util.Optional;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;

import lombok.RequiredArgsConstructor;
import org.arctic.wolf.mapper.UserVerificationTokenMapper;
import org.arctic.wolf.model.user.UserDB;
import org.arctic.wolf.model.user.verification.dto.UserVerificationToken;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.google.common.annotations.VisibleForTesting;

/**
 * @author lord_rex
 */
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@Service
@Transactional
public class UserVerificationService {
	private final UserVerificationTokenDBRepository repository;
	
	public void update(UserVerificationTokenDB userVerificationTokenDB) {
		repository.save(userVerificationTokenDB);
	}
	
	public IUserVerificationToken createToken(UserDB userDB) {
		final UserVerificationTokenDB userVerificationTokenDB = new UserVerificationTokenDB();
		userVerificationTokenDB.setUserDB(userDB);
		userVerificationTokenDB.setToken(UUID.randomUUID().toString());
		
		repository.save(userVerificationTokenDB);
		return UserVerificationTokenMapper.INSTANCE.toDTO(userVerificationTokenDB);
	}
	
	public void useToken(UserVerificationTokenDB userVerificationTokenDB) {
		userVerificationTokenDB.setUsed(true);
		
		update(userVerificationTokenDB);
	}
	
	@Transactional(readOnly = true)
	public Optional<UserVerificationTokenDB> findByToken(String token) {
		return Optional.ofNullable(repository.findByToken(token));
	}
	
	public Optional<UserVerificationToken> findByTokenAsDTO(String token) {
		return findByToken(token).map(UserVerificationTokenMapper.INSTANCE::toDTO);
	}
	
	@VisibleForTesting
	void deleteAll() {
		repository.deleteAll();
	}
}
