/*
 * Copyright (c) 2018 Reginald Ravenhorst <lordrex34@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.arctic.wolf.model.widget;

import java.util.List;

import org.arctic.wolf.model.content.IContentVisibilityOwner;
import org.arctic.wolf.model.link.ILink;

/**
 * @author lord_rex
 */
public interface IWidget extends IContentVisibilityOwner {
	void setId(String id);
	
	String getId();
	
	void setDisplayName(String displayName);
	
	String getDisplayName();
	
	void setGroupId(String groupId);
	
	String getGroupId();
	
	void setOrderInTheGroup(int orderInTheGroup);
	
	int getOrderInTheGroup();
	
	void setContent(String content);
	
	String getContent();
	
	void setLinks(List<ILink> links);
	
	List<ILink> getLinks();
	
	void setLang(String lang);
	
	String getLang();
	
	default void updateFrom(IWidget widget) {
		setDisplayName(widget.getDisplayName());
		setGroupId(widget.getGroupId());
		setOrderInTheGroup(widget.getOrderInTheGroup());
		setContent(widget.getContent());
		setLinks(widget.getLinks());
		setLang(widget.getLang());
		setVisibility(widget.getVisibility());
	}
}
