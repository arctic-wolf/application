/*
 * Copyright (c) 2017 Reginald Ravenhorst <lordrex34@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.arctic.wolf.model.widget;

import java.util.List;
import java.util.Set;
import jakarta.persistence.Column;
import jakarta.persistence.ElementCollection;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Lob;
import jakarta.persistence.Table;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import org.arctic.wolf.model.content.ContentVisibility;
import org.arctic.wolf.model.link.ILink;
import org.arctic.wolf.model.link.LinkDB;
import org.hibernate.annotations.Type;

/**
 * @author lord_rex
 */
@Entity
@Table(name = "widgets")
@EqualsAndHashCode(of = "id")
@Setter
@Getter
public class WidgetDB implements IWidget {
	@Id
	@Column(unique = true, nullable = false)
	private String id;
	
	@Column
	private String displayName;
	
	@Column(nullable = false)
	private String groupId;
	
	@Column(nullable = false)
	private int orderInTheGroup;
	
	@Lob
	@Column
	private String content;
	
	@ElementCollection(targetClass = LinkDB.class)
	@Column
	private List<ILink> links;
	
	@Column(nullable = false)
	private String lang;
	
	@ElementCollection
	@Column(nullable = false)
	private Set<ContentVisibility> visibility;
}
