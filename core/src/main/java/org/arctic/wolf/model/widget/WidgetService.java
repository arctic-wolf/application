/*
 * Copyright (c) 2017 Reginald Ravenhorst <lordrex34@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.arctic.wolf.model.widget;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;

import lombok.RequiredArgsConstructor;
import org.arctic.wolf.mapper.WidgetMapper;
import org.arctic.wolf.model.server.ServerSettingsService;
import org.arctic.wolf.model.user.UserDB;
import org.arctic.wolf.model.widget.dto.Widget;
import org.arctic.wolf.model.widget.dto.form.IWidgetForm;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.google.common.annotations.VisibleForTesting;

/**
 * @author lord_rex
 */
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@Service
@Transactional
public class WidgetService {
	private static final Comparator<WidgetDB> WIDGET_COMPARATOR = Comparator.comparingInt(WidgetDB::getOrderInTheGroup);
	
	private final WidgetDBRepository repository;
	
	public WidgetDB createByForm(IWidgetForm widgetForm) {
		final WidgetDB widgetDB = WidgetMapper.INSTANCE.toDB(widgetForm);
		return repository.save(widgetDB);
	}
	
	public void update(WidgetDB widgetDB) {
		repository.save(widgetDB);
	}
	
	public void updateByForm(WidgetDB widgetDB, IWidgetForm widgetForm) {
		widgetDB.updateFrom(widgetForm);
		
		update(widgetDB);
	}
	
	@Transactional(readOnly = true)
	public Optional<WidgetDB> findByIdDB(String id) {
		return repository.findById(id);
	}
	
	public Optional<Widget> findByIdDTO(String id) {
		return findByIdDB(id).map(WidgetMapper.INSTANCE::toDTO);
	}
	
	@Transactional(readOnly = true)
	public Optional<WidgetDB> getWidgetDB(String lang, String id) {
		return Optional.ofNullable(repository.findByLangAndId(lang, id));
	}
	
	public Optional<Widget> getWidgetDTO(String lang, String id) {
		return getWidgetDB(lang, id).map(WidgetMapper.INSTANCE::toDTO);
	}
	
	@Transactional(readOnly = true)
	public List<WidgetDB> getWidgetsDB(String lang) {
		return repository.findAllByLang(lang);
	}
	
	public List<Widget> getWidgetsDTO(String lang) {
		return getWidgetsDB(lang)
			.stream()
			.map(WidgetMapper.INSTANCE::toDTO)
			.collect(Collectors.toList());
	}
	
	public List<Widget> getWidgets(String lang, UserDB userDB, WidgetMenuService widgetMenuService, ServerSettingsService serverSettingsService) {
		return getWidgetsDB(lang)
			.stream()
			.sorted(WIDGET_COMPARATOR)
			.filter(widgetDB -> widgetDB.isVisibleFor(userDB))
			.map(WidgetMapper.INSTANCE::toDTO)
			.map(widget -> processWidget(widget, lang, userDB, widgetMenuService, serverSettingsService))
			.collect(Collectors.toList());
	}
	
	public List<String> getWidgetGroups(String lang, UserDB userDB) {
		return getWidgetsDB(lang)
			.stream()
			.sorted(WIDGET_COMPARATOR)
			.filter(widgetDB -> widgetDB.isVisibleFor(userDB))
			.map(WidgetDB::getGroupId)
			.distinct()
			.collect(Collectors.toList());
	}
	
	public List<Widget> getWidgetGroup(String lang, String groupId, UserDB userDB, WidgetMenuService widgetMenuService, ServerSettingsService serverSettingsService) {
		return getWidgetsDB(lang)
			.stream()
			.sorted(WIDGET_COMPARATOR)
			.filter(widgetDB -> widgetDB.getGroupId().equals(groupId))
			.filter(widgetDB -> widgetDB.isVisibleFor(userDB))
			.map(WidgetMapper.INSTANCE::toDTO)
			.map(widget -> processWidget(widget, lang, userDB, widgetMenuService, serverSettingsService))
			.collect(Collectors.toList());
	}
	
	private static Widget processWidget(Widget widget, String lang, UserDB userDB, WidgetMenuService widgetMenuService, ServerSettingsService serverSettingsService) {
		final String widgetContent = widget.getContent();
		if (widgetContent == null) {
			return widget;
		}
		
		if (widgetContent.equals(WidgetMenuService.PAGE_LIST)) {
			return widgetMenuService.getMenu(widget, lang, userDB);
		}
		
		final String processedWidgetContent = serverSettingsService.processContentVariables(widgetContent);
		widget.setContent(processedWidgetContent);
		
		return widget;
	}
	
	public void delete(String id) {
		repository.deleteById(id);
	}
	
	@VisibleForTesting
	void deleteAll() {
		repository.deleteAll();
	}
}
