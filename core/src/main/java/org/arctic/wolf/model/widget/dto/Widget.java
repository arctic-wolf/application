/*
 * Copyright (c) 2019 Reginald Ravenhorst <lordrex34@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.arctic.wolf.model.widget.dto;

import java.util.List;
import java.util.Set;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.arctic.wolf.mapper.WidgetMapper;
import org.arctic.wolf.model.content.ContentVisibility;
import org.arctic.wolf.model.link.ILink;
import org.arctic.wolf.model.plugin.IPluginContentOwner;
import org.arctic.wolf.model.widget.IWidget;

/**
 * @author lord_rex
 */
@NoArgsConstructor
@Data
public class Widget implements IWidget, IPluginContentOwner {
	private String id;
	private String displayName;
	private String groupId;
	private int orderInTheGroup;
	private String content;
	private List<ILink> links;
	private String lang;
	private Set<ContentVisibility> visibility;
	
	@Override
	public IPluginContentOwner withPluginContent(String pluginContent) {
		final Widget widget = WidgetMapper.INSTANCE.toWidget(this);
		widget.setContent(pluginContent);
		return widget;
	}
}
