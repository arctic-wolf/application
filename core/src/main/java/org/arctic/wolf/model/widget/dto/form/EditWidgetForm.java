/*
 * Copyright (c) 2018 Reginald Ravenhorst <lordrex34@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.arctic.wolf.model.widget.dto.form;

import static org.arctic.wolf.model.widget.dto.form.WidgetFormMessages.WIDGET_FORM_PROVIDE_CONTENT;
import static org.arctic.wolf.model.widget.dto.form.WidgetFormMessages.WIDGET_FORM_PROVIDE_DISPLAY_NAME;
import static org.arctic.wolf.model.widget.dto.form.WidgetFormMessages.WIDGET_FORM_PROVIDE_GROUP;
import static org.arctic.wolf.model.widget.dto.form.WidgetFormMessages.WIDGET_FORM_PROVIDE_ID;
import static org.arctic.wolf.model.widget.dto.form.WidgetFormMessages.WIDGET_FORM_PROVIDE_LANG;

import java.util.EnumSet;
import java.util.List;
import java.util.Set;
import jakarta.validation.constraints.NotBlank;

import lombok.Data;
import org.arctic.wolf.model.content.ContentVisibility;
import org.arctic.wolf.model.link.ILink;

/**
 * @author lord_rex
 */
@Data
public class EditWidgetForm implements IWidgetForm {
	@NotBlank(message = WIDGET_FORM_PROVIDE_ID)
	private String id;
	
	@NotBlank(message = WIDGET_FORM_PROVIDE_DISPLAY_NAME)
	private String displayName;
	
	@NotBlank(message = WIDGET_FORM_PROVIDE_GROUP)
	private String groupId;
	
	private int orderInTheGroup = 0;
	
	private boolean fragment = false;
	
	@NotBlank(message = WIDGET_FORM_PROVIDE_CONTENT)
	private String content;
	
	private List<ILink> links;
	
	@NotBlank(message = WIDGET_FORM_PROVIDE_LANG)
	private String lang;
	
	private Set<ContentVisibility> visibility = EnumSet.allOf(ContentVisibility.class);
}
