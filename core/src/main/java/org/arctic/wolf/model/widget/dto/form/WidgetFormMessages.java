/*
 * Copyright (c) 2018 Reginald Ravenhorst <lordrex34@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.arctic.wolf.model.widget.dto.form;

/**
 * @author lord_rex
 */
public interface WidgetFormMessages {
	String WIDGET_FORM_PROVIDE_ID = "widget_form_provide_id";
	String WIDGET_FORM_ID_ALREADY_USED = "widget_form_id_already_used";
	String WIDGET_FORM_PROVIDE_DISPLAY_NAME = "widget_form_provide_display_name";
	String WIDGET_FORM_PROVIDE_GROUP = "widget_form_provide_group";
	String WIDGET_FORM_PROVIDE_CONTENT = "widget_form_provide_content";
	String WIDGET_FORM_PROVIDE_LANG = "widget_form_provide_lang";
}
