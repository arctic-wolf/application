/*
 * Copyright (c) 2017 Reginald Ravenhorst <lordrex34@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.arctic.wolf.recaptcha;

import java.util.Map;

import org.apache.http.ParseException;
import org.arctic.wolf.recaptcha.dto.ReCaptchaResponse;
import org.json.JSONException;

/**
 * @author akunzai
 */
public interface ReCaptcha {
	/**
	 * Create reCAPTCHA Script resource (api.js) (https://developers.google.com/recaptcha/docs/display)
	 * @param parameters JavaScript resource (api.js) parameters
	 * @return String
	 */
	public String createScriptResource(Map<String, String> parameters);
	
	/**
	 * Create reCAPTCHA tag (https://developers.google.com/recaptcha/docs/display)
	 * @param parameters g-recaptcha tag attributes and grecaptcha.render parameters
	 * @return String
	 */
	public String createReCaptchaTag(Map<String, String> parameters);
	
	/**
	 * Get Site Key
	 * @return String siteKey
	 */
	public String getSiteKey();
	
	/**
	 * Verify reCAPTCHA response (https://developers.google.com/recaptcha/docs/verify)
	 * @param response (require)
	 * @return ReCaptchaResponse
	 * @throws JSONException
	 * @throws ParseException
	 */
	public ReCaptchaResponse verifyResponse(String response) throws ParseException, JSONException;
}
