/*
 * Copyright (c) 2019 Reginald Ravenhorst <lordrex34@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.arctic.wolf.repository.server;

import static org.arctic.wolf.model.server.ServerSettingsVariables.DEFAULT_REPOSITORY_SERVER_PASSWORD;
import static org.arctic.wolf.model.server.ServerSettingsVariables.DEFAULT_REPOSITORY_SERVER_URL;
import static org.arctic.wolf.model.server.ServerSettingsVariables.DEFAULT_REPOSITORY_SERVER_USERNAME;
import static org.arctic.wolf.model.server.ServerSettingsVariables.REPOSITORY_SERVER_PASSWORD;
import static org.arctic.wolf.model.server.ServerSettingsVariables.REPOSITORY_SERVER_URL;
import static org.arctic.wolf.model.server.ServerSettingsVariables.REPOSITORY_SERVER_USERNAME;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.Collections;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import jakarta.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.arctic.wolf.model.server.ServerSettingsService;
import org.arctic.wolf.model.user.dto.LoginResponse;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StreamUtils;
import org.springframework.web.client.RestTemplate;

/**
 * @author lord_rex
 */
@Slf4j
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@Service
@Transactional
public class RepositoryServerService {
	private final ServerSettingsService serverSettingsService;
	
	private RestTemplate restTemplate;
	private String tokenType;
	private String accessToken;
	private long nextLogin;
	
	@PostConstruct
	void init() {
		restTemplate = new RestTemplate();
		
		login();
	}
	
	private String getUrl() {
		final String url = serverSettingsService.getString(REPOSITORY_SERVER_URL, DEFAULT_REPOSITORY_SERVER_URL);
		return !url.endsWith("/") ? url + "/" : url;
	}
	
	private void login() {
		try {
			final ResponseEntity<LoginResponse> responseEntity = restTemplate.exchange(getUrl() + "auth/login", HttpMethod.POST, loginEntity(), LoginResponse.class);
			final LoginResponse responseEntityBody = Objects.requireNonNull(responseEntity.getBody(), "Login failed, response entity is null!");
			
			this.tokenType = responseEntityBody.getTokenType();
			this.accessToken = responseEntityBody.getAccessToken();
		}
		catch (Exception e) {
			LOGGER.warn("Could not connect to the repository server!");
		}
		
		this.nextLogin = System.nanoTime() + TimeUnit.MINUTES.toNanos(30);
	}
	
	private void checkLastLogin() {
		if (nextLogin < System.nanoTime()) {
			login();
		}
	}
	
	private HttpEntity<String> loginEntity() {
		final HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
		
		final JSONObject jsonObject = new JSONObject();
		try {
			jsonObject.put("username", serverSettingsService.getString(REPOSITORY_SERVER_USERNAME, DEFAULT_REPOSITORY_SERVER_USERNAME));
			jsonObject.put("password", serverSettingsService.getString(REPOSITORY_SERVER_PASSWORD, DEFAULT_REPOSITORY_SERVER_PASSWORD));
		}
		catch (JSONException e) {
			LOGGER.error("", e);
		}
		
		return new HttpEntity<>(jsonObject.toString(), headers);
	}
	
	public <T, B> ResponseEntity<T> exchange(String url, HttpMethod httpMethod, Map<String, String> additionalHeaders, B body, Class<T> responseType, Object... uriVariables) {
		checkLastLogin();
		
		url = getUrl() + url;
		
		final HttpHeaders headers = getHttpHeaders(additionalHeaders);
		final HttpEntity<?> requestEntity = body != null ? new HttpEntity<>(body, headers) : new HttpEntity<>(headers);
		
		return restTemplate.exchange(url, httpMethod, requestEntity, responseType, uriVariables);
	}
	
	private HttpHeaders getHttpHeaders(Map<String, String> additionalHeaders) {
		final HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", tokenType + " " + accessToken);
		if (additionalHeaders != null) {
			additionalHeaders.forEach(headers::add);
		}
		return headers;
	}
	
	public void download(String url, Map<String, String> additionalHeaders, File destinationFile) {
		checkLastLogin();
		
		url = getUrl() + url;
		
		final HttpHeaders headers = getHttpHeaders(additionalHeaders);
		restTemplate.execute(url, HttpMethod.GET,
			clientHttpRequest -> clientHttpRequest.getHeaders().addAll(headers),
			clientHttpResponse -> {
				try (OutputStream outputStream = new FileOutputStream(destinationFile)) {
					StreamUtils.copy(clientHttpResponse.getBody(), outputStream);
				}
				
				return destinationFile;
			});
	}
}
