/*
 * Copyright (c) 2019 Reginald Ravenhorst <lordrex34@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.arctic.wolf.security;

import java.util.Collection;
import java.util.Collections;
import java.util.Map;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.arctic.wolf.model.user.UserDB;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.oauth2.core.user.OAuth2User;

/**
 * @author Rajeev Kumar Singh <callicoder@gmail.com>
 * @author lord_rex
 */
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
@Getter
public class UserPrincipal implements OAuth2User, UserDetails {
	private final UserDB userDB;
	private final Map<String, Object> attributes;
	
	public static UserPrincipal create(UserDB userDB, Map<String, Object> attributes) {
		return new UserPrincipal(userDB, attributes);
	}
	
	public static UserPrincipal create(UserDB userDB) {
		return UserPrincipal.create(userDB, Collections.emptyMap());
	}
	
	public Long getId() {
		return userDB.getId();
	}
	
	@Override
	public String getUsername() {
		return userDB.getUsername();
	}
	
	@Override
	public String getPassword() {
		return userDB.getPassword();
	}
	
	@Override
	public String getName() {
		return String.valueOf(userDB.getId());
	}
	
	public String getEmail() {
		return userDB.getEmail();
	}
	
	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return userDB.getGrantedAuthorities();
	}
	
	@Override
	public boolean isAccountNonExpired() {
		return true;
	}
	
	@Override
	public boolean isAccountNonLocked() {
		return true;
	}
	
	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}
	
	@Override
	public boolean isEnabled() {
		return userDB.isActive();
	}
	
}
