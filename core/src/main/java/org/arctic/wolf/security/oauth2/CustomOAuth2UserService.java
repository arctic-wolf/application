/*
 * Copyright (c) 2019 Reginald Ravenhorst <lordrex34@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.arctic.wolf.security.oauth2;

import static org.arctic.wolf.model.server.ServerSettingsVariables.DEFAULT_USER_GROUP_NAME;
import static org.arctic.wolf.model.server.ServerSettingsVariables.USER_GROUP_NAME;

import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;

import lombok.RequiredArgsConstructor;
import org.arctic.wolf.exception.OAuth2AuthenticationProcessingException;
import org.arctic.wolf.exception.UserNotFoundException;
import org.arctic.wolf.model.group.GroupService;
import org.arctic.wolf.model.server.ServerSettingsService;
import org.arctic.wolf.model.user.UserDB;
import org.arctic.wolf.model.user.UserService;
import org.arctic.wolf.security.UserPrincipal;
import org.arctic.wolf.security.oauth2.user.OAuth2UserInfo;
import org.arctic.wolf.security.oauth2.user.OAuth2UserInfoFactory;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.security.config.oauth2.client.CommonOAuth2Provider;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.oauth2.client.userinfo.DefaultOAuth2UserService;
import org.springframework.security.oauth2.client.userinfo.OAuth2UserRequest;
import org.springframework.security.oauth2.core.OAuth2AuthenticationException;
import org.springframework.security.oauth2.core.user.OAuth2User;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

/**
 * @author Rajeev Kumar Singh <callicoder@gmail.com>
 */
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@Service
public class CustomOAuth2UserService extends DefaultOAuth2UserService {
	private final UserService userService;
	private final GroupService groupService;
	private final ServerSettingsService serverSettingsService;
	
	@Override
	public OAuth2User loadUser(OAuth2UserRequest oAuth2UserRequest) throws OAuth2AuthenticationException {
		final OAuth2User oAuth2User = super.loadUser(oAuth2UserRequest);
		
		try {
			return processOAuth2User(oAuth2UserRequest, oAuth2User);
		}
		catch (AuthenticationException e) {
			throw e;
		}
		catch (Exception e) {
			// Throwing an instance of AuthenticationException will trigger the OAuth2AuthenticationFailureHandler.
			throw new InternalAuthenticationServiceException(e.getMessage(), e.getCause());
		}
	}
	
	private OAuth2User processOAuth2User(OAuth2UserRequest oAuth2UserRequest, OAuth2User oAuth2User) {
		final String registrationId = oAuth2UserRequest.getClientRegistration().getRegistrationId();
		final OAuth2UserInfo oAuth2UserInfo = OAuth2UserInfoFactory.getOAuth2UserInfo(registrationId.toLowerCase(), oAuth2User.getAttributes());
		final String email = oAuth2UserInfo.getEmail();
		
		if (StringUtils.isEmpty(email)) {
			throw new OAuth2AuthenticationProcessingException("Email not found from OAuth2 provider!");
		}
		
		final Optional<UserDB> userDBOptional = userService.findByEmailDB(email);
		if (userDBOptional.isPresent()) {
			final UserDB userDB = userDBOptional.get();
			
			final CommonOAuth2Provider commonOAuth2Provider = CommonOAuth2Provider.valueOf(registrationId.toUpperCase());
			if (!commonOAuth2Provider.equals(userDB.getProvider())) {
				throw new OAuth2AuthenticationProcessingException(
					"Looks like you're signed up with " + userDB.getProvider() + " account. Please use your " + userDB.getProvider() + " account to login."
				);
			}
			
			userService.updateOAuth2User(userDB, oAuth2UserInfo);
		}
		else {
			userService.registerNewSSOUser(oAuth2UserRequest, oAuth2UserInfo);
			
			final UserDB newUser = userService.findByEmailDB(email)
				.orElseThrow(() -> new UserNotFoundException("OAuth2 registration of new user was not successful! User cannot be found by email " + email + "!"));
			
			// Add new user to users group.
			final String userGroupName = serverSettingsService.getString(USER_GROUP_NAME, DEFAULT_USER_GROUP_NAME);
			groupService.addUser(userGroupName, newUser);
		}
		
		// Query the newly created or updated user as a verification.
		final UserDB userDB = userService.findByEmailDB(email)
			.orElseThrow(UserNotFoundException::new);
		
		return UserPrincipal.create(userDB, oAuth2User.getAttributes());
	}
}
