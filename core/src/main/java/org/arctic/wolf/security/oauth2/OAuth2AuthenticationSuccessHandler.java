/*
 * Copyright (c) 2019 Reginald Ravenhorst <lordrex34@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.arctic.wolf.security.oauth2;

import static org.arctic.wolf.model.server.ServerSettingsVariables.AUTHORIZED_REDIRECT_URIS;
import static org.arctic.wolf.model.server.ServerSettingsVariables.DEFAULT_AUTHORIZED_REDIRECT_URIS;
import static org.arctic.wolf.security.oauth2.HttpCookieOAuth2AuthorizationRequestRepository.REDIRECT_URI_PARAM_COOKIE_NAME;

import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import lombok.RequiredArgsConstructor;
import org.arctic.wolf.exception.BadRequestException;
import org.arctic.wolf.model.server.ServerSettingsService;
import org.arctic.wolf.security.TokenProviderService;
import org.arctic.wolf.util.CookieUtils;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;
import org.springframework.stereotype.Component;
import org.springframework.web.util.UriComponentsBuilder;

/**
 * @author Rajeev Kumar Singh <callicoder@gmail.com>
 */
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@Component
public class OAuth2AuthenticationSuccessHandler extends SimpleUrlAuthenticationSuccessHandler {
	private static final List<String> DEFAULT_AUTHORIZED_REDIRECT_URIS_LIST = new ArrayList<>(List.of(DEFAULT_AUTHORIZED_REDIRECT_URIS));
	
	private final TokenProviderService tokenProvider;
	private final ServerSettingsService serverSettingsService;
	private final HttpCookieOAuth2AuthorizationRequestRepository httpCookieOAuth2AuthorizationRequestRepository;
	
	@Override
	public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
		final String targetUrl = getTargetUrl(request, response, authentication);
		
		if (response.isCommitted()) {
			logger.debug("Response has already been committed. Unable to redirect to: " + targetUrl);
			return;
		}
		
		clearAuthenticationAttributes(request, response);
		getRedirectStrategy().sendRedirect(request, response, targetUrl);
	}
	
	private String getTargetUrl(HttpServletRequest request, HttpServletResponse response, Authentication authentication) {
		final Optional<String> redirectUri = CookieUtils.getCookie(request, REDIRECT_URI_PARAM_COOKIE_NAME)
			.map(Cookie::getValue);
		
		if (redirectUri.isPresent() && !isAuthorizedRedirectUri(redirectUri.get())) {
			throw new BadRequestException("Sorry! We've got an Unauthorized Redirect URI and can't proceed with the authentication.");
		}
		
		final String targetUrl = redirectUri.orElse(getDefaultTargetUrl()).replace("#", "%23");
		
		final String token = tokenProvider.createToken(authentication);
		
		return UriComponentsBuilder.fromUriString(targetUrl)
			.queryParam("token", token)
			.build().toUriString().replace("%23", "#");
	}
	
	private void clearAuthenticationAttributes(HttpServletRequest request, HttpServletResponse response) {
		super.clearAuthenticationAttributes(request);
		httpCookieOAuth2AuthorizationRequestRepository.removeAuthorizationRequestCookies(request, response);
	}
	
	private boolean isAuthorizedRedirectUri(String uri) {
		final URI clientRedirectUri = URI.create(uri);
		final List<String> authorizedRedirectURIs = serverSettingsService.getList(AUTHORIZED_REDIRECT_URIS, String.class, DEFAULT_AUTHORIZED_REDIRECT_URIS_LIST);
		
		return authorizedRedirectURIs
			.stream()
			.anyMatch(authorizedRedirectUri -> {
				final URI authorizedURI = URI.create(authorizedRedirectUri);
				final String authorizedURIHost = authorizedURI.getHost();
				Objects.requireNonNull(authorizedURIHost, "Authorized URI Host is null!");
				
				return authorizedURIHost.equalsIgnoreCase(clientRedirectUri.getHost())
					&& authorizedURI.getPort() == clientRedirectUri.getPort();
			});
	}
}
