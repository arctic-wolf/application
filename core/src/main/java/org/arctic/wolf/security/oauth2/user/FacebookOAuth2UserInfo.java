/*
 * Copyright (c) 2019 Reginald Ravenhorst <lordrex34@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.arctic.wolf.security.oauth2.user;

import java.util.Map;

/**
 * @author Rajeev Kumar Singh <callicoder@gmail.com>
 */
public class FacebookOAuth2UserInfo extends OAuth2UserInfo {
	public FacebookOAuth2UserInfo(Map<String, Object> attributes) {
		super(attributes);
	}
	
	@Override
	public String getId() {
		return (String) attributes.get("id");
	}
	
	@Override
	public String getName() {
		return (String) attributes.get("name");
	}
	
	@Override
	public String getEmail() {
		return (String) attributes.get("email");
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public String getImageUrl() {
		if (!attributes.containsKey("picture")) {
			return null;
		}
		
		final Map<String, Object> picture = (Map<String, Object>) attributes.get("picture");
		if (!picture.containsKey("data")) {
			return null;
		}
		
		final Map<String, Object> data = (Map<String, Object>) picture.get("data");
		if (!data.containsKey("url")) {
			return null;
		}
		
		return (String) data.get("url");
	}
}
