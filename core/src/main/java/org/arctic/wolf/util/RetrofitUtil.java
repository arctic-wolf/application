/*
 * Copyright (c) 2018 Reginald Ravenhorst <lordrex34@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.arctic.wolf.util;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import com.fasterxml.jackson.databind.ObjectMapper;
import okhttp3.Cache;
import okhttp3.CacheControl;
import okhttp3.Credentials;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.ResponseBody;
import okio.BufferedSink;
import okio.Okio;
import retrofit2.Call;
import retrofit2.Converter;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * @author UnAfraid
 */
@Slf4j
public final class RetrofitUtil {
	private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();
	private static final String AUTHORIZATION = "Authorization";
	private static final String BEARER = "Bearer ";
	
	private RetrofitUtil() {
		// utility class
	}
	
	public static Retrofit get(String url, Converter.Factory factory, String username, String password, File cacheDir, int maxStale, TimeUnit timeUnit) {
		return new Retrofit.Builder()
			.baseUrl(url)
			.addConverterFactory(factory)
			.client(new OkHttpClient.Builder()
				.addInterceptor(chain -> {
					final Request original = chain.request();
					final Request newRequest = original
						.newBuilder()
						.addHeader(AUTHORIZATION, Credentials.basic(username, password))
						.cacheControl(new CacheControl
							.Builder()
							.maxStale(maxStale, timeUnit)
							.build())
						.build();
					return chain.proceed(newRequest);
				})
				.cache(cacheDir != null ? new Cache(cacheDir, Integer.MAX_VALUE) : null)
				.build())
			.build();
	}
	
	public static Retrofit get(String url, Converter.Factory factory, String token, File cacheDir, int maxStale, TimeUnit timeUnit) {
		return new Retrofit.Builder()
			.baseUrl(url)
			.addConverterFactory(factory)
			.client(new OkHttpClient.Builder()
				.addInterceptor(chain -> {
					final Request original = chain.request();
					final Request newRequest = original
						.newBuilder()
						.addHeader(AUTHORIZATION, BEARER + token)
						.cacheControl(new CacheControl
							.Builder()
							.maxStale(maxStale, timeUnit)
							.build())
						.build();
					return chain.proceed(newRequest);
				})
				.cache(cacheDir != null ? new Cache(cacheDir, Integer.MAX_VALUE) : null)
				.build())
			.build();
	}
	
	public static Retrofit get(String url, Converter.Factory factory, File cacheDir, int maxStale, TimeUnit timeUnit) {
		return new Retrofit.Builder()
			.baseUrl(url)
			.addConverterFactory(factory)
			.client(new OkHttpClient.Builder()
				.addInterceptor(chain -> {
					final Request original = chain.request();
					final Request newRequest = original
						.newBuilder()
						.cacheControl(new CacheControl
							.Builder()
							.maxStale(maxStale, timeUnit)
							.build())
						.build();
					return chain.proceed(newRequest);
				})
				.cache(cacheDir != null ? new Cache(cacheDir, Integer.MAX_VALUE) : null)
				.build())
			.build();
	}
	
	public static <T> T getResponse(Call<T> call) throws IOException {
		final Response<T> response = call.execute();
		if (!response.isSuccessful()) {
			throw new IOException(buildErrorResponse(response));
		}
		return response.body();
	}
	
	private static <T> String buildErrorResponse(Response<T> response) throws IOException {
		final StringBuilder sb = new StringBuilder(response.message());
		
		final ResponseBody errorBody = response.errorBody();
		errorBodyBuilding:
		if (errorBody != null) {
			final String string = errorBody.string();
			if (StringUtils.isEmpty(string)) {
				break errorBodyBuilding;
			}
			
			final ErrorResponse errorResponse = OBJECT_MAPPER.readValue(string, ErrorResponse.class);
			
			sb.append(" ");
			sb.append(errorResponse.getCode());
			
			sb.append(" ");
			sb.append(errorResponse.getMessage());
		}
		
		return sb.toString();
	}
	
	@Data
	private static class ErrorResponse {
		private int code;
		private String message;
	}
	
	public static void onResponseDownload(Response<ResponseBody> response, File file, String downloadURL) throws IOException {
		if (!response.isSuccessful()) {
			LOGGER.warn("Repository is not answering!");
			return;
		}
		
		try (BufferedSink sink = Okio.buffer(Okio.sink(file))) {
			final ResponseBody responseBody = response.body();
			Objects.requireNonNull(responseBody, "Response body is null!");
			sink.writeAll(responseBody.source());
			
			LOGGER.info("Downloaded file: '{}' from site: '{}'", file.getName(), downloadURL);
		}
	}
	
	public static String asUrlList(List<String> list) {
		final StringBuilder sb = new StringBuilder();
		list.forEach(s -> sb.append(s).append(","));
		return sb.toString();
	}
}
