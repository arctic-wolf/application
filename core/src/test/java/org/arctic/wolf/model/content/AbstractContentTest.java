/*
 * Copyright (c) 2019 Reginald Ravenhorst <lordrex34@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.arctic.wolf.model.content;

import java.util.Date;
import java.util.EnumSet;
import org.springframework.beans.factory.annotation.Autowired;

import org.arctic.wolf.model.content.dto.form.IContentForm;
import org.arctic.wolf.model.plugin.PluginService;
import org.arctic.wolf.model.server.ServerSettingsService;
import org.mockito.Mockito;

abstract class AbstractContentTest {
	static final String TEST_AUTHOR = "lord_rex";
	
	static final String TEST_PAGE_TITLE = "Page Title";
	static final String TEST_PAGE_CONTENT = "My page content goes here.";
	static final String TEST_PAGE_CONTENT_UPDATED = "My page content goes here. Updated.";
	static final String TEST_PAGE_FRIENDLY_URL = "test-page";
	
	static final String TEST_POST_TITLE = "Post Title";
	static final String TEST_POST_CONTENT = "My post content goes here.";
	static final String TEST_POST_CONTENT_UPDATED = "My post content goes here. Updated";
	static final String TEST_POST_FRIENDLY_URL = "test-post";
	
	static final String LANG = "en";
	
	@Autowired
	protected ContentService contentService;
	
	@Autowired
	protected PluginService pluginService;
	
	@Autowired
	protected ServerSettingsService serverSettingsService;
	
	static IContentForm mockContentForm(ContentType type, String friendlyURL, String title, String content) {
		final IContentForm contentForm = Mockito.mock(IContentForm.class);
		Mockito.when(contentForm.getType()).thenReturn(type);
		Mockito.when(contentForm.getFriendlyURL()).thenReturn(friendlyURL);
		Mockito.when(contentForm.getTitle()).thenReturn(title);
		Mockito.when(contentForm.getContent()).thenReturn(content);
		Mockito.when(contentForm.getAuthor()).thenReturn(TEST_AUTHOR);
		Mockito.when(contentForm.getLang()).thenReturn(LANG);
		Mockito.when(contentForm.getCreateTime()).thenReturn(new Date());
		Mockito.when(contentForm.getVisibility()).thenReturn(EnumSet.allOf(ContentVisibility.class));
		return contentForm;
	}
}
