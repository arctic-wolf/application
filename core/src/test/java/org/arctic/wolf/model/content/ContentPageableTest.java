/*
 * Copyright (c) 2019 Reginald Ravenhorst <lordrex34@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.arctic.wolf.model.content;

import static org.arctic.wolf.util.PageTestUtil.createPageable;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.arctic.wolf.exception.ContentNotFoundException;
import org.arctic.wolf.model.content.dto.Content;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.jdbc.EmbeddedDatabaseConnection;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * @author lord_rex
 */
@ExtendWith(SpringExtension.class)
@EnableTransactionManagement
@SpringBootTest
@AutoConfigureTestDatabase(connection = EmbeddedDatabaseConnection.H2)
class ContentPageableTest extends AbstractContentTest {
	@BeforeEach
	void before() {
		contentService.deleteAll();
	}
	
	private void create() {
		contentService.createContent(mockContentForm(ContentType.PAGE, TEST_PAGE_FRIENDLY_URL, TEST_PAGE_TITLE, TEST_PAGE_CONTENT));
	}
	
	@Test
	void testSearchDB() {
		create();
		
		final Page<ContentDB> page = contentService.pagedFuzzySearchDB(LANG, TEST_PAGE_TITLE, ContentType.PAGE, createPageable());
		assertEquals(TEST_PAGE_TITLE, page.getContent()
			.stream()
			.findFirst()
			.orElseThrow(ContentNotFoundException::new)
			.getTitle());
	}
	
	@Test
	void testSearchDTO() {
		create();
		
		final Page<Content> page = contentService.pagedFuzzySearchDTO(LANG, TEST_PAGE_TITLE, ContentType.PAGE, createPageable());
		assertEquals(TEST_PAGE_TITLE, page.getContent()
			.stream()
			.findFirst()
			.orElseThrow(ContentNotFoundException::new)
			.getTitle());
	}
}
