/*
 * Copyright (c) 2017 Reginald Ravenhorst <lordrex34@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.arctic.wolf.model.content;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.arctic.wolf.exception.ContentNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.jdbc.EmbeddedDatabaseConnection;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * @author lord_rex
 */
@ExtendWith(SpringExtension.class)
@EnableTransactionManagement
@SpringBootTest
@AutoConfigureTestDatabase(connection = EmbeddedDatabaseConnection.H2)
class ContentTest extends AbstractContentTest {
	@BeforeEach
	void before() {
		contentService.deleteAll();
	}
	
	private IContent createPage() {
		return contentService.createContent(mockContentForm(ContentType.PAGE, TEST_PAGE_FRIENDLY_URL, TEST_PAGE_TITLE, TEST_PAGE_CONTENT));
	}
	
	private IContent createPost() {
		return contentService.createContent(mockContentForm(ContentType.POST, TEST_POST_FRIENDLY_URL, TEST_POST_TITLE, TEST_POST_CONTENT));
	}
	
	@Test
	void testCreatePage() {
		final IContent page = createPage();
		
		assertTrue(contentService.getContentDB(ContentType.PAGE, LANG, page.getId()).isPresent());
		assertTrue(contentService.getContentDB(ContentType.PAGE, LANG, TEST_PAGE_FRIENDLY_URL).isPresent());
		assertTrue(contentService.getContentDTO(ContentType.PAGE, LANG, page.getId()).isPresent());
		assertTrue(contentService.getContentDTO(ContentType.PAGE, LANG, TEST_PAGE_FRIENDLY_URL).isPresent());
		assertNotNull(contentService.getContent(ContentType.PAGE, LANG, page.getId(), "", null, pluginService, serverSettingsService));
		assertNotNull(contentService.getContent(ContentType.PAGE, LANG, TEST_PAGE_FRIENDLY_URL, "", null, pluginService, serverSettingsService));
		assertTrue(contentService.exists(ContentType.PAGE, LANG, page.getId()));
		assertTrue(contentService.exists(ContentType.PAGE, LANG, TEST_PAGE_FRIENDLY_URL));
	}
	
	@Test
	void testCreatePost() {
		final IContent post = createPost();
		
		assertTrue(contentService.getContentDB(ContentType.POST, LANG, post.getId()).isPresent());
		assertTrue(contentService.getContentDB(ContentType.POST, LANG, TEST_POST_FRIENDLY_URL).isPresent());
		assertTrue(contentService.getContentDTO(ContentType.POST, LANG, post.getId()).isPresent());
		assertTrue(contentService.getContentDTO(ContentType.POST, LANG, TEST_POST_FRIENDLY_URL).isPresent());
		assertNotNull(contentService.getContent(ContentType.POST, LANG, post.getId(), "", null, pluginService, serverSettingsService));
		assertNotNull(contentService.getContent(ContentType.POST, LANG, TEST_POST_FRIENDLY_URL, "", null, pluginService, serverSettingsService));
		assertTrue(contentService.exists(ContentType.POST, LANG, post.getId()));
		assertTrue(contentService.exists(ContentType.POST, LANG, TEST_POST_FRIENDLY_URL));
	}
	
	@Test
	void testUpdatePageDB() {
		final IContent page = createPage();
		
		final ContentDB contentBeforeUpdate = contentService.getContentDB(ContentType.PAGE, LANG, page.getId())
			.orElseThrow(ContentNotFoundException::new);
		
		assertEquals(contentBeforeUpdate.getContent(), TEST_PAGE_CONTENT);
		
		contentBeforeUpdate.setContent(TEST_PAGE_CONTENT_UPDATED);
		
		contentService.update(contentBeforeUpdate);
		
		final ContentDB contentAfterUpdate = contentService.getContentDB(ContentType.PAGE, LANG, page.getId())
			.orElseThrow(ContentNotFoundException::new);
		
		assertEquals(contentAfterUpdate.getContent(), TEST_PAGE_CONTENT_UPDATED);
	}
	
	@Test
	void testUpdatePostDB() {
		final IContent post = createPost();
		
		final ContentDB contentBeforeUpdate = contentService.getContentDB(ContentType.POST, LANG, post.getId())
			.orElseThrow(ContentNotFoundException::new);
		
		assertEquals(contentBeforeUpdate.getContent(), TEST_POST_CONTENT);
		
		contentBeforeUpdate.setContent(TEST_POST_CONTENT_UPDATED);
		
		contentService.update(contentBeforeUpdate);
		
		final ContentDB contentAfterUpdate = contentService.getContentDB(ContentType.POST, LANG, post.getId())
			.orElseThrow(ContentNotFoundException::new);
		
		assertEquals(contentAfterUpdate.getContent(), TEST_POST_CONTENT_UPDATED);
	}
	
	@Test
	void testDeletePage() {
		final IContent page = createPage();
		
		assertTrue(contentService.exists(ContentType.PAGE, LANG, page.getId()));
		
		contentService.delete(page.getId());
		
		assertFalse(contentService.exists(ContentType.PAGE, LANG, page.getId()));
	}
	
	@Test
	void testDeletePost() {
		final IContent post = createPost();
		
		assertTrue(contentService.exists(ContentType.POST, LANG, post.getId()));
		
		contentService.delete(post.getId());
		
		assertFalse(contentService.exists(ContentType.POST, LANG, post.getId()));
	}
}
