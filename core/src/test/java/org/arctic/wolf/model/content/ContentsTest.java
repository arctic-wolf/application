/*
 * Copyright (c) 2019 Reginald Ravenhorst <lordrex34@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.arctic.wolf.model.content;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;

import org.arctic.wolf.model.content.dto.Content;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.jdbc.EmbeddedDatabaseConnection;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * @author lord_rex
 */
@ExtendWith(SpringExtension.class)
@EnableTransactionManagement
@SpringBootTest
@AutoConfigureTestDatabase(connection = EmbeddedDatabaseConnection.H2)
class ContentsTest extends AbstractContentTest {
	@BeforeEach
	void before() {
		contentService.deleteAll();
	}
	
	private void createPages() {
		for (int i = 0; i < 5; i++) {
			contentService.createContent(mockContentForm(ContentType.PAGE, "test-page-" + i, "Test Page " + i, "This is my test page " + i + "."));
		}
	}
	
	private void createPosts() {
		for (int i = 0; i < 10; i++) {
			contentService.createContent(mockContentForm(ContentType.POST, "test-post-" + i, "Test Post " + i, "This is my test post " + i + "."));
		}
	}
	
	@Test
	void testPagesDB() {
		createPages();
		
		final List<ContentDB> pagesDB = contentService.getContentsDB(ContentType.PAGE, LANG);
		assertEquals(5, pagesDB.size());
	}
	
	@Test
	void testPagesDTO() {
		createPages();
		
		final List<Content> pagesDTO = contentService.getContentsDTO(ContentType.PAGE, LANG);
		assertEquals(5, pagesDTO.size());
	}
	
	@Test
	void testPages() {
		createPages();
		
		final List<Content> pages = contentService.getContents(ContentType.PAGE, LANG, null);
		assertEquals(5, pages.size());
	}
	
	@Test
	void testPostsDB() {
		createPosts();
		
		final List<ContentDB> postsDB = contentService.getContentsDB(ContentType.POST, LANG);
		assertEquals(10, postsDB.size());
	}
	
	@Test
	void testPostsDTO() {
		createPosts();
		
		final List<Content> postsDTO = contentService.getContentsDTO(ContentType.POST, LANG);
		assertEquals(10, postsDTO.size());
	}
	
	@Test
	void testPosts() {
		createPosts();
		
		final List<Content> posts = contentService.getContents(ContentType.POST, LANG, null);
		assertEquals(10, posts.size());
	}
}
