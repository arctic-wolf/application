/*
 * Copyright (c) 2019 Reginald Ravenhorst <lordrex34@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.arctic.wolf.model.group;

import static org.arctic.wolf.util.PageTestUtil.createPageable;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Date;
import org.springframework.beans.factory.annotation.Autowired;

import org.arctic.wolf.exception.GroupNotFoundException;
import org.arctic.wolf.model.group.dto.Group;
import org.arctic.wolf.model.group.dto.form.IGroupForm;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.boot.jdbc.EmbeddedDatabaseConnection;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * @author lord_rex
 */
@ExtendWith(SpringExtension.class)
@EnableTransactionManagement
@SpringBootTest
@AutoConfigureTestDatabase(connection = EmbeddedDatabaseConnection.H2)
class GroupPageableTest {
	private static final String TEST_GROUP_NAME = "testGroupName";
	
	@Autowired
	private GroupService groupService;
	
	private void create() {
		final IGroupForm groupForm = Mockito.mock(IGroupForm.class);
		Mockito.when(groupForm.getName()).thenReturn(TEST_GROUP_NAME);
		Mockito.when(groupForm.getCreateTime()).thenReturn(new Date());
		groupService.createByForm(groupForm);
	}
	
	@BeforeEach
	void before() {
		groupService.deleteAll();
	}
	
	@Test
	void testUnpagedAllDB() {
		create();
		
		assertEquals(groupService.count(), groupService.getGroupsDB(Pageable.unpaged()).getTotalElements());
	}
	
	@Test
	void testUnpagedAllDTO() {
		create();
		
		assertEquals(groupService.count(), groupService.getGroupsDTO(Pageable.unpaged()).getTotalElements());
	}
	
	@Test
	void testSearchDB() {
		create();
		
		final Page<GroupDB> page = groupService.pagedFuzzySearchDB(TEST_GROUP_NAME, createPageable());
		
		assertEquals(TEST_GROUP_NAME, page.getContent()
			.stream()
			.findFirst()
			.orElseThrow(GroupNotFoundException::new)
			.getName());
	}
	
	@Test
	void testSearchDTO() {
		create();
		
		final Page<Group> page = groupService.pagedFuzzySearchDTO(TEST_GROUP_NAME, createPageable());
		
		assertEquals(TEST_GROUP_NAME, page.getContent()
			.stream()
			.findFirst()
			.orElseThrow(GroupNotFoundException::new)
			.getName());
	}
}
