/*
 * Copyright (c) 2018 Reginald Ravenhorst <lordrex34@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.arctic.wolf.model.group;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.EnumSet;
import org.springframework.beans.factory.annotation.Autowired;

import org.arctic.wolf.exception.GroupNotFoundException;
import org.arctic.wolf.model.group.dto.Group;
import org.arctic.wolf.model.group.dto.form.IGroupForm;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.boot.jdbc.EmbeddedDatabaseConnection;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * @author lord_rex
 */
@ExtendWith(SpringExtension.class)
@EnableTransactionManagement
@SpringBootTest
@AutoConfigureTestDatabase(connection = EmbeddedDatabaseConnection.H2)
class GroupTest {
	private static final String GROUP_NAME = "TestGroup";
	private static final String DESCRIPTION = "Test group description.";
	private static final String DESCRIPTION_UPDATED = DESCRIPTION + " Updated.";
	
	private static final String ADMIN_GROUP_NAME = "TestAdminGroup";
	private static final String ADMIN_DESCRIPTION = "Test group admin description.";
	
	@Autowired
	private GroupService groupService;
	
	@BeforeEach
	void before() {
		groupService.deleteAll();
	}
	
	private void create() {
		final IGroupForm groupForm = Mockito.mock(IGroupForm.class);
		Mockito.when(groupForm.getName()).thenReturn(GROUP_NAME);
		Mockito.when(groupForm.getDescription()).thenReturn(DESCRIPTION);
		Mockito.when(groupForm.getAuthorities()).thenReturn(EnumSet.of(Authority.REGULAR_USER));
		groupService.createByForm(groupForm);
	}
	
	private void createAdmin() {
		final IGroupForm groupForm = Mockito.mock(IGroupForm.class);
		Mockito.when(groupForm.getName()).thenReturn(ADMIN_GROUP_NAME);
		Mockito.when(groupForm.getDescription()).thenReturn(ADMIN_DESCRIPTION);
		Mockito.when(groupForm.getAuthorities()).thenReturn(EnumSet.of(Authority.ADMIN_SITE));
		groupService.createByForm(groupForm);
	}
	
	@Test
	void testCreate() {
		create();
		
		assertTrue(groupService.count() > 0);
		assertTrue(groupService.exists(GROUP_NAME));
		
		final GroupDB byNameDB = groupService.getGroupDB(GROUP_NAME)
			.orElseThrow(GroupNotFoundException::new);
		assertNotNull(byNameDB);
		
		final GroupDB byIdDB = groupService.getGroupDB(byNameDB.getId())
			.orElseThrow(GroupNotFoundException::new);
		assertNotNull(byIdDB);
		
		final Group byNameDTO = groupService.getGroupDTO(GROUP_NAME)
			.orElseThrow(GroupNotFoundException::new);
		assertNotNull(byNameDTO);
		
		final Group byIdDTO = groupService.getGroupDTO(byNameDTO.getId())
			.orElseThrow(GroupNotFoundException::new);
		assertNotNull(byIdDTO);
	}
	
	@Test
	void testUpdateDB() {
		create();
		
		final GroupDB groupBeforeUpdate = groupService.getGroupDB(GROUP_NAME)
			.orElseThrow(GroupNotFoundException::new);
		assertEquals(groupBeforeUpdate.getDescription(), DESCRIPTION);
		
		groupBeforeUpdate.setDescription(DESCRIPTION_UPDATED);
		groupService.update(groupBeforeUpdate);
		
		final GroupDB groupAfterUpdate = groupService.getGroupDB(GROUP_NAME)
			.orElseThrow(GroupNotFoundException::new);
		assertEquals(DESCRIPTION_UPDATED, groupAfterUpdate.getDescription());
	}
	
	@Test
	void testUpdateDTO() {
		create();
		
		final GroupDB groupBeforeUpdate = groupService.getGroupDB(GROUP_NAME)
			.orElseThrow(GroupNotFoundException::new);
		assertEquals(groupBeforeUpdate.getDescription(), DESCRIPTION);
		
		groupBeforeUpdate.setDescription(DESCRIPTION_UPDATED);
		groupService.update(groupBeforeUpdate);
		
		final Group groupAfterUpdate = groupService.getGroupDTO(GROUP_NAME)
			.orElseThrow(GroupNotFoundException::new);
		assertEquals(DESCRIPTION_UPDATED, groupAfterUpdate.getDescription());
	}
	
	@Test
	void testDelete() {
		create();
		
		final GroupDB groupDB = groupService.getGroupDB(GROUP_NAME)
			.orElseThrow(GroupNotFoundException::new);
		assertNotNull(groupDB);
		groupService.delete(groupDB.getId());
		assertTrue(groupService.getGroupDB(GROUP_NAME).isEmpty());
	}
}
