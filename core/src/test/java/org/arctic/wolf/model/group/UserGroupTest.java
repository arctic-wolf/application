/*
 * Copyright (c) 2018 Reginald Ravenhorst <lordrex34@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.arctic.wolf.model.group;

import static org.arctic.wolf.util.PageTestUtil.createPageable;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Date;
import java.util.EnumSet;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;

import org.arctic.wolf.exception.GroupNotFoundException;
import org.arctic.wolf.exception.UserNotFoundException;
import org.arctic.wolf.model.group.dto.form.IGroupForm;
import org.arctic.wolf.model.user.UserDB;
import org.arctic.wolf.model.user.UserService;
import org.arctic.wolf.model.user.dto.form.IUserForm;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.boot.jdbc.EmbeddedDatabaseConnection;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * @author lord_rex
 */
@ExtendWith(SpringExtension.class)
@EnableTransactionManagement
@SpringBootTest
@AutoConfigureTestDatabase(connection = EmbeddedDatabaseConnection.H2)
class UserGroupTest {
	
	private static final String USER_GROUP_NAME = "UserGroup";
	private static final String USER_GROUP_DESCRIPTION = "This is an user group for testing. Number in row is: ";
	private static final int TOTAL_GROUPS = 5;
	
	private static final String TEST_USERNAME = "testUsername";
	private static final String TEST_PASSWORD = "testPassword";
	private static final String TEST_EMAIL = "test@localhost";
	private static final String TEST_DISPLAY_NAME = "Test Display Name";
	private static final Date TEST_BIRTHDATE = new Date();
	private static final int TOTAL_USERS = 15;
	
	@Autowired
	private GroupService groupService;
	
	@Autowired
	private UserService userService;
	
	@BeforeEach
	void before() {
		groupService.deleteAll();
		userService.deleteAll();
		
		createGroups();
		createUsers();
	}
	
	private void createGroups() {
		for (int i = 0; i < TOTAL_GROUPS; i++) {
			final IGroupForm groupForm = Mockito.mock(IGroupForm.class);
			Mockito.when(groupForm.getName()).thenReturn(USER_GROUP_NAME + i);
			Mockito.when(groupForm.getDescription()).thenReturn(USER_GROUP_DESCRIPTION + i);
			Mockito.when(groupForm.getAuthorities()).thenReturn(EnumSet.of(Authority.REGULAR_USER));
			groupService.createByForm(groupForm);
		}
		
		assertEquals(TOTAL_GROUPS, groupService.count());
	}
	
	private void createUsers() {
		for (int i = 0; i < TOTAL_USERS; i++) {
			final IUserForm userForm = Mockito.mock(IUserForm.class);
			Mockito.when(userForm.getUsername()).thenReturn(TEST_USERNAME + i);
			Mockito.when(userForm.getPassword()).thenReturn(TEST_PASSWORD + i);
			Mockito.when(userForm.getEmail()).thenReturn(TEST_EMAIL + i);
			Mockito.when(userForm.getDisplayName()).thenReturn(TEST_DISPLAY_NAME + i);
			Mockito.when(userForm.getBirthDate()).thenReturn(TEST_BIRTHDATE);
			Mockito.when(userForm.isActive()).thenReturn(true);
			userService.createByForm(userForm);
		}
		
		assertEquals(TOTAL_USERS, userService.count());
	}
	
	private void addUsers(String groupName) {
		final GroupDB testGroup = groupService.getGroupDB(groupName)
			.orElseThrow(GroupNotFoundException::new);
		assertNotNull(testGroup);
		assertEquals(0, testGroup.getUsers().size());
		
		final List<UserDB> threeTestUsers = getThreeTestUsers();
		for (int i = 0; i < threeTestUsers.size(); i++) {
			final UserDB userDB = threeTestUsers.get(i);
			if (i == 2) {
				groupService.addUser(testGroup.getId(), userDB);
			}
			else {
				groupService.addUser(groupName, userDB);
			}
		}
		
		assertEquals(3, groupService.getUsersDB(groupName).size());
		assertEquals(3, groupService.getUsersDTO(groupName).size());
		assertEquals(3, groupService.getPagedUsersDTO(createPageable(), groupName).getTotalElements());
		
		final long groupId = testGroup.getId();
		assertEquals(3, groupService.getUsersDB(groupId).size());
		assertEquals(3, groupService.getUsersDTO(groupId).size());
		assertEquals(3, groupService.getPagedUsersDTO(createPageable(), groupId).getTotalElements());
	}
	
	private List<UserDB> getThreeTestUsers() {
		return List.of(
			userService.findByUsernameDB(TEST_USERNAME + 2).orElseThrow(UserNotFoundException::new),
			userService.findByUsernameDB(TEST_USERNAME + 3).orElseThrow(UserNotFoundException::new),
			userService.findByUsernameDB(TEST_USERNAME + 4).orElseThrow(UserNotFoundException::new)
		);
	}
	
	@Test
	void testAddAllUsers() {
		final String groupName = USER_GROUP_NAME + 1;
		addUsers(groupName);
	}
	
	@Test
	void testRemoveAllUsers() {
		final String groupName = USER_GROUP_NAME + 2;
		final GroupDB testGroup = groupService.getGroupDB(groupName)
			.orElseThrow(GroupNotFoundException::new);
		addUsers(groupName);
		
		final List<UserDB> threeTestUsers = getThreeTestUsers();
		for (int i = 0; i < threeTestUsers.size(); i++) {
			final UserDB userDB = threeTestUsers.get(i);
			if (i == 2) {
				groupService.removeUser(testGroup.getId(), userDB);
			}
			else {
				groupService.removeUser(groupName, userDB);
			}
		}
		
		assertEquals(0, groupService.getGroupDB(groupName)
			.orElseThrow(GroupNotFoundException::new).getUsers().size());
	}
	
	@Test
	void addSingleUser() {
		final String groupName = USER_GROUP_NAME + 3;
		
		final GroupDB testGroup = groupService.getGroupDB(groupName)
			.orElseThrow(GroupNotFoundException::new);
		assertNotNull(testGroup);
		assertEquals(0, testGroup.getUsers().size());
		
		final UserDB singleUser = userService.findByUsernameDB(TEST_USERNAME + 3).orElseThrow(UserNotFoundException::new);
		groupService.addUser(groupName, singleUser);
		
		assertTrue(groupService.hasUser(groupName, singleUser));
		assertTrue(groupService.hasUser(testGroup.getId(), singleUser));
		assertEquals(1, groupService.getGroupDB(groupName)
			.orElseThrow(GroupNotFoundException::new).getUsers().size());
	}
	
	@Test
	void removeSingleUser() {
		final String groupName = USER_GROUP_NAME + 4;
		
		addUsers(groupName);
		
		final UserDB singleUser = userService.findByUsernameDB(TEST_USERNAME + 3).orElseThrow(UserNotFoundException::new);
		groupService.removeUser(groupName, singleUser);
		
		assertFalse(groupService.hasUser(groupName, singleUser));
		assertEquals(2, groupService.getGroupDB(groupName)
			.orElseThrow(GroupNotFoundException::new).getUsers().size());
	}
}
