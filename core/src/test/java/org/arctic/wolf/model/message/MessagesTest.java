/*
 * Copyright (c) 2018 Reginald Ravenhorst <lordrex34@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.arctic.wolf.model.message;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;
import org.springframework.beans.factory.annotation.Autowired;

import org.apache.commons.lang3.StringUtils;
import org.arctic.wolf.exception.MessageNotFoundException;
import org.arctic.wolf.model.message.dto.form.IMessageForm;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.boot.jdbc.EmbeddedDatabaseConnection;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * TODO clean this mess up in JIRA CORE-75
 * @author lord_rex
 */
@ExtendWith(SpringExtension.class)
@EnableTransactionManagement
@SpringBootTest
@AutoConfigureTestDatabase(connection = EmbeddedDatabaseConnection.H2)
class MessagesTest {
	private static final String LANGUAGE = "en";
	private static final String MOCK_CODE = "my_messages_mock_code";
	private static final String MOCK_MESSAGE = "Yeah, I am mocking you!";
	
	@Autowired
	private MessageService messageService;
	
	@Autowired
	private MessageMaintenance messageMaintenance;
	
	@Test
	void testMessagesCount() throws IOException {
		final long messageServiceCount = messageService.count();
		final int propertiesCount = messageMaintenance.getProperties().size();
		
		assertNotEquals(0, messageServiceCount);
		assertNotEquals(0, propertiesCount);
		assertEquals(messageServiceCount, propertiesCount);
	}
	
	@Test
	void testMessageExistence() throws IOException {
		final String anyCode = String.valueOf(messageMaintenance.getProperties().entrySet().iterator().next().getKey());
		final String message = messageService.getMessage(anyCode, LANGUAGE);
		assertTrue(StringUtils.isNotEmpty(message));
	}
	
	private IMessageForm mockMessageForm() {
		final IMessageForm messageForm = Mockito.mock(IMessageForm.class);
		Mockito.when(messageForm.getCode()).thenReturn(MOCK_CODE);
		Mockito.when(messageForm.getLanguage()).thenReturn(LANGUAGE);
		Mockito.when(messageForm.getMessage()).thenReturn(MOCK_MESSAGE);
		return messageForm;
	}
	
	@Test
	void createAndDeleteTest() {
		messageService.createByForm(mockMessageForm());
		
		final MessageDB messageDB = messageService.getMessageDB(MOCK_CODE, LANGUAGE)
			.orElseThrow(MessageNotFoundException::new);
		final Long id = messageDB.getId();
		
		assertTrue(messageService.findByIdDB(id).isPresent());
		assertTrue(messageService.findByIdDTO(id).isPresent());
		
		messageService.delete(id);
		
		assertTrue(messageService.findByIdDB(id).isEmpty());
		assertTrue(messageService.findByIdDTO(id).isEmpty());
	}
}
