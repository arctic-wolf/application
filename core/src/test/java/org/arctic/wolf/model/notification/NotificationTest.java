/*
 * Copyright (c) 2019 Reginald Ravenhorst <lordrex34@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.arctic.wolf.model.notification;

import static org.arctic.wolf.model.user.UserTestVariables.createRandomUserForm;
import static org.arctic.wolf.model.user.UserTestVariables.createUserForm;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import org.springframework.beans.factory.annotation.Autowired;

import org.arctic.wolf.model.notification.dto.Notification;
import org.arctic.wolf.model.user.UserDB;
import org.arctic.wolf.model.user.UserService;
import org.arctic.wolf.model.user.dto.form.IUserForm;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.jdbc.EmbeddedDatabaseConnection;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@ExtendWith(SpringExtension.class)
@EnableTransactionManagement
@SpringBootTest
@AutoConfigureTestDatabase(connection = EmbeddedDatabaseConnection.H2)
class NotificationTest {
	private static final int TEST_MULTIPLE_USER_ROUNDS = 3;
	private static final int TEST_MULTIPLE_NOTIFICATION_ROUNDS = 10;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private NotificationService notificationService;
	
	UserDB createUser() {
		final IUserForm userForm = createUserForm();
		return userService.createByForm(userForm);
	}
	
	UserDB createRandomUser() {
		final IUserForm userForm = createRandomUserForm();
		return userService.createByForm(userForm);
	}
	
	INotification createNotification(UserDB userDB) {
		return notificationService.addNotification(userDB, "fa fa fas fa-heart", "Love Wolves " + ThreadLocalRandom.current().nextInt(), "Wolves are majestic animals. They are absolutely lovely." + ThreadLocalRandom.current().nextLong() + " wolves to be adopted!");
	}
	
	@BeforeEach
	void before() {
		userService.deleteAll();
		notificationService.deleteAll();
	}
	
	@Test
	void testCreate() {
		final UserDB userDB = createUser();
		final INotification notification = createNotification(userDB);
		
		final NotificationDB notificationDB = notificationService.findById(notification.getId()).orElseThrow();
		assertEquals(notification.getId(), notificationDB.getId());
		assertEquals(notification.getIcon(), notificationDB.getIcon());
		assertEquals(notification.getTitle(), notificationDB.getTitle());
		assertEquals(notification.getMessage(), notificationDB.getMessage());
		assertEquals(notification.getDateTime(), notificationDB.getDateTime());
		assertEquals(userDB, notificationDB.getUserDB());
	}
	
	@Test
	void testMultipleNotifications() {
		final UserDB userDB = createUser();
		
		for (int n = 0; n < TEST_MULTIPLE_NOTIFICATION_ROUNDS; n++) {
			createNotification(userDB);
		}
		
		assertEquals(TEST_MULTIPLE_NOTIFICATION_ROUNDS, notificationService.count());
	}
	
	@Test
	void testMultipleUsersAndNotifications() {
		for (int u = 0; u < TEST_MULTIPLE_USER_ROUNDS; u++) {
			final UserDB userDB = createRandomUser();
			
			for (int n = 0; n < TEST_MULTIPLE_NOTIFICATION_ROUNDS; n++) {
				createNotification(userDB);
			}
		}
		
		assertEquals(TEST_MULTIPLE_USER_ROUNDS * TEST_MULTIPLE_NOTIFICATION_ROUNDS, notificationService.count());
	}
	
	@Test
	void testGetNotificationsDB() {
		final UserDB userDB = createUser();
		final INotification notification = createNotification(userDB);
		
		final List<NotificationDB> notifications = notificationService.getNotificationsDB(userDB);
		assertNotNull(notification);
		assertEquals(1, notifications.size());
		
		for (int n = 0; n < TEST_MULTIPLE_NOTIFICATION_ROUNDS; n++) {
			createNotification(userDB);
		}
		
		assertEquals(1 + TEST_MULTIPLE_NOTIFICATION_ROUNDS, notificationService.getNotificationsDB(userDB).size());
	}
	
	@Test
	void testGetNotificationsDTO() {
		final UserDB userDB = createUser();
		final INotification notification = createNotification(userDB);
		
		final List<Notification> notifications = notificationService.getNotificationsDTO(userDB);
		assertNotNull(notification);
		assertEquals(1, notifications.size());
		
		for (int n = 0; n < TEST_MULTIPLE_NOTIFICATION_ROUNDS; n++) {
			createNotification(userDB);
		}
		
		assertEquals(1 + TEST_MULTIPLE_NOTIFICATION_ROUNDS, notificationService.getNotificationsDTO(userDB).size());
	}
	
	@Test
	void testRemoveNotification() {
		final UserDB userDB = createUser();
		
		INotification lastNotification = null;
		for (int n = 0; n < TEST_MULTIPLE_NOTIFICATION_ROUNDS; n++) {
			lastNotification = createNotification(userDB);
		}
		
		notificationService.removeNotification(userDB, lastNotification.getId());
		
		assertEquals(TEST_MULTIPLE_NOTIFICATION_ROUNDS - 1, notificationService.getNotificationsDB(userDB).size());
		assertEquals(TEST_MULTIPLE_NOTIFICATION_ROUNDS - 1, notificationService.getNotificationsDTO(userDB).size());
	}
	
	@Test
	void testEmpty() {
		final UserDB userDB = createUser();
		
		final List<NotificationDB> notifications = notificationService.getNotificationsDB(userDB);
		assertNotNull(notifications);
		assertTrue(notifications.isEmpty());
		
		final List<Notification> notificationsAsDTO = notificationService.getNotificationsDTO(userDB);
		assertNotNull(notificationsAsDTO);
		assertTrue(notificationsAsDTO.isEmpty());
	}
}
