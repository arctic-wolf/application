/*
 * Copyright (c) 2018 Reginald Ravenhorst <lordrex34@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.arctic.wolf.model.plugin;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

import org.springframework.beans.factory.annotation.Autowired;

import org.arctic.wolf.util.JarFileUtil;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.boot.jdbc.EmbeddedDatabaseConnection;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * @author lord_rex
 */
@ExtendWith(SpringExtension.class)
@EnableTransactionManagement
@SpringBootTest
@AutoConfigureTestDatabase(connection = EmbeddedDatabaseConnection.H2)
class PluginTest {
	private static final String TEST_NAME = "testPluginName";
	private static final int TEST_VERSION = 1;
	private static final int MOCK_VERSION = 2;
	
	@Autowired
	private PluginDBService pluginDBService;
	
	@Test
	void testCreateAndDelete() {
		pluginDBService.update(createPluginDB());
		
		final PluginDB pluginDB = pluginDBService.findByName(TEST_NAME);
		assertNotNull(pluginDB);
		
		pluginDBService.delete(pluginDB.getId());
		
		assertNull(pluginDBService.findByName(TEST_NAME));
	}
	
	private PluginDB createPluginDB() {
		final PluginDB pluginDB = new PluginDB();
		pluginDB.setName(TEST_NAME);
		pluginDB.setVersion(TEST_VERSION);
		pluginDB.setJarFileName(JarFileUtil.camelToHypenJarName(TEST_NAME));
		return pluginDB;
	}
	
	@Test
	void testUpdateFromSite() {
		pluginDBService.update(createPluginDB());
		assertNotNull(pluginDBService.findByName(TEST_NAME));
		
		pluginDBService.updateFromSite(mockRemotePlugin());
		
		final PluginDB pluginDB = pluginDBService.findByName(TEST_NAME);
		assertNotNull(pluginDB);
		assertEquals(MOCK_VERSION, pluginDB.getVersion());
	}
	
	private IRemotePlugin mockRemotePlugin() {
		final IRemotePlugin remotePlugin = Mockito.mock(IRemotePlugin.class);
		Mockito.when(remotePlugin.getName()).thenReturn(TEST_NAME);
		Mockito.when(remotePlugin.getVersion()).thenReturn(MOCK_VERSION);
		return remotePlugin;
	}
}
