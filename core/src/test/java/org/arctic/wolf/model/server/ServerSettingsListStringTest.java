/*
 * Copyright (c) 2019 Reginald Ravenhorst <lordrex34@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.arctic.wolf.model.server;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.jdbc.EmbeddedDatabaseConnection;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * @author lord_rex
 */
@ExtendWith(SpringExtension.class)
@EnableTransactionManagement
@SpringBootTest
@AutoConfigureTestDatabase(connection = EmbeddedDatabaseConnection.H2)
class ServerSettingsListStringTest {
	private static final String TEST_KEY = "testKey";
	private static final String TEST_VALUE = "testValue1,testValue2,testValue3";
	private static final String TEST_VALUE_UPDATED = TEST_VALUE + ",testValue4,testValue5";
	private static final String TEST_NEW_VALUE = "testNewValue";
	
	@Autowired
	private ServerSettingsService serverSettingsService;
	
	@BeforeEach
	void before() {
		serverSettingsService.deleteAll();
	}
	
	private void create() {
		serverSettingsService.setProperty(TEST_KEY, TEST_VALUE);
	}
	
	@Test
	void testCreate() {
		create();
		
		final List<String> afterCreate = serverSettingsService.getList(TEST_KEY, String.class, null);
		assertNotNull(afterCreate);
		
		for (int i = 0; i < 3; i++) {
			assertEquals("testValue" + (i + 1), afterCreate.get(i));
		}
	}
	
	@Test
	void testUpdateSetProperty() {
		create();
		
		serverSettingsService.setProperty(TEST_KEY, TEST_VALUE_UPDATED);
		
		final List<String> afterUpdate = serverSettingsService.getList(TEST_KEY, String.class, null);
		assertNotNull(afterUpdate);
		for (int i = 0; i < 5; i++) {
			assertEquals("testValue" + (i + 1), afterUpdate.get(i));
		}
	}
	
	@Test
	void testUpdateAddValue() {
		create();
		
		final List<String> beforeUpdate = serverSettingsService.getList(TEST_KEY, String.class, null);
		assertNotNull(beforeUpdate);
		
		beforeUpdate.add(TEST_NEW_VALUE);
		
		serverSettingsService.setProperty(TEST_KEY, beforeUpdate);
		
		final List<String> afterUpdate = serverSettingsService.getList(TEST_KEY, String.class, null);
		assertEquals(TEST_NEW_VALUE, afterUpdate.get(3));
	}
	
	@Test
	void testUpdateRemoveValue() {
		create();
		
		final List<String> beforeUpdate = serverSettingsService.getList(TEST_KEY, String.class, null);
		assertNotNull(beforeUpdate);
		
		beforeUpdate.remove(0);
		
		serverSettingsService.setProperty(TEST_KEY, beforeUpdate);
		
		final List<String> afterUpdate = serverSettingsService.getList(TEST_KEY, String.class, null);
		assertEquals(2, afterUpdate.size());
	}
	
	@Test
	void testDelete() {
		create();
		assertNotNull(serverSettingsService.findByName(TEST_KEY));
		serverSettingsService.delete(TEST_KEY);
		assertNull(serverSettingsService.findByName(TEST_KEY));
	}
}
