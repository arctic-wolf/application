/*
 * Copyright (c) 2019 Reginald Ravenhorst <lordrex34@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.arctic.wolf.model.server;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

import java.util.Map;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;

import org.arctic.wolf.model.server.dto.PublicServerProperty;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.jdbc.EmbeddedDatabaseConnection;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * @author lord_rex
 */
@ExtendWith(SpringExtension.class)
@EnableTransactionManagement
@SpringBootTest
@AutoConfigureTestDatabase(connection = EmbeddedDatabaseConnection.H2)
class ServerSettingsPublicPropertiesTest {
	private static final Map<String, Object> PUBLIC_SERVER_PROPERTY_NAMES = ServerPropertyRegistry.getPublicServerProperties();
	
	@Autowired
	private ServerSettingsService serverSettingsService;
	
	@BeforeEach
	void before() {
		serverSettingsService.deleteAll();
	}
	
	private void create() {
		PUBLIC_SERVER_PROPERTY_NAMES.forEach((propertyName, propertyValue) -> {
			// Yes, create-if-missing logic is to be applied here.
			serverSettingsService.getProperty(propertyName, propertyValue);
		});
	}
	
	@Test
	void testCount() {
		create();
		
		final Set<PublicServerProperty> publicServerProperties = serverSettingsService.getPublicServerProperties();
		assertFalse(publicServerProperties.isEmpty());
		assertEquals(PUBLIC_SERVER_PROPERTY_NAMES.size(), publicServerProperties.size());
	}
}
