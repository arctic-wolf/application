/*
 * Copyright (c) 2019 Reginald Ravenhorst <lordrex34@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.arctic.wolf.model.server;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.jdbc.EmbeddedDatabaseConnection;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * @author lord_rex
 */
@ExtendWith(SpringExtension.class)
@EnableTransactionManagement
@SpringBootTest
@AutoConfigureTestDatabase(connection = EmbeddedDatabaseConnection.H2)
class ServerSettingsSetIntegerTest {
	private static final String TEST_KEY = "testKey";
	private static final String TEST_VALUE = "1,2,3";
	private static final String TEST_VALUE_UPDATED = TEST_VALUE + ",4,5";
	private static final int TEST_NEW_VALUE = 9;
	
	@Autowired
	private ServerSettingsService serverSettingsService;
	
	@BeforeEach
	void before() {
		serverSettingsService.deleteAll();
	}
	
	private void create() {
		serverSettingsService.setProperty(TEST_KEY, TEST_VALUE);
	}
	
	@Test
	void testCreate() {
		create();
		
		final Set<Integer> afterCreate = serverSettingsService.getSet(TEST_KEY, Integer.class, null);
		assertNotNull(afterCreate);
		
		for (int i = 0; i < 3; i++) {
			assertTrue(afterCreate.contains(i + 1));
		}
	}
	
	@Test
	void testUpdateSetProperty() {
		create();
		
		serverSettingsService.setProperty(TEST_KEY, TEST_VALUE_UPDATED);
		
		final Set<Integer> afterUpdate = serverSettingsService.getSet(TEST_KEY, Integer.class, null);
		assertNotNull(afterUpdate);
		for (int i = 0; i < 5; i++) {
			assertTrue(afterUpdate.contains(i + 1));
		}
	}
	
	@Test
	void testUpdateAddValue() {
		create();
		
		final Set<Integer> beforeUpdate = serverSettingsService.getSet(TEST_KEY, Integer.class, null);
		assertNotNull(beforeUpdate);
		
		beforeUpdate.add(TEST_NEW_VALUE);
		
		serverSettingsService.setProperty(TEST_KEY, beforeUpdate);
		
		final Set<Integer> afterUpdate = serverSettingsService.getSet(TEST_KEY, Integer.class, null);
		assertTrue(afterUpdate.contains(TEST_NEW_VALUE));
	}
	
	@Test
	void testUpdateRemoveValue() {
		create();
		
		final Set<Integer> beforeUpdate = serverSettingsService.getSet(TEST_KEY, Integer.class, null);
		assertNotNull(beforeUpdate);
		
		beforeUpdate.remove(2);
		
		serverSettingsService.setProperty(TEST_KEY, beforeUpdate);
		
		final Set<Integer> afterUpdate = serverSettingsService.getSet(TEST_KEY, Integer.class, null);
		assertEquals(2, afterUpdate.size());
	}
	
	@Test
	void testDelete() {
		create();
		assertNotNull(serverSettingsService.findByName(TEST_KEY));
		serverSettingsService.delete(TEST_KEY);
		assertNull(serverSettingsService.findByName(TEST_KEY));
	}
}
