/*
 * Copyright (c) 2017 Reginald Ravenhorst <lordrex34@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.arctic.wolf.model.server;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

import org.springframework.beans.factory.annotation.Autowired;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.jdbc.EmbeddedDatabaseConnection;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * @author lord_rex
 */
@ExtendWith(SpringExtension.class)
@EnableTransactionManagement
@SpringBootTest
@AutoConfigureTestDatabase(connection = EmbeddedDatabaseConnection.H2)
class ServerSettingsTest {
	private static final String TEST_KEY = "testKey";
	private static final String TEST_VALUE = "testValue";
	private static final String TEST_VALUE_UPDATED = TEST_VALUE + "Updated";
	
	@Autowired
	private ServerSettingsService serverSettingsService;
	
	@BeforeEach
	void before() {
		serverSettingsService.deleteAll();
	}
	
	private void create() {
		// Yes, create-if-missing logic is to be applied here.
		serverSettingsService.getString(TEST_KEY, TEST_VALUE);
	}
	
	@Test
	void testCreate() {
		create();
		assertNotNull(serverSettingsService.findByName(TEST_KEY));
		assertThat(serverSettingsService.getString(TEST_KEY, null), is(TEST_VALUE));
	}
	
	@Test
	void testUpdate() {
		create();
		assertThat(serverSettingsService.getString(TEST_KEY, null), is(TEST_VALUE));
		
		final ServerSettingsDB serverSettingsDB = serverSettingsService.findByName(TEST_KEY);
		serverSettingsDB.setValue(TEST_VALUE_UPDATED);
		serverSettingsService.update(serverSettingsDB);
		
		assertThat(serverSettingsService.getString(TEST_KEY, null), is(TEST_VALUE_UPDATED));
	}
	
	@Test
	void testDelete() {
		create();
		assertNotNull(serverSettingsService.findByName(TEST_KEY));
		serverSettingsService.delete(TEST_KEY);
		assertNull(serverSettingsService.findByName(TEST_KEY));
	}
}
