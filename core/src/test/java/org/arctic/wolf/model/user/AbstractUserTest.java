/*
 * Copyright (c) 2019 Reginald Ravenhorst <lordrex34@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.arctic.wolf.model.user;

import static org.arctic.wolf.model.user.UserTestVariables.TEST_BIRTHDATE;
import static org.arctic.wolf.model.user.UserTestVariables.TEST_DISPLAY_NAME;
import static org.arctic.wolf.model.user.UserTestVariables.TEST_EMAIL;
import static org.arctic.wolf.model.user.UserTestVariables.TEST_PASSWORD;
import static org.arctic.wolf.model.user.UserTestVariables.TEST_USERNAME;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Date;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;

import org.arctic.wolf.exception.UserNotFoundException;
import org.arctic.wolf.model.user.dto.RegisterRequest;
import org.arctic.wolf.model.user.dto.form.IUserForm;
import org.arctic.wolf.security.oauth2.user.OAuth2UserInfo;
import org.arctic.wolf.security.oauth2.user.OAuth2UserInfoFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.security.config.oauth2.client.CommonOAuth2Provider;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.client.registration.ClientRegistration;
import org.springframework.security.oauth2.client.registration.ClientRegistrationRepository;
import org.springframework.security.oauth2.client.userinfo.OAuth2UserRequest;

abstract class AbstractUserTest {
	@Autowired
	protected UserService userService;
	
	@Autowired
	protected ClientRegistrationRepository clientRegistrationRepository;
	
	@Autowired
	protected PasswordEncoder passwordEncoder;
	
	@BeforeEach
	void before() {
		userService.deleteAll();
	}
	
	@Test
	void testCreate() {
		create();
		
		assertTrue(userService.count() > 0);
		assertNotNull(userService.findByUsernameDB(getUsername()));
	}
	
	@Test
	void testPassword() {
		create();
		
		final String password = userService.findByUsernameDB(getUsername())
			.orElseThrow(UserNotFoundException::new)
			.getPassword();
		assertNotNull(password);
		assertNotEquals("", password);
		
		// If the password is encrypted, it must be different than the specified one.
		assertNotEquals(TEST_PASSWORD, password);
		
		// Finally check if password is working or not.
		if (passwordMustWork()) {
			assertTrue(passwordEncoder.matches(TEST_PASSWORD, password));
		}
	}
	
	@Test
	void testDelete() {
		create();
		
		final UserDB userBeforeDelete = userService.findByUsernameDB(getUsername())
			.orElseThrow(UserNotFoundException::new);
		assertNotNull(userBeforeDelete);
		
		userService.delete(userBeforeDelete.getId());
		
		final UserDB userAfterDelete = userService.findByUsernameDB(getUsername()).orElse(null);
		assertNull(userAfterDelete);
	}
	
	@Test
	void findTest() {
		create();
		
		assertTrue(userService.findByEmailDB(TEST_EMAIL).isPresent());
		assertTrue(userService.findByEmailDTO(TEST_EMAIL).isPresent());
		assertTrue(userService.findByUsernameDB(getUsername()).isPresent());
		assertTrue(userService.findByUsernameDTO(getUsername()).isPresent());
	}
	
	abstract String getUsername();
	
	abstract boolean passwordMustWork();
	
	abstract void create();
	
	void createByRegistrationRequest() {
		// verify before create
		assertTrue(userService.findByUsernameDB(getUsername()).isEmpty());
		
		final RegisterRequest registerRequest = Mockito.mock(RegisterRequest.class);
		Mockito.when(registerRequest.getUsername()).thenReturn(TEST_USERNAME);
		Mockito.when(registerRequest.getPassword()).thenReturn(TEST_PASSWORD);
		Mockito.when(registerRequest.getEmail()).thenReturn(TEST_EMAIL);
		Mockito.when(registerRequest.getDisplayName()).thenReturn(TEST_DISPLAY_NAME);
		Mockito.when(registerRequest.getBirthDate()).thenReturn(TEST_BIRTHDATE);
		
		final IUser newRegularUser = userService.registerNewRegularUser(registerRequest);
		final Long newRegularUserId = newRegularUser.getId();
		
		// verify after create
		assertTrue(userService.findByIdDB(newRegularUserId).isPresent());
		assertTrue(userService.findByIdDTO(newRegularUserId).isPresent());
	}
	
	void createBySSORequest(CommonOAuth2Provider provider, Map<String, Object> attributes) {
		// verify before create
		assertTrue(userService.findByUsernameDB(getUsername()).isEmpty());
		
		final OAuth2UserRequest oAuth2UserRequest = Mockito.mock(OAuth2UserRequest.class);
		final ClientRegistration clientRegistration = clientRegistrationRepository.findByRegistrationId(provider.toString().toLowerCase());
		Mockito.when(oAuth2UserRequest.getClientRegistration()).thenReturn(clientRegistration);
		
		final OAuth2UserInfo oAuth2UserInfo = createOAuth2UserInfo(provider, attributes);
		
		final IUser newSSOUser = userService.registerNewSSOUser(oAuth2UserRequest, oAuth2UserInfo);
		final Long newSSOUserId = newSSOUser.getId();
		
		// verify after create
		assertTrue(userService.findByIdDB(newSSOUserId).isPresent());
		assertTrue(userService.findByIdDTO(newSSOUserId).isPresent());
	}
	
	void createByFormRequest() {
		// verify before create
		assertTrue(userService.findByUsernameDB(getUsername()).isEmpty());
		
		final IUserForm userForm = Mockito.mock(IUserForm.class);
		Mockito.when(userForm.getUsername()).thenReturn(TEST_USERNAME);
		Mockito.when(userForm.getPassword()).thenReturn(TEST_PASSWORD);
		Mockito.when(userForm.getEmail()).thenReturn(TEST_EMAIL);
		Mockito.when(userForm.getDisplayName()).thenReturn(TEST_DISPLAY_NAME);
		Mockito.when(userForm.getBirthDate()).thenReturn(TEST_BIRTHDATE);
		Mockito.when(userForm.getCreateTime()).thenReturn(new Date());
		Mockito.when(userForm.isActive()).thenReturn(true);
		
		final UserDB newFormUser = userService.createByForm(userForm);
		final Long newFormUserId = newFormUser.getId();
		
		// verify after create
		assertTrue(userService.findByIdDB(newFormUserId).isPresent());
		assertTrue(userService.findByIdDTO(newFormUserId).isPresent());
	}
	
	static OAuth2UserInfo createOAuth2UserInfo(CommonOAuth2Provider provider, Map<String, Object> attributes) {
		return OAuth2UserInfoFactory.getOAuth2UserInfo(provider.toString(), attributes);
	}
}
