/*
 * Copyright (c) 2019 Reginald Ravenhorst <lordrex34@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.arctic.wolf.model.user;

import static org.arctic.wolf.model.user.UserTestVariables.TEST_BIO;
import static org.arctic.wolf.model.user.UserTestVariables.TEST_BIRTHDATE;
import static org.arctic.wolf.model.user.UserTestVariables.TEST_DISPLAY_NAME;
import static org.arctic.wolf.model.user.UserTestVariables.TEST_IMAGE_URL;
import static org.arctic.wolf.model.user.UserTestVariables.TEST_USERNAME;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

import org.arctic.wolf.exception.UserNotFoundException;
import org.arctic.wolf.model.user.dto.UserUpdateRequest;
import org.arctic.wolf.model.user.dto.UserViewResponse;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.jdbc.EmbeddedDatabaseConnection;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * @author lord_rex
 */
@ExtendWith(SpringExtension.class)
@EnableTransactionManagement
@SpringBootTest
@AutoConfigureTestDatabase(connection = EmbeddedDatabaseConnection.H2)
class UserProfileTest extends AbstractUserTest {
	@Override
	String getUsername() {
		return TEST_USERNAME;
	}
	
	@Override
	boolean passwordMustWork() {
		return true;
	}
	
	@Override
	void create() {
		createByRegistrationRequest();
	}
	
	@Test
	void testUpdateProfileDB() {
		create();
		
		final UserDB userBeforeUpdate = userService.findByUsernameDB(getUsername())
			.orElseThrow(UserNotFoundException::new);
		assertNull(userBeforeUpdate.getBio());
		
		userService.updateProfile(userBeforeUpdate, createUserUpdateRequest());
		
		verifyUserAfterUpdate();
	}
	
	@Test
	void testUpdateProfileDTO() {
		create();
		
		final UserDB userBeforeUpdate = userService.findByUsernameDB(getUsername())
			.orElseThrow(UserNotFoundException::new);
		assertNull(userBeforeUpdate.getBio());
		
		userService.updateProfile(userBeforeUpdate, createUserUpdateRequest());
		
		verifyUserAfterUpdate();
	}
	
	private void verifyUserAfterUpdate() {
		final UserDB userAfterUpdate = userService.findByUsernameDB(getUsername())
			.orElseThrow(UserNotFoundException::new);
		final UserViewResponse userViewResponse = userService.viewProfile(userAfterUpdate.getId())
			.orElseThrow(UserNotFoundException::new);
		assertEquals(TEST_DISPLAY_NAME, userViewResponse.getDisplayName());
		assertEquals(TEST_IMAGE_URL, userViewResponse.getImageUrl());
		assertEquals(TEST_BIO, userViewResponse.getBio());
	}
	
	private static UserUpdateRequest createUserUpdateRequest() {
		final UserUpdateRequest userUpdateRequest = new UserUpdateRequest();
		userUpdateRequest.setDisplayName(TEST_DISPLAY_NAME);
		userUpdateRequest.setBirthDate(TEST_BIRTHDATE);
		userUpdateRequest.setImageUrl(TEST_IMAGE_URL);
		userUpdateRequest.setBio(TEST_BIO);
		return userUpdateRequest;
	}
}
