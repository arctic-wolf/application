/*
 * Copyright (c) 2019 Reginald Ravenhorst <lordrex34@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.arctic.wolf.model.user;

import static org.arctic.wolf.model.user.UserTestVariables.TEST_PASSWORD;
import static org.arctic.wolf.model.user.UserTestVariables.TEST_USERNAME;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.springframework.beans.factory.annotation.Autowired;

import org.arctic.wolf.exception.UserNotFoundException;
import org.arctic.wolf.model.user.dto.LoginRequest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.boot.jdbc.EmbeddedDatabaseConnection;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.authentication.DisabledException;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * @author lord_rex
 */
@ExtendWith(SpringExtension.class)
@EnableTransactionManagement
@SpringBootTest
@AutoConfigureTestDatabase(connection = EmbeddedDatabaseConnection.H2)
class UserRegularTest extends AbstractUserTest {
	@Autowired
	private AuthenticationService authenticationService;
	
	@Override
	String getUsername() {
		return TEST_USERNAME;
	}
	
	@Override
	boolean passwordMustWork() {
		return true;
	}
	
	@Override
	void create() {
		createByRegistrationRequest();
	}
	
	@Test
	void testActivateDB() {
		create();
		
		final UserDB userBeforeUpdate = userService.findByUsernameDB(getUsername())
			.orElseThrow(UserNotFoundException::new);
		assertFalse(userBeforeUpdate.isActive());
		userService.activateUser(userBeforeUpdate);
		
		final UserDB userAfterUpdate = userService.findByUsernameDB(getUsername())
			.orElseThrow(UserNotFoundException::new);
		assertTrue(userAfterUpdate.isActive());
	}
	
	@Test
	void testActivateDTO() {
		create();
		
		final UserDB userBeforeUpdate = userService.findByUsernameDB(getUsername())
			.orElseThrow(UserNotFoundException::new);
		assertFalse(userBeforeUpdate.isActive());
		userService.activateUser(userBeforeUpdate);
		
		final UserDB userAfterUpdate = userService.findByUsernameDB(getUsername())
			.orElseThrow(UserNotFoundException::new);
		assertTrue(userAfterUpdate.isActive());
	}
	
	@Test
	void testLoginActiveUser() {
		create();
		
		final LoginRequest loginRequest = createLoginRequest();
		
		final UserDB userDB = userService.findByUsernameDB(getUsername())
			.orElseThrow(UserNotFoundException::new);
		userService.activateUser(userDB);
		
		authenticationService.login(loginRequest);
	}
	
	@Test
	void testLoginInactiveUser() {
		create();
		
		final LoginRequest loginRequest = createLoginRequest();
		
		assertThrows(DisabledException.class, () -> authenticationService.login(loginRequest));
	}
	
	private LoginRequest createLoginRequest() {
		final LoginRequest loginRequest = Mockito.mock(LoginRequest.class);
		Mockito.when(loginRequest.getUsername()).thenReturn(getUsername());
		Mockito.when(loginRequest.getPassword()).thenReturn(TEST_PASSWORD);
		return loginRequest;
	}
}
