/*
 * Copyright (c) 2019 Reginald Ravenhorst <lordrex34@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.arctic.wolf.model.user;

import static org.arctic.wolf.model.user.UserTestVariables.TEST_DISPLAY_NAME;
import static org.arctic.wolf.model.user.UserTestVariables.TEST_EMAIL;
import static org.arctic.wolf.model.user.UserTestVariables.TEST_IMAGE_URL;
import static org.arctic.wolf.model.user.UserTestVariables.TEST_NEW_DISPLAY_NAME;
import static org.arctic.wolf.model.user.UserTestVariables.TEST_NEW_IMAGE_URL;
import static org.arctic.wolf.model.user.UserTestVariables.TEST_USERNAME;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.HashMap;
import java.util.Map;

import org.arctic.wolf.exception.UserNotFoundException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.jdbc.EmbeddedDatabaseConnection;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.config.oauth2.client.CommonOAuth2Provider;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * @author lord_rex
 */
@ExtendWith(SpringExtension.class)
@EnableTransactionManagement
@SpringBootTest
@AutoConfigureTestDatabase(connection = EmbeddedDatabaseConnection.H2)
abstract class UserSSOFacebookTest extends AbstractUserTest {
	private static Map<String, Object> ATTRIBUTES;
	private static final Map<String, Map<String, String>> TEST_FACEBOOK_PICTURE = Map.of("data", Map.of("url", TEST_IMAGE_URL));
	private static final Map<String, Map<String, String>> TEST_NEW_FACEBOOK_PICTURE = Map.of("data", Map.of("url", TEST_NEW_IMAGE_URL));
	
	static {
		ATTRIBUTES = new HashMap<>();
		ATTRIBUTES.put("id", TEST_USERNAME);
		ATTRIBUTES.put("name", TEST_DISPLAY_NAME);
		ATTRIBUTES.put("email", TEST_EMAIL);
		ATTRIBUTES.put("picture", TEST_FACEBOOK_PICTURE);
	}
	
	@Override
	String getUsername() {
		return UserService.getSSOUsername(createOAuth2UserInfo(CommonOAuth2Provider.FACEBOOK, ATTRIBUTES), CommonOAuth2Provider.FACEBOOK);
	}
	
	@Override
	boolean passwordMustWork() {
		// SSO users passwords are just random non-sense, so it doesn't really matter if their password work or not.
		return false;
	}
	
	@Override
	void create() {
		createBySSORequest(CommonOAuth2Provider.FACEBOOK, ATTRIBUTES);
	}
	
	@Test
	void testUpdateOAuth2User() {
		create();
		
		final UserDB userBeforeUpdate = userService.findByUsernameDB(getUsername())
			.orElseThrow(UserNotFoundException::new);
		assertEquals(TEST_DISPLAY_NAME, userBeforeUpdate.getDisplayName());
		assertEquals(TEST_FACEBOOK_PICTURE.get("data").get("url"), userBeforeUpdate.getDisplayName());
		
		final Map<String, Object> attributes = new HashMap<>(ATTRIBUTES);
		attributes.put("name", TEST_NEW_DISPLAY_NAME);
		attributes.put("picture", TEST_NEW_FACEBOOK_PICTURE);
		
		userService.updateOAuth2User(userBeforeUpdate, createOAuth2UserInfo(CommonOAuth2Provider.FACEBOOK, attributes));
		
		final UserDB userAfterUpdate = userService.findByUsernameDB(getUsername())
			.orElseThrow(UserNotFoundException::new);
		assertEquals(TEST_NEW_DISPLAY_NAME, userAfterUpdate.getDisplayName());
		assertEquals(TEST_NEW_FACEBOOK_PICTURE.get("data").get("url"), userAfterUpdate.getImageUrl());
	}
	
}
