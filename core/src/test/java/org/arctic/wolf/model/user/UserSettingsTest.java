/*
 * Copyright (c) 2019 Reginald Ravenhorst <lordrex34@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.arctic.wolf.model.user;

import static org.arctic.wolf.model.user.UserTestVariables.TEST_NEW_PASSWORD;
import static org.arctic.wolf.model.user.UserTestVariables.TEST_PASSWORD;
import static org.arctic.wolf.model.user.UserTestVariables.TEST_USERNAME;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.arctic.wolf.exception.UserNotFoundException;
import org.arctic.wolf.model.user.dto.UserSettingsUpdateRequest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.jdbc.EmbeddedDatabaseConnection;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * @author lord_rex
 */
@ExtendWith(SpringExtension.class)
@EnableTransactionManagement
@SpringBootTest
@AutoConfigureTestDatabase(connection = EmbeddedDatabaseConnection.H2)
class UserSettingsTest extends AbstractUserTest {
	private static final UserSettingsUpdateRequest EMPTY_USER_SETTINGS_UPDATE_REQUEST = new UserSettingsUpdateRequest();
	
	@Override
	String getUsername() {
		return TEST_USERNAME;
	}
	
	@Override
	boolean passwordMustWork() {
		return true;
	}
	
	@Override
	void create() {
		createByRegistrationRequest();
	}
	
	@Test
	void testUpdateUserSettingsDB() {
		create();
		
		final UserDB userBeforeUpdate = userService.findByUsernameDB(getUsername())
			.orElseThrow(UserNotFoundException::new);
		assertTrue(passwordEncoder.matches(TEST_PASSWORD, userBeforeUpdate.getPassword()));
		
		userService.updateUserSettings(userBeforeUpdate, createUserSettingsUpdateRequest());
		
		verifyUserAfterUpdate();
	}
	
	@Test
	void testUpdateUserSettingsDTO() {
		create();
		
		final UserDB userBeforeUpdate = userService.findByUsernameDB(getUsername())
			.orElseThrow(UserNotFoundException::new);
		assertTrue(passwordEncoder.matches(TEST_PASSWORD, userBeforeUpdate.getPassword()));
		
		userService.updateUserSettings(userBeforeUpdate, createUserSettingsUpdateRequest());
		
		verifyUserAfterUpdate();
	}
	
	private void verifyUserAfterUpdate() {
		final UserDB userAfterUpdate = userService.findByUsernameDB(getUsername())
			.orElseThrow(UserNotFoundException::new);
		assertFalse(passwordEncoder.matches(TEST_PASSWORD, userAfterUpdate.getPassword()));
		assertTrue(passwordEncoder.matches(TEST_NEW_PASSWORD, userAfterUpdate.getPassword()));
	}
	
	private static UserSettingsUpdateRequest createUserSettingsUpdateRequest() {
		final UserSettingsUpdateRequest userSettingsUpdateRequest = new UserSettingsUpdateRequest();
		userSettingsUpdateRequest.setPassword(TEST_NEW_PASSWORD);
		userSettingsUpdateRequest.setPasswordConfirm(TEST_NEW_PASSWORD);
		
		assertEquals(userSettingsUpdateRequest.getPassword(), userSettingsUpdateRequest.getPasswordConfirm());
		
		return userSettingsUpdateRequest;
	}
	
	@Test
	void testEmptyUserSettingsDB() {
		create();
		
		final UserDB userBeforeUpdate = userService.findByUsernameDB(getUsername())
			.orElseThrow(UserNotFoundException::new);
		assertTrue(passwordEncoder.matches(TEST_PASSWORD, userBeforeUpdate.getPassword()));
		
		userService.updateUserSettings(userBeforeUpdate, EMPTY_USER_SETTINGS_UPDATE_REQUEST);
		
		final UserDB userAfterUpdate = userService.findByUsernameDB(getUsername())
			.orElseThrow(UserNotFoundException::new);
		assertTrue(passwordEncoder.matches(TEST_PASSWORD, userAfterUpdate.getPassword()));
	}
	
	@Test
	void testEmptyUserSettingsDTO() {
		create();
		
		final UserDB userBeforeUpdate = userService.findByUsernameDB(getUsername())
			.orElseThrow(UserNotFoundException::new);
		assertTrue(passwordEncoder.matches(TEST_PASSWORD, userBeforeUpdate.getPassword()));
		
		userService.updateUserSettings(userBeforeUpdate, EMPTY_USER_SETTINGS_UPDATE_REQUEST);
		
		final UserDB userAfterUpdate = userService.findByUsernameDB(getUsername())
			.orElseThrow(UserNotFoundException::new);
		assertTrue(passwordEncoder.matches(TEST_PASSWORD, userAfterUpdate.getPassword()));
	}
}
