/*
 * Copyright (c) 2019 Reginald Ravenhorst <lordrex34@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.arctic.wolf.model.user;

import java.time.LocalDate;
import java.time.Month;
import java.util.Date;
import java.util.concurrent.ThreadLocalRandom;

import org.arctic.wolf.model.user.dto.form.IUserForm;
import org.mockito.Mockito;

public interface UserTestVariables {
	String TEST_USERNAME = "testUsername";
	String TEST_NEW_USERNAME = "testNewUsername";
	String TEST_PASSWORD = "testPassword";
	String TEST_NEW_PASSWORD = "testNewPassword";
	String TEST_EMAIL = "test@localhost";
	String TEST_NEW_EMAIL = "test@localhost.new";
	String TEST_DISPLAY_NAME = "Test Display Name";
	String TEST_NEW_DISPLAY_NAME = "Test New Display Name";
	Date TEST_BIRTHDATE = java.sql.Date.valueOf(
		LocalDate.of(1990, Month.APRIL, 1)
	);
	String TEST_IMAGE_URL = "https://whatever...";
	String TEST_NEW_IMAGE_URL = "https://whatever...new...";
	String TEST_BIO = "This is my fabulous bio, as you can see.";
	
	static IUserForm createUserForm() {
		final IUserForm userForm = Mockito.mock(IUserForm.class);
		Mockito.when(userForm.getUsername()).thenReturn(TEST_USERNAME);
		Mockito.when(userForm.getPassword()).thenReturn(TEST_PASSWORD);
		Mockito.when(userForm.getEmail()).thenReturn(TEST_EMAIL);
		Mockito.when(userForm.getDisplayName()).thenReturn(TEST_DISPLAY_NAME);
		Mockito.when(userForm.getBirthDate()).thenReturn(TEST_BIRTHDATE);
		Mockito.when(userForm.getCreateTime()).thenReturn(new Date());
		Mockito.when(userForm.isActive()).thenReturn(true);
		return userForm;
	}
	
	static IUserForm createRandomUserForm() {
		final IUserForm userForm = Mockito.mock(IUserForm.class);
		Mockito.when(userForm.getUsername()).thenReturn(TEST_USERNAME + ThreadLocalRandom.current().nextInt());
		Mockito.when(userForm.getPassword()).thenReturn(TEST_PASSWORD);
		Mockito.when(userForm.getEmail()).thenReturn(TEST_EMAIL + ThreadLocalRandom.current().nextInt());
		Mockito.when(userForm.getDisplayName()).thenReturn(TEST_DISPLAY_NAME);
		Mockito.when(userForm.getBirthDate()).thenReturn(TEST_BIRTHDATE);
		Mockito.when(userForm.getCreateTime()).thenReturn(new Date());
		Mockito.when(userForm.isActive()).thenReturn(true);
		return userForm;
	}
}
