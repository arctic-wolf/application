/*
 * Copyright (c) 2019 Reginald Ravenhorst <lordrex34@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.arctic.wolf.model.user.verification;

import static org.arctic.wolf.model.user.UserTestVariables.TEST_BIRTHDATE;
import static org.arctic.wolf.model.user.UserTestVariables.createUserForm;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;

import org.arctic.wolf.exception.UserVerificationTokenNotFoundException;
import org.arctic.wolf.model.user.UserDB;
import org.arctic.wolf.model.user.UserService;
import org.arctic.wolf.model.user.dto.form.IUserForm;
import org.arctic.wolf.model.user.verification.dto.UserVerificationToken;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.boot.jdbc.EmbeddedDatabaseConnection;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@ExtendWith(SpringExtension.class)
@EnableTransactionManagement
@SpringBootTest
@AutoConfigureTestDatabase(connection = EmbeddedDatabaseConnection.H2)
class UserVerificationTest {
	private static final java.sql.Date DATE_PAST = java.sql.Date.valueOf(LocalDate.of(1900, 1, 1));
	private static final java.sql.Date DATE_FUTURE = java.sql.Date.valueOf(LocalDate.of(2200, 1, 1));
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private UserVerificationService userVerificationService;
	
	@BeforeEach
	void before() {
		userService.deleteAll();
		userVerificationService.deleteAll();
	}
	
	IUserVerificationToken create() {
		final IUserForm userForm = createUserForm();
		
		final UserDB userDB = userService.createByForm(userForm);
		return userVerificationService.createToken(userDB);
	}
	
	@Test
	void testUserDB() {
		final IUserForm userForm = createUserForm();
		
		final UserDB userDB = userService.createByForm(userForm);
		final IUserVerificationToken userVerificationToken = userVerificationService.createToken(userDB);
		final UserVerificationTokenDB userVerificationTokenDB = userVerificationService.findByToken(userVerificationToken.getToken())
			.orElseThrow(UserVerificationTokenNotFoundException::new);
		
		assertEquals(userDB, userVerificationTokenDB.getUserDB());
	}
	
	@Test
	void testUpdate() {
		final IUserVerificationToken userVerificationToken = create();
		
		final UserVerificationTokenDB userVerificationTokenBeforeUpdate = userVerificationService.findByToken(userVerificationToken.getToken())
			.orElseThrow(UserVerificationTokenNotFoundException::new);
		
		assertNotEquals(TEST_BIRTHDATE, userVerificationTokenBeforeUpdate.getExpiryDate());
		
		userVerificationTokenBeforeUpdate.setExpiryDate(TEST_BIRTHDATE);
		userVerificationService.update(userVerificationTokenBeforeUpdate);
		
		final UserVerificationTokenDB userVerificationTokenAfterUpdate = userVerificationService.findByToken(userVerificationToken.getToken())
			.orElseThrow(UserVerificationTokenNotFoundException::new);
		
		assertEquals(TEST_BIRTHDATE, userVerificationTokenAfterUpdate.getExpiryDate());
	}
	
	@Test
	void testFindByTokenDB() {
		final IUserVerificationToken userVerificationToken = create();
		final UserVerificationTokenDB userVerificationTokenDB = userVerificationService.findByToken(userVerificationToken.getToken())
			.orElseThrow(UserVerificationTokenNotFoundException::new);
		
		assertEquals(userVerificationToken.getId(), userVerificationTokenDB.getId());
		assertEquals(userVerificationToken.getToken(), userVerificationTokenDB.getToken());
		assertEquals(userVerificationToken.getExpiryDate(), userVerificationTokenDB.getExpiryDate());
	}
	
	@Test
	void testFindByTokenDTO() {
		final IUserVerificationToken userVerificationToken = create();
		final UserVerificationToken userVerificationTokenDTO = userVerificationService.findByTokenAsDTO(userVerificationToken.getToken())
			.orElseThrow(UserVerificationTokenNotFoundException::new);
		
		assertEquals(userVerificationToken.getId(), userVerificationTokenDTO.getId());
		assertEquals(userVerificationToken.getToken(), userVerificationTokenDTO.getToken());
		assertEquals(userVerificationToken.getExpiryDate(), userVerificationTokenDTO.getExpiryDate());
	}
	
	@Test
	void testUseDB() {
		final IUserVerificationToken userVerificationToken = create();
		final UserVerificationTokenDB userVerificationTokenBeforeUpdate = userVerificationService.findByToken(userVerificationToken.getToken())
			.orElseThrow(UserVerificationTokenNotFoundException::new);
		
		assertFalse(userVerificationTokenBeforeUpdate.isUsed());
		
		userVerificationService.useToken(userVerificationTokenBeforeUpdate);
		
		final UserVerificationTokenDB userVerificationTokenAfterUpdate = userVerificationService.findByToken(userVerificationToken.getToken())
			.orElseThrow(UserVerificationTokenNotFoundException::new);
		
		assertTrue(userVerificationTokenAfterUpdate.isUsed());
	}
	
	@Test
	void testUseDTO() {
		final IUserVerificationToken userVerificationToken = create();
		final UserVerificationTokenDB userVerificationTokenBeforeUpdate = userVerificationService.findByToken(userVerificationToken.getToken())
			.orElseThrow(UserVerificationTokenNotFoundException::new);
		
		assertFalse(userVerificationTokenBeforeUpdate.isUsed());
		
		userVerificationService.useToken(userVerificationTokenBeforeUpdate);
		
		final UserVerificationToken userVerificationTokenAfterUpdate = userVerificationService.findByTokenAsDTO(userVerificationToken.getToken())
			.orElseThrow(UserVerificationTokenNotFoundException::new);
		
		assertTrue(userVerificationTokenAfterUpdate.isUsed());
	}
	
	@Test
	void testExpired() {
		final IUserVerificationToken past = Mockito.spy(IUserVerificationToken.class);
		Mockito.when(past.getExpiryDate()).thenReturn(DATE_PAST);
		assertTrue(past.isExpired());
		
		final IUserVerificationToken future = Mockito.spy(IUserVerificationToken.class);
		Mockito.when(future.getExpiryDate()).thenReturn(DATE_FUTURE);
		assertFalse(future.isExpired());
	}
}
