/*
 * Copyright (c) 2017 Reginald Ravenhorst <lordrex34@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.arctic.wolf.model.widget;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Collections;

import org.arctic.wolf.exception.WidgetNotFoundException;
import org.arctic.wolf.model.widget.dto.Widget;
import org.arctic.wolf.model.widget.dto.form.IWidgetForm;
import org.arctic.wolf.model.widget.dto.form.WidgetForm;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.boot.jdbc.EmbeddedDatabaseConnection;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * @author lord_rex
 */
@ExtendWith(SpringExtension.class)
@EnableTransactionManagement
@SpringBootTest
@AutoConfigureTestDatabase(connection = EmbeddedDatabaseConnection.H2)
class WidgetTest extends AbstractWidgetTest {
	@BeforeEach
	void before() {
		widgetService.deleteAll();
	}
	
	private void create() {
		final IWidgetForm widgetForm = Mockito.mock(WidgetForm.class);
		Mockito.when(widgetForm.getId()).thenReturn(ID);
		Mockito.when(widgetForm.getDisplayName()).thenReturn(DISPLAY_NAME);
		Mockito.when(widgetForm.getGroupId()).thenReturn(GROUP_ID);
		Mockito.when(widgetForm.getOrderInTheGroup()).thenReturn(0);
		Mockito.when(widgetForm.getContent()).thenReturn(CONTENT);
		Mockito.when(widgetForm.getLinks()).thenReturn(Collections.emptyList());
		Mockito.when(widgetForm.getLang()).thenReturn(LANG);
		
		widgetService.createByForm(widgetForm);
	}
	
	@Test
	void testCreate() {
		create();
		
		assertTrue(widgetService.findByIdDB(ID).isPresent());
		assertTrue(widgetService.getWidgetDB(LANG, ID).isPresent());
		assertTrue(widgetService.findByIdDTO(ID).isPresent());
		assertTrue(widgetService.getWidgetDTO(LANG, ID).isPresent());
	}
	
	@Test
	void testDelete() {
		create();
		
		widgetService.delete(ID);
		
		assertTrue(widgetService.getWidgetDB(LANG, ID).isEmpty());
		assertTrue(widgetService.getWidgetDTO(LANG, ID).isEmpty());
	}
	
	@Test
	void testUpdateDB() {
		create();
		
		final WidgetDB widgetBeforeUpdate = widgetService.getWidgetDB(LANG, ID)
			.orElseThrow(WidgetNotFoundException::new);
		
		assertEquals(DISPLAY_NAME, widgetBeforeUpdate.getDisplayName());
		
		widgetBeforeUpdate.setDisplayName(DISPLAY_NAME_UPDATED);
		widgetService.update(widgetBeforeUpdate);
		
		final WidgetDB widgetAfterUpdate = widgetService.getWidgetDB(LANG, ID)
			.orElseThrow(WidgetNotFoundException::new);
		
		assertEquals(DISPLAY_NAME_UPDATED, widgetAfterUpdate.getDisplayName());
	}
	
	@Test
	void testUpdateDTO() {
		create();
		
		final WidgetDB widgetBeforeUpdate = widgetService.getWidgetDB(LANG, ID)
			.orElseThrow(WidgetNotFoundException::new);
		
		assertEquals(DISPLAY_NAME, widgetBeforeUpdate.getDisplayName());
		
		widgetBeforeUpdate.setDisplayName(DISPLAY_NAME_UPDATED);
		widgetService.update(widgetBeforeUpdate);
		
		final Widget widgetAfterUpdate = widgetService.getWidgetDTO(LANG, ID)
			.orElseThrow(WidgetNotFoundException::new);
		
		assertEquals(DISPLAY_NAME_UPDATED, widgetAfterUpdate.getDisplayName());
	}
	
	//	@Test
	//	void testUpdateByForm() {
	//		create();
	//
	//		final WidgetDB widgetBeforeUpdate = widgetService.getWidgetDB(LANG, ID)
	//			.orElseThrow(WidgetNotFoundException::new);
	//		assertEquals(DISPLAY_NAME, widgetBeforeUpdate.getDisplayName());
	//
	//		final EditWidgetForm editWidgetForm = new EditWidgetForm();
	//		editWidgetForm.initFrom(widgetBeforeUpdate);
	//		editWidgetForm.setDisplayName(DISPLAY_NAME_UPDATED);
	//
	//		widgetService.updateByForm(widgetBeforeUpdate, editWidgetForm);
	//
	//		final WidgetDB widgetAfterUpdate = widgetService.getWidgetDB(LANG, ID)
	//			.orElseThrow(WidgetNotFoundException::new);
	//		assertEquals(DISPLAY_NAME_UPDATED, widgetAfterUpdate.getDisplayName());
	//	}
}
