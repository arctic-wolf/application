/*
 * Copyright (c) 2019 Reginald Ravenhorst <lordrex34@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.arctic.wolf.model.widget;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.Collections;
import java.util.EnumSet;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;

import org.arctic.wolf.exception.WidgetNotFoundException;
import org.arctic.wolf.model.content.ContentService;
import org.arctic.wolf.model.content.ContentType;
import org.arctic.wolf.model.content.ContentVisibility;
import org.arctic.wolf.model.content.dto.Content;
import org.arctic.wolf.model.link.ILink;
import org.arctic.wolf.model.server.ServerSettingsService;
import org.arctic.wolf.model.widget.dto.Widget;
import org.arctic.wolf.model.widget.dto.form.IWidgetForm;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.boot.jdbc.EmbeddedDatabaseConnection;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * @author lord_rex
 */
@ExtendWith(SpringExtension.class)
@EnableTransactionManagement
@SpringBootTest
@AutoConfigureTestDatabase(connection = EmbeddedDatabaseConnection.H2)
class WidgetsTest extends AbstractWidgetTest {
	@Autowired
	private WidgetMenuService widgetMenuService;
	
	@Autowired
	private ServerSettingsService serverSettingsService;
	
	@Autowired
	private ContentService contentService;
	
	@BeforeEach
	void before() {
		widgetService.deleteAll();
	}
	
	private void create(int index) {
		final IWidgetForm widgetForm = Mockito.mock(IWidgetForm.class);
		Mockito.when(widgetForm.getId()).thenReturn(ID + index);
		Mockito.when(widgetForm.getDisplayName()).thenReturn(DISPLAY_NAME + " " + index);
		Mockito.when(widgetForm.getGroupId()).thenReturn(GROUP_ID);
		Mockito.when(widgetForm.getOrderInTheGroup()).thenReturn(0);
		Mockito.when(widgetForm.getContent()).thenReturn(CONTENT);
		Mockito.when(widgetForm.getLinks()).thenReturn(Collections.emptyList());
		Mockito.when(widgetForm.getLang()).thenReturn(LANG);
		Mockito.when(widgetForm.getVisibility()).thenReturn(EnumSet.allOf(ContentVisibility.class));
		
		widgetService.createByForm(widgetForm);
	}
	
	private void createPageList() {
		final IWidgetForm widgetForm = Mockito.mock(IWidgetForm.class);
		Mockito.when(widgetForm.getId()).thenReturn(WidgetMenuService.PAGE_LIST);
		Mockito.when(widgetForm.getDisplayName()).thenReturn(DISPLAY_NAME);
		Mockito.when(widgetForm.getGroupId()).thenReturn(GROUP_ID);
		Mockito.when(widgetForm.getOrderInTheGroup()).thenReturn(0);
		Mockito.when(widgetForm.getContent()).thenReturn(WidgetMenuService.PAGE_LIST);
		Mockito.when(widgetForm.getLinks()).thenReturn(Collections.emptyList());
		Mockito.when(widgetForm.getLang()).thenReturn(LANG);
		Mockito.when(widgetForm.getVisibility()).thenReturn(EnumSet.allOf(ContentVisibility.class));
		
		widgetService.createByForm(widgetForm);
	}
	
	private void createWithNullContent() {
		final IWidgetForm widgetForm = Mockito.mock(IWidgetForm.class);
		Mockito.when(widgetForm.getId()).thenReturn(ID);
		Mockito.when(widgetForm.getDisplayName()).thenReturn(DISPLAY_NAME);
		Mockito.when(widgetForm.getGroupId()).thenReturn(GROUP_ID);
		Mockito.when(widgetForm.getOrderInTheGroup()).thenReturn(0);
		Mockito.when(widgetForm.getContent()).thenReturn(null);
		Mockito.when(widgetForm.getLinks()).thenReturn(Collections.emptyList());
		Mockito.when(widgetForm.getLang()).thenReturn(LANG);
		Mockito.when(widgetForm.getVisibility()).thenReturn(EnumSet.allOf(ContentVisibility.class));
		
		widgetService.createByForm(widgetForm);
	}
	
	@Test
	void testWidgets() {
		for (int i = 0; i < 5; i++) {
			create(i);
		}
		
		final int sizeDB = widgetService.getWidgetsDB(LANG).size();
		final int sizeDTO = widgetService.getWidgetsDTO(LANG).size();
		final List<Widget> widgets = widgetService.getWidgets(LANG, null, widgetMenuService, serverSettingsService);
		
		assertEquals(5, sizeDB);
		assertEquals(5, sizeDTO);
		assertEquals(sizeDB, sizeDTO);
		assertEquals(5, widgets.size());
	}
	
	@Test
	void testWidgetGroups() {
		for (int i = 0; i < 5; i++) {
			create(i);
		}
		
		final List<String> widgetGroups = widgetService.getWidgetGroups(LANG, null);
		assertEquals(1, widgetGroups.size());
		
		final List<Widget> widgetGroup = widgetService.getWidgetGroup(LANG, GROUP_ID, null, widgetMenuService, serverSettingsService);
		assertEquals(5, widgetGroup.size());
	}
	
	@Test
	void testWidgetMenu() {
		createPageList();
		
		final List<Widget> widgetGroup = widgetService.getWidgetGroup(LANG, GROUP_ID, null, widgetMenuService, serverSettingsService);
		final Widget widget = widgetGroup.stream()
			.filter(w -> w.getId().equals(WidgetMenuService.PAGE_LIST))
			.findFirst()
			.orElseThrow(WidgetNotFoundException::new);
		
		final List<Content> pages = contentService.getContents(ContentType.PAGE, LANG, null);
		final List<ILink> widgetLinks = widget.getLinks();
		
		assertEquals(pages.size(), widgetLinks.size());
	}
	
	@Test
	void testWithNullContent() {
		createWithNullContent();
		
		final List<Widget> widgetGroup = widgetService.getWidgetGroup(LANG, GROUP_ID, null, widgetMenuService, serverSettingsService);
		final Widget widget = widgetGroup.stream()
			.filter(w -> w.getId().equals(ID))
			.findFirst()
			.orElseThrow(WidgetNotFoundException::new);
		
		assertNotNull(widget);
	}
}
