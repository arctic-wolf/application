plugins {
	id("de.undercouch.download") version "4.0.1"
}

apply(plugin = "java")
apply(plugin = "eclipse")
apply(plugin = "idea")

repositories {
	mavenCentral()
}

//task clientClean(group = "build") {
//	doLast {
//		delete("client/build")
//		delete("client/src/theme")
//		delete("core")
//
//		val deploymentTheme = "../core/HTML/client/theme"
//		if (file(projectDir, deploymentTheme).exists()) {
//			delete(deploymentTheme)
//		}
//	}
//}
//
//task makeClient(group = "build") {
//	doLast {
//		val branchName = "master"
//		if (project.hasProperty("branchName")) {
//			branchName = project.property("branchName").toString()
//		}
//
//		download {
//			src "https://bitbucket.org/arctic-wolf/client/get/${branchName}.zip"
//			dest buildDir
//			overwrite true
//		}
//
//		copy {
//			from zipTree("$buildDir/${branchName}.zip")
//			into "$buildDir"
//		}
//
//		val awClient = findInDirectory(buildDir, "client-(.*)")
//		copy {
//			from(awClient)
//			into("client")
//		}
//		delete(awClient)
//	}
//}
//
//task makeTheme(group = "build", dependsOn: makeClient) {
//	doLast {
//		val clientTheme = "client/src/theme"
//		delete(clientTheme)
//		copy {
//			from("src/theme")
//			into(clientTheme)
//		}
//	}
//}
//
//task buildTheme(group = "build", dependsOn: makeTheme) {
//	doLast {
//		exec {
//			workingDir "client"
//
//			commandLine gradlew(), "build"
//		}
//	}
//}
//
//task deployTheme(group = "build", dependsOn: buildTheme) {
//	doLast {
//		val deploymentTheme = "../core/HTML/client/theme"
//		if (!new File(projectDir, deploymentTheme).exists()) {
//			throw new FileNotFoundException("Directory "" + deploymentTheme + "" is missing! You have to build "../client" and "../core" first to make theme deployment work.")
//		}
//
//		delete(deploymentTheme)
//		copy {
//			from("core/HTML/client/theme")
//			into(deploymentTheme)
//		}
//	}
//}
//
//jar {
//	archiveFileName = "${project.name}.jar"
//
//	from("core/HTML/client/theme")
//	from("src/theme") {
//		include("*preview.png")
//	}
//
//	manifest {
//		attributes(
//				"ThemeName": project.name,
//				"ThemePath" = "example",
//				"ThemeAuthor" = "lord_rex",
//				"ThemeCreatedAt" = "2018-06-06",
//				"ThemeDescription" = "A very simple example theme.",
//				"ThemeVersion": project.version,
//				"ThemePreviewImage" = "example-preview.png"
//		)
//	}
//}
//
//clean.dependsOn(clientClean)
//jar.dependsOn(buildTheme)
//
//static val findInDirectory(File directory, String regex) {
//	val files = listOf()
//
//	directory.eachFileRecurse(FileType.DIRECTORIES) {
//		if (it.name ==~ regex) {
//			files << it
//		}
//	}
//
//	return files[0]
//}
//
//static String gradlew() {
//	val gradlew: String = "gradlew"
//	if (Os.isFamily(Os.FAMILY_WINDOWS)) {
//		gradlew = "gradlew.bat"
//	}
//
//	return gradlew
//}
