import org.ajoberstar.grgit.Grgit
import java.text.SimpleDateFormat
import java.util.Date

plugins {
	distribution
}

apply(plugin = "java")
apply(plugin = "eclipse")
apply(plugin = "idea")
apply(plugin = "org.ajoberstar.grgit")
apply(plugin = "org.springframework.boot")
apply(plugin = "distribution")

repositories {
	mavenCentral()
}

dependencies {
	// Arctic Wolf
	implementation(project(":commons"))

	// Against flood
	implementation("com.giffing.bucket4j.spring.boot.starter:bucket4j-spring-boot-starter-springboot2:0.1.15")

	// Embedded WebServer
	implementation("org.springframework.boot:spring-boot-starter-tomcat")

	// Security
	implementation("io.jsonwebtoken:jjwt-impl:0.11.5")
	implementation("io.jsonwebtoken:jjwt-jackson:0.11.5")

	// Database
	implementation("org.hibernate.validator:hibernate-validator")

	// Database Drivers
	runtimeOnly("mysql:mysql-connector-java")
	runtimeOnly("org.postgresql:postgresql")
	testRuntimeOnly("com.h2database:h2")

	// Lombok
	compileOnly("org.projectlombok:lombok")
	annotationProcessor("org.projectlombok:lombok")

	// Test
	testImplementation("org.springframework.boot:spring-boot-starter-test")
	testImplementation("org.springframework.security:spring-security-test")
	testImplementation("org.junit.jupiter:junit-jupiter-engine:5.9.0")
}

tasks.bootJar {
	enabled = true
	archiveClassifier.set("bin")
}

tasks.jar {
	enabled = false
}

distributions {
	main {
		contents {
			mkdir("dist/plugins")
			mkdir("dist/themes")

			from("dist") {
				exclude("**/*.java")
			}

			from("keystore.jks")

			from(tasks.bootJar)

			into("config") {
				from("src/main/resources/application.yml")
			}
		}
	}
}

tasks.register("createThemesPluginsDirectory") {
	doFirst {
		mkdir("themes")
		mkdir("plugins")
	}
}

tasks.register("versionInfo") {
	doFirst {
		val grgit = Grgit.open {
			dir = project.rootDir
		}
		val createdBy = "Java " + System.getProperty("java.version") + " (" + System.getProperty("java.vendor") + ")"
		val builtBy = System.getProperty("user.name")
		val buildTime = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ").format(Date())
		val gitBranch = grgit.branch.current().name
		val gitHashFull = grgit.head().id
		val gitHashShort = grgit.head().abbreviatedId
		val gitCommitCount = grgit.log().size

		val versionInfoFile = file("$projectDir/src/main/resources/org/arctic/wolf/repository/server/version/version-info.properties")
		versionInfoFile.delete()
		versionInfoFile.createNewFile()

		versionInfoFile.appendText("# Version Info\n")
		versionInfoFile.appendText("Version=$version\n")
		versionInfoFile.appendText("CreatedBy=$createdBy\n")
		versionInfoFile.appendText("BuiltBy=$builtBy\n")
		versionInfoFile.appendText("BuildTime=$buildTime\n")
		versionInfoFile.appendText("GitBranch=$gitBranch\n")
		versionInfoFile.appendText("GitHashFull=$gitHashFull\n")
		versionInfoFile.appendText("GitHashShort=$gitHashShort\n")
		versionInfoFile.appendText("GitCommitCount=$gitCommitCount\n")
	}
}

tasks.processResources.get().dependsOn("createThemesPluginsDirectory", "versionInfo")

