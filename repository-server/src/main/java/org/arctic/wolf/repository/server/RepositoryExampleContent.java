/*
 * Copyright (c) 2018 Reginald Ravenhorst <lordrex34@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.arctic.wolf.repository.server;

import jakarta.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.arctic.wolf.repository.server.model.user.RepositoryUserForm;
import org.arctic.wolf.repository.server.model.user.RepositoryUserService;
import org.springframework.stereotype.Component;

/**
 * @author lord_rex
 */
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@Slf4j
@Component
public class RepositoryExampleContent {
	private final RepositoryUserService repositoryUserService;
	
	@PostConstruct
	void onStartup() {
		if (repositoryUserService.count() == 0) {
			createUser("admin", "admin@localhost", "admin", true);
			createUser("user", "user@localhost", "user", false);
		}
	}
	
	private void createUser(String username, String email, String password, boolean admin) {
		final RepositoryUserForm userForm = new RepositoryUserForm();
		userForm.setUsername(username);
		userForm.setEmail(email);
		userForm.setPassword(password);
		userForm.setAdmin(admin);
		repositoryUserService.createUser(userForm);
	}
}
