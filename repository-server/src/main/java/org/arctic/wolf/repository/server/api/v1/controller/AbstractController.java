package org.arctic.wolf.repository.server.api.v1.controller;

import org.arctic.wolf.exception.BadRequestException;
import org.arctic.wolf.repository.server.model.user.RepositoryUserDB;
import org.arctic.wolf.repository.server.security.UserPrincipal;
import org.springframework.validation.BindingResult;

/**
 * @author lord_rex
 */
public class AbstractController {
	protected static final String ATTACHMENT_FILENAME = "attachment; filename=";
	
	protected static void verifyBindingResult(BindingResult bindingResult) {
		if (bindingResult.hasErrors()) {
			final String firstErrorDefaultMessage = bindingResult.getAllErrors().get(0).getDefaultMessage();
			throw new BadRequestException(firstErrorDefaultMessage);
		}
	}
	
	protected static RepositoryUserDB getRepositoryUser(UserPrincipal userPrincipal) {
		return userPrincipal != null ? userPrincipal.getUserDB() : null;
	}
}
