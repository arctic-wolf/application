package org.arctic.wolf.repository.server.api.v1.controller;

import static org.springframework.http.MediaType.APPLICATION_OCTET_STREAM_VALUE;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import jakarta.servlet.http.HttpServletResponse;

import org.arctic.wolf.exception.ExternalComponentNotFoundException;
import org.arctic.wolf.model.IExternalComponent;
import org.arctic.wolf.repository.server.model.RepositoryServerExternalComponentService;
import org.arctic.wolf.repository.server.security.UserPrincipal;
import org.arctic.wolf.security.CurrentUser;
import org.arctic.wolf.util.PageUtil;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.HttpClientErrorException;

public abstract class ExternalComponentController<T extends IExternalComponent, S extends RepositoryServerExternalComponentService<T>> extends AbstractController {
	protected abstract S getService();
	
	protected abstract Path getJarFile(T component);
	
	@GetMapping("{name}/download")
	@PreAuthorize("hasRole('ROLE_USER')")
	public ResponseEntity<Resource> component(@CurrentUser UserPrincipal userPrincipal, @PathVariable String name) throws IOException {
		if (!getService().hasAccessToComponent(userPrincipal.getUserDB(), name)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		
		final T component = getService().getAvailableComponent(name).orElseThrow(ExternalComponentNotFoundException::new);
		final Path path = getJarFile(component);
		
		final HttpHeaders headers = new HttpHeaders();
		headers.add(HttpHeaders.CONTENT_DISPOSITION, ATTACHMENT_FILENAME + path.getFileName());
		
		final ByteArrayResource resource = new ByteArrayResource(Files.readAllBytes(path));
		
		return ResponseEntity.ok()
			.headers(headers)
			.contentLength(path.toFile().length())
			.contentType(MediaType.parseMediaType(APPLICATION_OCTET_STREAM_VALUE))
			.body(resource);
	}
	
	@GetMapping("all/info")
	@PreAuthorize("hasRole('ROLE_USER')")
	public ResponseEntity<List<T>> componentInfos(@CurrentUser UserPrincipal userPrincipal, @PageableDefault(size = 9) Pageable pageable, HttpServletResponse response) {
		final List<T> availableComponents = getService().getAvailableComponents(userPrincipal.getUserDB());
		final Page<T> pagedView = PageUtil.getPagedView(pageable, availableComponents);
		
		return ResponseEntity.ok(PageUtil.getPagedContent(response, pagedView));
	}
	
	@GetMapping("{name}/info")
	@PreAuthorize("hasRole('ROLE_USER')")
	public ResponseEntity<T> componentInfo(@CurrentUser UserPrincipal userPrincipal, @PathVariable String name) {
		if (!getService().hasAccessToComponent(userPrincipal.getUserDB(), name)) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
		
		final T externalComponent = getService().getAvailableComponent(name).orElseThrow(ExternalComponentNotFoundException::new);
		return ResponseEntity.ok(externalComponent);
	}
	
	@GetMapping("find/{search}")
	@PreAuthorize("hasRole('ROLE_USER')")
	public ResponseEntity<List<T>> findComponent(@CurrentUser UserPrincipal userPrincipal, @PathVariable String search, @PageableDefault(size = 9) Pageable pageable, HttpServletResponse response) {
		final Page<T> pagedView = getService().pagedFuzzySearch(userPrincipal.getUserDB(), search, pageable);
		
		return ResponseEntity.ok(PageUtil.getPagedContent(response, pagedView));
	}
	
	@GetMapping(value = "{name}/preview-image", produces = MediaType.IMAGE_JPEG_VALUE)
	@PreAuthorize("hasRole('ROLE_USER')")
	public @ResponseBody byte[] previewImage(@CurrentUser UserPrincipal userPrincipal, @PathVariable String name) {
		if (!getService().hasAccessToComponent(userPrincipal.getUserDB(), name)) {
			throw new HttpClientErrorException(HttpStatus.UNAUTHORIZED);
		}
		
		return getService().getPreviewImage(name);
	}
}
