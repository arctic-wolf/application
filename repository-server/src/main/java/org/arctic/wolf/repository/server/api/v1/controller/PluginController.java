package org.arctic.wolf.repository.server.api.v1.controller;

import java.nio.file.Path;
import org.springframework.beans.factory.annotation.Autowired;

import lombok.RequiredArgsConstructor;
import org.arctic.wolf.model.plugin.Plugins;
import org.arctic.wolf.model.plugin.dto.Plugin;
import org.arctic.wolf.repository.server.model.plugin.PluginServiceRepositoryServer;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@RestController
@RequestMapping("api/v1/plugins")
public class PluginController extends ExternalComponentController<Plugin, PluginServiceRepositoryServer> {
	private final PluginServiceRepositoryServer pluginService;
	
	@Override
	protected PluginServiceRepositoryServer getService() {
		return pluginService;
	}
	
	@Override
	protected Path getJarFile(Plugin component) {
		return Plugins.getJarFile(component);
	}
}
