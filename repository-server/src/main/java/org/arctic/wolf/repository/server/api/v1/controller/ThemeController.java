package org.arctic.wolf.repository.server.api.v1.controller;

import java.nio.file.Path;
import org.springframework.beans.factory.annotation.Autowired;

import lombok.RequiredArgsConstructor;
import org.arctic.wolf.model.theme.Themes;
import org.arctic.wolf.model.theme.dto.Theme;
import org.arctic.wolf.repository.server.model.theme.ThemeServiceRepositoryServer;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@RestController
@RequestMapping("api/v1/themes")
public class ThemeController extends ExternalComponentController<Theme, ThemeServiceRepositoryServer> {
	private final ThemeServiceRepositoryServer themeService;
	
	@Override
	protected ThemeServiceRepositoryServer getService() {
		return themeService;
	}
	
	@Override
	protected Path getJarFile(Theme component) {
		return Themes.getJarFile(component);
	}
}
