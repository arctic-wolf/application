package org.arctic.wolf.repository.server.api.v1.controller.admin;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.validation.Valid;

import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.arctic.wolf.exception.BadRequestException;
import org.arctic.wolf.exception.UserNotFoundException;
import org.arctic.wolf.model.plugin.dto.Plugin;
import org.arctic.wolf.model.theme.dto.Theme;
import org.arctic.wolf.model.version.VersionUtil;
import org.arctic.wolf.model.version.dto.VersionInfo;
import org.arctic.wolf.repository.server.api.v1.controller.AbstractController;
import org.arctic.wolf.repository.server.model.plugin.PluginServiceRepositoryServer;
import org.arctic.wolf.repository.server.model.theme.ThemeServiceRepositoryServer;
import org.arctic.wolf.repository.server.model.user.IRepositoryUser;
import org.arctic.wolf.repository.server.model.user.RepositoryUserDB;
import org.arctic.wolf.repository.server.model.user.RepositoryUserForm;
import org.arctic.wolf.repository.server.model.user.RepositoryUserService;
import org.arctic.wolf.repository.server.version.VersionResourceMarker;
import org.arctic.wolf.util.PageUtil;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@RestController
@RequestMapping("api/v1/admin")
public class AdminController extends AbstractController {
	private final ThemeServiceRepositoryServer themeService;
	private final PluginServiceRepositoryServer pluginService;
	private final RepositoryUserService repositoryUserService;
	
	@GetMapping("version-info")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public List<VersionInfo> getVersionInfo() {
		return VersionUtil.getVersionInfo(VersionResourceMarker.class);
	}
	
	@GetMapping("themes")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public List<Theme> getThemes(@PageableDefault(size = 15) Pageable pageable, HttpServletResponse response) {
		final Collection<Theme> themes = themeService.getAvailableComponents();
		final Page<Theme> pagedView = PageUtil.getPagedView(pageable, new ArrayList<>(themes));
		return PageUtil.getPagedContent(response, pagedView);
	}
	
	@GetMapping("plugins")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public List<Plugin> getPlugins(@PageableDefault(size = 15) Pageable pageable, HttpServletResponse response) {
		final Collection<Plugin> plugins = pluginService.getAvailableComponents();
		final Page<Plugin> pagedView = PageUtil.getPagedView(pageable, new ArrayList<>(plugins));
		return PageUtil.getPagedContent(response, pagedView);
	}
	
	@GetMapping("users/list")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public List<RepositoryUserDB> getUsers(@PageableDefault(size = 15) Pageable pageable, HttpServletResponse response) {
		final List<RepositoryUserDB> userList = repositoryUserService.findAll();
		final Page<RepositoryUserDB> pagedView = PageUtil.getPagedView(pageable, userList);
		return PageUtil.getPagedContent(response, pagedView);
	}
	
	@PostMapping("users/add")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public IRepositoryUser createUser(@Valid @RequestBody RepositoryUserForm userForm) {
		final String username = userForm.getUsername();
		final String email = userForm.getEmail();
		
		if (StringUtils.isNotEmpty(username) && repositoryUserService.findByUsername(username).isPresent()) {
			throw new BadRequestException("Username is already in use!");
		}
		
		if (StringUtils.isNotEmpty(email) && repositoryUserService.findByEmail(email) != null) {
			throw new BadRequestException("Email is already in use!");
		}
		
		repositoryUserService.createUser(userForm);
		
		return repositoryUserService.findByUsername(userForm.getUsername())
			.orElseThrow(UserNotFoundException::new);
	}
	
	@PutMapping("users/{id}/update")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public IRepositoryUser updateUser(@PathVariable long id, @Valid @RequestBody RepositoryUserForm userForm) {
		final RepositoryUserDB repositoryUserDB = repositoryUserService.findById(id).orElseThrow(UserNotFoundException::new);
		
		final String username = userForm.getUsername();
		final String email = userForm.getEmail();
		
		if (StringUtils.isNotEmpty(username) && repositoryUserService.findByUsername(username).isPresent() && !repositoryUserDB.getUsername().equals(username)) {
			throw new BadRequestException("Username is already in use!");
		}
		
		if (StringUtils.isNotEmpty(email) && repositoryUserService.findByEmail(email) != null && !repositoryUserDB.getEmail().equals(email)) {
			throw new BadRequestException("Email is already in use!");
		}
		
		repositoryUserService.updateUser(repositoryUserDB, userForm);
		
		return repositoryUserService.findByUsername(userForm.getUsername())
			.orElseThrow(UserNotFoundException::new);
	}
	
	@DeleteMapping("users/{id}/delete")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public boolean deleteUser(@PathVariable long id) {
		try {
			repositoryUserService.delete(id);
			return true;
		}
		catch (Exception e) {
			throw new BadRequestException("Failed to delete user!", e);
		}
	}
	
	@PostMapping("users/{id}/themes/add")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public Set<String> addThemes(@PathVariable long id, @RequestBody String... themes) {
		repositoryUserService.addThemes(id, themes);
		
		return repositoryUserService.findById(id)
			.orElseThrow(UserNotFoundException::new)
			.getThemes();
	}
	
	@DeleteMapping("users/{id}/themes/remove")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public Set<String> removeThemes(@PathVariable long id, @RequestBody String... themes) {
		repositoryUserService.removeThemes(id, themes);
		
		return repositoryUserService.findById(id)
			.orElseThrow(UserNotFoundException::new)
			.getThemes();
	}
	
	@PostMapping("users/{id}/plugins/add")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public Set<String> addPlugins(@PathVariable long id, @RequestBody String... plugins) {
		repositoryUserService.addPlugins(id, plugins);
		
		return repositoryUserService.findById(id)
			.orElseThrow(UserNotFoundException::new)
			.getPlugins();
	}
	
	@DeleteMapping("users/{id}/plugins/remove")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public Set<String> removePlugins(@PathVariable long id, @RequestBody String... plugins) {
		repositoryUserService.removePlugins(id, plugins);
		
		return repositoryUserService.findById(id)
			.orElseThrow(UserNotFoundException::new)
			.getPlugins();
	}
}
