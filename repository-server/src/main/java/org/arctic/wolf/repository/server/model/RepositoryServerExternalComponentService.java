package org.arctic.wolf.repository.server.model;

import java.util.List;
import java.util.stream.Collectors;

import org.arctic.wolf.model.ExternalComponentService;
import org.arctic.wolf.model.IExternalComponent;
import org.arctic.wolf.repository.server.model.user.RepositoryUserDB;
import org.arctic.wolf.util.FuzzySearchUtil;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public abstract class RepositoryServerExternalComponentService<T extends IExternalComponent> extends ExternalComponentService<T> {
	public List<T> getAvailableComponents(RepositoryUserDB repositoryUserDB) {
		return getAvailableComponents()
			.stream()
			.filter(component -> hasAccessToComponent(repositoryUserDB, component.getName()))
			.collect(Collectors.toList());
	}
	
	public abstract boolean hasAccessToComponent(RepositoryUserDB repositoryUserDB, String name);
	
	public Page<T> pagedFuzzySearch(RepositoryUserDB repositoryUserDB, String search, Pageable pageable) {
		return FuzzySearchUtil.pagedFuzzySearch(pageable, getAvailableComponents(repositoryUserDB), search);
	}
}
