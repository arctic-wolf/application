package org.arctic.wolf.repository.server.model.plugin;

import java.io.IOException;
import java.nio.file.Path;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.arctic.wolf.model.plugin.Plugins;
import org.arctic.wolf.model.plugin.dto.Plugin;
import org.arctic.wolf.repository.server.model.RepositoryServerExternalComponentService;
import org.arctic.wolf.repository.server.model.user.RepositoryUserDB;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author lord_rex
 */
@Slf4j
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@Service
@Transactional
public class PluginServiceRepositoryServer extends RepositoryServerExternalComponentService<Plugin> {
	@Order(2)
	@EventListener(ApplicationReadyEvent.class)
	public void onStartup() {
		loadAll();
	}
	
	@Override
	protected Plugin createNewComponent() {
		return new Plugin();
	}
	
	@Override
	protected List<Path> getJARs() throws IOException {
		return Plugins.getPluginJARs();
	}
	
	@Override
	public boolean hasAccessToComponent(RepositoryUserDB repositoryUserDB, String name) {
		return repositoryUserDB != null && (repositoryUserDB.isAdmin() || repositoryUserDB.getPlugins().contains(name));
	}
	
	@Override
	protected Path getPath(Plugin externalComponent) {
		return Plugins.getPath(externalComponent);
	}
	
	@Override
	protected Path getJarFile(Plugin externalComponent) {
		return Plugins.getJarFile(externalComponent);
	}
}
