package org.arctic.wolf.repository.server.model.theme;

import java.io.IOException;
import java.nio.file.Path;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.arctic.wolf.model.theme.Themes;
import org.arctic.wolf.model.theme.dto.Theme;
import org.arctic.wolf.repository.server.model.RepositoryServerExternalComponentService;
import org.arctic.wolf.repository.server.model.user.RepositoryUserDB;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author lord_rex
 */
@Slf4j
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@Service
@Transactional
public class ThemeServiceRepositoryServer extends RepositoryServerExternalComponentService<Theme> {
	@Order(1)
	@EventListener(ApplicationReadyEvent.class)
	public void onStartup() {
		loadAll();
	}
	
	@Override
	protected Theme createNewComponent() {
		return new Theme();
	}
	
	@Override
	protected List<Path> getJARs() throws IOException {
		return Themes.getThemeJARs();
	}
	
	@Override
	public boolean hasAccessToComponent(RepositoryUserDB repositoryUserDB, String name) {
		return repositoryUserDB != null && (repositoryUserDB.isAdmin() || repositoryUserDB.getThemes().contains(name));
	}
	
	@Override
	protected Path getPath(Theme externalComponent) {
		return Themes.getPath(externalComponent);
	}
	
	@Override
	protected Path getJarFile(Theme externalComponent) {
		return Themes.getJarFile(externalComponent);
	}
}
