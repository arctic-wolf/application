/*
 * Copyright (c) 2018 Reginald Ravenhorst <lordrex34@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.arctic.wolf.repository.server.model.user;

import java.util.HashSet;
import java.util.Set;
import jakarta.persistence.CollectionTable;
import jakarta.persistence.Column;
import jakarta.persistence.ElementCollection;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Indexed;
import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * @author lord_rex
 */
@Indexed
@Entity
@Table(name = "repository_users")
@Setter
@Getter
@EqualsAndHashCode(of = { "id", "username", "email" })
@ToString(callSuper = true)
public class RepositoryUserDB implements IRepositoryUser {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(unique = true, nullable = false)
	private Long id;
	
	@Field
	@Column(unique = true, nullable = false)
	private String username;
	
	@JsonIgnore
	@Column(nullable = false)
	private String password;
	
	@Field
	@Column(unique = true, nullable = false)
	private String email;
	
	@Column(nullable = false)
	private boolean admin = false;
	
	@CollectionTable(name = "repository_user_plugins")
	@ElementCollection(fetch = FetchType.EAGER)
	@Column(name = "plugin")
	private Set<String> plugins = new HashSet<>();
	
	@CollectionTable(name = "repository_user_themes")
	@ElementCollection(fetch = FetchType.EAGER)
	@Column(name = "theme")
	private Set<String> themes = new HashSet<>();
}
