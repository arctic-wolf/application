/*
 * Copyright (c) 2018 Reginald Ravenhorst <lordrex34@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.arctic.wolf.repository.server.model.user;

import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;

import lombok.RequiredArgsConstructor;
import org.arctic.wolf.exception.UserNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author lord_rex
 */
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@Service
@Transactional
public class RepositoryUserService {
	private final RepositoryUserDBRepository repository;
	private final PasswordEncoder repositoryPasswordEncoder;
	
	public long count() {
		return repository.count();
	}
	
	public Optional<RepositoryUserDB> findById(Long id) {
		return repository.findById(id);
	}
	
	public void delete(Long id) {
		repository.deleteById(id);
	}
	
	public void deleteAll() {
		repository.deleteAll();
	}
	
	public Optional<RepositoryUserDB> findByUsername(String name) {
		return Optional.ofNullable(repository.findByUsername(name));
	}
	
	public RepositoryUserDB findByEmail(String email) {
		return repository.findByEmail(email);
	}
	
	public List<RepositoryUserDB> findAll() {
		return repository.findAll();
	}
	
	public void createUser(IRepositoryUserForm userForm) {
		updateUser(new RepositoryUserDB(), userForm);
	}
	
	public void updateUser(RepositoryUserDB repositoryUserDB, IRepositoryUserForm userForm) {
		repositoryUserDB.setUsername(userForm.getUsername());
		repositoryUserDB.setPassword(repositoryPasswordEncoder.encode(userForm.getPassword()));
		repositoryUserDB.setEmail(userForm.getEmail());
		repositoryUserDB.setAdmin(userForm.isAdmin());
		repository.save(repositoryUserDB);
	}
	
	public void addThemes(Long id, String... themes) {
		final RepositoryUserDB repositoryUserDB = findById(id).orElseThrow(UserNotFoundException::new);
		
		for (String theme : themes) {
			repositoryUserDB.getThemes().add(theme);
		}
		
		repository.save(repositoryUserDB);
	}
	
	public void addPlugins(Long id, String... plugins) {
		final RepositoryUserDB repositoryUserDB = findById(id).orElseThrow(UserNotFoundException::new);
		
		for (String plugin : plugins) {
			repositoryUserDB.getPlugins().add(plugin);
		}
		
		repository.save(repositoryUserDB);
	}
	
	public void removeThemes(Long id, String... themes) {
		final RepositoryUserDB repositoryUserDB = findById(id).orElseThrow(UserNotFoundException::new);
		
		for (String theme : themes) {
			repositoryUserDB.getThemes().remove(theme);
		}
		
		repository.save(repositoryUserDB);
	}
	
	public void removePlugins(Long id, String... plugins) {
		final RepositoryUserDB repositoryUserDB = findById(id).orElseThrow(UserNotFoundException::new);
		
		for (String plugin : plugins) {
			repositoryUserDB.getPlugins().remove(plugin);
		}
		
		repository.save(repositoryUserDB);
	}
}
