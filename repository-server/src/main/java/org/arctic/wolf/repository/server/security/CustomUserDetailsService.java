package org.arctic.wolf.repository.server.security;

import org.springframework.beans.factory.annotation.Autowired;

import lombok.RequiredArgsConstructor;
import org.arctic.wolf.exception.UserNotFoundException;
import org.arctic.wolf.repository.server.model.user.RepositoryUserDB;
import org.arctic.wolf.repository.server.model.user.RepositoryUserService;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@Service
public class CustomUserDetailsService implements UserDetailsService {
	private final RepositoryUserService repositoryUserService;
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		final RepositoryUserDB repositoryUserDB = repositoryUserService.findByUsername(username)
			.orElseThrow(() -> new UsernameNotFoundException("User with username '" + username + "' does not exist!"));
		
		return UserPrincipal.create(repositoryUserDB);
	}
	
	public UserDetails loadUserById(Long id) {
		final RepositoryUserDB repositoryUserDB = repositoryUserService.findById(id)
			.orElseThrow(() -> new UserNotFoundException("Couldn't find user with ID " + id + "!"));
		
		return UserPrincipal.create(repositoryUserDB);
	}
}
