/*
 * Copyright (c) 2019 Reginald Ravenhorst <lordrex34@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.arctic.wolf.repository.server.security;

import java.util.Date;
import java.util.concurrent.TimeUnit;
import jakarta.annotation.PostConstruct;
import javax.crypto.SecretKey;
import org.springframework.beans.factory.annotation.Autowired;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.UnsupportedJwtException;
import io.jsonwebtoken.security.Keys;
import io.jsonwebtoken.security.SignatureException;

/**
 * @author Rajeev Kumar Singh <callicoder@gmail.com>
 */
@Slf4j
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@Service
public class TokenProviderService {
	private SecretKey secretKey;
	
	@PostConstruct
	void onStartup() {
		secretKey = Keys.secretKeyFor(SignatureAlgorithm.HS512);
		
		LOGGER.info("Generated secret key.");
	}
	
	public String createToken(Authentication authentication) {
		final UserPrincipal userPrincipal = (UserPrincipal) authentication.getPrincipal();
		
		final Date now = new Date();
		final Date expiryDate = new Date(now.getTime() + TimeUnit.HOURS.toMillis(1));
		
		return Jwts.builder()
			.setSubject(Long.toString(userPrincipal.getId()))
			.setIssuedAt(new Date())
			.setExpiration(expiryDate)
			.signWith(secretKey)
			.compact();
	}
	
	Long getUserIdFromToken(String token) {
		final Claims claims = Jwts.parser()
			.setSigningKey(secretKey)
			.parseClaimsJws(token)
			.getBody();
		
		return Long.parseLong(claims.getSubject());
	}
	
	boolean validateToken(String authToken) {
		try {
			Jwts.parser().setSigningKey(secretKey).parseClaimsJws(authToken);
			return true;
		}
		catch (SecurityException e) {
			LOGGER.error("Invalid JWT signature!");
		}
		catch (MalformedJwtException e) {
			LOGGER.error("Invalid JWT token!");
		}
		catch (ExpiredJwtException e) {
			LOGGER.error("Expired JWT token!");
		}
		catch (UnsupportedJwtException e) {
			LOGGER.error("Unsupported JWT token!");
		}
		catch (IllegalArgumentException e) {
			LOGGER.error("JWT claims string is empty!");
		}
		catch (SignatureException e) {
			LOGGER.error("Signatures does not match!");
		}
		catch (Exception e) {
			LOGGER.error("Invalid JWT token!", e);
		}
		
		return false;
	}
}
