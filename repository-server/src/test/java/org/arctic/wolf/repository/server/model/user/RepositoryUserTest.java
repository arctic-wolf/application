/*
 * Copyright (c) 2018 Reginald Ravenhorst <lordrex34@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.arctic.wolf.repository.server.model.user;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.springframework.beans.factory.annotation.Autowired;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.boot.jdbc.EmbeddedDatabaseConnection;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * @author lord_rex
 */
@ExtendWith(SpringExtension.class)
@EnableTransactionManagement
@SpringBootTest
@AutoConfigureTestDatabase(connection = EmbeddedDatabaseConnection.H2)
public class RepositoryUserTest {
	private static final String TEST_USERNAME = "testUsername";
	private static final String TEST_PASSWORD = "testPassword";
	private static final String TEST_EMAIL = "test@localhost";
	
	@Autowired
	private RepositoryUserService repositoryUserService;
	
	@Autowired
	private PasswordEncoder passwordEncoder;
	
	@BeforeEach
	public void before() {
		repositoryUserService.deleteAll();
	}
	
	private void create() {
		assertFalse(repositoryUserService.findByUsername(TEST_USERNAME).isPresent());
		
		final IRepositoryUserForm userForm = Mockito.mock(IRepositoryUserForm.class);
		Mockito.when(userForm.getUsername()).thenReturn(TEST_USERNAME);
		Mockito.when(userForm.getPassword()).thenReturn(TEST_PASSWORD);
		Mockito.when(userForm.getEmail()).thenReturn(TEST_EMAIL);
		repositoryUserService.createUser(userForm);
	}
	
	@Test
	public void testCreate() {
		create();
		
		assertTrue(repositoryUserService.count() > 0);
		assertTrue(repositoryUserService.findByUsername(TEST_USERNAME).isPresent());
	}
	
	@Test
	public void testPassword() {
		create();
		
		final RepositoryUserDB repositoryUserDB = repositoryUserService.findByUsername(TEST_USERNAME).orElse(null);
		assertNotNull(repositoryUserDB);
		
		final String password = repositoryUserDB.getPassword();
		assertNotNull(password);
		assertNotEquals(password, "");
		
		// If the password is encrypted, it must be different than the specified one.
		assertNotEquals(TEST_PASSWORD, password);
		
		// Finally check if password is working or not.
		assertTrue(passwordEncoder.matches(TEST_PASSWORD, password));
	}
	
	@Test
	public void testDelete() {
		create();
		
		final RepositoryUserDB repositoryUserDB = repositoryUserService.findByUsername(TEST_USERNAME).orElse(null);
		assertNotNull(repositoryUserDB);
		
		repositoryUserService.delete(repositoryUserDB.getId());
		
		assertFalse(repositoryUserService.findByUsername(TEST_USERNAME).isPresent());
	}
	
	@Test
	public void findTest() {
		create();
		
		assertNotNull(repositoryUserService.findByUsername(TEST_USERNAME));
	}
}
