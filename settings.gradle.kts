rootProject.name = "arctic-wolf-application"

include(":commons")
include(":plugin-sdk")
include(":core")
include(":client")
include(":admin-client")
include(":repository-server")
include(":example-theme")
